import React, { Component } from 'react';
import { makeStyles, createMuiTheme, withStyles, MuiThemeProvider } from '@material-ui/core/styles';
import { AppBar, Toolbar, Button, Typography } from '@material-ui/core';
import IconButton from '@material-ui/core/IconButton';
import MenuIcon from '@material-ui/icons/Menu';
// import { NavLink } from "react-router-dom";


// import logo from '../assets/images/iconAB128.png';
// import logoText from '../assets/images/textAB.png';
// import logoText from '../assets/images/logoText.png';
import { ToLogin, ToRegister, ToDashboard, ToOrganigram} from '../constants/config-redirect-url';
import IconArrowRight from '../assets/images/Group_1222.png';
import IconInfo from '../assets/images/Group_1097.png';
import Redirect from '../utilities/Redirect';

const theme = createMuiTheme({

//    spacing: 1,

    palette: {
        primary: {
            main: '#cc0707', //#cc0707, #c62828
            light: '#ff5f52',
            dark: '#8e0000',
            contrastText: '#ffffff'
        }
    },
    overrides: {

       MuiToolbar : {

        root:{
            backgroundColor: 'white'
          }
        },
        MuiTypography: {
            root: {
                // textTransform: 'lowercase'
                
            },
            // button: {
            //     textTransform: 'lowercase',
            //     color: 'red'
            // }
        }
    }

  });


// const useStyles = makeStyles(theme => ({
const styles = theme => ({

    root: {
      flexGrow: 1,
    },
    menuButton: {
      marginRight: theme.spacing(2),
      color: 'grey',
      
    },
    title: {
      flexGrow: 1,
      color: 'black',
      paddingBottom: theme.spacing(0),
    //   marginTop: theme.spacing(1),
      fontFamily: 'Nunito'
    },
    question: {
        color: 'grey',
        textTransform : 'capitalize'
        
    },
    buttonAction: {
        textTransform : 'capitalize'
    },
    logoText: {
        flexGrow: 1,
        width: 120,
        height: 24
    },
    logo: {
        width: 30,
        height: 30
    },
    iconInfoStyle: {
        width: 22,
        height: 22
    },
    buttonBackDashboard: {  
        
        // marginLeft:theme.spacing(1),
        // marginTop:theme.spacing(2),
        // marginBottom:theme.spacing(3),
        borderRadius: 20,
        fontFamily:'Nunito',
        textTransform : 'capitalize',
        color: 'white',
        background: 'linear-gradient(1deg, #c1272d, #c1272d 30%, #d1354a 67%, #f28181)',
       
    },
  });

const HeadStructureOrganization = props => {

    // const classes = useStyles();
    const { classes} = props;

    // const currentUrl = window.location.pathname;

    return (
        <MuiThemeProvider theme={theme}>
            <AppBar position="sticky">
                <div className={classes.root}>
                    <Toolbar>
                        {/* <IconButton edge="start" className={classes.menuButton} color="inherit" aria-label="Menu">
                            <img src={logo} alt='Logo' className={classes.logo}/>
                        </IconButton> */}

                        <Typography variant='h6' className={classes.title}>
                            {/* <img src={logoText} alt='logo-text' className={classes.logoText} /> */}
                            <b>Struktur Organisasi</b>
                        </Typography>
                        <IconButton edge="start" className={classes.menuButton} color="inherit" aria-label="Menu">
                            <img src={IconInfo} alt='icon-info' className={classes.iconInfoStyle}/>
                        </IconButton>
                        <Button 
                            onClick={() => Redirect(ToDashboard)}
                            variant="contained" 
                            size="medium" 
                            className={classes.buttonBackDashboard}
                        >
                            <b>Dashboard</b> &nbsp;&nbsp;&nbsp;
                            <img src={IconArrowRight} alt='icon-right-arrow' className={classes.iconArrowLeft} />
                        </Button>
                        
                    </Toolbar>
                </div>
            </AppBar>
        </MuiThemeProvider>
    )
};

// export default Header;
export default withStyles(styles)(HeadStructureOrganization);