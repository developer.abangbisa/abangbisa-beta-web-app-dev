import { all } from 'redux-saga/effects'
// import { watchActivityBooking} from '../middleware-saga/booking_calendar_available';
import { watchAndLog } from './log'


//*Single entry point to start all Sagas at once
export function* rootSaga(){

    yield all([watchAndLog()])
}