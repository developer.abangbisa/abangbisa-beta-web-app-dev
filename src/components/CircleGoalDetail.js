import React, { useCallback, useEffect, useState, useContext, Fragment} from "react";
import { makeStyles, createMuiTheme, withStyles, MuiThemeProvider } from '@material-ui/core/styles';
import { 

    Container, Paper, Typography, Grid, TextField, Checkbox, CircularProgress,
    FormControlLabel, Box, Button, MenuItem, Dialog, DialogTitle, DialogContent, 
    DialogContentText, DialogActions, Breadcrumbs, Link, List, ListItem, ListItemText, 
    ListItemAvatar, Avatar, IconButton, InputBase, Divider, Tooltip, Table, Icon,
    TableBody, TableRow, TableCell, TablePagination, TableSortLabel, TableHead, Menu,
    ListItemIcon, Chip
    
} from '@material-ui/core';

import moment from 'moment';

import { CircularProgressbarWithChildren, CircularProgressbar, buildStyles } from 'react-circular-progressbar';

const numeral = require('numeral');

const CircleProgress = props => {

    const { classes, goalDetailState  } = props;

    console.log('goalDetailState : ', goalDetailState);

    if(goalDetailState.id !== null){

        return (
    
            <Fragment>
    
                   {/* 
                        ``````````````````````````````````````````````````````````````````````````````````````````````````````````````````
                        - INI SCENARIO KALAU ANTARA "GAP" LEBIH BESAR DARI "GOAL RESULT" ==> MERAH untuk "trail GOAL RESULT/Pencapaiannya" 
                        
                        - INI SCENARIO KALAU "POSISI AMAN" LEBIH BESAR DARI "GOAL RESULT" ==> YELLOW untuk "trail GAP-nya"
                        
                        `````````````````````````````````````````````````````````````````````````````````````````````````````````````````
                        
                    */}
    
                    {
                    goalDetailState.calculatedValue.expected_value_percent > goalDetailState.calculatedValue.result_value_percent && (
    
                        <CircularProgressbarWithChildren
                            value={numeral(goalDetailState.calculatedValue.expected_value_percent).format('0,0')}
                            styles= {
    
                                    buildStyles({
                                        pathColor: '#ffec59',  
                                        trailColor: '#eee'  
                                    })
                                   
                            }
                        >
                            <CircularProgressbar

                                // strokeWidth={50}
                                

                                value={numeral(goalDetailState.calculatedValue.result_value_percent).format('0,0')} 
                                styles = {
    
                                    goalDetailState.calculatedValue.gap_value_percent > goalDetailState.calculatedValue.result_value_percent ||
                                    goalDetailState.calculatedValue.expected_value_percent > goalDetailState.calculatedValue.result_value_percent
                                    ?
    
                                        buildStyles({
    
                                            pathColor:'red',                                                             
                                            trailColor: 'transparent',
                                            textColor: 'black'
                                        }) : 
    
                                            buildStyles({
    
                                                pathColor:'rgba(61, 255, 41, 0.87)', //*Green                                                          
                                                trailColor: 'transparent',
                                                textColor: 'black',
                                            })
                                }
    
                                text={`${numeral(goalDetailState.calculatedValue.result_value_percent).format('0,0')}%`}
                            />
                        </CircularProgressbarWithChildren>
                    )
                }
    
    
                {/* 
                    ``````````````````````````````````````````````````````````````````````````````````````````````````````````
                    
                    - INI SCENARIO KALAU "GOAL RESULT" LEBIH BESAR DARI "POSISI AMAN" ==> GREEN untuk "trail GOAL RESULT-nya" 
    
                        (Warna GREEN mendahului warna YELLOW GAP)
                    
                    ``````````````````````````````````````````````````````````````````````````````````````````````````````````
                */}
                
                {
    
                    goalDetailState.calculatedValue.result_value_percent > goalDetailState.calculatedValue.expected_value_percent ||
                    goalDetailState.calculatedValue.result_value_percent == goalDetailState.calculatedValue.expected_value_percent  ? (
    
                        <CircularProgressbarWithChildren
                            value = { numeral(goalDetailState.calculatedValue.result_value_percent).format('0,0') }
    
                            styles= {
    
                                    buildStyles({
                                        pathColor:'#36cf5f',
                                        trailColor: '#eee'
                                    })
                            }
                        >
    
                            <CircularProgressbar
                                value={numeral(goalDetailState.calculatedValue.expected_value_percent).format('0,0')} 
                                styles = {
    
                                    buildStyles({
    
                                        // pathColor:'yellow',   
                                        // pathColor: 'rgba(61, 255, 41, 0.87)', 
                                        pathColor: '#2ef062',                                                                         
                                        trailColor: 'transparent',
                                        textColor: 'black'
                                    })                                                                     
                                }
    
                                text={`${numeral(goalDetailState.calculatedValue.result_value_percent).format('0,0')}%`}
                            />
    
                        </CircularProgressbarWithChildren>
    
                    ) : null
                }
    
            </Fragment>
        );

    } else {

        return <CircularProgress size={20} styles={{color: 'red', }} />
    }

};

export default CircleProgress;