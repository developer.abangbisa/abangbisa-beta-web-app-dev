import React, { Component, useEffect, useState } from "react";
import { makeStyles, createMuiTheme, withStyles, MuiThemeProvider } from '@material-ui/core/styles';
import { Container, Paper, Typography, Grid, TextField, Checkbox, 
        FormControlLabel, Box, Button, MenuItem, Popover, Fab, Chip,
        Dialog, DialogTitle, DialogContent, DialogContentText, DialogActions

    } from '@material-ui/core';


import PictInfo from '../assets/images/icon-info-24px.svg';


const DialogError = props => {

    const { classes, isOpenDialogError, setOpenDialogError, textErrorInformation  } = props;

    return (

        <Dialog
            open={isOpenDialogError}
            onClose={() => setOpenDialogError(false)}
            aria-labelledby="alert-dialog-title-422"
            aria-describedby="alert-dialog-description-422"
        >
            <DialogTitle id="alert-dialog-title-422" style={{textAlign: "center"}}>
                <img src={PictInfo} alt="info-icon-pict-info" style={{width: 300, height: 100 }} />  
            </DialogTitle>
            <DialogContent>
                <DialogContentText id="alert-dialog-description-422">
                    <Typography variant='h6' className={classes.title}>
                        {textErrorInformation != '' ? textErrorInformation : 'Oops, something went wrong !'}
                    </Typography>
                </DialogContentText>
            </DialogContent>
            <DialogActions style={{alignItems: "center", justifyContent:'center'}}>
                <Button 
                    variant='contained' 
                    onClick={() => setOpenDialogError(false)} 
                    color="primary" 
                    size='small'
                    className={classes.title}
                >  
                    Hubungi IT Administrator !
                </Button>
            </DialogActions>
            <br />
            <br />
        </Dialog>
    )

};

export default DialogError;
 