import React from 'react';
import { render } from '@testing-library/react';
import CommentList from '../CommentList';

describe('Comment List', () => {

    /* Tulis SPEC dulu ! */
    test('It renders a list of comment cards with their comment and author tag', () => {
      
        /*  ARRANGE  */
        const comment1 = {
            id: 1,
            comment: 'I do love writing tests',
            author: 'The Notester'
        };

        const comment2 = {
            id: 2,
            comment: 'Nothing is better than a good comment app',
            author: 'Comment Hater'
        };

        const props = {
            comments: [ comment1, comment2 ]
        }

        /* ACT */
        const { getByText } = render (<CommentList {...props} />);



        /* ASSERT */
        const firstCommentNode = getByText(comment1.comment)
        const firstAuthorTagNode = getByText(`${comment1.author}`)
        const secondCommentNode = getByText(comment2.comment)
        const secondAuthorTagNode = getByText(`${comment2.author}`)

        expect(firstCommentNode).toBeDefined()
        expect(firstAuthorTagNode).toBeDefined()
        expect(secondCommentNode).toBeDefined()
        expect(secondAuthorTagNode).toBeDefined()

    })
  })


  /* 

    ```````````````````````````````````````````````````````````
    https://testing-library.com/docs/react-testing-library/api

    ```````````````````````````````````````````````````````````

*/