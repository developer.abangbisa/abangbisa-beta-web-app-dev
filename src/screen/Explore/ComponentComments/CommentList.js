import React from 'react' 
import CommentCard from './CommentCard';

const CommentList = props => {

    return (
        <div>
            {props.comments.map((item, i) => {
                // console.log("Item : ", item)
                return (
                    <CommentCard key={i} {...item} />
                )
            })}
        </div>
    )
};
export default CommentList