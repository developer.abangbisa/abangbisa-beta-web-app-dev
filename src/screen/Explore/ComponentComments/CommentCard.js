import React, { useState} from 'react';

const CommentCard = props => {

    return (
        <div>
            <h1>{props.author}</h1>
            <p>{props.comment}</p>
        </div>
    )
}

export default CommentCard;