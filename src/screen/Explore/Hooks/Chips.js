import React, { Component, useEffect, useState, createRef, useRef } from "react";
import { makeStyles, createMuiTheme, withStyles, MuiThemeProvider } from '@material-ui/core/styles';
import { 
    Container, Paper, Typography, Grid, TextField, Checkbox, CircularProgress,
    FormControlLabel, Box, Button, MenuItem, Dialog, DialogTitle, DialogContent, 
    DialogContentText, DialogActions, Breadcrumbs, Link, IconButton, InputBase, Divider,
    Tooltip, List, ListItem, ListItemText, Avatar, Table, TableBody, TableRow, TableCell,
    Chip, TablePagination, TableHead, TableSortLabel, Fab, Icon

} from '@material-ui/core';

// import useHover from '../../../utilities-hook/useHover';

// import useHover from '@react-hook/hover';
// import { useHover } from 'react-use';
import EditIcon from '@material-ui/icons/Edit';

const theme = createMuiTheme({
    
    palette: {

        primary: {
            main: '#cc0707', //#cc0707, #c62828
            light: '#ff5f52',
            dark: '#8e0000',
            contrastText: '#ffffff'
        }
    }       
});
    
const styles = theme => ({
    
})

const Chips = props => {

    const { classes } = props;

    const nameRef = useRef(null);

    const handleFocus = () => {

        nameRef.current.focus();

    };

    return (

        <div>
            <input ref={nameRef} />
            <button onClick={handleFocus}>Handle</button>
        </div>
        // <Chip
        //     style={{fontFamily: 'Nunito', marginLeft: 40, marginTop: 40, backgroundColor: '#ffbc39', color: 'white', fontWeight: 'bold'}}                        
        //     label='Test'
            // onDelete = { () => alert("Deleting...") }
            // icon = {
            //     <Tooltip title="Edit" placement="bottom">
            //         <IconButton
            //             onClick={ () => alert('Editing...')}
            //         >
            //             <EditIcon style={{fontSize: 17, color: 'white', cursor: 'pointer'}} />
            //         </IconButton>
            //     </Tooltip>
            // }
        // />
    )
};


export default withStyles(styles)(Chips);
