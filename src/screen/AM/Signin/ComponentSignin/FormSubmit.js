import React, { Component, useEffect, useState } from "react";
import axios from 'axios';
import {fromJS } from 'immutable';
import { 
    Container, Paper, Typography, Grid, TextField, Checkbox, FormControlLabel, Box, Button, CircularProgress,
    Card, CardActionArea, CardMedia, CardContent, CardActions, Dialog, DialogTitle, DialogContent, DialogContentText,
    DialogActions, Hidden

} from '@material-ui/core';
import Modal from 'react-responsive-modal';
import Visibility from '@material-ui/icons/Visibility';
import VisibilityOff from '@material-ui/icons/VisibilityOff';
import IconButton from '@material-ui/core/IconButton';
import InputAdornment from '@material-ui/core/InputAdornment';

import { makeStyles, createMuiTheme, withStyles, MuiThemeProvider } from '@material-ui/core/styles';
import { URL_API } from '../../../../constants/config-api';
import PictInfo from '../../../../assets/images/icon-info-24px.svg';
import Redirect from '../../../../utilities/Redirect';
import { ToDashboard, ToMaintenance, ToLogin } from '../../../../constants/config-redirect-url';
import { MessageErrorValidationEmail } from '../../../../constants/config-messages-user';

const styles = theme => ({

    textField: {
        marginLeft: theme.spacing(1),
        marginRight: theme.spacing(1),
        minWidth: 300,
        width: 425,
        [theme.breakpoints.only('xs')]: {
    
            width: 366,
            // backgroundColor: 'red',
        },
    },
    textFieldPassword: {
        marginLeft: theme.spacing(1),
        marginRight: theme.spacing(1),
        minWidth: 300,
        width: 425,
        [theme.breakpoints.only('xs')]: {
    
            width: 366,
            // backgroundColor: 'red',
        },
    },
    input: {
        color: 'grey',
    },
    checkbox: {
        marginLeft:theme.spacing(1)
    },
    button: {
        width: '425px',
        height: '42px',
        borderRadius: 5,
        background: 'linear-gradient(1deg, #c1272d, #c1272d 30%, #d1354a 67%, #f28181)',
        border:0,
        fontFamily:'Nunito',
        marginLeft:theme.spacing(1),
        [theme.breakpoints.only('xs')]: {
    
            width: 366,
            // backgroundColor: 'red',
        },
    },
    buttonModal: {
        height: '42px',
        borderRadius: 5,
        background: 'linear-gradient(1deg, #c1272d, #c1272d 30%, #d1354a 67%, #f28181)',
        border:0,
        fontFamily:'Nunito',
        marginLeft:theme.spacing(1)
    },
    media: {
        height: 80,
        width: 80,
        marginLeft: theme.spacing(7)
    },
    mediaOke: {
        height: 80,
        width: 80,
    },
    buttonModalDelete: {
        borderRadius: 5,
        background: 'linear-gradient(1deg, #c1272d, #c1272d 30%, #d1354a 67%, #f28181)',
        border:0,
        fontFamily:'Nunito',
        marginLeft:theme.spacing(1),
        textTransform: 'capitalize',
        marginRight: theme.spacing(4),
        color: 'white'
    },
    title: {

        fontFamily: 'Nunito'
    },

})

const FormSumit = props => {

    const { classes } = props;

    const [response404, setResponse404] = useState(null);
    // const [isLoading, setIsLoading ] = useState(false);
    const [isResponse404, setIsResponse404] = useState(false);
    const [isResponse200, setIsResponse200] = useState(false);
    const [isResponse400, setIsResponse400] = useState(false);

    /*
        `````````````````````````````````````````````````````````````````````
        HANDLE CHANGE EMAIL
            
        `````````````````````````````````````````````````````````````````````
    */

    const [form, setFormEmail] = useState({

        email: ""

    });

    const [isErrorEmailValidation, setErrorEmailValidation ] = useState(false);

    const handleOnChangeEmail = (e) => {

        e.preventDefault();
        
        /* 
            `````````````````````````````````````````````````````````````````````
            NEXT REFACTOR WITH CODE BELOW HERE UNTUK TAMBAHKAN VALIDATION EMAIL :
            
            `````````````````````````````````````````````````````````````````````
        */
        const emailValid = e.target.value.match(/^([\w.%+-]+)@([\w-]+\.)+([\w]{2,})$/i);

        if(!emailValid){
            console.log('Email is Invalid !');
            setErrorEmailValidation(true);

        } else {

            setErrorEmailValidation(false);
        } 
            // fieldValidationErrors.email = emailValid ? '' : ' Email is invalid';
            // setEmail(e.target.value);
        
        setFormEmail({
            ...form,
            [e.target.name]: e.target.value
        });
    };

    const [response, setResponse] = useState({
        
        data: null,
        complete: false,
        pending: false,
        error: false
    });

    const [response200, setResponse200] = useState({
        data: null,
        complete: false,
        pending: false,
        error: false 
    });

    const [response400, setResponse400] = useState({
        data: undefined,
        complete: false,
        pending: false,
        error: false 
    });

    /*

        ``````````````````````
        Password 

        ``````````````````````

    */

    const [values, setValues] = useState({
        amount: '',
        password: '',
        showPassword: false

    });

    const handleChangePassword = prop => event => {

        setValues({ ...values, [prop]: event.target.value });
    };

    const handleClickShowPassword = () => {

        setValues({ ...values, showPassword: !values.showPassword });
    };

    /*

        ``````````````````````
        Checkbox 

        ``````````````````````

    */

    const [isChecked, setChecked] = useState({

        checkedA: true

    });

    const handleChangeCheckbox = name => event => {

        setChecked({ ...isChecked, [name]: event.target.checked });
    };

    /*

        ``````````````````````````````````
        MODAL LOADING, MODAL RESPONSE, etc

        ``````````````````````````````````

    */
    const [isLoading, setIsLoading] = useState(false);
    const handleCloseLoading = () => setIsLoading(false);

    const onCloseModalResponse404 = () => {

        setIsLoading(false);
        setIsResponse404(false);
    
    };

    const onCloseModalResponse400 = () => {

        setIsLoading(false);
        setIsResponse400(false);
        
    };

    /*
        ```````````````````````
        MODAL STATUS COMPLETION 

        ```````````````````````
    */

    const [ isStatusInEmailVerification, setStatusInEmailVerification ] = useState(false);



    /*

        ```````````````````````````
        HANDLE SUBMIT ENTER SIGN-IN

        ```````````````````````````
    */
    
    const handleSubmitEnter = (e) => {

        if(e.keyCode == 13){

            handleSubmit(e);
        };
    };

    /*

        ``````````````````````
        HANDLE SUBMIT SIGN-IN

        ``````````````````````

    */
    const handleSubmit = (e) => {

        e.preventDefault();
        setIsLoading(true); 

        let data = {
            Auth: {
                email: form.email,
                password: values.password
            }
        };
        
        const header =  {
            'Accept': "application/json",
            'Content-Type' : "application/json",
        };

        axios.defaults.headers.common = header;
        axios
            .post(URL_API + `/auth/login`, data)
            .then(function(response){
                
                setIsLoading(false);

                console.log("Response Original : ", response.data);

                if(response.data !== null){

                    if(response.data.data.completion_status_name == "otp" ){

                        setStatusInEmailVerification(true);

                    } else {

                        localStorage.setItem('userToken', response.data.data.access_token);
                        localStorage.setItem('status_user_login', JSON.stringify(response.data.data));
    
                        Redirect(ToDashboard);
                    }

                };



            })
            .catch(function(error){
                console.log("Error : ", error.response)
                const immutableError = fromJS(error.response);
                
                if(immutableError !== undefined){

                    if(immutableError.getIn(['status']) == 404 ){
    
                        if(immutableError.getIn(['data', 'info', 'message']) !== undefined){
                            setIsLoading(false)
                            setIsResponse404(true)
                            setResponse404(immutableError.getIn(['data', 'info', 'message']))
                           
                        };
                    };
    
                    if(immutableError.getIn(['status']) == 400){
                        setIsLoading(false)
                        setIsResponse400(true);
    
                        if(immutableError.getIn(['data','info', 'errors', 'email'])){
                            setResponse400({
                                data: "Email yang Anda masukan belum terdaftar !",
                                complete: true,
                                pending: false,
                                error: false
                            }); 
                        }
                    };
                    if(immutableError.getIn(['status']) == 500){
                        setIsLoading(false)
                        setIsResponse400(true);
    
                            setResponse400({ // ==> HARUS-NYA PAKAI MODAL KHSUSUS RESPONSE 500 !
                                data: "Oops, something went wrong !",
                                complete: true,
                                pending: false,
                                error: false
                            }); 
                    };


                } else {
                    alert('Periksa koneksi internet Anda !')
                };
            })
    };

    return (

        <Grid 
            container 
            justify="center" 
            alignItems="center"
        >  

            <Grid item xs={12} sm={12}  style={{textAlign: 'center'}}>

                    <Typography variant="h5" component="h3">
                        Sign In
                    </Typography>

                    <br />
                    
                    <TextField
                        id="outlined-email-input"
                        label="Email Address"
                        className={classes.textField}
                        type="email"
                        name="email"
                        autoComplete="email"
                        margin="normal"
                        variant="outlined"
                        inputProps={{className: classes.input}} //==> WORK
                        onChange= {handleOnChangeEmail}
                        // helperText={isErrorEmailValidation == true ? MessageErrorValidationEmail : ''} //* ==> Waiting request from pak boz !
                        // error={isErrorEmailValidation} //* ==> Waiting request from pak boz !
                    />

                    <br />
                    <br />
                    <TextField
                        id="outlined-adornment-password"
                        className={classes.textFieldPassword}
                        variant="outlined"
                        type={values.showPassword ? 'text' : 'password'}
                        label="Password"
                        value={values.password}
                        onChange={handleChangePassword('password')}
                        onKeyDown={handleSubmitEnter}
                        

                        InputProps={{
                            endAdornment: (
                                <InputAdornment position="end">
                                    <IconButton 
                                        aria-label="Toggle password visibility" 
                                        onClick={handleClickShowPassword}
                                    >
                                        {values.showPassword ? <VisibilityOff /> : <Visibility />}
                                    </IconButton>
                                </InputAdornment>
                            )
                        }}
                    />
            </Grid>

            
            <Grid item xs={12}>
                <FormControlLabel
                    value="ingat"
                    control={
                        <Checkbox 
                            color="primary" 
                            checked={isChecked.checkedA}
                            onChange={handleChangeCheckbox('checkedA')}
                            value="checkedA"
                            className={classes.checkbox}
                        />
                    }
                    label="Ingat saya"
                    labelPlacement="end"
                    // className={classes.labelCheckbox}
                    style={{fontSize: 10, color: 'grey' }}
                    
                    
                />
                <br />
                <br />
                <Button 
                    variant="contained" 
                    color="primary" 
                    className={classes.button} 
                    onClick={handleSubmit}

                >
                    Sign In                         
                </Button>

                
            </Grid>

            <Modal 
                open={isLoading} 
                onClose={handleCloseLoading}  
                closeIconSize={20} 
                showCloseIcon={false}  
                center 
                styles={{ modal: { background: "transparent", boxShadow:'none' } }} 
            >
                <CircularProgress size={32} style={{color: 'red'}} />    
            </Modal>

            <Modal 
                open={isResponse404} 
                onClose={onCloseModalResponse404}  
                closeIconSize={20} 
                showCloseIcon={false}  
                center
                styles={{ modal: { background: "transparent", boxShadow:'none' } }}
            >
                <div className='text-center' style={{padding:'30px'}}>
                    <img src={PictInfo} className={classes.media} alt="info-icon" />                
                    <br/>
                    <br />
                    <h6>Password Anda salah !</h6>
                    <br />
                    <Button 
                        variant='contained' 
                        onClick={onCloseModalResponse404} 
                        color="primary" 
                        className={classes.buttonModal} 
                        size='small'
                    >  
                        Silahkan coba lagi
                    </Button>
                </div>
            </Modal>

            <Modal 
                open={isResponse400} 
                onClose={onCloseModalResponse400}  
                closeIconSize={20} 
                showCloseIcon={false}  
                center 
                // styles={{ modal: { background: "transparent", boxShadow:'none' } }}
            >
                <div className='text-center' style={{padding:'30px'}}>
                    <img src={PictInfo} className={classes.media} alt="info-icon" />                
                    <br/>
                    <br />
                    <Typography variant='h6'>
                        {response400.data != undefined ? response400.data : 'Password Anda salah !'}
                    </Typography>
                    <br />
                    <Button 
                        variant='contained' 
                        onClick={onCloseModalResponse400}
                        color="primary" 
                        className={classes.buttonModal} 
                    >
                        Silahkan coba lagi
                    </Button>
                </div>
            </Modal>

          

        <Dialog
            open={isStatusInEmailVerification}
            onClose={() => console.log("Maaf ! Silahkan lakukan verifikasi email terlebih dahulu")}
            aria-labelledby="alert-dialog-title"
            aria-describedby="alert-dialog-description"
            
        >
            <DialogTitle id="alert-dialog-title" style={{textAlign: "center"}}>
                <img src={PictInfo} className={classes.mediaOke} alt="info-icon" />                

            </DialogTitle>
            <DialogContent style={{textAlign: "center"}}>
                <DialogContentText id="alert-dialog-description">
                    <Typography variant='h5' className={classes.title} style={{color: 'black'}}>
                        <b>Silahkan lakukan verifikasi email terlebih dahulu !</b>
                    </Typography>
                </DialogContentText>
            </DialogContent>
            <DialogActions style={{alignItems: "center", justifyContent:'center'}}>
                <Button 
                    onClick={() => window.location.reload()}
                    variant='contained' 
                    className={classes.buttonModalDelete}
                    // fullWidth
                >  
                    Oke, mengerti ! 
                </Button>
            </DialogActions>
            <br />
        </Dialog>

            {/* 
            
            
                NEXT, BIKIN MODAL RESPONSE UNTUK 419
            
            */}
        </Grid>
    )
};

export default withStyles(styles)(FormSumit);