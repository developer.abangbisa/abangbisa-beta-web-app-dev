import React, { useCallback, useEffect, useState, useContext } from "react";
import axios from 'axios';
import { makeStyles, createMuiTheme, withStyles, MuiThemeProvider } from '@material-ui/core/styles';
import { 

    Container, Paper, Typography, Grid, TextField, Checkbox, CircularProgress,
    FormControlLabel, Box, Button, MenuItem, Dialog, DialogTitle, DialogContent, 
    DialogContentText, DialogActions, Breadcrumbs, Link, List, ListItem, ListItemText, 
    ListItemAvatar, Avatar, IconButton, InputBase, Divider, Tooltip, Table, Icon,
    TableBody, TableRow, TableCell, TablePagination, TableSortLabel, TableHead, Switch
    
  } from '@material-ui/core';

import { Tab, Tabs, TabList, TabPanel } from 'react-tabs';

import ListStructureOrgRole from './ComponetViewRole/ListStructureOrgRole';
import DialogModalUbahHakAkses from './ComponetViewRole/DialogModalUbahHakAkses';

// import DialogModalTambahHakAkses from './ComponetViewRole/DialogModalTambahHakAkses';
import DialogModalTambahHakAkses from './ComponentsViewRole_NEW_v2/DialogModalTambahHakAkses';
import DialogDeleteModulPrivilage from './ComponentDelete/DialogDeleteModulPrivilage';


// import DialogModalTambahHakAkses_v2 from './ComponetViewRole/DialogModalTambahHakAkses_v2';


import PictDefaultEmpty from '../../../assets/images/Group_2232.png';

// import ContextRole from './Context/ContextRole';
import { useGetHttp } from './Hook/useGetHttp';
import { URL_API } from '../../../constants/config-api';
import Redirect from '../../../utilities/Redirect';
import Capitalize from '../../../utilities/Capitalize';

import { ToDashboard, ToRoleAdd, ToRole} from '../../../constants/config-redirect-url';

const theme = createMuiTheme({
  
  palette: {

      primary: {
          main: '#cc0707', //#cc0707, #c62828
          light: '#ff5f52',
          dark: '#8e0000',
          contrastText: '#ffffff'
      }
  }
});

const styles = theme => ({

  root: {
      borderRadius: 2,
      width: '100%',
      marginTop: theme.spacing(1),

  },
  button: {
      // width: '503px',
      // height: '42px',
      borderRadius: 5,
      background: 'linear-gradient(1deg, #c1272d, #c1272d 30%, #d1354a 67%, #f28181)',
      // backgroundColor: 'cyan',
      border:0,
      fontFamily:'Nunito',
      marginLeft:theme.spacing(1),
      textTransform: 'capitalize',
      color: 'white'
  },
  buttonDisabled: {
    // width: '503px',
    // height: '42px',
    borderRadius: 5,
    background: 'grey',
    // backgroundColor: 'cyan',
    border:0,
    fontFamily:'Nunito',
    marginLeft:theme.spacing(1),
    textTransform: 'capitalize',
    color: 'white'
  },
  buttonUbah: {
      borderRadius: 5,
      background: 'linear-gradient(1deg, #c1272d, #c1272d 30%, #d1354a 67%, #f28181)',
      border:0,
      fontFamily:'Nunito',
      marginLeft:theme.spacing(4),
      textTransform: 'capitalize',
      color: 'white'
  },
  buttonHapusRole: {

    borderRadius: 5,
    // background: 'linear-gradient(1deg, #c1272d, #c1272d 30%, #d1354a 67%, #f28181)',
    border:0,
    fontFamily:'Nunito',
    marginLeft:theme.spacing(4),
    textTransform: 'capitalize',
    color: 'grey'

  },
  buttonModalCancel: {

    fontFamily:'Nunito',
    textTransform: 'capitalize'

  },
  title: {

    fontFamily: 'Nunito'
  },
  titleBackDaftarRole: {
    fontFamily: 'Nunito',
    color: '#cc0707',
    marginTop: theme.spacing(2),
    marginLeft: theme.spacing(2)
    // cursor: 'pointer'
  },
  titleNameRole: {
    fontFamily: 'Nunito',
    marginTop: theme.spacing(3),
    marginLeft: theme.spacing(4)
  },
  titleLabelNamaRole: {
    fontFamily: 'Nunito',
    marginBottom: theme.spacing(1),
    marginLeft: theme.spacing(4)
  },
  titleLabelSwitch: {
    fontFamily: 'Nunito',
    marginBottom: theme.spacing(1),
    marginLeft: theme.spacing(3)
  },
  titleBerikanKetentuan: {
    fontFamily: 'Nunito',
    marginTop: theme.spacing(5),
    marginLeft: theme.spacing(4)
  },
  textField: {
    minWidth: 425,
    marginLeft: theme.spacing(4)
  },
  switch: {
    // marginLeft: theme.spacing(4)
  },

  // iconTitleBackDaftarRole: {
  //   color: '#cc0707',
  //   cursor: 'pointer'
  // }

  /*
    `````````````````````````````
    DIALOG MODAL TAMBAH HAK AKSES
    
    `````````````````````````````
  */

  dropDown: {

    minWidth: 300,
    width: 425,

  },
  paperStyleSelected: {
    borderRadius:7,
    marginTop: theme.spacing(2),
    cursor: 'pointer',
    backgroundColor: '#d13b4a', 
    color: 'white',
    
  },
  paperStyle: {
    // backgroundColor: 'grey', 
    borderRadius:7,
    marginTop: theme.spacing(2),
    cursor: 'pointer',
    '&:hover': {
      backgroundColor: '#d13b4a', 
      color: 'white'
   },
  },
  titlePaperStyle: {
    fontFamily: 'Nunito',
    paddingBottom: theme.spacing(3),
    paddingTop: theme.spacing(3),
  },

  /*

    ````````````````````
    TABLE LIST HAK AKSES
    
    `````````````````````

  */

  rootPaperTable: {

    width: '100%',
    marginTop: theme.spacing(1),
    overflowX: 'auto',

  },

  tablePaper: {

    minWidth: 1024,
  },

  iconEditTable : {

    color: '#cc0707'
  },
  iconDeleteTable: {

    color: '#cc0707'
  },
  iconDisabled: {

    color: 'grey'
  },
  titleModulTable: {
    fontFamily: 'Nunito',
    marginLeft: theme.spacing(3)
  }
  



});

const ViewRoleDetail = props => {

  const { classes } = props;




  /*

    ````````
    CONTEXT

    *Context belum bisa jalan, kalau pakai utilities 'Redirect' *_*

    ````````

  */

  // const context = useContext(ContextRole); // SEMENTARA PAKAI LOCAL STORAGE dulu aja !!! '_'

  /*
      `````````````````````````````````````
      LOCAL STORAGE, GET ID & NAME OF ROLE 

      `````````````````````````````````````
  */
    
  const idRole = localStorage.getItem('idRole');
  const nameRole = localStorage.getItem('nameRole');
  const isLockedStatus = localStorage.getItem('isLockedStatus');

  const modulHakAkses = localStorage.getItem('modulHakAkses');

  const userToken = localStorage.getItem('userToken');
  
  const [idRoleState, setIdRoleState] = useState('');
  const [nameRoleState, setNameRoleState] = useState('');
  const [ isLockedStatusState, setLockedStatusState] = useState('');

  // const [ modulHakAksesState, setModulHakAksesState] = useState('');

  const [ userTokenState, setUserTokenState ] = useState('');

  const [updatedAt, setUpdatedAt] = useState('')
  // const [dataLabelUpdateMasterRole, setDataLabelUpdateMasterRole] = useState();


    /*
    ``````````````````````````````
    DIALOG MODAL UBAH HAK AKSES 

    ``````````````````````````````

  */
 const [isModalUbahHakAkses, setModalUbahHakAkses ] = useState(false)
 
 /*

   `````````````````
   HANDLE UBAH ROLE 

   `````````````````

 */

 const [ isDisabledTabPengaturan, setDisabledTabPengaturan ] = useState(false);

 const handlePatchUbahRole = () => {

   let data = { 

     MasterRole: {

         name: valueFieldEditRoleName,
         updated_at: updatedAtAfterClickTab,
         is_locked: isLockedRole,

     },

     StructureRole: [
       {
         fk_id: ''
       },
       {

         category_id: ''
       }
     ],
     
     _method: 'patch'
   };

   console.log(`Data Updated Role : `, data);
   

   if(userTokenState !== undefined && userTokenState !== null ){
           
     const header =  {       
         'Accept': "application/json",
         'Content-Type' : "application/json",
         'Authorization' : "bearer " + userTokenState,
         
     };
   
     axios.defaults.headers.common = header;    

     axios
         .post(URL_API + `/account-management/master-role/${idRoleState}`, data)
         .then(function(response){

             console.log("Response Original UBAH ROLE : ", response)

             if(response.status == 200 ){

                 // window.location.reload();
                 setDisabledTabPengaturan(true)

                 Redirect(ToRole);

                 if(response.data.data !== undefined){
                     
                 };
             };
         })
         .catch(function(error){
             
             console.log("Error : ", error.response)
             if(error.response == 500){

               alert(error.response.data.message !== undefined ? error.response.data.message : "500")

             } else {

               alert("Ooops, something went wrong !")
             }
             
         })

   } else { console.log("No Access Token available!")};

 };

  /*

    ````
    TAB

    ````

  */

  const [tabIndex, setTabIndex] = useState(1);

  const [loading, fetchedData, setFetchedData ] = useGetHttp(URL_API+ `/account-management/master-role/${idRole}/patch`, [tabIndex]);

  let updatedAtAfterClickTab = undefined;

  if(fetchedData !== null && fetchedData !== undefined){

    updatedAtAfterClickTab=  fetchedData.updated_at;
  };

  
  function handleTab(tabIndex){

    // setUpdatedAt()
    console.log("Updated At : ", updatedAtAfterClickTab);
    

    setTabIndex(tabIndex);

  };


  /*

    ```````````````````````````````
    GET LABEL, UPDATED_AT ROLE, etc

    ```````````````````````````````

  */
  useEffect(() => {
 
    setIdRoleState(idRole);
    setNameRoleState(nameRole);
    setLockedStatusState(isLockedStatus);
    setUserTokenState(userToken);

    const header =  {       
      'Accept': "application/json",
      'Content-Type' : "application/json",
      'Authorization' : "bearer " + userToken,
    };

    axios.defaults.headers.common = header;    
      
    axios
      .get(URL_API + `/account-management/master-role/${idRole}/patch`)
      .then(function(response){

          // console.log("Response Original ** : ", JSON.stringify(response.data.data));

          if(response.status == 200){

            setUpdatedAt(response.data.data.updated_at);

          };

          let dataDropdownTemplate = {
            
          };
          
          // setDataLabelUpdateMasterRole(response.data.data);

      })
      .catch(function(error){
          
          console.log("Error : ", error.response)
      
      });

      /*

        ``````````````````
        GET LIST HAK AKSES

        ``````````````````
      */
      axios
        .get(URL_API + `/account-management/master-role/${idRole}`)
        .then(function(response){

            console.log("Response Original GET LIST HAK AKSES :  ", response);

            if(response.status == 200){
              
              
              if(response.data.data.embedded !== undefined ){

                
                if(response.data.data.embedded.privilegeSet !== undefined){

                  setRoleModul(response.data.data.embedded.privilegeSet);
                  
                  if(response.data.data.embedded.privilegeSet !== undefined){
                    
                    
                    if(response.data.data.embedded.privilegeSet.length > 0){
                      
                      setDisabledTabPengaturan(true);

                      let items = [];

                      response.data.data.embedded.privilegeSet.map((item, i) => {
                        
                        if(item.dataType !== null){
                          if(item.dataType.length > 0){

                            item.dataType.map(data => {

                              items.push(data)

                            })
                          }
                        }
                       
                      })

                      setListTipeData(items);

                    } else {

                      setListTipeData([]);
                    };


                  }
                }
              };

              // setUpdatedAt(response.data.data.updated_at);

            };

        })
        .catch(function(error){
            
            console.log("Error : ", error.response);
            alert('Oops, something whent wrong !')
        
        });

                        // setDisabledTabPengaturan(false)
  }, [isDisabledTabPengaturan])


  /*

    ``````````````````````
    HANDLE EDIT FIELD ROLE

    ``````````````````````

  */
  const [ valueFieldEditRoleName, setValueFieldEditRoleName] = useState('');

  const handleUbahRole = (e) => {

    // console.log("Name Role : ", nameRoleState);
    setValueFieldEditRoleName(e.target.value);

  };

  /*
    ``````````````
    HANDLE SWITCH

    ``````````````
  */

  const [ isLockedRole, setLockedRole] = useState(1);

  const [switchState, setSwitchState] = useState({

    checked: false
  });

  const handleChangeSwitch = name => event => {

    setSwitchState({ ...switchState, [name]: event.target.checked });

    // console.log(`Value checked ${event.target.checked}`)

    if(event.target.checked == true ){

      setLockedRole(0);

    } else {

      setLockedRole(1);

    };


    /*
      ````````````````````````
      SWITCH UPDATE FUNCTION

      ```````````````````````

    */

    const header =  {       
      'Accept': "application/json",
      'Content-Type' : "application/json",
      'Authorization' : "bearer " + userTokenState,
    };

    axios.defaults.headers.common = header;    
      
    axios
      .put(URL_API + `/account-management/master-role/${idRoleState}/toggle`)
      .then(function(response){

          if(response.status == 200){

            console.log("Response Original : ", response.data.data);    
            console.log("Switch functional, work !");
            // alert("Berhasil !");
            
            if(response.data.data.is_locked !== null){

              setLockedStatusState(response.data.data.is_locked);

            } else {
              
              setLockedStatusState(response.data.data.is_locked);

            };
            
            // setUpdatedAt(response.data.data.updated_at);

            

          };
      })
      .catch(function(error){

          alert('Whoops, something went wrong !')
          console.log("Error : ", error.response)
      
      });

  };

  /*
    ``````````````````````````````
    DIALOG MODAL TAMBAH HAK AKSES 

    ``````````````````````````````

  */

  const [ isModalTambahHakAkses, setModalTambahHakAkses ] = useState(false);



  /*

    ```````````````````````
    LIST HAK AKSES OF ROLE

    ```````````````````````
    
  */

  const [ listTipeData, setListTipeData ] = useState([]);
  const [ roleModul, setRoleModul ] = useState([]);


  /*
    ``````````````````````````
    DELETE MODUL PRIVILAGE SET

    ```````````````````````````

  */

  const [ isOpenModalDelete , setOpenModalDelete ] = useState(false);
  const [ dataDetail, setDataDetail ] = useState(null); 
 


  const handleDeleteModulPrivilageSet = (e, data) => {

    e.preventDefault();
    
    setDataDetail(data);
    setOpenModalDelete(true);


    console.log("Data yang mau di delete : ", data);



  };


  return(

    <MuiThemeProvider theme={theme}>
    
      <Paper className={classes.root}  elevation={0}>

        <Grid  
            container
            spacing={10}
            direction="row"
            justify="flex-end"
            alignItems="center"
        >   
            <Grid item sm={12}>

                <Typography variant='h6' className={classes.titleBackDaftarRole}>
                   <IconButton
                    onClick={() => Redirect(ToRole)}
                   >
                    <i className="material-icons" style={{ color: '#cc0707', cursor: 'pointer'}}>
                        keyboard_backspace
                    </i>
                  </IconButton> 
                    <b>Daftar Role</b>                             
                </Typography>
            </Grid>
        </Grid>

        <Grid container>   
            <Grid item sm={12}>
                <Typography variant='h5' className={classes.titleNameRole}>
                    <b>{nameRoleState}</b>                             
                </Typography>
            </Grid>
        </Grid>
      </Paper>

      <br />
      <Tabs 
        selectedIndex={tabIndex} 
        onSelect={tabIndex => handleTab(tabIndex)} 
        style={{backgroundColor: 'white'}}

      >
        <TabList>
            <Tab disabled={isDisabledTabPengaturan !== true ? true : false}>
              <Tooltip 
                // open={isDisabledTabPengaturan !== true ? true : false}
                // open={() => setTimeout(false, 5000)}
                title={isDisabledTabPengaturan !== true ? "Anda harus menambahkan Hak Akses terlebih dahulu sebelum melakukan Pengaturan !" : ''}
                placement='bottom-start'
              >
                <Typography variant='subtitle1' className={classes.title}>
                    <b>Pengaturan</b>
                </Typography>
              </Tooltip>
            </Tab>
            <Tab>
                <Typography variant='subtitle1' className={classes.title}>
                    <b>Hak Akses</b>
                </Typography>
            </Tab>
        </TabList>

        <TabPanel>

          <br />
          <br />
          <Typography variant='subtitle2' className={classes.titleLabelNamaRole}>
            <b>Nama <i>Role</i></b>
          </Typography>
          <TextField
              id="outlined-bare"
              className={classes.textField}
              onChange= {handleUbahRole}
              placeholder={nameRole}
              // color='primary'
              // onKeyDown={handleEnterPress}
              variant="outlined"
              disabled= {isLockedStatusState == 1 ? true : false}
              // fullWidth
          />

          <br />
          <br />
          <br />
          <Typography variant='subtitle2' className={classes.titleLabelSwitch}>
            {console.log("Status Locked : ",isLockedStatusState)}
            <Switch 
                checked={isLockedStatusState == 0 ? true : false} 
                className={classes.switch}
                onChange={handleChangeSwitch('checked')}

            />
            <b>Izinkan Modifikasi</b>
          </Typography>
        

          {/*     
              ````````````````````````````````````````

              BERIKAN KETENTUAN & HAK AKSES COMPONENT 

              ````````````````````````````````````````

          */}
          <ListStructureOrgRole 
            classes={classes}
            isLockedStatusState= {isLockedStatusState}
          />

          <br />
          <Button 
            onClick={() => alert("Dialog Modal Ubah is under development...")}
            variant="outlined" 
            // className={classes.buttonUbah}
            // className={classes.buttonHapusRole}
            className={classes.buttonHapusRole  } 
            disabled= {isLockedStatusState == 1 ? true : false}
          >
              Hapus &nbsp;<i>Role</i>
          </Button>

          <Button 
            onClick={handlePatchUbahRole}
            variant="contained" 
            className={classes.buttonUbah}
            // className={isLockedStatusState == 1 ? classes.buttonDisabled :classes.buttonUbah  } 
            // disabled= {isLockedStatusState == 1 ? true : false}
          >
              Simpan
          </Button>
          <br />
          <br />
          <br />
          <br />
          <br />
          <br />
        </TabPanel>

        <TabPanel>
          <br />
          <br />
          <br />

          {/* 

              ``````````````
              LIST HAK AKSES
              
              ``````````````
            
          */}

          {
            roleModul.length > 0 && (

              <Grid container>
                <Grid item xs={12} style={{textAlign: 'right'}}>
                  <Button 
                      onClick={() => setModalTambahHakAkses(true)}
                      variant="contained" 
                      style={{marginTop: 10, marginRight: 14}}
                      className={isLockedStatusState == 1 ? classes.buttonDisabled :classes.button  } 
                      disabled= {isLockedStatusState == 1 ? true : false}
                    >
                      + Tambah Hak Akses
                  </Button>
                </Grid>


                <Paper className={classes.rootPaperTable}>
                  <Table className={classes.tablePaper}  >
                    <TableHead>
                      <TableRow>

                        <TableCell className={classes.title} align='center'>Modul</TableCell>
                        {/* <TableCell className={classes.title} align="left" >Hak Akses</TableCell> */}
                        <TableCell  className={classes.title} align="left">Tipe Data</TableCell>
                        <TableCell  className={classes.title} align="left"></TableCell>
                        <TableCell  className={classes.title} align="left"></TableCell>
                      </TableRow>
                    </TableHead>
                    <TableBody>

          
                      { 
                        roleModul.length > 0 && roleModul.map((row, i) => (
                          
                          <TableRow 
                            tabIndex={-1}
                            key={i}
                          >
                                <TableCell  className={classes.title} align='center' component="th" scope="row">
                                  <b>{row.name}</b>
                                </TableCell>
                                
                                {/* <TableCell  className={classes.title} align="left">***</TableCell> */}
                                <TableCell  className={classes.title} align="left">
                                
                                    {
                                      listTipeData.length > 0 && listTipeData.map((item,k) => (

                                        <i key={k}>{Capitalize(item.name) + ","} </i>
                                      ))
                                    }
                                
                                </TableCell>
                                
                                <TableCell  className={classes.title} align="right">

                                  {/* 
                                    <IconButton 
                                      onClick={() => setModalUbahHakAkses(true)}
                                      className={classes.iconEditTable}
                                      disabled= {isLockedStatusState == 1 ? true : false}
                                    >
                                      <i className='material-icons'>edit</i>
                                    </IconButton> 
                                  */}

                                </TableCell>

                                <TableCell  className={classes.title} align="left">
                                  <IconButton 
                                    // onClick={() => alert('API delete Hak Akses still progress...')}
                                    onClick={(e) => handleDeleteModulPrivilageSet(e, row)}
                                    className={classes.iconDeleteTable}
                                    disabled= {isLockedStatusState == 1 ? true : false}
                                  >
                                    <i className='material-icons'>delete</i>
                                  </IconButton>
                                </TableCell>                            
                            </TableRow>
                          ))
                        }
                    </TableBody>
                  </Table>
                </Paper>
              </Grid>
            )
          }


          {
            roleModul.length == 0 && (
             
             <div>

                <Grid container>   
                  <Grid item sm={12} style={{textAlign: 'center'}}>
                    <img src={PictDefaultEmpty} alt='Pict Default Empty' />
                  </Grid>
                </Grid>

                <br />
                <Grid container>  
                  <Grid item sm={4}></Grid>
                  <Grid item sm={4} style={{textAlign: 'center'}}>
                    <Typography variant='subtitle1' className={classes.title} style={{color: 'grey'}}>
                      <i>Role</i> ini belum memiliki hak akses. <br />
                      Tambahkan hak akses sebelum diberikan kepada <i>user</i>.
                    </Typography>
                  </Grid>
                  <Grid item sm={4}></Grid>
                </Grid>
                <Grid container>  
                  <Grid item sm={12} style={{textAlign: 'center'}}>
                    <Typography variant='subtitle1' className={classes.title} style={{color: 'grey'}}>
                      Semua <i>user</i> yang mendapatkan <i>role</i> ini akan memiliki seluruh hak akses  yang diberikan 
                    </Typography>
                    <br />
                    <br />
                    <Button 
                        onClick={() => setModalTambahHakAkses(true)}
                        variant="contained" 
                        style={{marginTop: 10, marginRight: 14}}
                        className={isLockedStatusState == 1 ? classes.buttonDisabled :classes.button  } 
                        disabled= {isLockedStatusState == 1 ? true : false}
                      >
                          + Tambah Hak Akses
                      </Button>
                  </Grid>
                </Grid>
              </div>
            )
          }
          <br />
          <br />
          <br />
          <br />

        </TabPanel>
      </Tabs>


      <DialogModalTambahHakAkses 
        classes={classes}
        isModalTambahHakAkses= {isModalTambahHakAkses}
        setModalTambahHakAkses={setModalTambahHakAkses}
        idRoleState={idRoleState}
        userTokenState= {userTokenState}
        nameRoleState={nameRoleState}
        updatedAt= {updatedAt}

        setDisabledTabPengaturan= {setDisabledTabPengaturan}


      />
      
      {/* 
        <DialogModalTambahHakAkses_v2 
          classes={classes}
          isModalTambahHakAkses= {isModalTambahHakAkses}
          setModalTambahHakAkses={setModalTambahHakAkses}
        /> 
      */}

      <DialogModalUbahHakAkses
        classes={classes}
        isModalUbahHakAkses= {isModalUbahHakAkses}
        setModalUbahHakAkses={setModalUbahHakAkses}
        idRoleState={idRoleState}
        userTokenState= {userTokenState}
        nameRoleState={nameRoleState}
        updatedAt= {updatedAt}

      />

      <DialogDeleteModulPrivilage 
        classes = { classes }
        idRoleState={idRoleState}
        setOpenModalDelete = { setOpenModalDelete }
        isOpenModalDelete = { isOpenModalDelete }
        dataDetail = { dataDetail }

      />
    </MuiThemeProvider>
  );
};

  export default withStyles(styles)(ViewRoleDetail);


  function createData(name, calories, fat, carbs, protein) {
    return { name, calories, fat, carbs, protein };
  }
  
  const rows = [
    createData('Frozen yoghurt', 159, 6.0, 24, 4.0),
    createData('Ice cream sandwich', 237, 9.0, 37, 4.3),
    createData('Eclair', 262, 16.0, 24, 6.0),
    createData('Cupcake', 305, 3.7, 67, 4.3),
    createData('Gingerbread', 356, 16.0, 49, 3.9),
  ];
