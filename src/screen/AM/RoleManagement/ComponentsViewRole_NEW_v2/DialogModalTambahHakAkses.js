import React, { Component, useEffect, useState } from "react";
import axios from 'axios'; 
import { 
    Container, Paper, Typography, Grid, TextField, Checkbox, FormControlLabel, Box, 
    Button, CircularProgress, Card, CardActionArea, CardMedia, CardContent, CardActions, 
    FormControl, MenuItem,Dialog, DialogTitle, DialogContent, DialogContentText, DialogActions,
    Snackbar, IconButton, Radio, FormLabel
    
} from '@material-ui/core';

import DialogModalHakAksesQuestion from './DialogModalHakAksesQuestion';

import PictInfo from '../../../../assets/images/icon-info-24px.svg';
import Redirect from '../../../../utilities/Redirect';
import {ToDashboard, ToOTP} from '../../../../constants/config-redirect-url';

import { useGetHttp } from '../Hook/useGetHttp';
import { URL_API } from '../../../../constants/config-api';



const DialogModalTambahHakAkses = props => {

    const { 

        classes, 
        isModalTambahHakAkses, 
        setModalTambahHakAkses, 
        idRoleState, 
        userTokenState, 
        nameRoleState, 
        updatedAt, 
        setDisabledTabPengaturan 

    } = props;

    // console.log(" props : ", props);

    /*

        ````````````````````
        COMPONENT DID MOUNT

        ````````````````````

        6ce37e74-d86a-4a18-bf8b-6e09b703505b

    */
   

    // const [ loading, fetchedData, setFetchedData ] = useGetHttp(URL_API + `/account-management/master-role/${idRoleState}/patch`, [isModalTambahHakAkses])

    // let upadatedAtLet = '';
    
    // if(fetchedData !== null){

    //     if(fetchedData !== undefined ){

    //         console.log("Fetched Data : ", fetchedData);
    
    //         upadatedAtLet = fetchedData.updated_at;
    //     };
    // };


    /*
        `````````````````````````
        GET PRIVILAGE SET FROM DB

        `````````````````````````
    */
    // const [ upadatedAtLet, setUpdatedAt ] = useState('');
    const [ listPrivilageSetDropdown, setListPrivilageSetDropdown ] = useState([]);

    useEffect(() => {

        if(userTokenState !== undefined && userTokenState !== null ){
                
            const header =  {       
                'Accept': "application/json",
                'Content-Type' : "application/json",
                'Authorization' : "bearer " + userTokenState,
            };

            axios.defaults.headers.common = header;    

            axios
                .get(URL_API + `/account-management/master-role/${idRoleState}/patch`)
                .then(function(response){

                    console.log("Response Original in 'listPrivilageSetDropdown' : ", response)

                    if(response.status == 200 ){
                        
                        // setUpdatedAt(response.data.data.updated_at);

                        if(response.data.data !== undefined){
                            if(response.data.data.privilegeSetCollections.length > 0){

                                setListPrivilageSetDropdown(response.data.data.privilegeSetCollections);
                            }
                        }
                    };
                })
                .catch(function(error){
                    
                    console.log("Error : ", error.response);
                    // console.log("Error from 'Question Data Restricted !")
                    // alert("Ooops, something went wrong !")
                    
                })

        } else { console.log("No Access Token available!")};


    }, [isModalTambahHakAkses])
        


    

    /*
        ``````````````````````
        Dropdown

        ``````````````````````

    */

    //*

    let dataDummy = {

        descendent: []
    };
    
    const [selectedModule, setSelectedModule ] = useState(dataDummy);
    
    //**
    const [moduls, setModulePrivilage] = useState({
        
        privilage: ''
    });   

    //***
    const handleChangeDropdown = name => event => {

        setModulePrivilage({ ...moduls, [name]: event.target.value });

        console.log("Value Dropdown : ", event.target.value);

        setSelectedModule(event.target.value);

        //** clear in DROP DOWN
        // setListValuePaper({descendentSingle: []});


    };


    /*
        ````````````````````````````````
        HANDLE MODAL HAK AKSES QUESTION

        ````````````````````````````````

    */

    const [ isModalQuestionHakAkses, setModalQuestionHakAkses ] = useState(false);


    return (
        <div>
            <Dialog
                open={isModalTambahHakAkses}
                onClose={() => setModalTambahHakAkses(false)}
                aria-labelledby="alert-dialog-title"
                aria-describedby="alert-dialog-description"
            >
                <DialogTitle id="alert-dialog-title" style={{textAlign: "center"}}>
                    <Typography variant='subtitle1' className={classes.title}>
                        <b>Tambah Hak Akses</b>
                    </Typography>
                </DialogTitle>
                <DialogContent>

                    <Typography variant='subtitle2' className={classes.title}>
                        <b>Pilih modul :</b>
                    </Typography>

                    <TextField
                        id="outlined-select-privilage"
                        select
                        // label="Pilih bahasa : "
                        className={classes.dropDown}
                        value={moduls.privilage}
                        onChange={handleChangeDropdown('privilage')}
                        SelectProps={{
                            MenuProps: {
                                className: classes.menu,
                            },
                        }}
                        margin="normal"
                        variant="outlined"
                    >
                        {/* {
                            modulePrivilage.map (

                                option => (
                                    <MenuItem key={option.value} value={option}>
                                        {option.label}
                                    </MenuItem>
                                )
                            )
                        } */}

                        {
                            listPrivilageSetDropdown.length > 0 ? listPrivilageSetDropdown.map((option, i) => {

                                return (
                                    <MenuItem key={i} value={option}>
                                        {option.name}
                                    </MenuItem> 
                                )
                            }) : null
                        }

                    </TextField>
                </DialogContent>

                <DialogActions style={{alignItems: "right", justifyContent:'flex-end', marginRight: 24}}>
                    <Button 
                        variant='contained' 
                        onClick={() => setModalQuestionHakAkses(true)} 
                        // onClick={}
                        // onClick={handlePOSTTambahAkses}
                        color="primary" 
                        size='small'
                        className={classes.button}
                        // className={isEnabledButton !== true ? classes.buttonDisabled : classes.button}
                        // disabled={isEnabledButton !== true ? true : false}
                    >  
                        Lanjut 
                    </Button>

                
                </DialogActions>


                <br />
            </Dialog>

            <DialogModalHakAksesQuestion 
                
                classes={classes}
                isModalQuestionHakAkses={isModalQuestionHakAkses}
                setModalQuestionHakAkses={setModalQuestionHakAkses}
                selectedModule={selectedModule}

                userTokenState={userTokenState}
                nameRoleState={nameRoleState}
                updatedAt={updatedAt}
                idRoleState={idRoleState}

                setDisabledTabPengaturan= {setDisabledTabPengaturan}
                setModalTambahHakAkses={setModalTambahHakAkses}

                
            />

        </div>
    
    );  
};

export default DialogModalTambahHakAkses;


const modulePrivilage = [

    {
        value: 'a0b8197e-c1e4-479f-93a3-5340f5a1935e',
        label: 'All',
        child: []
    },
    {
        value: 'a4ee2739-d7d9-486e-bb27-4d3646e9885a',
        label: '7WD',
        child: [
            {
                id: "2d62ddbd-868c-4c51-8b32-3ac3a5fbe9de",
                name: "7WD"
            }
        ]
    },

    {
        value: 'fddcf257-29ba-4950-a549-f74726a73d9e',
        label: 'View Downward Sensitive Data',
        child: []
    },

    {
        value: 'efef19fb-4e08-46f4-a8f7-a5ed2214d064',
        label: 'Self',
        child: [
            {
                id : "10c25f7a-6fc8-454f-8877-808282ebd336",
                name: "view"
            }
        ]
    },
    {
        value: '38660ffc-61b6-4539-a133-5afc348c0b9c',
        label: 'Human Resource ( Employee, Position )',
        child: [
            {
                id: "8fdad977-1ab2-4853-9f2b-f98bdf1b2640",
				name: "restricted",
            },
            {
				id: "dbdc866b-2fa7-4961-9923-3b5f239137c7",
				name: "manage"
            },
            {
				id: "2f1fb00e-789d-490f-b34f-89c15c44a231",
				name: "sensitive"
            },
            {
                id: "03973984-72cc-4437-b897-c628b90e2cf0",
				name: "view",
            }
        ]
    },
    {
        value: 'ae9dec73-24fc-4e95-828d-abc869025821',
        label: 'Account Management (Group)',
        child: [
            {
				id: "76c9a8ce-0a0d-4504-b5ef-47f3a54cf06c",
				name: "restricted"
            },
            {
				id: "baaf6c25-65cc-4ec8-a6a9-da9ee02e38c7",
				name: "manage"
            },
            {
                id: "1cb605e2-3f45-4745-944f-f9693cc8fd6b",
				name: "sensitive"               
            },
            {
				id: "079eb9f1-d90a-4eda-b8fe-0ddc6ab6cfaf",
				name: "view"
            }
        ]
    },
    {
        value: '32c005e7-5387-42b2-a8ff-c53014b0b3d4',
        label: 'Account Management (User)', 
        child: [
            {
				id: "5745542c-6138-45cf-b4ff-8c70cc37d2b0",
				name: "restricted",
            },
            {
				id: "623c7658-7034-4ad3-a4f1-de9b2bc952d6",
				name: "manage",                
            },
            {
				id: "2f3da493-cca7-4a9c-98bb-47852dabfdf3",
				name: "sensitive"
            },
            {
				id: "8009b5d6-befb-4f89-8b72-6322606cd63d",
				name: "view",
            }
        ]
    },
  ];
