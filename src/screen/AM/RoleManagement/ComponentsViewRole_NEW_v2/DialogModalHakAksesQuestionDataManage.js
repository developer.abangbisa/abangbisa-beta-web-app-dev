import React, { Component, useEffect, useState } from "react";
import axios from 'axios'; 
import { 
    Container, Paper, Typography, Grid, TextField, Checkbox, FormControlLabel, Box, 
    Button, CircularProgress, Card, CardActionArea, CardMedia, CardContent, CardActions, 
    FormControl, MenuItem,Dialog, DialogTitle, DialogContent, DialogContentText, DialogActions,
    Snackbar, IconButton, Radio, FormLabel
    
} from '@material-ui/core';

import DialogModalHakAksesQuestionDataRestricted from './DialogModalHakAksesQuestionDataRestricted';

const DialogModalHakAksesQuestionDataManage = props => {

    const { 
            classes, 
            hakAksesViewID, 
            hakAksesSensitifID, 
            modalQuestionDataManage, 
            setModalQuestionDataManage, 
            selectedModule,
            
            userTokenState,
            nameRoleState,
            updatedAt,
            idRoleState,

            // setDisabledTabPengaturan,
            setModalTambahHakAkses,
            setModalQuestionHakAkses,
            setModalQuestionDataSensitif


        } = props;

    /*
        ````````````````````
        HANDLE RADIO BUTTON 

        ````````````````````
        
    */

   const [ hakAksesManageIDState, sethakAksesManageIDState] = useState('');
       
   let hakAksesManageID = undefined;


    //*
    const [selectedValue, setSelectedValue] = useState('');

    const handleChangeRadioButton = (event) => {

       setSelectedValue(event.target.value);

       if(event.target.value == 'manage'){
           
           console.log("MANAGE - Question_03");

           console.log("selectedModule : ", selectedModule);

           if(selectedModule !== undefined){
               if(selectedModule.embedded.child.length > 0){
   
                hakAksesManageID = selectedModule.embedded.child.map(item => {
                       
                       return item.name == event.target.value ? item.id : null
                   })
               };
           };

           console.log("hakAkses id manage: ", hakAksesManageID[1]);      
       }; 

       //**
       if(hakAksesManageID !== undefined){

           sethakAksesManageIDState(hakAksesManageID[1]);
       };

       if(event.target.value == 'Tidak'){
           
            console.log("User choose 'Tidak' ! ");
            sethakAksesManageIDState('');
            //setModalQuestionDataSensitif(false);

       };

    };

    /*
        `````````````````````````````````````````
        HANDLE DIALOG MODAL UNTUK DATA RESTRICTED

        `````````````````````````````````````````

    */

    const [ modalQuestionDataRestricted, setModalQuestionDataRestricted ] = useState(false);


    return (
        <div>
            <Dialog
                open={modalQuestionDataManage}
                onClose={() => setModalQuestionDataManage(false)}
                aria-labelledby="alert-dialog-title"
                aria-describedby="alert-dialog-description"
            >
                <DialogTitle id="alert-dialog-title" style={{textAlign: "center"}}>
                    <Typography variant='subtitle1' className={classes.title}>
                        <b>Tambah Hak Akses</b>
                    </Typography>
                </DialogTitle>
                <DialogContent>

                    <Typography variant='subtitle2' className={classes.title}>
                        <b>Apakah <i>Role</i> ini memiliki hak akses untuk mengatur data ?</b>
                    </Typography>
                    
                    <br />
                    <FormControlLabel 
                        style={{fontFamily: 'Nunito'}}
                        label="Ya" 
                        control={
                            <Radio 
                                checked={selectedValue === 'manage'}
                                onChange={handleChangeRadioButton}
                                value="manage"
                                name='answer-ya'
                                label='Ya'
                                labelPlacement="end"
                            />
                        } 
                    />
                    
                    <br />
                    <FormControlLabel 
                        style={{fontFamily: 'Nunito'}}
                        label="Tidak" 
                        control={
                            <Radio 
                                checked={selectedValue === 'Tidak'}
                                onChange={handleChangeRadioButton}
                                value="Tidak"
                                name='answer-no'
                                label='Tidak'
                            />
                        } 

                    />
                    <DialogContentText id="alert-dialog-description">
                    
                    </DialogContentText>
                </DialogContent>
                <DialogActions style={{alignItems: "right", justifyContent:'flex-end'}}>

                    <Button 
                        onClick={() => setModalQuestionDataManage(false)} 
                        variant='outlined' 
                        size='small'
                        className={classes.buttonModalCancel}    
                    >
                        Batal
                    </Button>

                    <Button 
                        variant='contained' 
                        color="primary" 
                        size='small'
                        className={classes.button}
                        style={{marginRight: 24}}
                        onClick={() => setModalQuestionDataRestricted(true)}
                    >  
                        Lanjut 
                    </Button>
                </DialogActions>
                <br />
                <br />
            </Dialog>

            <DialogModalHakAksesQuestionDataRestricted 
                classes={classes}
                modalQuestionDataRestricted={modalQuestionDataRestricted}
                hakAksesViewID={hakAksesViewID}
                hakAksesSensitifID={hakAksesSensitifID}
                hakAksesManageID={hakAksesManageIDState}
                selectedModule={selectedModule}

                userTokenState={userTokenState}
                nameRoleState={nameRoleState}
                updatedAt={updatedAt}
                idRoleState={idRoleState}
                
                // setDisabledTabPengaturan= {setDisabledTabPengaturan}
                setModalTambahHakAkses={setModalTambahHakAkses}
                setModalQuestionHakAkses={setModalQuestionHakAkses}
                setModalQuestionDataSensitif={setModalQuestionDataSensitif}
                setModalQuestionDataManage={setModalQuestionDataManage}
                setModalQuestionDataRestricted={setModalQuestionDataRestricted}


            />
        </div>
    );
};

export default DialogModalHakAksesQuestionDataManage;