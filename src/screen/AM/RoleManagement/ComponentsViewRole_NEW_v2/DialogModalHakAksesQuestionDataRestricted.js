import React, { Component, useEffect, useState } from "react";
import axios from 'axios'; 
import { 
    Container, Paper, Typography, Grid, TextField, Checkbox, FormControlLabel, Box, 
    Button, CircularProgress, Card, CardActionArea, CardMedia, CardContent, CardActions, 
    FormControl, MenuItem,Dialog, DialogTitle, DialogContent, DialogContentText, DialogActions,
    Snackbar, IconButton, Radio, FormLabel
    
} from '@material-ui/core';

import { URL_API } from '../../../../constants/config-api';

const DialogModalHakAksesQuestionDatarestricted = props => {

    const { 
            classes, 
            hakAksesViewID, 
            hakAksesSensitifID, 
            hakAksesManageID, 
            modalQuestionDataRestricted, 
            selectedModule,
            
            userTokenState,
            nameRoleState,
            updatedAt,
            idRoleState,
            
            setModalTambahHakAkses,
            setModalQuestionHakAkses,
            setModalQuestionDataSensitif,
            setModalQuestionDataManage,
            setModalQuestionDataRestricted, 

        } = props;

    /*
        ```````````````````
        COMPONENT DID MOUNT

        ```````````````````
    */

    const [ privilageSetTypeList, setPrivilageSetTypeList ] = useState([]);

    useEffect(() => {

        if(userTokenState !== undefined && userTokenState !== null ){
                
            const header =  {       
                'Accept': "application/json",
                'Content-Type' : "application/json",
                'Authorization' : "bearer " + userTokenState,
            };
        
            axios.defaults.headers.common = header;    

            axios
                .get(URL_API + `/account-management/master-role/${idRoleState}`)
                .then(function(response){

                    console.log("Response Original in RESCTRICTED : ", response)

                    if(response.status == 200 ){
                        if(response.data.data !== undefined){
                            if(response.data.data.embedded.privilegeSetDataTypeList.length > 0){
                                setPrivilageSetTypeList(response.data.data.embedded.privilegeSetDataTypeList);
                            }
                        }
                    };
                })
                .catch(function(error){
                    
                    console.log("Error : ", error.response);
                    console.log("Error from 'Question Data Restricted !")
                    // alert("Ooops, something went wrong !")
                    
                })

        } else { console.log("No Access Token available!")};


    }, [modalQuestionDataRestricted])

    /*
        ````````````````````
        HANDLE RADIO BUTTON 

        ````````````````````    
    */
    
    const [ hakAksesRestrictedIDState, setHakAksesRestrictedIDState] = useState('');

    let hakAksesRestrictedID = undefined;

    //*

    const [ selectedValue, setSelectedValue ] = useState('');

    const handleChangeRadioButton = (event) => {

        setSelectedValue(event.target.value);

        if(event.target.value == 'restricted'){
           
           console.log("RESTRICTED - Question_04");

           console.log("selectedModule : ", selectedModule);

           if(selectedModule !== undefined){

               if(selectedModule.embedded.child.length > 0){
   
                hakAksesRestrictedID = selectedModule.embedded.child.map(item => {
                       
                       return item.name == event.target.value ? item.id : null
                   })
               };
           };

           console.log("hakAkses id restricted: ", hakAksesRestrictedID[0]);      

        }; 

        //**
        if(hakAksesRestrictedID !== undefined){

            setHakAksesRestrictedIDState(hakAksesRestrictedID[0]);
        };

        if(event.target.value == 'Tidak'){
            
            console.log("User choose 'Tidak' ! ");
            setHakAksesRestrictedIDState('');
            //    setModalQuestionDataSensitif(false);

        };
    };

    /*
        ````````````````````
        HANDLE TAMBAH AKSES 

        ````````````````````
    */
    const handlePOSTTambahAkses = () => {
        
        
        let PrivilegeSetTemplate = []; 


        if(privilageSetTypeList.length > 0){
            
            privilageSetTypeList.map((detail) => {
                
                PrivilegeSetTemplate.push(detail);
            })

        
        };


        /*
            ``````````````````````````
            ``````````````````````````
            ``````````````````````````
        */

        PrivilegeSetTemplate.push(hakAksesViewID);
        PrivilegeSetTemplate.push(hakAksesSensitifID);
        PrivilegeSetTemplate.push(hakAksesManageID);
        PrivilegeSetTemplate.push(hakAksesRestrictedIDState);
        
        let PrivilegeSet = PrivilegeSetTemplate.filter(function (el) {
            return el != '';

        });
          
        console.log("Filtered : ", PrivilegeSet);
    
        let data = {    
            MasterRole: {
                name: nameRoleState,
                updated_at: updatedAt,
                // is_locked: 0,
            },
            PrivilegeSet,
            // PrivilegeSet: [
                //     hakAksesViewID, 
                //     hakAksesSensitifID, 
                //     hakAksesManageID,
                //     hakAksesRestrictedIDState
                // ],
            _method: 'patch'
        };

        console.log("DATA PATCH : ", data);

        
        if(userTokenState !== undefined && userTokenState !== null ){
                
            const header =  {       
                'Accept': "application/json",
                'Content-Type' : "application/json",
                'Authorization' : "bearer " + userTokenState,
            };
        
            axios.defaults.headers.common = header;    

            axios
                .post(URL_API + `/account-management/master-role/${idRoleState}`, data)
                .then(function(response){

                    console.log("Response Original : ", response)

                    if(response.status == 200 ){
                        
                        window.location.reload();

                        if(response.data.data !== undefined){
                            
                        };
                    };
                })
                .catch(function(error){
                    
                    console.log("Error : ", error.response)
                    alert("Ooops, something went wrong !")
                    
                })

        } else { console.log("No Access Token available!")};
    
    };

    return (

        <div>
        <Dialog
            open={modalQuestionDataRestricted}
            onClose={() => setModalQuestionDataRestricted(false)}
            aria-labelledby="alert-dialog-title"
            aria-describedby="alert-dialog-description"
        >
            <DialogTitle id="alert-dialog-title" style={{textAlign: "center"}}>
                <Typography variant='subtitle1' className={classes.title}>
                    <b>Tambah Hak Akses</b>
                </Typography>
            </DialogTitle>
            <DialogContent>

                <Typography variant='subtitle2' className={classes.title}>
                    <b>Apakah <i>Role</i> ini di izinkan untuk mengatur data <i>restricted</i> ?</b>
                </Typography>
                
                <br />
                <FormControlLabel 
                    style={{fontFamily: 'Nunito'}}
                    label="Ya" 
                    control={
                        <Radio 
                            checked={selectedValue === 'restricted'}
                            onChange={handleChangeRadioButton}
                            value="restricted"
                            name='answer-ya'
                            label='Ya'
                            labelPlacement="end"
                        />
                    } 
                />
                
                <br />
                <FormControlLabel 
                    style={{fontFamily: 'Nunito'}}
                    label="Tidak" 
                    control={
                        <Radio 
                            checked={selectedValue === 'Tidak'}
                            onChange={handleChangeRadioButton}
                            value="Tidak"
                            name='answer-no'
                            label='Tidak'
                        />
                    } 

                />
                <DialogContentText id="alert-dialog-description">
                
                </DialogContentText>
            </DialogContent>
            <DialogActions style={{alignItems: "right", justifyContent:'flex-end'}}>

                <Button 
                    onClick={() => setModalQuestionDataRestricted(false)} 
                    variant='outlined' 
                    size='small'
                    className={classes.buttonModalCancel}    
                >
                    Batal
                </Button>

                <Button 
                    variant='contained' 
                    color="primary" 
                    size='small'
                    className={classes.button}
                    style={{marginRight: 24}}
                    onClick={handlePOSTTambahAkses}
                >  
                    Lanjut 
                </Button>
            </DialogActions>
            <br />
            <br />
        </Dialog>
    </div>
    );
};

export default DialogModalHakAksesQuestionDatarestricted;