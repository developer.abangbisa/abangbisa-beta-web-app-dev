import React, { Component, useEffect, useState } from "react";
import axios from 'axios'; 
import { 
    Container, Paper, Typography, Grid, TextField, Checkbox, FormControlLabel, Box, 
    Button, CircularProgress, Card, CardActionArea, CardMedia, CardContent, CardActions, 
    FormControl, MenuItem,Dialog, DialogTitle, DialogContent, DialogContentText, DialogActions,
    Snackbar, IconButton, Radio, FormLabel
    
} from '@material-ui/core';

import DialogModalHakAksesQuestionDataManage from './DialogModalHakAksesQuestionDataManage';

const DialogModalHakAksesQuestionDataSensitif = props => {

    const { 
            classes, 
            hakAksesViewID, 
            modalQuestionDataSensitif, 
            setModalQuestionDataSensitif, 
            selectedModule,
            
            userTokenState,
            nameRoleState,
            updatedAt,
            idRoleState,

            // setDisabledTabPengaturan,
            setModalTambahHakAkses,
            setModalQuestionHakAkses


        } = props;

        // console.log("hakAksesViewID in SENSITIF : ", hakAksesViewID)

    /*
        ````````````````````
        HANDLE RADIO BUTTON 

        ````````````````````
        
    */
    const [ hakAksesSensitifIDState, setHakAksesSensitifIDState] = useState('');

    let hakAksesSensitifID = undefined;


    //*
    const [selectedValue, setSelectedValue] = useState('');

    const handleChangeRadioButton = (event) => {

       setSelectedValue(event.target.value);

       if(event.target.value == 'sensitive'){
           
           console.log("SENSITIVE - Question_02");

           console.log("selectedModule : ", selectedModule);

           if(selectedModule !== undefined){
               if(selectedModule.embedded.child.length > 0){
   
                hakAksesSensitifID = selectedModule.embedded.child.map(item => {
                       
                       return item.name == event.target.value ? item.id : null
                   })
               };
           };

           console.log("hakAkses id sensitive: ", hakAksesSensitifID[2]);
                       
       }; 


       //**
       if(hakAksesSensitifID !== undefined){

           setHakAksesSensitifIDState(hakAksesSensitifID[2]);
       };

       if(event.target.value == 'Tidak'){
           
           console.log("User choose 'Tidak' ! ");
           setHakAksesSensitifIDState('');
        //    setModalQuestionDataSensitif(false);

       };
   };

    /*
        `````````````````````````````````````
        HANDLE DIALOG MODAL UNTUK DATA MANAGE

        `````````````````````````````````````

    */

   const [ modalQuestionDataManage, setModalQuestionDataManage ] = useState(false);


    return (
        <div>
            <Dialog
                open={modalQuestionDataSensitif}
                onClose={() => setModalQuestionDataSensitif(false)}
                aria-labelledby="alert-dialog-title"
                aria-describedby="alert-dialog-description"
            >
                <DialogTitle id="alert-dialog-title" style={{textAlign: "center"}}>
                    <Typography variant='subtitle1' className={classes.title}>
                        <b>Tambah Hak Akses</b>
                    </Typography>
                </DialogTitle>
                <DialogContent>

                    <Typography variant='subtitle2' className={classes.title}>
                        <b>Apakah <i>Role</i> ini di izinkan untuk melihat data sensitif ?</b>
                    </Typography>
                    
                    <br />
                    <FormControlLabel 
                        style={{fontFamily: 'Nunito'}}
                        label="Ya" 
                        control={
                            <Radio 
                                checked={selectedValue === 'sensitive'}
                                onChange={handleChangeRadioButton}
                                value="sensitive"
                                name='answer-ya'
                                label='Ya'
                                labelPlacement="end"
                            />
                        } 
                    />
                    
                    <br />
                    <FormControlLabel 
                        style={{fontFamily: 'Nunito'}}
                        label="Tidak" 
                        control={
                            <Radio 
                                checked={selectedValue === 'Tidak'}
                                onChange={handleChangeRadioButton}
                                value="Tidak"
                                name='answer-no'
                                label='Tidak'
                            />
                        } 

                    />
                    <DialogContentText id="alert-dialog-description">
                    
                    </DialogContentText>
                </DialogContent>
                <DialogActions style={{alignItems: "right", justifyContent:'flex-end'}}>

                    <Button 
                        onClick={() => setModalQuestionDataSensitif(false)} 
                        variant='outlined' 
                        size='small'
                        className={classes.buttonModalCancel}    
                    >
                        Batal
                    </Button>

                    <Button 
                        variant='contained' 
                        color="primary" 
                        size='small'
                        className={classes.button}
                        style={{marginRight: 24}}
                        onClick={() => setModalQuestionDataManage(true)}
                    >  
                        Lanjut 
                    </Button>
                </DialogActions>
                <br />
                <br />
            </Dialog>

            <DialogModalHakAksesQuestionDataManage 
                classes = { classes }
                hakAksesSensitifID={ hakAksesSensitifIDState }
                hakAksesViewID = { hakAksesViewID }
                modalQuestionDataManage = {modalQuestionDataManage}
                selectedModule={selectedModule}
                
                userTokenState={userTokenState}
                nameRoleState={nameRoleState}
                updatedAt={updatedAt}
                idRoleState={idRoleState}
                
                // setDisabledTabPengaturan= {setDisabledTabPengaturan}
                setModalTambahHakAkses={setModalTambahHakAkses}
                setModalQuestionHakAkses={setModalQuestionHakAkses}
                setModalQuestionDataSensitif={setModalQuestionDataSensitif}
                setModalQuestionDataManage = { setModalQuestionDataManage}


            />
        </div>
    );
};

export default DialogModalHakAksesQuestionDataSensitif;