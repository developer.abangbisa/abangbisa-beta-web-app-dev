import React, { useCallback, useEffect, useState, useContext} from "react";
import { makeStyles, createMuiTheme, withStyles, MuiThemeProvider } from '@material-ui/core/styles';
import { 
    Container, Paper, Typography, Grid, TextField, Checkbox, CircularProgress,
    FormControlLabel, Box, Button, MenuItem, Dialog, DialogTitle, DialogContent, 
    DialogContentText, DialogActions, Breadcrumbs, Link, List, ListItem, ListItemText, 
    ListItemAvatar, Avatar, IconButton, InputBase, Divider, Tooltip, Table, Icon,
    TableBody, TableRow, TableCell, TablePagination, TableSortLabel, TableHead
    
  } from '@material-ui/core';

import SearchIcon from '@material-ui/icons/Search';
import NonActiveIcon from '@material-ui/icons/Close';

import moment from 'moment';

import ContextGlobalFormUnit from './Context/ContextRole';

import DialogModalTambahRole from './ComponetViewRole/DialogModalTambahRole';
import IconPreviewGrid from '../../../assets/images/SVG/Group_1002.svg';
import IconPreviewList from '../../../assets/images/SVG/Group_1001_red.svg';
import IconFilterNew from '../../../assets/images/SVG/Group_1117.svg';
import ButtonColorGrey from '../../../components/ButtonColorGrey';
import EnhancedTableToolbar from './ComponetViewRole/EnhancedTableToolbar';

import { useGetHttp } from './Hook/useGetHttp';
import { URL_API } from '../../../constants/config-api';
import Redirect from '../../../utilities/Redirect';
import { ToDashboard, ToRoleAdd, ToRoleEdit, ToRoleDetail} from '../../../constants/config-redirect-url';

import { styles } from './Style/StyleRole';
// import ContextRole from "./Context/ContextRole"; // SEMENTARA PAKAI LOCAL STORAGE dulu aja !!! '_'

  const themeRole = createMuiTheme({
    
    palette: {

        primary: {
            main: '#cc0707', //#cc0707, #c62828
            light: '#ff5f52',
            dark: '#8e0000',
            contrastText: '#ffffff'
        }
    }, 
    overrides: {

      MuiList: {
          root: {
              width: 244,
              // marginLeft: 14
          }
      },
      MuiListItemText: {
          root: {
              fontSize: 17
          }
      }
  }
});

let counter = 0;    

function createData(name, calories, fat, carbs, protein) {

  counter += 1;
  return { id: counter, name, calories, fat, carbs, protein };

};

function desc(a, b, orderBy) {

  if (b[orderBy] < a[orderBy]) {
    return -1;
  };

  if (b[orderBy] > a[orderBy]) {
    return 1;
  };

  return 0;

};  

function stableSort(array, cmp) {

    if(array !== null && array !== undefined){

      const stabilizedThis = array.map((el, index) => [el, index]);

      stabilizedThis.sort((a, b) => {
      
        const order = cmp(a[0], b[0]);
      
        if (order !== 0) return order;
      
        return a[1] - b[1];

      });
  
      return stabilizedThis.map(el => el[0]);
    };

    return [];

};
  
function getSorting(order, orderBy) {
  
  return order === 'desc' ? (a, b) => desc(a, b, orderBy) : (a, b) => -desc(a, b, orderBy);
    
};
    
const rows = [

  { id: 'name', align: 'justify', disablePadding: false, label: 'Nama Role' },
  // { id: 'diberikan', align: 'center', disablePadding: false, label: 'Diberikan' },
  // { id: 'hakakses', align: 'center', disablePadding: false, label: 'Hak Akses' },
  { id: 'dibuat', align: 'left', disablePadding: false, label: 'Di buat' },
  { id: 'modifikasi', align: 'left', disablePadding: false, label: 'Modifikasi' },
  { id: 'buttondetail', align: 'center', disablePadding: false, label: '' }

];

const ViewRole = props => {

    const { classes } = props;

    const [order, setOrder] = React.useState('asc');
    const [orderBy, setOrderBy] = React.useState('calories');
    const [selected, setSelected] = React.useState([]);

    /*

      ````````
      CONTEXT 

      *Context belum bisa jalan, kalau pakai utilities 'Redirect' *_*

      ````````

    */
    // const context = useContext(ContextRole); // SEMENTARA PAKAI LOCAL STORAGE dulu aja !!! '_'
    
    /* 
    
      ``````````````
      Get List Role

      ``````````````
      
    */
    const [loading, data, setFetchedData ] = useGetHttp(URL_API + `/account-management/master-role?options[embedded][]=module&options[embedded][]=component&options[embedded][]=masterStructure`, []);
    console.log("List Role : ", data);




    // const inisiateDummy = [
    //   {
    //     id: 1,
    //     name: 'Hr Admin',
    //     diberikan: '',
    //     hakAkses: 1,
    //     diBuatPadaTanggal: '',
    //     diModifikasi: ''
    //   },
    //   {
    //     id: 2,
    //     name: 'Admin',
    //     diberikan: '',
    //     hakAkses: 1,
    //     diBuatPadaTanggal: '',
    //     diModifikasi: ''
    //   },
    //   {
    //     id: 3,
    //     name: 'Top Management',
    //     diberikan: '',
    //     hakAkses: 1,
    //     diBuatPadaTanggal: '',
    //     diModifikasi: ''
    //   },
    //   {
    //     id: 4,
    //     name: 'User',
    //     diberikan: '',
    //     hakAkses: 1,
    //     diBuatPadaTanggal: '',
    //     diModifikasi: ''
    //   },
    // ];
    
    // const [data] = useState(inisiateDummy)
    const [page, setPage] = useState(0);
    const [rowsPerPage, setRowsPerPage] = useState(5);
  
    function handleRequestSort(event, property) {

      const isDesc = orderBy === property && order === 'desc';
      setOrder(isDesc ? 'asc' : 'desc');
      setOrderBy(property);
    
    };
  
    function handleSelectAllClick(event) {

      if (event.target.checked) {
        const newSelecteds = data.map(n => n.id);
        setSelected(newSelecteds);
        return;
      };

      setSelected([]);
    }
  
    const [idRole, setIdRole ] = useState([]);

    /*

      ````````````````
        Get ID Role

      ````````````````
    */

    function handleClick(event, id) {

      let ids = [];
      console.log("Id :", id);
      const newIds = [...ids, {id: id}]

      setIdRole(newIds);

      /*

        ````````````````
          
          *********

        ````````````````
      */

      const selectedIndex = selected.indexOf(id);

      let newSelected = [];

      if (selectedIndex === -1) {

        newSelected = newSelected.concat(selected, id);

      } else if (selectedIndex === 0) {

        newSelected = newSelected.concat(selected.slice(1));

      } else if (selectedIndex === selected.length - 1) {

        newSelected = newSelected.concat(selected.slice(0, -1));

      } else if (selectedIndex > 0) {

        newSelected = newSelected.concat(
          selected.slice(0, selectedIndex),
          selected.slice(selectedIndex + 1),
        );
      };

      setSelected(newSelected);

    };
  
    function handleChangePage(event, newPage) {
      setPage(newPage);
    };
  
    function handleChangeRowsPerPage(event) {
      setRowsPerPage(event.target.value);
    };
  
    const isSelected = id => selected.indexOf(id) !== -1;
  
    const emptyRows = rowsPerPage - Math.min(rowsPerPage, (data !== null && data !== undefined  ? data.length : 0 ) - page * rowsPerPage);

    /* 
      ```````````````````
      Row Tab List & Grid

      ```````````````````
    */
    const [ isModalTambahRole, setModalTambahRole ] = useState(false);

    /* 
      ````````````````````
      HANDLE BUTTON DETAIL 

      ````````````````````
    */

    // const [isModalDetail, setModalDetail] = useState(false)
    const handleButtonDetail = (e, data) => {
    
      console.log("Data Detail clicked : ", data);

      localStorage.setItem('idRole', data.id);
      localStorage.setItem('nameRole', data.name);
      localStorage.setItem('isLockedStatus', data.is_locked);
      Redirect(ToRoleDetail) 

    };

    return (

        <MuiThemeProvider theme={themeRole}>
            <br />
            <Paper className={classes.root}  elevation={0}>

            {/* 
            
                ```````````````````
                Row Tab List & Grid

                ```````````````````
            */}

            <Grid  
                container
                spacing={10}
                direction="row"
                justify="flex-end"
                alignItems="center"
            >   
                <Grid item sm={10}>
                    <List dense >
                        <ListItem button>
                        <ListItemText 
                            // primary="Pengaturan Role"
                            primary={
                                <Typography variant='h6' className={classes.title}>
                                    Pengaturan Role 
                                </Typography>
                            } 
                         />
                        <ListItemAvatar>  
                          {/* <Avatar className={classes.avatarJumlahRole}>
                            {data !== null && data !== undefined ? data.length : '0'}
                          </Avatar> */}
                        </ListItemAvatar>
                        </ListItem>
                    </List>
                </Grid>
                <Grid item sm={2} style={{textAlign: 'right'}}>
                    <img src={IconPreviewList} className={classes.iconPreviewList} />
                    <img src={IconPreviewGrid} className={classes.iconPreviewGrid} />
                </Grid>
            </Grid>

            {/* 
            
                ````````````````````````````````````
                    Row Button Tambah Role & Filter 

                ````````````````````````````````````
                
            */}

            <br />
            <Grid  
                container
                spacing={0}
                direction="row"
                justify="flex-end"
            >   
                <Grid item sm={6} style={{textAlign: 'right'}}></Grid>
                <Grid item sm={3} style={{textAlign: 'right'}}>
                    <Paper className={classes.paper} elevation={1}>
                        <IconButton className={classes.iconButton} aria-label="Search">
                            <SearchIcon />
                        </IconButton>
                        <InputBase className={classes.input} placeholder="Cari Role..." />
                        <Divider className={classes.divider} />
                    </Paper>
                </Grid>
                <Grid item sm={1} style={{textAlign: 'right'}}>
                    <Tooltip title="Filter" placement="right-start">
                        <img src={IconFilterNew} className={classes.iconFilter} />
                    </Tooltip>
                </Grid>
                <Grid item sm={2} style={{textAlign: 'center'}}>
                    <Button 
                      onClick={() => setModalTambahRole(true)}
                      variant="contained" className={classes.button} style={{marginTop: 10, marginRight: 14}}>
                        + Tambah Role
                    </Button>
                </Grid>
            </Grid>
            <br />

            {/* 
            
                `````````````````
                    TABLE

                `````````````````
            */}

            <div className={classes.tableWrapper}>

                {
                    data == null && (
                    <Grid  
                        container
                        spacing={10}
                        direction="row"
                        justify="center"
                        alignItems="center"
                    >
                        
                        <Grid item sm={12} style={{textAlign: 'center'}}> 
                        <CircularProgress size={32} style={{marginTop: 64, color: 'red'}} />
                        </Grid>
                        
                    </Grid>
                    )
                }

                <Table className={classes.table} aria-labelledby="tableTitle">

                    {
                        data !== null && data !== undefined && data.length > 0 ? (

                            <EnhancedTableHead
                                numSelected={selected.length}
                                order={order}
                                orderBy={orderBy}
                                onSelectAllClick={handleSelectAllClick}
                                onRequestSort={handleRequestSort}
                                rowCount={data !== null ? data.length : 0}
                            /> 
                        ) : null
                    }
                

                    <TableBody>

                        {
                            stableSort(data, getSorting(order, orderBy))
                                .slice(page * rowsPerPage, page * rowsPerPage + rowsPerPage)
                                .map(n => {

                                    // console.log("n : ", n);
                                    
                                    const isItemSelected = isSelected(n.id);
                                    
                                    return (

                                        <TableRow
                                            hover
                                            onClick={event => handleClick(event, n.id)}
                                            role="checkbox"
                                            aria-checked={isItemSelected}
                                            tabIndex={-1}
                                            key={n.id}
                                            selected={isItemSelected}
                                            // style={{textAlign: 'center'}}
                                            
                                        >
                                            <TableCell padding="checkbox">
                                            {/* <Checkbox checked={isItemSelected} /> */}
                                                {
                                                  n.is_locked !== 1 ? (

                                                      <Checkbox checked={isItemSelected} />

                                                      ) : (
                                                        
                                                        <Checkbox disabled checked /> 

                                                      )
                                                }
                                            </TableCell>

                                            <TableCell 
                                              align='justify'  
                                              component="th" 
                                              scope="row" 
                                              // padding="inset"
                                              inset='true'
                                              style={{fontFamily: 'Nunito', fontWeight: 'bold'}}
                                            >
                                                {n.name}
                                            </TableCell>

                                            <TableCell 
                                              align='left' 
                                              style={{fontFamily: 'Nunito'}}
                                            >
                                                {moment(n.created_at).format('DD MMMM YYYY')}
                                            </TableCell>
                                            <TableCell align='left'>

                                              {
                                                n.is_locked == 1 ? (
                                                  
                                                    <NonActiveIcon className={classes.iconNonActive} />
                                                ) : (
                                                  <Icon className={classes.iconDone}>
                                                      done
                                                  </Icon>

                                                )
                                              }
                                              
                                            </TableCell>
                                            <TableCell align='center'> 
                                              <ButtonColorGrey  
                                                  title='Detail' 
                                                  handleClick={(e) => handleButtonDetail(e, n)} 
                                              
                                              />
                                            </TableCell>
                                        </TableRow>

                                    );
                                })
                        }
                        {/* {
                            emptyRows > 0 && (
                                <TableRow style={{ height: 49 * emptyRows }}>
                                <TableCell colSpan={6} />
                                </TableRow>
                            )
                        } */}
                        </TableBody>
                    </Table>
                </div>

                {/* ROW OF DELETE  */}
                <EnhancedTableToolbar numSelected={selected.length} idRole={idRole} />
        
                <TablePagination
                    rowsPerPageOptions={[5, 10, 25]}
                    component="div"
                    count={data !== null && data !== undefined ? data.length : 0}
                    rowsPerPage={rowsPerPage}
                    page={page}
                    backIconButtonProps={{
                    'aria-label': 'Previous Page',
                    }}
                    nextIconButtonProps={{
                    'aria-label': 'Next Page',
                    }}
                    onChangePage={handleChangePage}
                    onChangeRowsPerPage={handleChangeRowsPerPage}
                />

            <DialogModalTambahRole 
              classes= {classes}
              isModalTambahRole={isModalTambahRole}
              setModalTambahRole={setModalTambahRole}
              setFetchedData= {setFetchedData}
              data= {data}
            />
            </Paper>

            
        </MuiThemeProvider>
    )
};

export default withStyles(styles)(ViewRole);


function EnhancedTableHead(props) {

    const { onSelectAllClick, order, orderBy, numSelected, rowCount, onRequestSort } = props;
  
    const createSortHandler = property => event => {
  
      onRequestSort(event, property);
    };
  
    return (
  
      <TableHead>
        <TableRow>
          <TableCell padding="checkbox">
            <Checkbox
              indeterminate={numSelected > 0 && numSelected < rowCount}
              checked={numSelected === rowCount}
              onChange={onSelectAllClick}
            />
          </TableCell>
          {
            rows.map(
  
              row => (
  
                <TableCell
                  align={row.align}
                  key={row.id}
                  numeric={row.numeric}
                  padding={row.disablePadding ? 'none' : 'default'}
                  sortDirection={orderBy === row.id ? order : false}

                  style={{fontFamily: 'Nunito', fontWeight: 'bold', color: 'black'}}
                >
                  <Tooltip
                    title="Sort"
                    placement={row.numeric ? 'bottom-end' : 'bottom-start'}
                    enterDelay={300}
                  >
                    <TableSortLabel
                      active={orderBy === row.id}
                      direction={order}
                      onClick={createSortHandler(row.id)}
                    >
                      {row.label}
                    </TableSortLabel>
                  </Tooltip>
                </TableCell>
  
              ), this
            )
          }
        </TableRow>
      </TableHead>
    );
  };