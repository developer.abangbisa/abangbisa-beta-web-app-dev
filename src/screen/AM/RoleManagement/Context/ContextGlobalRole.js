
import React, { useState, useReducer, useEffect, useRef } from 'react';

import ContextRole from './ContextRole';
const ContextGlobalRole = props => {


    /* 
      ```````````````````````
      HANDLE DATA DETAIL ROLE

      ````````````````````````
    */

    const [dataDetailRole, setDataDetailRole] = useState();

    return (

        <ContextRole.Provider

            value={{

                setDataDetailRole: setDataDetailRole,
                dataDetailRole: dataDetailRole
            
            
            }}
        >
            {props.children}

        </ContextRole.Provider>
    );
};

export default ContextGlobalRole;