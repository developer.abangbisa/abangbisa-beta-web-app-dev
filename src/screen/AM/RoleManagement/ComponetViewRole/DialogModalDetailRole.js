import React, { Component, useEffect, useState } from "react";
import axios from 'axios'; 
import { 
    Container, Paper, Typography, Grid, TextField, Checkbox, FormControlLabel, Box, 
    Button, CircularProgress, Card, CardActionArea, CardMedia, CardContent, CardActions, 
    FormControl, MenuItem,Dialog, DialogTitle, DialogContent, DialogContentText, DialogActions,
    Snackbar, IconButton
    
} from '@material-ui/core';


import PictInfo from '../../../../assets/images/icon-info-24px.svg';
import Redirect from '../../../../utilities/Redirect';
import {ToDashboard, ToOTP} from '../../../../constants/config-redirect-url';

import { useGetHttp } from '../Hook/useGetHttp';
import { URL_API } from '../../../../constants/config-api';

const DialogModalDetailRole = props => {

    const { classes, isModalDetail, setModalDetail  } = props;

    return (

        <Dialog
            open={isModalDetail}
            onClose={() => setModalDetail(false)}
            aria-labelledby="alert-dialog-title"
            aria-describedby="alert-dialog-description"
        >
            <DialogTitle id="alert-dialog-title" style={{textAlign: "center"}}>
                <img src={PictInfo} className={classes.media} alt="info-icon" />  
            </DialogTitle>
            <DialogContent>
                <DialogContentText id="alert-dialog-description">
                    <Typography variant='h6' className={classes.title}>
                        Oops, something went wrong
                    </Typography>
                </DialogContentText>
            </DialogContent>
            <DialogActions style={{alignItems: "center", justifyContent:'center'}}>
                <Button 
                    variant='contained' 
                    onClick={() => setModalDetail(false)} 
                    color="primary" 
                    size='small'
                    className={classes.buttonModal}
                >  
                    Tambah 
                </Button>
            </DialogActions>
            <br />
            <br />
        </Dialog>
    )
};

export default DialogModalDetailRole;