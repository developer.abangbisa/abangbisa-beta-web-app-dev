import React, { Component, useEffect, useState } from "react";
import axios from 'axios'; 
import { 
    Container, Paper, Typography, Grid, TextField, Checkbox, FormControlLabel, Box, 
    Button, CircularProgress, Card, CardActionArea, CardMedia, CardContent, CardActions, 
    FormControl, MenuItem,Dialog, DialogTitle, DialogContent, DialogContentText, DialogActions,
    Snackbar, IconButton, Radio
    
} from '@material-ui/core';

import PictInfo from '../../../../assets/images/icon-info-24px.svg';
import Redirect from '../../../../utilities/Redirect';
import {ToDashboard, ToOTP} from '../../../../constants/config-redirect-url';

import { useGetHttp } from '../Hook/useGetHttp';
import { URL_API } from '../../../../constants/config-api';

const DialogModalUbahHakAkses = props => {

    const { classes, isModalUbahHakAkses, setModalUbahHakAkses, idRoleState, nameRoleState, userTokenState, updatedAt} = props;

    /*
        ``````````````````````
        Dropdown

        ``````````````````````

    */

    //*
    let dataDummy = {

        descendent: []
    };
    
    const [selectedModule, setSelectedModule ] = useState(dataDummy);
    
    //**
    const [moduls, setModulePrivilage] = useState({
        
        privilage: ''
    });   

    //***
    const handleChangeDropdown = name => event => {

        setModulePrivilage({ ...moduls, [name]: event.target.value });

        console.log("Value Dropdown : ", event.target.value);

        setSelectedModule(event.target.value);

        //** clear in DROP DOWN
        setListValuePaper({descendentSingle: []});

        //*** Set enable button
        if(event.target.value !== undefined){

            if(event.target.value.label == 'All'){
    
                setEnabledButton(true);

                localStorage.setItem('modulHakAkses', event.target.value.label )


            } else {
                
                setEnabledButton(false);
                localStorage.setItem('modulHakAkses', event.target.value.label )
            }
        };

    };
    
    /*
        ````````````
        Radio Change

        ````````````

    */

    const [listValuePaper, setListValuePaper] = useState({

        descendentSingle: []

    });

    const [selectedOption, setSelectedOption ] = useState({

        id: undefined
    });

    const handleRadioChange = (e, data) => {

        //*
        setSelectedOption(data)
        console.log("Data Radio Button Value: ", data);

        //**
        setListValuePaper(data);
    };

    /*
        ``````````````````
        HANDLE PAPER

        `````````````````
    */
    
    const [ isEnabledButton, setEnabledButton ] = useState(false);

    const [valuePaper,setValuePaper] = useState({
        id: undefined

    });

    const handlePaper = (e, data) => {

        e.preventDefault();
        console.log("Data Paper: ",data)
        setValuePaper(data);

        //** Disabled button Tambah hak akses
        setEnabledButton(true)
    };

    /*
        ````````````````````
        HANDLE TAMBAH AKSES 

        ````````````````````
    */
    const handlePOSTTambahAkses = () => {

        // let idPrivilageSet = moduls.privilage.value ;
        let idPrivilageSet = valuePaper.id !== undefined ? valuePaper.id : moduls.privilage.value;
        
        let data = {    
                MasterRole: {
                    name: nameRoleState,
                    updated_at: updatedAt,
                    is_locked: 0,

                },
                PrivilegeSet: [
                    idPrivilageSet
                ],
                _method: 'patch'
        };

        console.log("DATA PATCH : ", data);

        if(userTokenState !== undefined && userTokenState !== null ){
            
            const header =  {       
                'Accept': "application/json",
                'Content-Type' : "application/json",
                'Authorization' : "bearer " + userTokenState,
            };
          
            axios.defaults.headers.common = header;    

            axios
                .post(URL_API + `/account-management/master-role/${idRoleState}`, data)
                .then(function(response){

                    console.log("Response Original : ", response)

                    if(response.status == 200 ){

                        window.location.reload();

                        if(response.data.data !== undefined){
                            
                        };
                    };
                })
                .catch(function(error){
                    
                    console.log("Error : ", error.response)
                    alert("Ooops, something went wrong !")
                    
                })

        } else { console.log("No Access Token available!")};
        
    };

    return (

        <Dialog
        
            open={isModalUbahHakAkses}
            onClose={() => setModalUbahHakAkses(false)}
            aria-labelledby="alert-dialog-title"
            aria-describedby="alert-dialog-description"
        >
            <DialogTitle id="alert-dialog-title" style={{textAlign: "center"}}>
                <Typography variant='subtitle1' className={classes.title}>
                    <b>Edit Hak Akses</b>
                </Typography>
            </DialogTitle>
            <DialogContent>

                <Typography variant='subtitle2' className={classes.title}>
                    <b>Pilih modul :</b>
                </Typography>

                <TextField
                    id="outlined-select-privilage"
                    select
                    // label="Pilih bahasa : "
                    className={classes.dropDown}
                    value={moduls.privilage}
                    onChange={handleChangeDropdown('privilage')}
                    SelectProps={{
                        MenuProps: {
                            className: classes.menu,
                        },
                    }}
                    margin="normal"
                    variant="outlined"
                >
                    {
                        modulePrivilage.map (

                            option => (
                                <MenuItem key={option.value} value={option}>
                                    {option.label}
                                </MenuItem>
                            )
                        )
                    }

                </TextField>
                <Typography variant='subtitle2' className={classes.title}>
                    {
                         selectedModule.descendent.length > 0 && (

                             <b>Hak akses : </b>
                         )
                    }
                    {
                        selectedModule.descendent.length > 0 && selectedModule.descendent.map((item, i) => {

                            return (
                                <span key={i}>
                                    <Radio
                                        checked={selectedOption.id === item.id}
                                        value={item.id}
                                        onChange={(e) => handleRadioChange(e, item)}
                                        // onChange={handleRadioChange('descendent')}
                                        name="radio-button-demo"
                                        // inputProps={{ 'aria-label': 'viewOnly' }}
                                    />    
                                    <span style={{fontFamily: 'Nunito'}}>{item.name}</span>&nbsp;&nbsp;
                                </span>
                            )
                        })
                    }

                </Typography>
                
                {
                    listValuePaper.descendentSingle.length > 0 && listValuePaper.descendentSingle.map((item, i) => (

                        <Paper 
                            key={i}
                            elevation={2} 
                            className={valuePaper.id == item.id ? classes.paperStyleSelected : classes.paperStyle} 
                            onClick={(e) => handlePaper(e, item)}
                           
                        >
                            <Typography variant='h5' className={classes.titlePaperStyle} style={{textAlign: 'center'}}>
                                
                                {item.name}
                            </Typography>
                        </Paper>
                    ))

                }
                <DialogContentText id="alert-dialog-description">
                   
                </DialogContentText>
            </DialogContent>
            <DialogActions style={{alignItems: "center", justifyContent:'center'}}>
                <Button 
                    variant='contained' 
                    // onClick={() => setModalUbahHakAkses(false)} 
                    onClick={handlePOSTTambahAkses}
                    color="primary" 
                    // size='small'
                    className={isEnabledButton !== true ? classes.buttonDisabled : classes.button}
                    disabled={isEnabledButton !== true ? true : false}
                >  
                    Simpan 
                </Button>
            </DialogActions>
            <br />
            <br />
        </Dialog>
    )
};

export default DialogModalUbahHakAkses;

const modulePrivilage = [

    {
        value: '8009b5d6-befb-4f89-8b72-6322606cd63d',
        label: 'All',
        descendent: []
    },
    {
        value: 'id54325345AM_AM_USER',
        label: 'Account Management (User)',
        descendent: [
            {
                id: 'view_only_78733679_id',
                name: "view only",
                descendentSingle: [
                    {
                        id: '03973984-72cc-4437-b897-c628b90e2cf0',
                        name: 'insensitive data only',
                        descendentSingle: []

                    }
                ]
            },
            {
                id: 'manage_7873379_id',
                name: "manage",
                descendentSingle: [
                    {
                        id: 'dbdc866b-2fa7-4961-9923-3b5f239137c7',
                        name: 'unrestricted data only',
                        descendentSingle: []
                    },
                    {
                        id: '8fdad977-1ab2-4853-9f2b-f98bdf1b2640',
                        name: 'with restricted data',
                        descendentSingle: []
                    }
                ]
            },
            {
                id: '2f1fb00e-789d-490f-b34f-89c15c44a231',
                name: "view",
                descendentSingle: []
            },
        ]
    },
    {
        value: 'id54325345AM_AM_GROUP',
        label: 'Account Management (Group)',
        descendent: [
            {
                id: 'view_only_78443979_id',
                name: "view only",
                descendentSingle: [
                    {
                        id: 'a0b8197e-c1e4-479f-93a3-5340f5a1935e',
                        name: 'insensitive data only',
                        descendentSingle: []
                    },
                    {
                        id: 'efef19fb-4e08-46f4-a8f7-a5ed2214d064',
                        name: 'with sensitive data',
                        descendentSingle: []
                    }
                ]
            },
            {
                id: 'manage_784333379_id',
                name: "manage",
                descendentSingle: [
                    {
                        id: 'fddcf257-29ba-4950-a549-f74726a73d9e',
                        name: 'unrestricted data only',
                        descendentSingle: []
                    },
                    {
                        id: 'a4ee2739-d7d9-486e-bb27-4d3646e9885a',
                        name: 'with restricted data',
                        descendentSingle: []
                    }
                ]
            }
        ]
    },
    {
        value: 'id7897979798_HR',
        label: 'Human Resource',
        descendent: [
            {
                id: 'view_only_787979_id',
                name: "view only",
                descendentSingle: [
                    {
                        id: 'c91eccd0-d2f7-4ab1-915a-7598522824da',
                        name: 'insensitive data only',
                        descendentSingle: []
                    },
                    {
                        id: '4f78d822-bc8a-44f6-831a-6259f44d997d',
                        name: 'with sensitive data',
                        descendentSingle: []
                    }
                ]
            },
            {
                id: 'manage_787979_id',
                name: "manage",
                descendentSingle: [
                    {
                        id: 'bc4f50d9-c234-45e4-8b3b-57490acbc86d',
                        name: 'unrestricted data only'
                    },
                    {
                        id: '38660ffc-61b6-4539-a133-5afc348c0b9c',
                        name: 'with restricted data'
                    }   

                ]
            }
        ]
    }

  ];
  
// const modulePrivilage = [

//     {
//         value: 'id54325345all',
//         label: 'All',
//         descendent: []
//     },
//     {
//         value: 'id54325345AM_AM_USER',
//         label: 'Account Management (User)',
//         descendent: [
//             {
//                 id: 'view_only_787979_id',
//                 name: "view only",
//                 descendentSingle: [
//                     {
//                         id: 'insensitive_data_only_66876876_id',
//                         name: 'insensitive data only'

//                     }
//                 ]
//             },
//             {
//                 id: 'manage_787979_id',
//                 name: "manage",
//                 descendentSingle: [
//                     {
//                         id: 'unrestricted_data_only_66876876_id',
//                         name: 'unrestricted data only'
//                     },
//                     {
//                         id: 'with_restricted_data_66876876_id',
//                         name: 'with restricted data'
//                     }
//                 ]
//             },
//             {
//                 id: 'view_787_id',
//                 name: "view",
//                 descendentSingle: []
//             },
//         ]
//     },
//     {
//         value: 'id54325345AM_AM_GROUP',
//         label: 'Account Management (Group)',
//         descendent: [
//             {
//                 id: 'view_only_787979_id',
//                 name: "view only",
//                 descendentSingle: [
//                     {
//                         id: 'unrestricted_data_only_43545452_id',
//                         name: 'unrestricted data only'
//                     },
//                     {
//                         id: 'with_restricted_data_54643563456_id',
//                         name: 'with restricted data'
//                     }
//                 ]
//             },
//             {
//                 id: 'manage_78435379_id',
//                 name: "manage",
//                 descendentSingle: [
//                     {
//                         id: 'unrestricted_data_only_987987987_id',
//                         name: 'unrestricted data only'
//                     },
//                     {
//                         id: 'with_restricted_data_6878686_id',
//                         name: 'with restricted data'
//                     }
//                 ]
//             },
//         ]
//     },
//     {
//         value: 'id7897979798_HR',
//         label: 'Human Resource',
//         descendent: [
//             {
//                 id: 'view_only_787979_id',
//                 name: "view only",
//                 descendentSingle: [
//                     {
//                         id: 'insensitive_data_only_66876876_id',
//                         name: 'insensitive data only'
//                     },
//                     {
//                         id: 'with_sensitive_data_66876876_id',
//                         name: 'with sensitive data'
//                     }
//                 ]
//             },
//             {
//                 id: 'manage_787979_id',
//                 name: "manage",
//                 descendentSingle: [
//                     {
//                         id: 'unrestricted_data_only_987987987_id',
//                         name: 'unrestricted data only'
//                     },
//                     {
//                         id: 'with_restricted_data_6878686_id',
//                         name: 'with restricted data'
//                     }   

//                 ]
//             }
//         ]
//     }

//   ];

