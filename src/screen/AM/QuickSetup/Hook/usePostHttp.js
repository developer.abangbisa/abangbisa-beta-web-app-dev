import { useState, useEffect } from 'react';
import axios from 'axios';

export const usePostHttp = (url, dependencies, data) => {

    const {token, setToken} = useState('');
    const [isLoading, setIsLoading] = useState(false);
    const [response, setResponse] = useState(null);

    // const [response, setResponse] = useState({
        
    //     data: null,
    //     complete: false,
    //     pending: false,
    //     error: false
    // });

    // const [response200, setResponse200] = useState({
    //     data: null,
    //     complete: false,
    //     pending: false,
    //     error: false 
    // });

    // const [response400, setResponse400] = useState({
    //     data: undefined,
    //     complete: false,
    //     pending: false,
    //     error: false 
    // });

  //   fetch('https://swapi.co/api/people')
//   useEffect(() => {

//     setIsLoading(true);
//     // console.log('Sending Http request to URL: ' + url);
    
//     fetch(url)
//       .then(response => {
//         if (!response.ok) {
//           throw new Error('Failed to fetch.');
//         }
//         return response.json();
//       })
//       .then(data => {
//         setIsLoading(false);
//         setFetchedData(data);
//       })
//       .catch(err => {
//         console.log(err);
//         setIsLoading(false);
//       });
//   }, dependencies);

    useEffect(() => {
        
        const userToken = localStorage.getItem('userToken');
        // console.log("userToken in usePostHttp : ", userToken)

        if(userToken !== undefined && data !== null && data !== undefined){

            // setIsLoading(true);

            console.log("url : ", url);
            console.log("dependencies : ", dependencies);
            console.log("data : ", data);
            
            const header =  {       
                'Accept': "application/json",
                'Content-Type' : "application/json",
                // 'Authorization' : "bearer " + data.access_token != undefined ? data.access_token : ''
                'Authorization' : "bearer " + userToken,
                // 'Access-Control-Allow-Origin': '*',
                // 'crossorigin':true,
                // 'crossDomain': true
            };
        
            axios.defaults.headers.common = header;    
        
            axios
                .post(url, data)
                .then(function(response){
                    console.log("Response Original : ", response)
                    setResponse(response);
                    
                })
                .catch(function(error){
                    
                    console.log("Error : ", error.response)
                    // setResponse(error.response);
                    // setIsLoading(false);
                })
        } else { console.log("No Access Token available!")};
        

    }, dependencies !== [] ? [dependencies] : dependencies);

  return [isLoading, response];

};