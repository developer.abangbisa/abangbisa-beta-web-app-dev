import React,{Component, useState, useEffect, useContext, useRef } from 'react';
// import Redirect from 'react-router/Redirect';
import axios from 'axios';
import { Grid, CardHeader, withStyles, CardContent, Card, CircularProgress, Typography } from '@material-ui/core';
import Menu from '@material-ui/core/Menu';
import MenuItem from '@material-ui/core/MenuItem';
import MoreVertIcon from '@material-ui/icons/MoreVert';
import IconButton from '@material-ui/core/IconButton';

import {URL_API} from '../../../../constants/config-api';
import {useGetHttp_v2} from '../Hook/useGetHttp_v2';
import ModalDeleteNamaJabatan from '../ComponentViewDefineStructureDefault/ModalDeleteNamaJabatan';
import ModalUpdateNamaJabatan from '../ComponentViewDefineStructureDefault/ModalUpdateNamaJabatan';
import ModalInputNamaJabatan from '../ComponentViewDefineStructureDefault/ModalInputNamaJabatan';


import PictNamaJabatan from '../../../../assets/images/Group_1219.png';
import ButtonCustom from '../../../../components/Button';

const NamaJabatanList = (props) => {

    const { classes } = props;
 
    const [loading, dataRealNamaJabatan] = useGetHttp_v2(URL_API + '/human-resource/master-structure-position-title', []);
    
    // console.log(" dataRealNamaJabatan : ", dataRealNamaJabatan);
    
    useEffect(() => {

        if(dataRealNamaJabatan !== null){
            const newData = [...dataRealNamaJabatan]
            // console.log("newData : ", newData)
            props.setListNamaJabatan(newData)
        };

    },[dataRealNamaJabatan])

    const [ anchorEl, setAnchorEl] = useState(null);
    const open = Boolean(anchorEl);

    const [ idNameJabatan, setIdNamaJabatan ] = useState();
    const [ nameJabatan, setNamejabatan ] = useState();
    const [ isOpenModalUpdate, setOpenModalUpdate ] = useState(false);
    const [ isOpenModalDelete, setOpenModalDelete ] = useState(false);

    function handleMenu(event, params) {

        setIdNamaJabatan(params.bind.id);
        setNamejabatan(params.bind.name);        
        setAnchorEl(event.currentTarget);
        
    };

    function handleMenuClose(params) {

        console.log("Params : ", params)

        if(params.bind == "Ubah"){

            // console.log("Id Tingkat Jabatan : ", idTingkatJabatan);
            // console.log("Nama Tingkat Jabatan : ", nameTingkatJabatan);            
            setOpenModalUpdate(true)
            setAnchorEl(null);

        } else if(params.bind == "Hapus"){
            setOpenModalDelete(true)
            setAnchorEl(null);

        } else {
            
            setAnchorEl(null);
        };
    };

    /* 

        ``````````````````````````````

        Mulai ! Adding Nama Jabatan

        ``````````````````````````````

    */

    const [isChooseButtonPlus, setChooseButtonPlus ] = useState(false);
    const handleOpenModalMulai = (e) => {

        e.preventDefault();
        setChooseButtonPlus(true);
    };

    /* 
    
        ``````````````````````````````
        Create Quick fill Nama Jabatan 
        
        ```````````````````````````````
        
    */

    // const [ isChooseUnitUmum, setIsChooseUnitUmum ] = useState(false);
    // const [isResponse422, setIsResponse422 ] = useState(false);

   
    // function handleCreateQuickFill(e){

    //     e.preventDefault();
    //     const userToken = localStorage.getItem('userToken');

    //     const header =  {

    //         'Accept': "application/json",
    //         'Content-Type' : "application/json",
    //         'Authorization' : 'bearer ' + userToken
            
    //     };

    //     axios.defaults.headers.common = header;
    //     axios
    //         .post(URL_API + `/human-resource/structure-wizard/quick-fill/typical`)
    //         .then((response) => {
             
    /* 
    
        ``````````````````````````````
        Create Quick fill Nama Jabatan 
        
        ```````````````````````````````
        
    */   
    //             console.log("Response Original : ", response)
    //             setIsChooseUnitUmum(true);
    //         })
    //         .catch((error) => {
    //             console.log("Error : ", error.response)
    //             if(error.response.status == 422){
    //                 // setIsModalRadioButton(false);
    //                 setIsResponse422(true);

    //             };
    //         })
        
    // };

    // function closeModalResponse422(){

    //     setIsResponse422(false);
    //     setIsChooseUnitUmum(true)
    // };

    // if(isChooseUnitUmum == true){

    //     return (

    //         <Redirect to='/umum-data' />
    //     );
    // };


    return (
       
        <Grid  
            container
            spacing={0}
            direction="row"
            justify="flex-start"
            alignItems="center"
        >
                {
                    props.listNamaJabatan.length !==  [] ? props.listNamaJabatan.map((item, i) => (
        
                        <Card className={classes.cardNamaJabatan} key={i} >
                            <CardHeader
                                style={{marginBottom: 0, padding:0 }}
                                action= {
                                    <div>
                                        <IconButton
                                            aria-label="More"
                                            aria-owns={open ? 'long-menu' : undefined}
                                            aria-haspopup="true"
                                            onClick={(e) => handleMenu(e, {bind: item})}
                                            style={{marginRight: 7, marginTop: 7}}
                                        >   
                                            <MoreVertIcon />
                                        </IconButton>
                                        <Menu id="simple-menu" anchorEl={anchorEl} open={Boolean(anchorEl)} onClose={handleMenuClose}>
                                            <MenuItem onClick={() => handleMenuClose({bind: 'Ubah'})}>Ubah</MenuItem>
                                            <MenuItem onClick={() => handleMenuClose({bind: 'Hapus'})}>Hapus</MenuItem>
                                        </Menu>
                                    </div>            
                                }
                            />
                            <CardContent style={{ textAlign: 'center', marginTop: 0, paddingTop:0  }}>
                                <Typography variant='subtitle1' className={classes.title}>
                                    <b>{item.name}</b>
                                </Typography>
                            </CardContent>
                        </Card>

                    )) : null

                        // (
                        //     <Grid  
                        //         container
                        //         spacing={0}
                        //         direction="row"
                        //         justify="center"
                        //         alignItems="center"
                        //     >
                        //         <CircularProgress size={32} style={{marginTop: 24, color: 'red'}} />
                        //     </Grid>
                        // )
                }

                {
                    props.listNamaJabatan.length ==  0 ? (

                        <Grid  
                            container
                            spacing={0}
                            direction="row"
                            justify="center"
                            alignItems="center"
                        >
                            <Grid item sm={4}></Grid>

                            <Grid item sm={4} style={{textAlign: "center"}}>
                                <br />
                                <br />
                                    <img src={PictNamaJabatan} alt='Nama Jabatan' />
                                <br />
                                <br />
                                <Typography variant='subtitle1' className={classes.title}>
                                    <b>Tambah Nama jabatan</b>
                                </Typography>
                                <br />
                                <Typography variant='subtitle1' className={classes.title} style={{color: 'gray', textAlign: 'center'}}>
                                    Sebutkan apa saja nama jabatan yang ada pada perusahaan Anda
                                </Typography>
                                <br />
                                <br />
                                <ButtonCustom 
                                    title='Mulai' 
                                    handleClick={(e) => handleOpenModalMulai(e)} 
                                />  
                                <br />
                            </Grid>

                            <Grid item sm={4}></Grid>

                            {/* 

                                ```````````````````````````````````````````
                                Modal Response 422 Hasil Create Quick Fill 

                                ```````````````````````````````````````````
                                
                            */}

                            {/* <Modal show={isResponse422} onHide={closeModalResponse422} centered >
                                <div className='text-center' style={{padding:'30px'}}>
                                    <img src={PictInfo} className="App-icon-80" alt="info-icon" />                
                                    <br/>
                                    <br />
                                    <h6>Anda sudah pernah melakukan pengaturan Standard !</h6>
                                    <br />
                                    <ButtonCustom title='Ok, mengerti!' handleClick={(e) => closeModalResponse422(e)} />  
                                </div>
                            </Modal> */}

                        </Grid>
                        
                    ) : null
                }

            <ModalDeleteNamaJabatan 
                isOpenModalDelete = {isOpenModalDelete}
                setOpenModalDelete = {setOpenModalDelete}
                idNameJabatan = {idNameJabatan}
                nameJabatan = {nameJabatan}
                listNamaJabatan= {props.listNamaJabatan}
                setListNamaJabatan = {props.setListNamaJabatan}
                classes={props.classes}

            />

            <ModalUpdateNamaJabatan 
                    
                isOpenModalUpdate={isOpenModalUpdate}
                setOpenModalUpdate={setOpenModalUpdate}
                idNameJabatan = {idNameJabatan}
                nameJabatan = {nameJabatan}
                listNamaJabatan= {props.listNamaJabatan}
                setListNamaJabatan = {props.setListNamaJabatan}
                classes={props.classes}

            />

            <ModalInputNamaJabatan 
                setChooseButtonPlus={setChooseButtonPlus}
                isChooseButtonPlus={isChooseButtonPlus}
                listNamaJabatan={props.listNamaJabatan}
                setListNamaJabatan= {props.setListNamaJabatan}
                classes={props.classes}
            />

        </Grid>
    )

};

// export default withStyles(styles)(NamaJabatanList);
export default NamaJabatanList;

