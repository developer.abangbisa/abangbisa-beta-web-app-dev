import React,{useState, useEffect, useContext} from 'react';

import ContextDefineStrucutureDefault from '../../AccountManagement/Context/ContextDefineStructureDefault';    
// import ContexStoreTingkatJabatan from '../Context/ContextStoreTingkatJabatan';                             
import MoreVertIcon from '@material-ui/icons/MoreVert';
import {useGetHttp} from '../../AccountManagement/Hook/useGetHttp';
import {URL_API} from '../../../constants/config-api';

import { Typography } from '@material-ui/core';
import { Button} from 'react-bootstrap'; 

import ModalDeleteTingkatJabatan from '../../AccountManagement/ComponentViewDefineStructureDefault/ModalDeleteTingkatJabatan';
import ModalUpdateTingkatJabatan from '../../AccountManagement/ComponentViewDefineStructureDefault/ModalUpdateTingkatJabatan';

import {    
            Grid, 
            CardHeader, 
            withStyles, 
            CardContent, 
            CircularProgress, 
            Chip, 
            Paper, 
            Card, 
            CardActions,
            IconButton,
            Menu, 
            MenuItem,
            List,
            ListItem,
            ListItemText,
            ListItemAvatar,
            Avatar
        
        } from '@material-ui/core';

const styles = {

    card: {
        // minWidth: 100,
        width: 240,
        marginTop: 3,
        paddingTop: 0,
        paddingBottom: 0,
        marginLeft:7

    },

    cardContent: {
        textAlign: 'center',
        marginTop: 0,
        paddingTop:0, 
        paddingBottom: 0,
        paddingLeft: 0,
        paddingRight: 0 
    },

    root: {
       
    },

    textField: {
        marginLeft: 7,
        marginRight: 7,
        minWidth: 425   

    },
    actions: {
        display: 'flex',
        direction:'row-reverse'
    },

    fontListItemText : {
        fontFamily: 'Nunito',
        fontSize:'1rem',
        fontWeight: 'bold',
        lineHeight: '1.76',
        letterSpacing: 'normal'
    },
};

const TingkatJabatanList = props => {

    const { 
            classes, 
            // listTingkatJabatan,
            // setListTingkatJabatan
        } = props;

    const open = Boolean(anchorEl);
    const [ anchorEl, setAnchorEl] = useState(null);
  
    const [idTingkatJabatan, setTingkatJabatan ] = useState('');
    const [nameTingkatJabatan, setNameTingkatJabatan ] = useState('');

    const [isLoading_v3, dataRealTingkatJabatan] = useGetHttp(URL_API + '/human-resource/master-structure-position-level', []);
    // console.log("dataRealTingkatJabatan : ", dataRealTingkatJabatan)

    /* Delete */
    const [ isOpenModalDelete, setOpenModalDelete ] = useState(false);

    /* Update */
    const [ isOpenModalUpdate, setOpenModalUpdate ] = useState(false);

    useEffect(() => {
        
        if(dataRealTingkatJabatan !== null){
            const newData = [...dataRealTingkatJabatan.data.data]
            // console.log("newData : ", newData)
            props.setListTingkatJabatan(newData)
        }
    },[dataRealTingkatJabatan])
 
    function handleMenu(event, params, index) {

        // console.log("Params handle menu : ", params );
        
        setTingkatJabatan(params.bind.id);
        setNameTingkatJabatan(params.bind.name);
        
        setAnchorEl(event.currentTarget);

    };

    function handleMenuClose(params) {

        if(params.bind == "Ubah"){
        
            setOpenModalUpdate(true)
            setAnchorEl(null);

        } else if(params.bind == "Hapus"){
            setOpenModalDelete(true)
            setAnchorEl(null);                       

        } else {
            
            setAnchorEl(null);                        
        };

        setAnchorEl(null);

    };

    /*

        ````````````````
            LIST NAME   

        ````````````````

    */

    const [dense, setDense] = useState(false);

    return (

        <div>
            <br />
            <br />
            {
                props.listTingkatJabatan.length > 0 || props.listTingkatJabatan !== null ?  props.listTingkatJabatan.map((item, i) => (

                    <Card className={classes.card} key={i}>
                        <CardHeader
                            style={{marginBottom: 0, padding:0 }}
                            action= {
                                <div>
                                    <IconButton
                                        aria-label="More"
                                        aria-owns={open ? 'long-menu' : undefined}
                                        aria-haspopup="true"
                                        onClick={(e) => handleMenu(e, {bind: item}, i)}
                                        style={{marginRight: 7, marginTop: 7}}
                                    >   
                                        <MoreVertIcon />
                                    </IconButton>
                                    <Menu id="simple-menu" anchorEl={anchorEl} open={Boolean(anchorEl)} onClose={handleMenuClose}>
                                        <MenuItem onClick={() => handleMenuClose({bind: 'Ubah'})}>Ubah</MenuItem>
                                        <MenuItem onClick={() => handleMenuClose({bind: 'Hapus'})}>Hapus</MenuItem>
                                    </Menu>
                                </div>            
                            }

                        />
                        <CardContent className={classes.cardContent}>
                            <List dense={dense} style={{paddingTop:0, marginTop:0, paddingBottom: 0, marginLeft: 34}}>
                                <ListItem style={{paddingBottom: 0, paddingTop: 0}}>
                                    <ListItemAvatar>
                                        <Avatar style={{width: 30, height: 30, fontSize: '1rem'}}>
                                            {i+1}
                                        </Avatar>
                                    </ListItemAvatar>
                                    <ListItemText
                                        primary={<Typography className={classes.fontListItemText}>{item.name}</Typography>}  
                                    />
                                </ListItem>
                            </List>                       
                        </CardContent>
                    </Card>
                
                )) : null
                
                // (
                //         <Grid  
                //             container
                //             spacing={0}
                //             direction="row"
                //             justify="center"
                //             alignItems="center"
                //         >
                //             <CircularProgress size={32} style={{marginTop: 24, color: 'red'}} />

                //         </Grid>
                //     )

            }

            <ModalDeleteTingkatJabatan 
                isOpenModalDelete = {isOpenModalDelete}
                setOpenModalDelete = {setOpenModalDelete}
                idTingkatJabatan = { idTingkatJabatan}
                nameTingkatJabatan = { nameTingkatJabatan} 
                listTingkatJabatan= { props.listTingkatJabatan }
                setListTingkatJabatan = { props.setListTingkatJabatan}
            />  

            <ModalUpdateTingkatJabatan 
                isOpenModalUpdate={isOpenModalUpdate}
                setOpenModalUpdate={setOpenModalUpdate}
                idTingkatJabatan = { idTingkatJabatan}
                nameTingkatJabatan = { nameTingkatJabatan} 
                listTingkatJabatan={props.listTingkatJabatan}
                setListTingkatJabatan ={ props.setListTingkatJabatan}
                // currentData ={ currentData}
              
            />          
        </div>
    )


}


export default withStyles(styles)(TingkatJabatanList);