import React, { useState, useReducer, useEffect, useRef } from 'react';

import { URL_API } from '../../../../constants/config-api';
import { useGetHttp } from '../Hook/useGetHttp';
import { useGetHttp_formUnit } from '../Hook/useGetHttp_formUnit';

import { fromJS } from 'immutable';

import ContextFormUnit from './ContextFormUnit';

const ContextGlobalFormUnit = props => {
    

    /* 

        ```````````````````````````````````````````````
        * GET List Label Unit Type 

        * GET List Label Icon  Unit Type



        ```````````````````````````````````````````````
        
    */
    const [isLoading, fetchedData] = useGetHttp(URL_API + '/human-resource/master-structure-unit-type/create', []);

    let labelJenisUnit = {};
    let labelIconJenisUnit = [];

    const immutablelabelAndIconJenisUnit = fromJS(fetchedData)

    if(fetchedData !== null){
        
        if(immutablelabelAndIconJenisUnit.getIn(['data','data','fields']) !== undefined){

            const fetchData = fetchedData.data.data.fields
            labelJenisUnit = {...fetchData};

            fetchedData.data.data.iconCollections.map((data) => {
                labelIconJenisUnit.push(data)
            })
        
        };
    };

    console.log("labelJenisUnit : ", labelJenisUnit);
    console.log("labelIconJenisUnit : ", labelIconJenisUnit);


    /* 

        ```````````````````````````````````````````````
        GET STAMP ICON,


        ID id dapat dari response POST Jenis Unit !!!

        ```````````````````````````````````````````````
        
    */

    // const [isLoading, fetchedData] = useGetHttp(URL_API + '/account-management/master-structure-unit/create', []);

    const [idResponse200JenisUnit, setIdResponse200JenisUnit] = useState();

    function handleResponse200JenisUnit(data){

        console.log("Response 200 from Jenis Unit : ", data.id);
        setIdResponse200JenisUnit(data.id);
    };

    // const [loading, labelTimeStampIcon] = useGetHttp_formUnit(URL_API + `/human-resource/master-structure-unit-type/${idResponse200JenisUnit}`, [idResponse200JenisUnit]);

    // console.log("labelTimeStampIcon : ", labelTimeStampIcon);

    /*

        ````````````````````````
        
            NAMA UNIT/ COMPONENT

        ````````````````````````

    */
   
    const [loadingNamaComponent, labelNamaComponent] = useGetHttp_formUnit(URL_API + `/human-resource/master-structure-unit/create`, []);
    console.log("labelNamaComponent : ", labelNamaComponent)
   
    return (

        <ContextFormUnit.Provider

            value={{

                labelJenisUnit : labelJenisUnit, // ==> GANTI PAKAI OBJECT DESTRUCTION
                labelIconJenisUnit: labelIconJenisUnit,
                handleResponse200JenisUnit: handleResponse200JenisUnit,
                idResponse200JenisUnit: idResponse200JenisUnit,
                // labelTimeStampIcon: labelTimeStampIcon !== null ? labelTimeStampIcon.updated_at : "",
                labelStructureUnitId: labelNamaComponent !== null ? labelNamaComponent.fields.structure_unit_type_id.value: ''
            
            }}
        >
            {props.children}

        </ContextFormUnit.Provider>
    );
};

export default ContextGlobalFormUnit;