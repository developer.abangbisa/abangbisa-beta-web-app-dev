import React, { useState, useReducer, useEffect, useRef } from 'react';

import axios from 'axios';
// import { fromJS } from 'immutable'; 
// import { usePostHttp } from '../Hook/usePostHttp';
import { URL_API } from '../../../../constants/config-api';
import { useGetHttp } from '../Hook/useGetHttp';
import { fromJS } from 'immutable';
// import { shopReducer, ADD_PRODUCT, REMOVE_PRODUCT } from './reducers';
import ContextDefineStructureDefault from './ContextDefineStructureDefault';
// import { setContext } from '@redux-saga/core/effects';

const ContextGlobalDefineStructureDefault = props => {

    /*

        ``````````````
        NAMA JABATAN

        ``````````````

    */

    const [isReloadInTabNamaJabatan, setTabNamaJabatan ] = useState(false);
    const [isLoading_v1, fetchedData] = useGetHttp(URL_API + '/human-resource/master-structure-position-title', [isReloadInTabNamaJabatan]);
    const handleTabNamaJabatan = () => setTabNamaJabatan(true);

    /*

        ``````````````
        TINGKAT JABATAN

        ``````````````

    */

    // const [isLoading_v2, dataLabelTingkatJabatan] = useGetHttp(URL_API + '/account-management/master-structure-position-level/create', []);

    const [isReloadInTabTingkatJabatan, setTabTingkatJabatan ] = useState();
    // const [isLoading_v3, dataRealTingkatJabatan] = useGetHttp(URL_API + '/account-management/master-structure-position-level', [isReloadInTabTingkatJabatan]);

    const handleTabTingkatJabatan = () => setTabTingkatJabatan(true);

    /*

        ``````````````
        ANGGOTA

        ``````````````

    */

//    const [isLoading_v5, dataLabelAnggota] = useGetHttp(URL_API + '/account-management/member/create', []);
   
   const [isReloadInTabAnggota, setTabAnggota ] = useState(false);
//    const [isLoading_v4, dataRealAnggota] = useGetHttp(URL_API + '/account-management/member', [isReloadInTabAnggota]);

    const handleTabAnggota = () => setTabAnggota(true);

 /*

        ```````````````````````
        DATA UPDATE JENIS UNIT

        ``````````````````````

        *PARAMS

        let idJenisUnit = ''

        let data = {

            MasterStructureUnit : {
                name: '',
                icon_id: '',
                updated_at: ''

            }
        };

    */

    const [nameJabatan , setNameJabatan ] = useState();
    const [nameJabatanDefault , setNameJabatanDefault ] = useState("");

    const [listJabatanDefault, setListJabatanDefault] = useState();

    function updateJenisUnit(name){

        console.log("[Context] Name Jenis Unit   : ", name);  

    };

    const handleDefaultDataWantToUpdate = (data) => setNameJabatanDefault(data);
  
    const immutableData = fromJS(fetchedData);
    // const immutableDataRealAnggota = fromJS(dataRealAnggota)

    if(fetchedData != null ){

        if(immutableData.getIn(['status']) == 200){
            if(immutableData.getIn(['data','data'])){
                // if(immutableDataRealAnggota.getIn(['data','data'])){

                    return (
    
                        <ContextDefineStructureDefault.Provider
    
                            value={{
    
                                // listNamaJabatan: fetchedData.data.data,
                                isReloadInTabNamaJabatan: isReloadInTabNamaJabatan,
                                isReloadInTabTingkatJabatan: isReloadInTabTingkatJabatan,
                                isReloadInTabAnggota: isReloadInTabAnggota,
                                handleTabNamaJabatan: handleTabNamaJabatan,
                                handleTabTingkatJabatan: handleTabTingkatJabatan,
                                handleTabAnggota: handleTabAnggota,
                                
                                // labelTingkatJabatan: dataLabelTingkatJabatan !== null ? dataLabelTingkatJabatan.data.data.fields : null,
                                // listTingkatJabatan: dataRealTingkatJabatan !== null ? dataRealTingkatJabatan.data.data : null,
    
                                // listAnggota: dataRealAnggota !== null ? dataRealAnggota.data.data : null,
                                // labelAnggota: dataLabelAnggota !== null ? dataLabelAnggota.data.data : null,

                                updateJenisUnit: updateJenisUnit,
                                handleDefaultDataWantToUpdate: handleDefaultDataWantToUpdate,
                                nameJabatan: nameJabatan,
                                // nameJabatanDefault: nameJabatanDefault,
                                // listJabatanDefault: parentDataTemplate
                           
                            }}
                        >
                            {props.children}
                
                        </ContextDefineStructureDefault.Provider>
                    );
                    
                // }

              
            };
        };
    };

   
    return (

        <ContextDefineStructureDefault.Provider
            value={{

                listNamaJabatan: [],
                labelTingkatJabatan:{},
                // listTingkatJabatan: [],

                listNamaJabatan: [],
                isReloadInTabNamaJabatan: [],
                isReloadInTabTingkatJabatan: [],
                isReloadInTabAnggota: [],
                handleTabNamaJabatan: [],
                handleTabTingkatJabatan: [],
                handleTabAnggota: [],
                

                listAnggota: [],
                labelAnggota: [],
                updateJenisUnit: updateJenisUnit,
                handleDefaultDataWantToUpdate: handleDefaultDataWantToUpdate,
                nameJabatan: nameJabatan,
                // nameJabatanDefault: nameJabatanDefault,
                // listJabatanDefault: undefined
            
            }}
        >
            {props.children}

        </ContextDefineStructureDefault.Provider>
    );
 
};

export default ContextGlobalDefineStructureDefault;