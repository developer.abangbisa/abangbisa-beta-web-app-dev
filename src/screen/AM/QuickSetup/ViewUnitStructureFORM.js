/* 


    ``````````````````````````````````````````````````````````````````````

    File ini merepresentasikan dari "FORM 'STEPPER'" untuk user saat user 
    hendak isi FORM !

    ``````````````````````````````````````````````````````````````````````

*/

import React, { Component, useEffect, useState, useContext } from 'react';
import { 
    Container, Paper, Typography, Grid, TextField, Checkbox, CircularProgress,
    FormControlLabel, Box, Button, MenuItem, Dialog, DialogTitle, DialogContent, 
    DialogContentText, DialogActions, Breadcrumbs, Link,CardHeader, Card, CardContent, Chip, Avatar, IconButton

} from '@material-ui/core';
import { Tab, Tabs, TabList, TabPanel } from 'react-tabs';
import { withStyles, createMuiTheme, MuiThemeProvider} from '@material-ui/core/styles';
// import styled from 'styled-components';
import  StepperComponent  from './ComponentViewUnitStructurFORM/StepperComponent';
import MoreVertIcon from '@material-ui/icons/MoreVert';

const theme = createMuiTheme({
    
    palette: {

        primary: {
            main: '#cc0707', //#cc0707, #c62828
            light: '#ff5f52',
            dark: '#8e0000',
            contrastText: '#ffffff'
        }
    }
});

const styles = theme => ({

    card: {
      minWidth: 275,
      backgroundColor: 'transparent',
      marginTop: 34
    },
    cardNewData: {
        minWidth: 300,
        // backgroundColor: 'transparent',
        backgroundColor: 'white',
        // marginTop: 34
        marginLeft: 27
    },
    bullet: {
      display: 'inline-block',
      margin: '0 2px',
      transform: 'scale(0.8)',
    },
    title: {
      fontFamily: 'Nunito'
    },
    pos: {
      marginBottom: 12,
    },
    help: {
        color: '#97a2a5'
    },
    chip: {
        margin: 7 / 2,
        fontFamily: 'Nunito'
    },

    /*
        ````````````````````````
        STEPPER COMPONENT STYLE

        ````````````````````````
    */
    root: {
        width: '90%',
    },
    button:{
        // width: '503px',
        // height: '42px',
        borderRadius: 5,
        background: 'linear-gradient(1deg, #c1272d, #c1272d 30%, #d1354a 67%, #f28181)',
        // backgroundColor: 'cyan',
        border:0,
        fontFamily:'Nunito',
        textTransform: 'capitalize',
        color: 'white',
        marginTop:theme.spacing(1),
        marginLeft:theme.spacing(1),
        marginRight:theme.spacing(9),
        // marginTop: 30,
        // marginRight: 40,
    },
    buttonOutlined: {
        borderRadius: 5,
        // background: 'linear-gradient(1deg, #c1272d, #c1272d 30%, #d1354a 67%, #f28181)',
        // backgroundColor: 'cyan',
        border:0,
        fontFamily:'Nunito',
        textTransform: 'capitalize',
        color: 'white',
        borderColor: 'transperent',
        borderWidth: 0
        // marginTop:theme.spacing(4),
        // marginRight:theme.spacing(8),
        // marginTop: 30,
        // marginRight: 40,
    },
    actionsContainer: {
        marginBottom: 20 * 2,
    },

    resetContainer: {
        padding: 30 * 3,
        background: '#eaf3f5'
        
    },
});

const ViewUnitStructureFORM = props => {
    
    const { classes } = props;
    const [tabIndex, setTabIndex] = useState(0);

    const [isAnyJenisUnit, setAnyJenisUnit ] = useState('');
    const [isAnyArrayComponent, setArrayComponent] = useState([]);

    function handleTab(tabIndex){

        setTabIndex(tabIndex)
    };

    return (
        //   <div style={{backgroundColor:'#eaf3f5'}}>
        <MuiThemeProvider theme={theme}>
            <Tabs selectedIndex={tabIndex} onSelect={tabIndex => handleTab(tabIndex)}>
                <TabList>
                    <Tab>
                        <Typography variant='subtitle1' className={classes.title}>
                            <b>Unit</b>
                        </Typography>
                    </Tab>
                </TabList>
                <TabPanel style={{ backgroundColor: '#eaf3f5'}}>
                    <br />
                    <Grid  
                        container
                        spacing={24}
                        direction="row"
                        justify="center"
                        
                    >
                        <Grid item sm={3}>

                            <br />

                            {
                                isAnyJenisUnit !== '' ? (

                                    <Card className={classes.cardNewData}>
                                        <CardHeader 
                                            style={{textAlign: 'center', backgroundColor: '#F8BB14', fontFamily: 'Nunito'}}
                                            title={
                                                <Typography variant='subtitle1' className={classes.title} style={{textAlign: 'left', marginLeft: 8}}>  
                                                    <b>{isAnyJenisUnit}</b>
                                                </Typography>
                                            }
                                        />
                                        <CardContent style={{ textAlign: 'left' }}>
                                            {
                                                isAnyArrayComponent.length > 0 ? isAnyArrayComponent.map((item, index) => (

                                                    <div key={index}>
                                                        <Chip
                                                            avatar={<Avatar>{index + 1}</Avatar>}
                                                            label={item}
                                                            className={classes.chip}
                                                        />
                                                        {/* <hr /> */}
                                                    </div>
                                                )) : null
                                            }
                                        </CardContent>
                                    </Card>   
                                    
                                ) : null
                            }

                        </Grid>

                        <Grid
                            item 
                            sm={6}
                        >
                            <StepperComponent 
                                classes={classes}
                                isAnyJenisUnit={isAnyJenisUnit}
                                setAnyJenisUnit={setAnyJenisUnit}

                                isAnyArrayComponent={isAnyArrayComponent}
                                setArrayComponent = { setArrayComponent}
                            />
                        </Grid>
                        <Grid item sm={3}></Grid>
                    </Grid>
                    <br />
                    <br />
                    <br />
                    <br />
                    <br />
                    <br />
                    <br />
                    <br />
                </TabPanel>
            </Tabs>
        </MuiThemeProvider>
    )
};



export default withStyles(styles)(ViewUnitStructureFORM)
