import React, { Component, useEffect, useState } from "react";
import axios from 'axios';
import { Grid, TextField, Button, CircularProgress, Typography} from '@material-ui/core';
import { withStyles, MuiThemeProvider, createMuiTheme } from '@material-ui/core/styles';
import ImageForgetPassword from '../../../assets/images/Mask_Group_7.png';
// import { green, purple } from "@material-ui/core/colors";
// import Redirect from "react-router/Redirect";

import Redirect from '../../../utilities/Redirect';
import { ToLogin, ToSOMaster } from '../../../constants/config-redirect-url';
import { URL_API } from '../../../constants/config-api';
import { red } from "@material-ui/core/colors";
import ImageOKEBerhasil from '../../../assets/images/Group_24.png';


const theme = createMuiTheme({

    palette: {
        primary: {
            main: '#cc0707', //#cc0707, #c62828
            light: '#ff5f52',
            dark: '#8e0000',
            contrastText: '#ffffff'
        }
    }
    
})

const styles = theme => ({

    /*
        `````````````````````````````````````````````````````````````````````````
        If you want styling Text Field without 'Theme Pallet', using below code : 

        `````````````````````````````````````````````````````````````````````````

    */

    // cssLabel: {
    //     '&$cssFocused': {
    //       color: purple[500],
    //     },
    // },
    // cssFocused: {},
    // cssUnderline: {

    //     '&:after': {
    //         borderBottomColor: purple[500],
    //     },
    // },
    // cssOutlinedInput: {
    //     '&$cssFocused $notchedOutline': {
    //       borderColor: purple[500],
    //     },
    // },

    button: {
        width: '400px',
        height: '42px',
        borderRadius: 5,
        background: 'linear-gradient(1deg, #c1272d, #c1272d 30%, #d1354a 67%, #f28181)',
        // backgroundColor: 'cyan',
        border:0,
        fontFamily:'Nunito',
        textTransform: 'capitalize'

    },
    buttonProgress: {
        // color: red[500],
        color: 'white',
        position: 'absolute',
        top: '50%',
        left: '50%',
        marginTop: -12,
        marginLeft: -12,
      },
      title: {
          fontFamily: 'Nunito'
      }
});


const ViewCompletionCompanyStructureFormulirKosong = props => {

    const { classes } = props;

    /*

        ``````````````````` 
        ONCHANGE EMAIL

        ```````````````````
    */
    const [namajabatanTertinggi, setNamajabatanTertinggi ] = useState('');

    const handleChangeNamaJabatanTertinggi = (e) => {

        setNamajabatanTertinggi(e.target.value);

    };
    

    /*

        ``````````````````` 
        POST SUBMIT EMAIL

        ```````````````````
    */

    const [ userTokenState, setUserTokenState ] = useState('')

    const [sentEmailSuccess, setSentEmailSuccess] = useState(false);
    const [loading, setLoading] = useState(false);


    // const handleTrueEmailSuccess = () => setSentEmailSuccess(true);
    // setLoading(false);
    const userToken = localStorage.getItem('userToken');

    const handleJabatanTertinggi = () => {



        setLoading(true);

        const data = {

            StructurePosition : {

                highest_position_name: namajabatanTertinggi
            }
        };

        if(userToken !== undefined){

            const header =  {       
                'Accept': "application/json",
                'Content-Type' : "application/json",
                'Authorization' : "bearer " + userToken,
            };
        
            axios.defaults.headers.common = header;    
    
    
            axios
                .post(URL_API + `/human-resource/structure-position/root`, data)
                .then((response) => {
                    
                    console.log("Response Original : ", response)
                    
                    setLoading(false);
                    Redirect(ToSOMaster);
                    
                })
                .catch((error) => {

                    setLoading(false)
    
                    alert('Whoops, something went wrong !')
                    console.log("Error : ", error.response)
                   
                })
        
        } else {

            alert('Whoops, something went wrong !');
        }

    };


    return (
        
        <MuiThemeProvider theme ={theme}>

            <Grid  
                container
                spacing={8}
                direction="row"
                justify="center"
                alignItems="center"
            >
                <Grid item sm={12} style={{textAlign:'center'}}>
                    <br />
                    <br />
                    {/* <img src={ImageForgetPassword} alt='Image Forget Password' /> */}
                    <br />
                    <br />
                    <br />
                    <br />
                    <Typography variant='h6' className={classes.title}>
                        <b>Apa nama jabatan tertinggi dalam organisasi Anda ? </b>
                    </Typography>
                    <br />
                    <br />
                    <TextField
                        // className={classes.margin}
                        // label="Email"
                        // className={classes.width}
                        placeholder='Ketik di sini...'
                        variant="outlined"
                        id="custom-css-outlined-input"
                        style={{width: 400}}
                        onChange={handleChangeNamaJabatanTertinggi}
                        value={ namajabatanTertinggi }

                        InputLabelProps={{
                            classes: {
                                root: classes.cssLabel,
                                focused: classes.cssFocused,
                            },
                        }}
                        InputProps={{
                            classes: {
                                root: classes.cssOutlinedInput,
                                // root: classes.width,
                                focused: classes.cssFocused,
                                notchedOutline: classes.notchedOutline,
                            },
                        }}
                     
                        
                    />
                            
                    

                    <br />
                    <br />
                    
                    {
                        namajabatanTertinggi !== '' ? (

                            <Button                                 
                                variant="contained" 
                                color="primary" 
                                className={classes.button} 
                                onClick ={ handleJabatanTertinggi}
                                // onClick={() => handleResetPassword(namajabatanTertinggi)}
                                // disabled
                            >
                                {loading !== true ? "Lanjut" : null }
                                {loading && <CircularProgress size={24} className={classes.buttonProgress} />}  
                                
                            </Button>
                            
                        ) : null
                    }
                    


                    <br />
                    <br />
                    {/* <Typography variant='h6' className={classes.title}
                        onClick={redirectToLogin}                     
                        style={{color: '#cc0707', cursor: 'pointer' }}
                    >
                            <b>Kembali ke halaman masuk</b>    
                    </Typography> */}
                </Grid>
            </Grid>
        </MuiThemeProvider>
    )
};


export default withStyles(styles)(ViewCompletionCompanyStructureFormulirKosong);