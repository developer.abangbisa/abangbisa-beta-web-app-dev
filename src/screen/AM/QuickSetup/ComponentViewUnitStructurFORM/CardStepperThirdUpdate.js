
import React, {useState, useEffect, useContext, useCallback } from 'react';
import axios from 'axios';
import { withStyles } from '@material-ui/core/styles';
import Card from '@material-ui/core/Card';
import CardContent from '@material-ui/core/CardContent';
import TextField from '@material-ui/core/TextField';
import { Grid } from '@material-ui/core';
import HelpUploadIcon from '../../../assets/images/Group_1096.png';
import UnitIcon_754 from '../../../assets/images/Group_754.png';
import UnitIcon_755 from '../../../assets/images/Group_755.png';
import UnitIcon_756 from '../../../assets/images/Group_756.png';
import UnitIcon_757 from '../../../assets/images/Group_757.png';

import IconButton from '@material-ui/core/IconButton';

// import ContextFormUnit from '../../AccountManagement/Context/ContextFormUnit';

import styled from 'styled-components';
import { URL_API} from '../../../constants/config-api';
const { red, blue, green, grey } = require('@material-ui/core/colors');

const styles = {
    card: {
        minWidth: 425,
        marginRight:32
    },
    cardHelp: {
        minWidth: 275,
        backgroundColor: 'transparent',
        // marginTop: 34
    },
    textField: {
        marginLeft: 7,
        marginRight: 7,
        minWidth: 425   

      },
      help: {
        color: '#97a2a5'
    },
    icon: {
        margin: 2 * 2,
      },
    iconHover: {
        margin: 2 * 2,

        color: red[500],
        '&:hover': {
            color: red[800],
        },
    },
  };

const CardStepperThirdUpdate = (props) => {

    const { classes } = props;
    const [isOpen, setOpen] = useState(false);

    // const context = useContext(ContextFormUnit)
    // console.log("Context from Stepper Third : ", context.idResponse200JenisUnit);

    function handleOpen(e){

        e.preventDefault();
        setOpen(true);

    };

    function handleClose(){
        
        setOpen(false);

    };


    const handleChooseImage = (params) => {

        // const data = {

        //     MasterStructureUnit: {
        //         icon_id: params.bind.id,
        //         updated_at: context.labelTimeStampIcon
        //     },
        //     _method: 'patch'

        // };

        // console.log("Data : ", data)

        // const userToken = localStorage.getItem('userToken');
  
        // if(userToken !== undefined && data !== null && data !== undefined){
            
        //     const header =  {       
        //         'Accept': "application/json",
        //         'Content-Type' : "application/json",
        //         'Authorization' : "bearer " + userToken,
                
        //     };
          
        //     axios.defaults.headers.common = header;    

        //     axios
        //         .post(URL_API + `/account-management/master-structure-unit/${context.idResponse200JenisUnit}`, data)
        //         .then(function(response){

        //             // closeModalAnggota();
        //             console.log("Response Original : ", response)

        //             if(response.status == 200 ){
        //                 if(response.data.data !== undefined){
        //                     // context.handleTabAnggota();
        //                     // context.handleResponse200JenisUnit(response.data.data)
        //                 }
        //             };
        //         })
        //         .catch(function(error){
                    
        //             console.log("Error : ", error.response)
                    
        //         })

        // } else { console.log("No Access Token available!")};



    };

    
    return(
        <Grid container wrap="nowrap">
            <Card className={classes.card}>
                <CardContent>
                    {/*
                        context.labelIconJenisUnit.length !== [] ? context.labelIconJenisUnit.map((item, i) => {
                            
                            return (
                                <IconButton
                                    onClick ={() => handleChooseImage({bind: item})} 
                                    color="secondary" 
                                    className={classes.iconButton} 
                                    aria-label="Icon" 
                                    key={i}
                                    datakey={item}
                                >
                               
                                
                                    <img 
                                        src={URL_API + "/" +item.url} 
                                        style={{width:54}} 
                                        alt={item.name}
                                        />
                                   
                                </IconButton>
                            )
                        }) : null
                    */}
                    
                </CardContent>
            </Card> 

            <Card className={classes.cardHelp} nowrap="true">
                <CardContent style={{ textAlign: 'center' }}>
                    <h6 className={classes.help}>Pilih icon yang tepat untuk unit yang ingin anda tambahkan</h6>
                    <br />
                    <img src={HelpUploadIcon}  alt="info-icon" />  
                    <br />
                    <br />
                    <h6>
                        <b>Butuh bantuan memilih icon ?</b>
                    </h6> 

                    <br />
                    <h6 className={classes.help}>
                        Dengan menekan tombol dibawah, saya mengizinkan untuk melengkapi form dengan data yang telah disediakan
                    </h6>

                    <br />
                    <PilihIcon /> 
                </CardContent>
            </Card>        
        </Grid>
    )
};

export default withStyles(styles)(CardStepperThirdUpdate);


const Wrapper = styled.button`

    width: 236.5px;
    height: 49px;
    opacity: 0.17;
    border-radius: 5px;
    background-color: #84b7fc;
    border-color: 1px solid cyan;
    cursor: pointer;
    font-family: Nunito;
    font-size: 18px;
    font-weight: 600;
    font-style: normal;
    font-stretch: normal;
    line-height: 2.06;
    letter-spacing: normal;    
    
`;

const PilihIcon = () => (
    
    <Wrapper>
        Pilih Icon untuk saya
    </Wrapper>
);

