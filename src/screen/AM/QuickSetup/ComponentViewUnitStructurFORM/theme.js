import { createMuiTheme } from "@material-ui/core/styles";
import { cyan, lightBlue, lightGreen, grey, red} from "@material-ui/core/colors";
// import grey from '@material-ui/core/colors/grey';
export default createMuiTheme({
  palette: {
    primary: red, //linear-gradient(1deg, #c1272d, #c1272d 30%, #d1354a 67%, #f28181)
    secondary: grey
  },
  overrides: {
    // MuiButton: {
    //   root: {
    //     marginTop: 30,
    //     marginRight: 40,
    //     width: '122.2px',
    //     height: '40px',
    //     borderRadius: 5,
    //     background: 'linear-gradient(1deg, #c1272d, #c1272d 30%, #d1354a 67%, #f28181)',
    //     border:0,
    //     textTransform: 'capitalize',
    //     color: 'white'
    //   },
    //   text: {
    //     color: 'white'
    //   },
    //   textSecondary: {
    //     color: 'white',
    //     fontFamily: 'Nunito'
    //   },
    // },
    MuiStepper:{
      root:{
        backgroundColor: '#eaf3f5'
      }
    },
    MuiStepContent:{
        root:{
          backgroundColor: '#eaf3f5'
        }
    },
    MuiStepIcon: {
      root: {
          color: 'grey', // or 'rgba(0, 0, 0, 1)'
          '&$active': {
              color: '#c2322d',
          },
          '&$completed': {
              color: 'green',
          },
      },
  } 
  }
});