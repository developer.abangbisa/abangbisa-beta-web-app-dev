
import React, {useState, useEffect, useContext, useCallback} from 'react';
import axios from 'axios';
import { withStyles } from '@material-ui/core/styles';
import Card from '@material-ui/core/Card';
import CardContent from '@material-ui/core/CardContent';
import TextField from '@material-ui/core/TextField';
import { Grid } from '@material-ui/core';
import Snackbar from '@material-ui/core/Snackbar';
import IconButton from '@material-ui/core/IconButton';
import Avatar from '@material-ui/core/Avatar';
import Chip from '@material-ui/core/Chip';
import CloseIcon from '@material-ui/icons/Close';
import { amber, green } from '@material-ui/core/colors';
import { URL_API} from '../../../constants/config-api';
import { useDebounce, useDebouncedCallback } from "use-debounce";
import ContextFormUnit from '../../AccountManagement/Context/ContextFormUnit';

import HelpUploadIcon from '../../../assets/images/Group_1096.png';
import styled from 'styled-components';

const styles = {
    card: {
      minWidth: 425,
      height: 100
    },
    cardHelp: {
        minWidth: 275,
        backgroundColor: 'transparent',
        // marginTop: 34
        marginLeft: 17
    },
    textField: {
        marginLeft: 7,
        marginRight: 7,
        minWidth: 380   

    },
    close: {
        // padding: theme.spacing.unit / 2,
        padding: 7 / 2,
    },
    warning: {
        backgroundColor: amber[700],
      },

    success: {
        backgroundColor: green[500],
    },
    help: {
        color: '#97a2a5'
    },
    chip: {
        margin: 3 / 2,
    },
  };




const CardStepperFirst = (props) => {
    
    const context = useContext(ContextFormUnit);
    // console.log("Context : ", context);

    console.log("Props : ", props)

    const { classes } = props;

    const [isHideTextHelpJenisUnit, setHideTextHelpJenisUnit ] = useState(false);
    const [debouncedFunction, cancel] = useDebouncedCallback(
        // to memoize debouncedFunction we use useCallback hook.
        // In this case all linters work correctly
        useCallback(value => {

            // console.log("Value : ", value)
            const stringDataLength = value.length;
            // console.log("stringDataLength : ", stringDataLength);

            if(stringDataLength > 0){
                setHideTextHelpJenisUnit(true);
            };

            let data = {

                MasterStructureUnitType: {
                    
                    name: value,
                    icon_id: '',
                    prev_id: context.labelJenisUnit !== undefined ? context.labelJenisUnit.prev_id.value : "null",
                    next_id : context.labelJenisUnit !== undefined ? context.labelJenisUnit.next_id.value : "null"
                
                }
            };

            // console.log("Data MasterStructure Unit : ", data)

            const userToken = localStorage.getItem('userToken');
            
            if(userToken !== undefined && data !== null && data !== undefined){
            
                const header =  {       
                    'Accept': "application/json",
                    'Content-Type' : "application/json",
                    'Authorization' : "bearer " + userToken,
        
                };

                axios.defaults.headers.common = header;    

                axios
                    .post(URL_API + '/human-resource/master-structure-unit-type', data)
                    .then(function(response){

                        console.log("Response Original : ", response)

                        if(response.status == 200 ){
                            if(response.data.data !== undefined){
                                context.handleResponse200JenisUnit(response.data.data)
                                setOpenResponse200(true)
                                setContentResponse200("Jenis unit berhasil di tambahkan :)")

                                // props.getStepContent(1);
                            }
                        };
                    })
                    .catch(function(error){
                        
                        console.log("Error : ", error.response)
                        if(error.response.data !== undefined){
                            if(error.response.data.info !== undefined){
                                if(error.response.data.info.status == 400){
                                    if(error.response.data.info.errors !== undefined){
    
                                        setOpenResponse400(true);
                                        setContentErrorResponse400(error.response.data.info.errors.name);
                                    }
                                };

                                if(error.response.data.info.status == 500){
                                    setOpenResponse500(true);
                                    setContentErrorResponse500("Coba lagi !");
                                };
                            }
                        }
                        
                    })

            } else { console.log("No Access Token available!")};

        }, [context.labelJenisUnit]), 1500, // DEBOUNCE

        // The maximum time func is allowed to be delayed before it's invoked:
        { maxWait: 3000 }
      );

        
    /*

        ````````````````````````````
        Error Response 400 Snackbar

        ````````````````````````````

    */
    const [ isOpenErrorResponse400, setOpenResponse400] = useState(false);
    const [ contentErrorResponse400, setContentErrorResponse400] = useState()
    
    function handleSnackbar() {
        setOpenResponse400(true);
      }
    
      function handleCloseSnackbar(event, reason) {
        if (reason === 'clickaway') {
          return;
        }

        setOpenResponse400(false);
      }

    /*

        ````````````````````````````
        Error Response 500 Snackbar

        ````````````````````````````

    */

   const [ isOpenErrorResponse500, setOpenResponse500] = useState(false);
   const [ contentErrorResponse500, setContentErrorResponse500] = useState()
   
   function handleSnackbar500() {
    setOpenResponse500(true);
  }

  function handleCloseSnackbar500(event, reason) {
    if (reason === 'clickaway') {
      return;
    }

    setOpenResponse500(false);
  }

  /*

        ````````````````````````````
        Response 200 OKE Snackbar

        ````````````````````````````

    */

   const [ isOpenResponse200, setOpenResponse200] = useState(false);
   const [ contentResponse200, setContentResponse200] = useState()
   
   function handleSnackbar200() {
    setOpenResponse200(true);
  }

  function handleCloseSnackbar200(event, reason) {
    if (reason === 'clickaway') {
      return;
    }

    setOpenResponse200(false);
    
  };

    return (
        <Grid container wrap="nowrap">
            <Card className={classes.card}>
                <CardContent>
                    {/* <h1>Bismillah</h1> */}
                    <TextField
                        id="outlined-bare"
                        className={classes.textField}
                        variant="outlined"
                        defaultValue={""}    
                        onChange={e => debouncedFunction(e.target.value)}                    
                    />

                </CardContent>
            </Card> 

            {/* HELP CARD CONTENT */}

            {
                isHideTextHelpJenisUnit !== true ? (

                    <Card className={classes.cardHelp} nowrap="true">
                        <CardContent style={{ textAlign: 'center' }}>
                            <h6 className={classes.help}>Ketik Jenis Unit struktur dalam organisasi Anda, contoh :</h6>
                            <Chip
                                avatar={<Avatar>1</Avatar>}
                                // icon={icon}
                                label="Direksi"
                                // onDelete={handleDeleteComponent}
                                className={classes.chip}
                            />
                            <br />
                            <br />
                            <Chip
                                avatar={<Avatar>2</Avatar>}
                                // icon={icon}
                                label="Divisi"
                                // onDelete={handleDeleteComponent}
                                className={classes.chip}
                            />
                            <hr />
                            <img src={HelpUploadIcon}  alt="info-icon" />  
                            <br />
                            <br />
                            <h6>
                                <b>Butuh bantuan mengisi form ?</b>
                            </h6> 

                            <br />
                            <h6 className={classes.help}>
                                Dengan menekan tombol dibawah, saya mengizinkan untuk melengkapi form dengan data yang telah disediakan
                            </h6>

                            <br />
                            <PilihIcon /> 
                        </CardContent>
                    </Card>        
                ) : null
            }
            

            {/* RESPONSE 400 */}
            <Snackbar

                anchorOrigin={{
                    vertical: 'bottom',
                    horizontal: 'left',
                }}
                open={isOpenErrorResponse400}
                autoHideDuration={6000}
                onClose={handleCloseSnackbar}
                ContentProps={{
                    'aria-describedby': 'message-id',
                    classes: {
                        root: classes.warning
                    }

                }}
                message={<span id="message-id">{contentErrorResponse400}</span>}
                action={[
                    <IconButton
                        key="close"
                        aria-label="Close"
                        color="inherit"
                        className={classes.close}
                        onClick={handleCloseSnackbar}
                    >
                        <CloseIcon />
                    </IconButton>,
                ]}
            />

            {/*  RESPONSE 500 */}
            <Snackbar

                anchorOrigin={{
                    vertical: 'bottom',
                    horizontal: 'left',
                }}
                open={isOpenErrorResponse500}
                autoHideDuration={6000}
                onClose={handleCloseSnackbar500}
                ContentProps={{
                    'aria-describedby': 'message-id',
                    classes: {
                        root: classes.warning
                    }

                }}
                message={<span id="message-id">{contentErrorResponse500}</span>}
                action={[
                    <IconButton
                        key="close"
                        aria-label="Close"
                        color="inherit"
                        className={classes.close}
                        onClick={handleCloseSnackbar500}
                    >
                        <CloseIcon />
                    </IconButton>,
                ]}
            />
            
             {/*  RESPONSE 200 */}
            <Snackbar

                anchorOrigin={{
                    vertical: 'bottom',
                    horizontal: 'left',
                }}
                open={isOpenResponse200}
                autoHideDuration={6000}
                onClose={handleCloseSnackbar200}
                ContentProps={{
                    'aria-describedby': 'message-id',
                    classes: {
                        root: classes.success
                    }

                }}
                message={<span id="message-id">{contentResponse200}</span>}
                action={[
                    <IconButton
                        key="close"
                        aria-label="Close"
                        color="inherit"
                        className={classes.close}
                        onClick={handleCloseSnackbar200}
                    >
                        <i className="material-icons">
                                done
                        </i>
                    </IconButton>,
                ]}
            />

        </Grid>
    )
};

export default withStyles(styles)(CardStepperFirst);

const Wrapper = styled.button`

    width: 236.5px;
    height: 49px;
    opacity: 0.17;
    border-radius: 5px;
    background-color: #84b7fc;
    border-color: 1px solid cyan;
    cursor: pointer;
    font-family: Nunito;
    font-size: 18px;
    font-weight: 600;
    font-style: normal;
    font-stretch: normal;
    line-height: 2.06;
    letter-spacing: normal;    
    
`;

const PilihIcon = () => (
    
    <Wrapper>
        Isikan otomatis Jenis Unit
    </Wrapper>
);
