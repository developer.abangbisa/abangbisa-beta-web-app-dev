
import React, {useState, useEffect, useContext} from 'react';
import axios from 'axios';
import { withStyles } from '@material-ui/core/styles';
import Card from '@material-ui/core/Card';
import CardContent from '@material-ui/core/CardContent';
import TextField from '@material-ui/core/TextField';
import Grid from '@material-ui/core/Grid';
// import ShareIcon from '@material-ui/icons/Share';
import {red, grey, green} from '@material-ui/core/colors';
import Icon from '@material-ui/core/Icon';

import InputLabel from '@material-ui/core/InputLabel';
import Paper from '@material-ui/core/Paper';
import InputBase from '@material-ui/core/InputBase';
import Divider from '@material-ui/core/Divider';
import IconButton from '@material-ui/core/IconButton';
import ContextFormUnit from '../../AccountManagement/Context/ContextFormUnit';
import { URL_API} from '../../../constants/config-api';


const styles = {
    card: {
      minWidth: 425,
    },
    textField: {
        marginLeft: 7,
        marginRight: 7,
        minWidth: 425   

      },
    icon : {
        top: 0  
    },
      root: {
        padding: '2px 4px',
        display: 'flex',
        alignItems: 'center',
        width: 400,
      },
      input: {
        marginLeft: 8,
        flex: 1,
      },
      iconButton: {
        padding: 10,
      },
      divider: {
        width: 1,
        height: 28,
        margin: 4,
      },
      icon: {
        margin: 2 * 2,
      },
      iconHover: {
        margin: 2 * 2,
        color: red[800],
        '&:hover': {
          color: grey[500],
          
        },
      },
      iconHoverAdd: {
        margin: 2 * 2,
        color: green[800],
        '&:hover': {
          color: grey[800],
        },
      },
  };

const CardStepperSecondUpdate = (props) => {

    const { classes } = props;
    const context = useContext(ContextFormUnit);
    console.log("Context : ", context);
    

    const [todoValue, setTodoValue] = useState('');
    const [todos, setTodo] = useState([])

    const handleChange = (e) => setTodoValue(e.target.value);

    const handleSubmit = e => {

      e.preventDefault();    

      const todo = {
  
        value: todoValue,
        done: false      
      };
      
      if(!todoValue) return;
      
        setTodo([...todos, todo]);
        document.getElementById('todoValue').value = '';

        // context
        const data = {
          MasterStructureComponent: {
	
            name: todoValue,
            structure_unit_id: context.idResponse200JenisUnit
          
          }
        };

        console.log("Data from CardStepperSecondUpdate : ", data);

        const userToken = localStorage.getItem('userToken');
            
        if(userToken !== undefined && data !== null && data !== undefined){
        
            const header =  {       
                'Accept': "application/json",
                'Content-Type' : "application/json",
                'Authorization' : "bearer " + userToken,
    
            };

            axios.defaults.headers.common = header;    

            axios
                .post(URL_API + '/account-management/master-structure-component', data)
                .then(function(response){

                    // closeModalAnggota();
                    console.log("Response Original : ", response)

                    if(response.status == 200 ){
                        if(response.data.data !== undefined){
                            // context.handleTabAnggota();
                            // context.handleResponse200JenisUnit(response.data.data)
                        }
                    };
                })
                .catch(function(error){
                    
                    console.log("Error : ", error.response)
                    
                })

        } else { console.log("No Access Token available!")};

    };

    const handleDelete = e => {

      const { id } = e.target.parentElement;
  
      todos.splice(id, 1)
  
      setTodo([...todos]);
    }
  
    const handleDone = e => {
  
      const { id } = e.target.parentElement;
      todos[id].done = !todos[id].done
      setTodo([...todos])
    }
    
  
    return (
    
                <Card className={classes.card}>
                    <CardContent>

                         {
                            todos && todos.map((todo, i) => (
                              <div key={todo.value} id={i}>
                              <Paper className={classes.root} elevation={1} >
                              
                                  <InputBase 
                                    disabled
                                    className={classes.input} 
                                    // placeholder={todo.value}
                                    defaultValue={todo.value}
                                    onChange={handleDone}
                                  />
                                  <Divider className={classes.divider} />

                                  <IconButton 
                                    onClick={handleDelete}
                                    color="secondary" 
                                    className={classes.iconButton} 
                                    aria-label="Remove"
                                  >
                                    <Icon className={classes.iconHover} color="secondary" style={{ fontSize: 27 }}>
                                      remove_circle
                                    </Icon>
                                  </IconButton>

                                  {/* <button className={todo.done ? 'done' : 'not-done'} onClick={handleDone}>{todo.value}</button> */}
                                  {/* <button className="delete-todo" onClick={handleDelete} >x</button>               */}
                              </Paper>
                              <br/>
                              </div>
                            ))
                        }

                        
                        <Paper className={classes.root} elevation={1}>
                            <InputBase 
                              className={classes.input} 
                              placeholder="" 
                              id="todoValue" 
                              onChange={handleChange}
                            />
                            <Divider className={classes.divider} />
                            <IconButton 
                              onClick={handleSubmit}
                              color="primary" 
                              className={classes.iconButton} 
                              aria-label="Adding"
                              
                            >
                                <Icon className={classes.iconHoverAdd} color="secondary" style={{ fontSize: 27 }}>
                                    add_circle
                                </Icon>
                            </IconButton>
                        </Paper>

                    </CardContent>
                </Card> 
    )
};


/* 
    BACKUP

*/

// return (
    
//   <Card className={classes.card}>
//       <CardContent>
//           <Paper className={classes.root} elevation={1}>
//               <InputBase className={classes.input} placeholder="" />
//               <Divider className={classes.divider} />
//               <IconButton color="secondary" className={classes.iconButton} aria-label="Remove">
//                   <Icon className={classes.iconHover} color="secondary" style={{ fontSize: 27 }}>
//                       remove_circle
//                   </Icon>
//               </IconButton>
//               <IconButton color="primary" className={classes.iconButton} aria-label="Adding">
//                   {/* <DirectionsIcon /> */}
//                   <Icon className={classes.iconHoverAdd} color="secondary" style={{ fontSize: 27 }}>
//                       add_circle
//                   </Icon>
//               </IconButton>
//           </Paper>
//           <br />

//           <Paper className={classes.root} elevation={1}>
//               <InputBase className={classes.input} placeholder="" />
//               <Divider className={classes.divider} />
//               <IconButton color="secondary" className={classes.iconButton} aria-label="Remove">
//                   <Icon className={classes.iconHover} color="secondary" style={{ fontSize: 25 }}>
//                       remove_circle
//                   </Icon>
//               </IconButton>
//               <IconButton color="primary" className={classes.iconButton} aria-label="Remove">
//                   {/* 
//                       ```````
//                       SPACER
//                       ```````
//                   */}
//               </IconButton>
//               <IconButton color="primary" className={classes.iconButton} aria-label="Remove">
//                    {/* 
//                       ```````
//                       SPACER
//                       ```````
//                   */}
//               </IconButton>
//               <IconButton color="primary" className={classes.iconButton} aria-label="Remove">
//                    {/* 
//                       ```````
//                       SPACER
//                       ```````
//                   */}
//               </IconButton>
             
//           </Paper>

//           <App />

          
         

          
//       </CardContent>
//   </Card> 
// )


export default withStyles(styles)(CardStepperSecondUpdate);


