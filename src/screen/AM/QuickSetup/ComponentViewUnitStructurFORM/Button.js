import React from 'react';
import { makeStyles } from '@material-ui/styles';
import ReactDOM from "react-dom";
import MuiThemeProvider from "@material-ui/core/styles/MuiThemeProvider";
import Card from '@material-ui/core/Card';
import CardContent from '@material-ui/core/CardContent';
import CardActions from '@material-ui/core/CardActions';
import Button from '@material-ui/core/Button';
import Typography from '@material-ui/core/Typography';
import theme from './theme';

const useStyles = makeStyles({

  root: {
    background: 'linear-gradient(45deg, #FE6B8B 30%, #FF8E53 90%)',
    border: 0,
    borderRadius: 3,
    boxShadow: '0 3px 5px 2px rgba(255, 105, 135, .3)',
    color: 'white',
    height: 48,
    width: 100,
    padding: '0 30px',

  },
  chip: {
    margin:5,
  },
});

export default function Hook() {

  const classes = useStyles();
  return (
      <div>

        <Button className={classes.root}>Hook</Button>

        <MuiThemeProvider theme={theme}>
            <Button variant="outlined">Button styled with MUI theme only</Button>
            <Card>
                <CardContent>
                    <Typography className={classes.title} color="textSecondary" gutterBottom>
                    Word of the Day
                    </Typography>
                </CardContent>
            </Card>
        </MuiThemeProvider>
      </div>

  ) 
  
}