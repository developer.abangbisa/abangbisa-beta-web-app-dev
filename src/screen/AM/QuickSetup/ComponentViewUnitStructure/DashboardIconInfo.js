import React,{ Component } from 'react';
import styled from 'styled-components';
import IconInfo from '../../../assets/images/Group_1097.png';


const Img = styled.img`

    width: 22px;
    height: 22px;
    cursor: pointer;

`;

const DashboardIconInfo = () => (

   
        <span > 
            <Img src={IconInfo} />
        </span>
);


export default DashboardIconInfo;