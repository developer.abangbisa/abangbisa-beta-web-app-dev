import React,{ Component } from 'react';
import styled from 'styled-components';


const Wrapper = styled.button`
    
    border-radius: 18px;
    background-color: #cccccc;
    margin: 0 0.5em 0 0;
    padding: 0.25em 1em;
    border: 1px solid white;


`;

const White = styled.span`
    color: white;
`;

const ButtonRadiusJabatan = ({titleJabatan}) => (

    <Wrapper type="button"><White>{titleJabatan}</White></Wrapper>
);

export default ButtonRadiusJabatan;

