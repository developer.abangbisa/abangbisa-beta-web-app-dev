import React,{ Component } from 'react';
import styled from 'styled-components';
import ObengPicture from '../../../../assets/images/Group_1215.png';

const Img = styled.img`

    width: 60.1px;
    height: 60.1px;
    top:0;
    position:absolute;
    margin-left: 10px;

`;

const ImageMenyesuaikan = () => (

    <Img src={ObengPicture} />
);

export default ImageMenyesuaikan;