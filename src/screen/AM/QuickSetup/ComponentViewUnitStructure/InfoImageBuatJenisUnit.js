import React,{ Component } from 'react';
import styled from 'styled-components';
import ImageBuatJenisUnit from '../../../assets/images/Group_1122.png';


const Img = styled.img`

    width: 245px;
    height: 245px;
    
`;
    
// background-color: #f2f2f2;

const InfoImageBuatJenisUnit = () => (

    <span> 
        <Img src={ImageBuatJenisUnit} />
    </span>
);


export default InfoImageBuatJenisUnit;