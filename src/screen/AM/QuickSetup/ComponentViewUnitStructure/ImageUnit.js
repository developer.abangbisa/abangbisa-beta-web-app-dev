import React,{ Component } from 'react';
import styled from 'styled-components';
import BuildingPicture from '../../../assets/images/Group_755.png';

const Img = styled.img`

    width: 64px;
    height: 54px;
    position:relative;
    margin-left: 10px;
    
    
`;
    // top:0;

const ImageUnit = () => (

    <Img src={BuildingPicture} />
);

export default ImageUnit;