import React,{ Component } from 'react';
import styled from 'styled-components';
import GearPicture from '../../../../assets/images/Group_1214.png';

const Img = styled.img`

    width: 60.1px;
    height: 60.1px;
    top:0;
    position:absolute;
    margin-left: 10px;
    
    
`;

const ImageUmum = () => (

    <Img src={GearPicture} />
);

export default ImageUmum;