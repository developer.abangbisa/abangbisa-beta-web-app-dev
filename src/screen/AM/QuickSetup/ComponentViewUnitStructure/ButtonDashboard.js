import React,{ Component } from 'react';
import styled from 'styled-components';
// import IconArrowRight from '../assets/images/Group_1222.png';
import IconArrowRight from '../../../assets/images/Group_1222.png';


const Wrapper = styled.button`
    
    border-radius: 22px;
    background-image: linear-gradient(2deg, #c1272d, #c1272d 30%, #d1354a 67%, #f28181);
    margin: 0 1em;
    padding: 0.25em 1em;
    border: 1px solid palevioletred;
    cursor: pointer;

`;

const White = styled.span`
    color: white;
`;

const Img = styled.img`

    margin-left: 14px;
`;

const ButtonDashboard = ({handleClick}) => (
    <Wrapper type="button" onClick={handleClick}>
        <White> Dashboard 
            <Img src={IconArrowRight}   />
        </White>
    </Wrapper>    
)
// class ButtonDashboard extends Component {
//     render(){

//         return (
            
//             <Wrapper type="button"><White> Dashboard <Img src={IconArrowRight} onClick={handleClick}  /></White></Wrapper>

//         )
//     };
// };

export default ButtonDashboard;

