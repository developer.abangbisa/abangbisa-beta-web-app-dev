import React,{Component, useState, useEffect, useContext } from 'react';
import { 
    Grid, CardHeader, withStyles, CardContent, Card, CircularProgress, Typography, Button, Dialog, DialogTitle, DialogContent, DialogContentText, DialogActions, 
    List, ListItem, ListItemIcon, ListItemText, Radio
} from '@material-ui/core';
import axios from 'axios';
import { URL_API } from '../../../../constants/config-api';

import ButtonCustom from '../../../../components/Button';
// import Redirect from 'react-router/Redirect';

import { Container, Row, Col,     Modal, ModalFooter} from 'react-bootstrap';
// import ImageUmum from '../ComponentViewUnitStructure/ImageUmum';
// import ImageMenyesuaikan from '../../AccountManagement/ComponentViewUnitStructure/ImageMenyesuaikan';
// import ImageMenyesuaikan from '../ComponentViewUnitStructure/ImageMenyesuaikan';
import GearPicture from '../../../../assets/images/Group_1214.png';
import ObengPicture from '../../../../assets/images/Group_1215.png';
import PictInfo from '../../../../assets/images/icon-info-24px.svg';

import Redirect from '../../../../utilities/Redirect';
import { ToFormUnit} from '../../../../constants/config-redirect-url';


const ModalComponentPictDefaultUnitType = props => {

    const { classes } = props;

    const [isModalRadioButton, setIsModalRadioButton ] = useState(false);
    const [ isChooseUnitMenyesuaikan, setIsChooseUnitMenyesuaikan] = useState(false);
    const [selectedOption, setSelectedOption ] = useState("");

    const [isResponse422, setIsResponse422 ] = useState(false);

    function openModalRadioButton (){

        setIsModalRadioButton(true)
    };

    function closeModalRadioButton (){
        
        setIsModalRadioButton(false)
    };

    function closeModalResponse422(){

        setIsResponse422(false);
    };

    function handleRadioChange(e){
        // console.log("Data : ", e.target.value);

        setSelectedOption(e.target.value)
        
    };

    function chooseOption (e) {
        e.preventDefault();
        const userToken = localStorage.getItem('userToken');
        console.log("selectedOption : ", selectedOption)

        if(selectedOption == "umum"){
            console.log("Choosed : ", selectedOption)

            const header =  {
    
                'Accept': "application/json",
                'Content-Type' : "application/json",
                'Authorization' : 'bearer ' + userToken
             
            };
    
            axios.defaults.headers.common = header;
            axios
                // .post(URL_API + `/human-resource/structure-wizard/quick-fill/typical`)
                .post(URL_API + `/human-resource/structure-wizard/quick-fill/master-structure-unit/typical`)
                .then((response) => {
                    
                    console.log("Response Original : ", response)
                    // setIsChooseUnitUmum(true);
                    window.location.reload();
                })
                .catch((error) => {
                    console.log("Error : ", error.response)
                    if(error.response.status == 422){
                        setIsModalRadioButton(false);
                        setIsResponse422(true);

                    };
                })
        };

        if(selectedOption == "menyesuaikan"){
            console.log("Choosed : ", selectedOption)

            setIsChooseUnitMenyesuaikan(true);
        };
    };
        
    if(isChooseUnitMenyesuaikan == true){

        Redirect(ToFormUnit)
        // return (
            // <Redirect to='/form-unit' />
            // <h1>Form Unit</h1>
        // );
    };

    return (

        <div >
             <Row className="justify-content-md-center">
                <ButtonCustom title="Mulai" handleClick={() => openModalRadioButton()} style={{marginTop: 44}} />
            </Row>

            {/* <Modal show={isModalRadioButton} onHide={closeModalRadioButton}  centered>
                <Modal.Header closeButton>
                    <Modal.Title id="contained-modal-title-vcenter">
                    </Modal.Title>
                </Modal.Header> 
                <Modal.Body>
                    <Container>
                        <form>
                            <Row style={{marginLeft: 120, position:'absolute'}}>
                                <h5 style={{marginTop: 7}}><b>Umum</b></h5>
                            </Row>
                            <Row style={{marginLeft: 120, position:'absolute'}}>
                                <p style={{marginTop: 32}}>Atur dengan pengaturan standar kami !</p>
                            </Row>
                            <div className="form-check">
                                <label>
                                    <input
                                        type="radio"
                                        name="react-tips"
                                        value="umum"
                                        onChange={handleRadioChange}
                                        className="form-check-input"
                                    />
                                    <ImageUmum />
                                </label>
                            </div>

                            <Row style={{marginLeft: 120, position:'absolute'}}>
                                <h5 style={{marginTop: 72}}><b>Menyesuaikan</b></h5>
                            </Row>

                            <Row style={{marginLeft: 120, position:'absolute'}}>
                                <p style={{marginTop: 102}}>Susun pengaturan struktur Anda sendiri !</p>
                            </Row>

                            <div className="form-check" style={{marginTop: 72, marginBottom:72}}>
                                <label>
                                    <input
                                        type="radio"
                                        name="react-tips"
                                        value="menyesuaikan"
                                        onChange={handleRadioChange}
                                        className="form-check-input"
                                    />
                                    
                                    <ImageMenyesuaikan />
                                </label>
                            </div>
                        </form>
                    </Container>
                </Modal.Body>

                <Modal.Footer>
                    <Row style={{marginRight: 3}}>
                        <ButtonCustom title='Lanjut' handleClick={(e) => chooseOption(e)} />  
                    </Row>
                </Modal.Footer>
            </Modal>   */}

            <Dialog
                open={isModalRadioButton}
                onClose={closeModalRadioButton}
                aria-labelledby="alert-dialog-title"
                aria-describedby="alert-dialog-description"            
            >
                <DialogTitle id="alert-dialog-title" style={{textAlign: "left"}}>
                    <Typography variant='h6' className={classes.title}>
                    
                    </Typography>
                    {/* <hr/> */}
                </DialogTitle>
                <DialogContent style={{textAlign: "center"}}>

                    <List>
                        <ListItem 
                            role={undefined} 
                            dense 
                            button 
                            // onClick={handleToggle(value)}
                        >
                            <ListItemIcon>
                                <Radio
                                    checked={selectedOption === 'umum'}
                                    value="umum"
                                    onChange={handleRadioChange}
                                    name="radio-button-demo"
                                    inputProps={{ 'aria-label': 'Umum' }}
                                />    
                            </ListItemIcon>
                            <img src={GearPicture} alt="Gear Picture" />
                            <ListItemText 
                                // inset 
                                className={classes.listItemText}
                                primary={
                                    <Typography variant='h6' className={classes.title}>
                                        <b>Umum</b>
                                    </Typography>
                                } 
                                secondary={
                                    <Typography variant='subtitle1' className={classes.title}>
                                        Atur dengan pengaturan standar kami !
                                    </Typography>
                                } 
                            />
                        </ListItem>
                        
                        <br />
                        <ListItem 
                            role={undefined} 
                            dense 
                            button 
                            // onClick={handleToggle(value)}
                        >
                            <ListItemIcon>
                                <Radio
                                    checked={selectedOption === 'menyesuaikan'}
                                    value="menyesuaikan"
                                    onChange={handleRadioChange}
                                    name="radio-button-demo"
                                    inputProps={{ 'aria-label': 'Menyesuaikan' }}
                                />    
                            </ListItemIcon>
                            <img src={ObengPicture} alt="Obeng Picture" />
                            <ListItemText 
                                // inset 
                                className={classes.listItemText}
                                primary={
                                    <Typography variant='h6' className={classes.title}>
                                        <b>Menyesuaikan</b>
                                    </Typography>
                                } 
                                secondary={
                                    <Typography variant='subtitle1' className={classes.title}>
                                        Susun pengaturan struktur Anda sendiri  !
                                    </Typography>
                                } 
                            />
                        </ListItem>
                    </List>

                
                    <DialogContentText id="alert-dialog-description">
                        <Typography variant='subtitle1' className={classes.titleModal}>
                            
                        </Typography>
                    </DialogContentText>
                </DialogContent>
                <DialogActions style={{alignItems: "right", justifyContent:'flex-end'}}>
                    <Button 
                        onClick={(e) => chooseOption(e)}
                        variant='contained' 
                        className={classes.buttonModalPictOptions}
                        // fullWidth
                    >  
                        Lanjut
                    </Button>
                </DialogActions>
                <br />
            </Dialog>
            
            <Modal show={isResponse422} onHide={closeModalResponse422} centered >
                <div className='text-center' style={{padding:'30px'}}>
                    <img src={PictInfo} className="App-icon-80" alt="info-icon" />                
                    <br/>
                    <br />
                    <h6>Anda sudah pernah melakukan pengaturan Standard !</h6>
                    <br />
                    <ButtonCustom title='Ok, mengerti!' handleClick={() => window.location.reload()} />  
                </div>
            </Modal>
        </div>
    )
};

export default ModalComponentPictDefaultUnitType;