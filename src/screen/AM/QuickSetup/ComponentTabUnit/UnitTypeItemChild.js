import React,{useState, useEffect, useContext} from 'react';
import axios from 'axios';
import { Grid, CardHeader, withStyles, CardContent, Card, CircularProgress, CardActions, Chip } from '@material-ui/core';

import { URL_API } from '../../../../constants/config-api';

const styles = {
    
    card: {
        width: 742,
        marginTop: 3,
        marginLeft:7
    },
    root: {

        display: 'flex',
        justifyContent: 'center',
        flexWrap: 'wrap',
        padding: 1 / 2,
    },

    chip: {
        margin: 3 / 2,

    }
};

const UnitTypeItemChild = props => {

    const { classes } = props;

    const [listChildArray, setListChildArray] = useState([]);

    useEffect(() => {
        
        // console.log("childCompoentn : ", props.childComponent)
        setListChildArray(props.childComponent)
               
    },[])


    function handleDeleteComponent(e, item, index) {

        e.preventDefault();

        // console.log("List Child Array: ", listChildArray);
        // console.log("Item deleted : ", item.bind.id);
        
        const resultFilter = listChildArray.filter(detail => detail.id !== item.bind.id);
        // console.log("resultFilter : ", resultFilter);
        setListChildArray(resultFilter);

           const userToken = localStorage.getItem('userToken');
        
           if(userToken !== undefined && item.bind.id !== undefined){
        
               const header =  {       
                   'Accept': "application/json",
                   'Content-Type' : "application/json",
                   'Authorization' : "bearer " + userToken,
    
               };

               axios.defaults.headers.common = header;    

               axios 
                   .delete(URL_API + `/human-resource/master-structure-unit/${item.bind.id}`)
                   .then(function(response){

                        console.log("Response Original : ", response)
                      


                   })
                   .catch(function(error){
                    
                       console.log("Error : ", error.response)
                
                   })

           } else { console.log("No Access Token available!")};
            
    };
    
    let icon = null;

    return (

        <div>
            {
                listChildArray.length > 0 ? listChildArray.map((childAgain, i) => {

                    return (

                         <Chip
                            key={i}
                            icon={icon}
                            label={childAgain.name}
                            onDelete={(e) => handleDeleteComponent(e, {bind: childAgain}, i)}
                            className={classes.chip}
                            style={{fontFamily: 'Nunito'}}

                        />
                    )
                }) : null
            }   
        </div>
        
    )
};

export default withStyles(styles)(UnitTypeItemChild);

/* v1 
function getUnique(arr, comp) {

    const unique = arr
         .map(e => e[comp])
  
       // store the keys of the unique objects
      .map((e, i, final) => final.indexOf(e) === i && i)
  
      // eliminate the dead keys & store unique objects
      .filter(e => arr[e]).map(e => arr[e]);
  
     return unique;
  }

*/


/* v2
function merge_array(array1, array2) {
    var result_array = [];
    var arr = array1.concat(array2);
    var len = arr.length;
    var assoc = {};

    while(len--) {
        var item = arr[len];

        if(!assoc[item]) 
        { 
            result_array.unshift(item);
            assoc[item] = true;
        }
    }

    return result_array;
}

 */

/* v3
Array.prototype.unique = function() {
    var a = this.concat();
    for(var i=0; i<a.length; ++i) {
        for(var j=i+1; j<a.length; ++j) {
            if(a[i] === a[j])
                a.splice(j--, 1);
        }
    }

    return a;
};

*/