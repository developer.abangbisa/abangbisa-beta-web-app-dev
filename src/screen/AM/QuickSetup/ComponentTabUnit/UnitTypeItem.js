import React,{useState, useEffect, useContext} from 'react';
import { DragDropContext, Droppable, Draggable } from "react-beautiful-dnd";
import { Grid, CardHeader, withStyles, CardContent, Card, CircularProgress, CardActions, Chip, Typography } from '@material-ui/core';
import Menu from '@material-ui/core/Menu';
import MenuItem from '@material-ui/core/MenuItem';
import MoreVertIcon from '@material-ui/icons/MoreVert';
import IconButton from '@material-ui/core/IconButton';

import Number from '../ComponentViewUnitStructure/Number';
import Title from '../ComponentViewUnitStructure/Title';
import { URL_API } from '../../../../constants/config-api';
import ModalDeleteUnit from '../ComponentsModal/ModalDeleteUnit';

// import ModalUpdateUnit from '../../AccountManagement/ComponentViewUmumData/ModalUpdateUnit';
import ModalUpdateUnit from '../ComponentsModal/ModalUpdateUnit';
import UnitTypeItemChild from './UnitTypeItemChild';

const styles = {
    
    // card: {
    //     width: 742,
    //     marginTop: 3,
    //     marginLeft:7
    // },
    // root: {

    //     display: 'flex',
    //     justifyContent: 'center',
    //     flexWrap: 'wrap',
    //     padding: 1 / 2,
    // },

    // chip: {
    //     margin: 3 / 2,

    // },
    
    // /* 
    //     ````````````````````````
    //     FIGURE BOX 
        
    //     ````````````````````````
    // */
    // figureBoxPictMahkota: {

    //     width: 63,
    //     height: 63,
    //     borderStyle: 'solid',
    //     borderWidth: 1,
    //     borderRadius: 4,
    //     borderColor:'#f67070',
    //     display: 'inline-block',
    //     margin: 5,
    //     textAlign: 'center',
    //     cursor: 'pointer'
    // },
    // buttonModal: {
    //     // height: '42px',
    //     borderRadius: 5,
    //     background: 'linear-gradient(1deg, #c1272d, #c1272d 30%, #d1354a 67%, #f28181)',
    //     // backgroundColor: 'cyan',
    //     border:0,
    //     fontFamily:'Nunito',
    //     // marginLeft:theme.spacing(1),
    //     color: 'white',
    //     textTransform: 'capitalize',
    //     // textAlign: 'center'
        
    // },
};

const UnitTypeItem = ({ item, index, props }) => {

    const { classes } = props;
    const [ anchorEl, setAnchorEl] = useState(null);
    const open = Boolean(anchorEl);
    const [ idUnit, setIdUnit ] = useState();
    const [ nameUnit, setNameUnit ] = useState();

    const [prevId, setPrevId ] = useState();
    const [nextId, setNextId ] = useState();
    const [updatedAt, setUpdatedAt ] = useState();   

    const [ listChildUnit, setListChildUnit ] = useState();
    const [ isOpenModalUpdate, setOpenModalUpdate ] = useState(false);
    const [ isOpenModalDelete, setOpenModalDelete ] = useState(false);
    
    function handleMenu(event, params) {

        // console.log("Params Umum Data : ", params);
 
        setIdUnit(params.bind.id);
        setNameUnit(params.bind.name); 
        setListChildUnit(params.bind.embedded.masterStructureUnit);
        setAnchorEl(event.currentTarget);
 
        setPrevId(params.bind.prev_id);
        setNextId(params.bind.next_id)
        setUpdatedAt(params.bind.updated_at);
        
    };

    function handleMenuClose(params) {
  
        if(params.bind == "Ubah"){
            
            setOpenModalUpdate(true);
            setAnchorEl(null);
 
        } else if(params.bind == "Hapus"){
 
          
             setOpenModalDelete(true)
             setAnchorEl(null);
 
        } else if(params.bind == "component"){
 
 
        } else {
            
            setAnchorEl(null);
        };
    };

    function handlItemDetail(e, item){

        e.preventDefault();
        console.log("Item Detail : ", item);

    };

    function handleDeleteComponent(e, item, index) {

        e.preventDefault();
        console.log("Item deleted : ", item.bind.id);


    /* SEMENTARA */
    //    const userToken = localStorage.getItem('userToken');
        
    //    if(userToken !== undefined && item.bind.id !== undefined){
       
    //        const header =  {       
    //            'Accept': "application/json",
    //            'Content-Type' : "application/json",
    //            'Authorization' : "bearer " + userToken,
   
    //        };

    //        axios.defaults.headers.common = header;    

    //        axios 
    //            .delete(URL_API + `/human-resource/master-structure-unit/${item.bind.id}`)
    //            .then(function(response){

    //                 console.log("Response Original : ", response)
    //                 window.location.reload();

    //            })
    //            .catch(function(error){
                   
    //                console.log("Error : ", error.response)
              
    //            })

    //    } else { console.log("No Access Token available!")};
        
    };



    return (
        <Draggable draggableId={item.id} index={index} key={index} >

            { 
                provided => (
                
                    <div
                        
                        style={{padding:0, margin:0}}
                        ref={provided.innerRef}
                        {...provided.draggableProps}
                        {...provided.dragHandleProps}
                    >
                         <Card className={classes.card} key={index}>
                            <CardHeader 
                                style={{ paddingTop:0, paddingBottom:0}}
                                action={
                                    <div>
                                        <IconButton
                                            style={{marginBottom:'-30px', paddingBottom:0, marginTop:12}}
                                            aria-label="More"
                                            aria-owns={open ? 'long-menu' : undefined}
                                            aria-haspopup="true"
                                            onClick={(e) => handleMenu(e, {bind: item})}
                                        >   
                                            <MoreVertIcon />
                                        </IconButton>
                                        <Menu id="simple-menu" anchorEl={anchorEl} open={Boolean(anchorEl)} onClose={handleMenuClose}>
                                            <MenuItem onClick={() => handleMenuClose({bind: 'Ubah'})}>Ubah Unit</MenuItem>
                                            <MenuItem onClick={() => handleMenuClose({bind: 'Hapus'})}>Hapus Unit</MenuItem>                                            
                                        </Menu>
                                    </div>
                                }
                            />

                            <CardContent style={{ marginTop: 0, paddingTop:0, paddingBottom:0, marginBottom:0  }}>

                            <span>
                                <Number 
                                    onClick={(e) => handlItemDetail(e, {bind: item})}
                                    index={index + 1}
                                />
                                <IconButton>
                                    <img src={URL_API + '/' + item.self.rel.icon} style={{height: 32  }}/>
                                </IconButton>
                                    &nbsp;&nbsp;
                                <b>{item.name}</b>
                            </span>
                            </CardContent>
                    
                            <CardActions style={{marginLeft:100, paddingTop:0, marginTop:0}}>

                                {

                                    item.embedded.masterStructureUnit.length != [] ? item.embedded.masterStructureUnit.map((wew, index) => {
                                        
                                        let copyChildComponent = [];
                                        copyChildComponent.push(wew);

                                        return (

                                            <UnitTypeItemChild child={wew} index={index} childComponent={copyChildComponent} key={index} />
                                        );

                                        // return (
                                        //         <Chip
                                        //             key={index}
                                        //             icon={icon}
                                        //             label={wew.name}
                                        //             onDelete={(e) => handleDeleteComponent(e, {bind: wew}, index)}
                                        //             className={classes.chip}

                                        //         />
                                        // )

                                    }) : null

                                }

                            </CardActions>
                        
                        </Card>

                        <ModalDeleteUnit 
                            classes={classes}
                            isOpenModalDelete={isOpenModalDelete}
                            setOpenModalDelete={setOpenModalDelete}
                            idUnit={idUnit}
                            nameUnit={nameUnit}
                        />
                        
                        <ModalUpdateUnit 
                            isOpenModalUpdate = {isOpenModalUpdate}
                            setOpenModalUpdate = {setOpenModalUpdate}
                            nameUnit={nameUnit}
                            listChildUnit={listChildUnit}
                            setListChildUnit={setListChildUnit}
                            idUnit={idUnit}
                            prevId={prevId}
                            nextId={nextId}
                            updatedAt={updatedAt}

                        />
                    </div>
                )
            }

        </Draggable>
        
    )
}

export default withStyles(styles)(UnitTypeItem);

// function filteredArray(arr, elem) {

//     console.log("Elem : ", elem)

//     return arr.filter(function(item){
//           // keep items that does not include elem

//           console.log("Item from filter : ", item.embedded.masterStructureUnit)
//           return !item.embedded.masterStructureUnit.includes(elem.id);
//       });
// };