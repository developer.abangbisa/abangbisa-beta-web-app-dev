import React,{useState, useEffect, useContext} from 'react';
import { Container, Paper, Typography, Grid, TextField, Checkbox, FormControlLabel, Box, Button, MenuItem, Dialog, DialogTitle, DialogContent, DialogContentText, DialogActions} from '@material-ui/core';
import Redirect from '../../../../utilities/Redirect';
import { ToStructureOrganization, ToFormUnit } from '../../../../constants/config-redirect-url';

const AddUnitType = props => {

    const { classes } = props;

    const [isRedirectToUnitStructureMenyesuaikan, setRedirectToUnitStructureMenyesuaikan] = useState(false);

    function handleOpenModalUnitStructureUmum(){

        setRedirectToUnitStructureMenyesuaikan(true);
        
    };

    if(isRedirectToUnitStructureMenyesuaikan !== false){
        
        Redirect(ToFormUnit);
    };

    return (

        <Grid  
            container
            spacing={8}
            direction="row"
            justify="flex-end"
            // alignItems="center"
        >
           
            <Grid item sm={8} style={{textAlign: 'right'}}></Grid>
            <Grid item sm={2} style={{textAlign: 'right'}}>
                <Button 
                    variant='contained'            
                    className={classes.buttonPlusAddAnggota}        
                    // style={{ marginBottom:7}}
                    onClick={() => handleOpenModalUnitStructureUmum()}
                >
                    <span style={{padding: 5, fontSize:'17px'}}>
                        +
                    </span>
                </Button>

            </Grid>

            <Grid item sm={2} style={{textAlign: 'right'}}></Grid>
        </Grid>
    )
};

export default AddUnitType;