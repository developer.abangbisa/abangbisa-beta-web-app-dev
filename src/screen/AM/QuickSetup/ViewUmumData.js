import React, { useCallback, useEffect, useState } from "react";
import { makeStyles, createMuiTheme, withStyles, MuiThemeProvider } from '@material-ui/core/styles';
import { 
    Container, Paper, Typography, Grid, TextField, Checkbox, CircularProgress,
    FormControlLabel, Box, Button, MenuItem, Dialog, DialogTitle, DialogContent, 
    DialogContentText, DialogActions, Breadcrumbs, Link

} from '@material-ui/core';

import { Tab, Tabs, TabList, TabPanel } from 'react-tabs';
import { DragDropContext, Droppable, Draggable } from "react-beautiful-dnd";
import { amber, green } from '@material-ui/core/colors';
import styled from 'styled-components';

// /* v2 Unit Type */
import ImagePart1 from '../../../assets/images/Group_1121.png';
import ImagePart2 from '../../../assets/images/Group_1122.png';
import ImagePart3 from '../../../assets/images/Group_1118.png';
import ModalComponentPictDefaultUnitType from './ComponentTabUnit/ModalComponentPictDefaultUnitType';

// /* v2 Tingkat Jabatan */
import TingkatJabatanList from './ComponentViewUmumData/TingkatJabatanList';
import AddTingkatJabatan from './ComponentViewUmumData/AddTingkatJabatan';
import PictTingkatJabatan from '../../../assets/images/Group_1218.png';

import ModalComponentTingkatJabatan from './ComponentViewUmumData/ModalComponentTingkatJabatan';

// /* v2 Nama Jabatan */
import NamaJabatanList from './ComponentViewUmumData/NamaJabatanList';
import AddNamaJabatan from './ComponentViewUmumData/AddNamaJabatan';

// /* v2 Anggota */
// // import AddAnggota from '../AccountManagement-v2/ComponentViewUmumData/AddAnggota';
import AnggotaList from './ComponentViewUmumData/AnggotaList';
import AddAnggota from './ComponentViewUmumData/AddAnggota';

// /* v2 Unit */
import UnitTypeList from './ComponentTabUnit/UnitTypeList';
import AddUnitType from './ComponentTabUnit/AddUnitType';

import './styles/view-define-structure-default.css';

import Redirect from '../../../utilities/Redirect';
import { ToDashboard} from '../../../constants/config-redirect-url';

const theme = createMuiTheme({
    
    palette: {

        primary: {
            main: '#cc0707', //#cc0707, #c62828
            light: '#ff5f52',
            dark: '#8e0000',
            contrastText: '#ffffff'
        }
    }, 
    overrides : {
        // MuiListItemText: {
        //     fontWeight: 'bold'
        // }
    } 
});

const styles = theme => ({

    root: {
        
        // padding: theme.spacing(5, 2),
        // marginTop: theme.spacing(4),
        // width: 575,
        borderRadius: 2

    },

    button: {

        borderRadius: 5,
        background: 'linear-gradient(1deg, #c1272d, #c1272d 30%, #d1354a 67%, #f28181)',
        // backgroundColor: 'cyan',
        border:0,
        fontFamily:'Nunito',
        marginLeft:theme.spacing(1),
        textTransform: 'capitalize',
        color: 'white'
    },
    breadcrumb: {
        marginLeft: theme.spacing(5)
    },
    title: {
        fontFamily: 'Nunito'
    },
    titleModal: {
        fontFamily: 'Nunito',
        color: 'black'
    },
    titleLater: {
        fontFamily: 'Nunito',
        cursor: 'pointer'
    },
    titleUnitType: {
        fontFamily: 'Nunito',
        // marginBottom: theme.spacing(3)
    },

    /*
        `````````````````````````````````````````
        - DIALOG MODAL INPUT ANGGOTA, 
        
        - ANGGOTA LIST

        `````````````````````````````````````````
    */
    cardAnggotaList: {

        minWidth: 218,
        marginLeft:7,
        marginBottom: 7
    },

    bigAvatarAnggotaList: {

        marginLeft: 65,
        width: 60,
        height: 60

    },
    realAvatar: {
        // margin: 10,
        width: 60,
        height: 60,
    },
    bigAvatar: {
        
        width: 60,
        height: 60,
    },
    buttonModal: {
        // height: '42px',
        borderRadius: 5,
        background: 'linear-gradient(1deg, #c1272d, #c1272d 30%, #d1354a 67%, #f28181)',
        // backgroundColor: 'cyan',
        border:0,
        fontFamily:'Nunito',
        // marginLeft:theme.spacing(1),
        color: 'white',
        textTransform: 'capitalize',
        // textAlign: 'center'
        
    },
    buttonModalInputAnggota: {

        borderRadius: 5,
        background: 'linear-gradient(1deg, #c1272d, #c1272d 30%, #d1354a 67%, #f28181)',
        border:0,
        fontFamily:'Nunito',
        marginRight:theme.spacing(2),
        color: 'white',
        textTransform: 'capitalize',
        
    },
    buttonModalUpdateAnggota: {

        borderRadius: 5,
        background: 'linear-gradient(1deg, #c1272d, #c1272d 30%, #d1354a 67%, #f28181)',
        border:0,
        fontFamily:'Nunito',
        marginRight:theme.spacing(1),
        color: 'white',
        textTransform: 'capitalize',

    },
    buttonPlusAddAnggota: {

        borderRadius: 5,
        background: 'linear-gradient(1deg, #c1272d, #c1272d 30%, #d1354a 67%, #f28181)',
        border:0,
        fontFamily:'Nunito',
        color: 'white',
        textTransform: 'capitalize',

    },
    textField: {
        // marginLeft: 7,
        // marginRight: 7,
        minWidth: 425   

    },
    textError: {

        marginLeft: 7,
        color: 'red',
        fontWeight: 'bold',
        borderBottom: '2px dashed red'
    },
    warning: {

        backgroundColor: amber[700],
    },

    success: {

        backgroundColor: green[500],
    },

    /*
        ````````````````````
        - NAMA JABATAN LIST 

        ````````````````````

    */
    cardNamaJabatan: {

        // minWidth: 270,
        width: 230,
        marginTop: 3,
        marginLeft:7

    },
    buttonModalUpdateNamaJabatan: {

        borderRadius: 5,
        background: 'linear-gradient(1deg, #c1272d, #c1272d 30%, #d1354a 67%, #f28181)',
        border:0,
        fontFamily:'Nunito',
        marginRight:theme.spacing(2),
        color: 'white',
        textTransform: 'capitalize',

    },

    /*

        ``````````````````````````
        TINGKAT JABATAN LIST, etc

        ``````````````````````````

    */
    listItemText:{

        marginLeft:theme.spacing(2),
    },

    buttonModalPictOptions: {

        borderRadius: 5,
        background: 'linear-gradient(1deg, #c1272d, #c1272d 30%, #d1354a 67%, #f28181)',
        border:0,
        fontFamily:'Nunito',
        marginRight:theme.spacing(4),
        color: 'white',
        textTransform: 'capitalize',
    },

    buttonPlusAddTingkatJabatan: {
        
        borderRadius: 5,
        background: 'linear-gradient(1deg, #c1272d, #c1272d 30%, #d1354a 67%, #f28181)',
        border:0,
        fontFamily:'Nunito',
        color: 'white',
        textTransform: 'capitalize',
        marginLeft: theme.spacing(12),
        marginTop: theme.spacing(7)

    },
    cardTingkatJabatan: {
        // minWidth: 100,
        width: 240,
        marginTop: 3,
        paddingTop: 0,
        paddingBottom: 0,
        marginLeft:7

    },

    cardContent: {
        textAlign: 'center',
        marginTop: 0,
        paddingTop:0, 
        paddingBottom: 0,
        paddingLeft: 0,
        paddingRight: 0 
    },

    textFieldTingkatJabatan: {
        marginLeft: 7,
        marginRight: 7,
        minWidth: 425   

    },
    actions: {
        display: 'flex',
        direction:'row-reverse'
    },

    fontListItemText : {
        fontFamily: 'Nunito',
        fontSize:'1rem',
        fontWeight: 'bold',
        lineHeight: '1.76',
        letterSpacing: 'normal'
    },


    /*

        `````````
        UNIT TYPE 

        `````````

    */

    card: {

        width: 742,
        marginTop: 3,
        marginLeft:7
    },

    chip: {

        margin: 3 / 2,

    },

    /* 
        ````````````````````````
        FIGURE BOX 
        
        ````````````````````````
    */

    figureBoxPictMahkota: {

        width: 63,
        height: 63,
        borderStyle: 'solid',
        borderWidth: 1,
        borderRadius: 4,
        borderColor:'#f67070',
        display: 'inline-block',
        margin: 5,
        textAlign: 'center',
        cursor: 'pointer'

    },


});

const ViewUmumData = props => {

    const { classes } = props;

    const [tabIndex, setTabIndex] = useState(0);
    const handleTab = (tabIndex) => setTabIndex(tabIndex);

    /* 
        
        ``````````````````````
        LIST TINGKAT JABATAN 

        ``````````````````````    
    
    */

    const [listTingkatJabatan, setListTingkatJabatan] = useState([]);

    /* 
        
        ``````````````````````
        LIST NAMA JABATAN 

        ``````````````````````    
    
    */

    const [ listNamaJabatan, setListNamaJabatan ] = useState([]);

    /* 
        
        ``````````````````````
        LIST ANGGOTA 

        ``````````````````````    
    
    */

   const [ listAnggota, setListAnggota ] = useState([]);


    /* 
        
        ``````````````````````
        LIST UNIT TYPE

        ``````````````````````    
    
    */
    
   const [ listUnit, setListUnitType ] = useState([]);


    /* 
        
        ``````````````````````
        ABOUT DRAG & DROP     

        ``````````````````````    
    
    */

    const reorder = (list, startIndex, endIndex) => {

        const result = Array.from(list);
        const [removed] = result.splice(startIndex, 1);
    
        result.splice(endIndex, 0, removed);
    
        return result;
    };
    
    /* 
        ```````````````````````
        onDragEnd 'Tingkat Jabatan' 

        ```````````````````````
        
    */
    function onDragEnd(result) {
        
        // console.log("onDragEnd result ", result);
        if (!result.destination) {
            return;
        }

        if (result.destination.index === result.source.index) {
            return;
        
        };
        
        const resultReorderTingkatJabatan = reorder ( listTingkatJabatan, result.source.index, result.destination.index);

        setListTingkatJabatan(resultReorderTingkatJabatan)
    };

    /* 
        
        ```````````````````````
        onDragEnd 'UNIT TYPE'  

        ```````````````````````
    
    */

    function onDragEndUnitType(result){

        if (!result.destination) {
            return;
        }

        if (result.destination.index === result.source.index) {
            return;
        
        };

        const resultReorderUnitType = reorder ( listUnit, result.source.index, result.destination.index);

        setListUnitType(resultReorderUnitType)
    };

    return (

        <div style={{backgroundColor:'#eaf3f5'}}>
            <MuiThemeProvider theme={theme}>
            <Tabs selectedIndex={tabIndex} onSelect={tabIndex => handleTab(tabIndex)}>
                <TabList>
                    <Tab>
                        <Typography variant='subtitle1' className={classes.title}>
                            <b>Unit</b>
                        </Typography>
                    </Tab>
                    <Tab>
                        <Typography variant='subtitle1' className={classes.title}>
                            <b>Tingkat Jabatan</b>
                        </Typography>
                    </Tab>
                    <Tab>
                        <Typography variant='subtitle1' className={classes.title}>
                            <b>Nama Jabatan</b>
                        </Typography>
                    </Tab>
                    <Tab>
                        <Typography variant='subtitle1' className={classes.title}>
                            <b>Anggota</b>
                        </Typography>
                    </Tab>
                </TabList>
                <TabPanel style={{ backgroundColor: '#eaf3f5'}}>
                    <br />
                    <br />

                    {
                        listUnit.length > 0 ? (

                            <AddUnitType 
                                classes={classes}
                            />

                        ) : null
                    }
                    
                    <Grid  
                        container
                        // spacing={8}
                        direction="row"
                        justify="center"
                        alignItems="center"
                    >
                        <Grid item sm={2}></Grid>
                        <Grid item sm={10}>                     
                            <DragDropContext onDragEnd={onDragEndUnitType}>
                                <Droppable droppableId="list">

                                {
                                            
                                    provided => {
                                        
                                        return (
                                                <div ref={provided.innerRef} {...provided.droppableProps}>
                                                    <UnitTypeList 
                                                        listUnit={listUnit} 
                                                        setListUnitType={setListUnitType} 
                                                        classes={classes}
                                                    />
                                                    {provided.placeholder}
                                                </div>
                                            
                                        )
                                    }
                                }
                                
                                </Droppable>
                            </DragDropContext> 
                        </Grid>
                    </Grid>

                    {
                        listUnit.length ==  0 ? (

                                <Grid  
                                    container
                                    // spacing={8}
                                    direction="row"
                                    justify="center"
                                    alignItems="center"
                                >
                                    <Grid item sm={4} style={{textAlign:'center'}}>
                                        <br />
                                        <br />
                                        <img src={ImagePart1} alt='Image part-01' />
                                        <br />
                                        <br />
                                        <Typography variant='subtitle1' className={classes.title}>
                                            <b>Buat Tipe Struktur Unit</b>
                                        </Typography>
                                        <br />
                                        <br />
                                        <br />
                                        <br />
                                    </Grid>

                                    <Grid item sm={4} style={{textAlign: "center"}}>
                                        <br />
                                        <br />
                                        <img src={ImagePart2} alt='Image part-02'  />
                                        <br />
                                        <br />
                                        <Typography variant='subtitle1' className={classes.title}>
                                            <b>Buat Nama Unit</b>
                                        </Typography>
                                        <br />
                                        <br />
                                        <br />
                                        <br />
                                    </Grid>

                                    <Grid item sm={4} style={{textAlign: "center"}}>
                                        <br />
                                        <br />
                                        <img src={ImagePart3} alt='Image part-03' />
                                        <br />
                                        <br />
                                        <Typography variant='subtitle1' className={classes.title}>
                                            <b>Pilih Icon</b>
                                        </Typography>
                                        <br />
                                        <br />
                                        <br />
                                        <br />
                                    </Grid>

                                    <Grid item sm={12} style={{textAlign: "center"}}>
                                        <br />
                                        <br />
                                        <ModalComponentPictDefaultUnitType 
                                            classes={classes}
                                        />
                                    </Grid>

                                </Grid>

                            ) : null
                        }   

                        <br />
                        <br />
                        <br />
                        <br />
                        <br />
                        <br />
                        <br />
                        <br />
                        <br />
                        <br />
                        <br />
                        <br />
                        <br />
                        <br />
                        <br />
                </TabPanel>
                 {/* 

                    ``````````````````
                    TINGKAT JABATAN 
                    
                    ``````````````````

                */}

                <TabPanel style={{ backgroundColor: '#eaf3f5'}}>
                    <br />
                    <br />
                    <Grid  
                        container
                        spacing={8}
                        direction="row"
                        justify="center"
                        alignItems="center"
                    >
                        <Grid item sm={4}></Grid>
                        <Grid 
                            item sm={2} 
                            style={{textAlign: 'right'}}
                        >

                            {
                                listTingkatJabatan.length > 0 ? (

                                    <AddTingkatJabatan 
                                        setListTingkatJabatan={setListTingkatJabatan}
                                        listTingkatJabatan={listTingkatJabatan}  
                                        classes={classes} 
                                    />
                                
                                ) : null
                            
                            }

                        </Grid>
                        <Grid item sm={6}></Grid>
                    </Grid>

                    <Grid  
                        container
                        spacing={8}
                        direction="row"
                        justify="center"
                        alignItems="center"
                    >
                        <Grid item sm={4}></Grid>
                        <Grid item sm={4} >
                            <DragDropContext onDragEnd={onDragEnd}>
                                <Droppable droppableId="list">

                                    {
                                        
                                        provided => {

                                            return (
                                                <div ref={provided.innerRef} {...provided.droppableProps}>
                                                    <TingkatJabatanList 
                                                        setListTingkatJabatan={setListTingkatJabatan}
                                                        listTingkatJabatan={listTingkatJabatan} 
                                                        classes={classes}
                                                        /> 

                                                    {provided.placeholder}
                                                </div>
                                                
                                            )
                                        }
                                    }
                                </Droppable>
                            </DragDropContext>
                        </Grid>
                        <Grid item sm={4}></Grid>
                    </Grid>

                    {
                        listTingkatJabatan.length ==  0 ? (

                                <Grid  
                                    container
                                    spacing={0}
                                    direction="row"
                                    justify="center"
                                    alignItems="center"
                                >
                                    <Grid item sm={4}></Grid>

                                    <Grid item sm={4} style={{textAlign: "center"}}>
                                        <br />
                                        <br />
                                        <img src={PictTingkatJabatan} alt='Tingkat Jabatan' />
                                        <br />
                                        <br />

                                        <Typography variant='subtitle1' className={classes.title}>
                                            <b>Tambah tingkat jabatan</b>
                                        </Typography>
                                        <br />
                                        <Typography variant='subtitle2' className={classes.title} style={{color: 'gray', textAlign: 'center'}}>
                                            Definisikan tingkat jabatan sesuai struktur perusahaan Anda Seperti CEO, General Manager, Supervisor, Staff
                                        </Typography>
                                        <br />
                                        <br />
                                        <ModalComponentTingkatJabatan 
                                            setListTingkatJabatan={setListTingkatJabatan}
                                            listTingkatJabatan={listTingkatJabatan}   
                                            classes={classes}
                                        />
                                        <br />
                                    </Grid>

                                    <Grid item sm={4}></Grid>
                                </Grid>

                            ) : null
                        }        
                        <br />
                        <br />
                        <br />
                        <br />
                </TabPanel>

                  {/* 

                    ``````````````````
                    NAMA JABATAN 
                    
                    ``````````````````

                */}
                
                <TabPanel style={{ backgroundColor: '#eaf3f5'}}>
                    <br />
                    <br />
                    <Grid  
                        container
                        spacing={0}
                        direction="row"
                        justify="flex-end"
                        alignItems="center"
                    >
                        <Grid item sm={11}></Grid>

                        <Grid item sm={1}>
                            <AddNamaJabatan
                                listNamaJabatan= {listNamaJabatan}
                                setListNamaJabatan = {setListNamaJabatan}
                                classes={classes}
                            />
                        </Grid>

                    </Grid>
                
                    <NamaJabatanList 
                        listNamaJabatan= {listNamaJabatan}
                        setListNamaJabatan = {setListNamaJabatan}
                        classes={classes}
                    />
                    <br />
                    <br />
                    <br />
                    <br />
                    <br />
                    <br />
                    <br />
                    <br />
                    <br />
                    <br />
                    <br />
                    <br />
                    <br />
                    <br />
                    <br />
                    <br />
                    <br />
                </TabPanel> 


                 {/* 

                    ``````````````````
                    ANGGOTA
                    
                    ``````````````````
                */}

                <TabPanel style={{ backgroundColor: '#eaf3f5'}}>
                    <br />
                    <br />
                    <Grid  
                        container
                        spacing={0}
                        direction="row"
                        justify="flex-end"
                        alignItems="center"
                    >
                        <Grid item sm={11}></Grid>
                        <Grid item sm={1}>

                            <AddAnggota 
                                classes={classes}
                                setListAnggota={setListAnggota}
                                listAnggota = {listAnggota}
                            />
                        </Grid>
                    </Grid>

                    <AnggotaList 
                        setListAnggota={setListAnggota}
                        listAnggota = {listAnggota}
                        classes={classes}
                    />
                    <br />
                    <br />
                    <br />
                    <br />
                    <br />
                    <br />
                    <br />
                    <br />
                    <br />
                    <br />
                    <br />
                    <br />
                    <br />
                </TabPanel>   
            </Tabs>
            </MuiThemeProvider>
        </div>

    )
};

export default withStyles(styles)(ViewUmumData)