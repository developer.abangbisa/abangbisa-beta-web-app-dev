import React, { useCallback, useEffect, useState } from "react";
import { makeStyles, createMuiTheme, withStyles, MuiThemeProvider } from '@material-ui/core/styles';
import { 
    Container, Paper, Typography, Grid, TextField, Checkbox, CircularProgress,
    FormControlLabel, Box, Button, MenuItem, Dialog, DialogTitle, DialogContent, 
    DialogContentText, DialogActions, Breadcrumbs, Link

} from '@material-ui/core';

import axios from 'axios';
import { URL_API } from '../../../constants/config-api';


import ImageForgetPassword from '../../../assets/images/Mask_Group_7.png';
import ImageOne from '../../../assets/images/Group_1619.png';
import ImageSecond from '../../../assets/images/Group_161a.png';

import Redirect from '../../../utilities/Redirect';
import { ToStructureOrganization, ToCompletionCompanyStructure, ToCompletionCompanyStructureFormulirKosong } from '../../../constants/config-redirect-url';

const theme = createMuiTheme({
    
    palette: {

        primary: {
            main: '#cc0707', //#cc0707, #c62828
            light: '#ff5f52',
            dark: '#8e0000',
            contrastText: '#ffffff'
        }
    }       
});

const styles = theme => ({

    root: {
        
        // padding: theme.spacing(5, 2),
        // marginTop: theme.spacing(4),
        // width: 575,
        borderRadius: 2

    },
    button: {
        // width: '503px',
        // height: '42px',
        borderRadius: 5,
        background: 'linear-gradient(1deg, #c1272d, #c1272d 30%, #d1354a 67%, #f28181)',
        // backgroundColor: 'cyan',
        border:0,
        fontFamily:'Nunito',
        marginLeft:theme.spacing(1),
        textTransform: 'capitalize',
        color: 'white'
    },
    breadcrumb: {
        marginLeft: theme.spacing(5)
    },
    title: {
        fontFamily: 'Nunito'
    },
    titleSub: {
        fontFamily: 'Nunito',
        color: 'grey'
    },
    titleLater: {
        fontFamily: 'Nunito',
        cursor: 'pointer',
        marginLeft: theme.spacing(5),
        color: 'grey'
    },
    titleLaterSecond: {
        fontFamily: 'Nunito',
        cursor: 'pointer',
        // marginLeft: theme.spacing(5),
        // color: 'grey'
    }
});

const ViewCompletionCompanyStructureQuestionThird = props => {

    const { classes } = props;

    const userToken = localStorage.getItem('userToken');

    const handlePOSTWithBantuan = () => {


        if(userToken !== undefined){
   
            const header =  {       
                'Accept': "application/json",
                'Content-Type' : "application/json",
                'Authorization' : "bearer " + userToken,
            };
        
            axios.defaults.headers.common = header;    
 
            axios
                .post(URL_API + `/human-resource/structure-wizard/master-structure`)
                .then(function(response){
 
                    console.log("Response Original   : ", response);
                    Redirect(ToCompletionCompanyStructureFormulirKosong);
 
                })
                .catch(function(error){
                    
                    console.log("Error : ", error.response)
                })
        };

    };

    
    
    return (

        <MuiThemeProvider theme={theme}>
           
            <br />
            <br />
            <br />
            <br />
            <br />
            <br />
            <Paper className={classes.root} elevation={0}>
                <Grid container >  
                    <Grid item xs={6} style={{textAlign: 'center'}}>
                        <img src={ImageOne} style={{width: 600}} alt='Pict Contoh model Struktur Organisai' />
                    </Grid>

                    <Grid item xs={6} style={{textAlign: 'left'}}>
                        <Typography variant="h5" className={classes.title}>
                            <b>Struktur Organisasi</b>
                        </Typography>

                        <Typography variant="subtitle1" className={classes.titleSub}>
                            Gunakan contoh di samping sebagai struktur organisasi Anda. 
                            Tentukan lapisan Unit dalam organisasi Anda dengan mengkategorikan jenis unit dan nama- nama unit organisasi Anda
                        </Typography>

                        <br />
                        <br />
                        <span 
                            // variant='subtitle1' 
                            className={classes.titleLaterSecond}
                            // onClick={handleBackInsideComponentCrop}
                            onClick={() => Redirect(ToCompletionCompanyStructure)}
                        >
                            <b>Batal</b> &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
                        </span>
                        <Button 
                            // onClick={() => Redirect(ToCompletionCompanyStructureFormulirKosong)}
                            onClick={handlePOSTWithBantuan}
                            variant='contained' 
                            size='medium' 
                            className={classes.button}
                        >
                            Gunakan
                        </Button>  
                    </Grid>
                </Grid>
            </Paper>
            <br />
            <br />
            <br />
            <br />
            <br />
            <br />
            <br />
            <br />
            <br />
            <br />
        </MuiThemeProvider>
    )
};


export default withStyles(styles)(ViewCompletionCompanyStructureQuestionThird);