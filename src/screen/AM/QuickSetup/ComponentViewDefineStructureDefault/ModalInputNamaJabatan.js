import React,{Component, useState, useEffect, useContext, useRef} from 'react';
import { Dialog, DialogTitle, Typography, DialogContent, DialogContentText, DialogActions, Button, TextField} from '@material-ui/core';
import axios from 'axios';
import { URL_API } from '../../../../constants/config-api';

const ModalInputNamaJabatan = (props) => {

    // console.log("props : ", props.classes)
    
    const { 
    
        classes, 
        isChooseButtonPlus, 
        setChooseButtonPlus

    } = props;

    const [valueNamaJabatan, setValueNamaJabatan] = useState('')
    const closeModalNamaJabatan = () => setChooseButtonPlus(false);
    
    const handleChange = (e) => setValueNamaJabatan(e.target.value);

    const handleEnterPress = (e) => {

        if(e.keyCode == 13){

            setValueNamaJabatan(e.target.value);
            handleAdd(e);
        };
    };
    
    const handleAdd = (e) => {

        e.preventDefault();
        
        let data = {

            MasterStructurePositionTitle: {
            
                name: valueNamaJabatan,
                code: listLabelNamaJabatan
                
            }
        };

        // console.log("Data ModalInputNamaJabatan : ", data);

        const userToken = localStorage.getItem('userToken');
        
        if(userToken !== undefined && data !== null && data !== undefined){
        
            const header =  {       
                'Accept': "application/json",
                'Content-Type' : "application/json",
                'Authorization' : "bearer " + userToken,
    
            };

            axios.defaults.headers.common = header;    

            axios
                .post(URL_API + '/human-resource/master-structure-position-title', data)
                .then(function(response){

                    console.log("Response Original : ", response)
                    closeModalNamaJabatan();
                    
                    if(response.status == 200 ){
                        if(response.data.data !== undefined){
                            props.setListNamaJabatan([...props.listNamaJabatan, response.data.data ])
                        }
                    };
                })
                .catch(function(error){
                    
                    console.log("Error : ", error.response)
                   
                })

        } else { console.log("No Access Token available!")};
    };

    /*

        `````````````````````````````` 
        GET LABEL, CODE NAMA JABATAN

        `````````````````````````````
    */

    const [listLabelNamaJabatan, setListLabelNamaJabatan] = useState();

    const handleChangeLabelCodeNamaJabatan = (e) => setListLabelNamaJabatan(e.target.value);

    useEffect(() => {
        
        if(isChooseButtonPlus == true){

            const userToken = localStorage.getItem('userToken');

            if(userToken !== undefined){
        
                const header =  {       
                    'Accept': "application/json",
                    'Content-Type' : "application/json",
                    'Authorization' : "bearer " + userToken,
                };
            
                axios.defaults.headers.common = header;    
                    
                axios
                    .get(URL_API + `/human-resource/master-structure-position-title/create`)
                    .then(function(response){

                        // console.log("Response Original : ", response)
                        const newDataCode = {...response.data.data };

                        if(newDataCode.fields.code !== null && newDataCode.fields.code !== undefined){
                            
                            setListLabelNamaJabatan(newDataCode.fields.code.value);
                        };
                    })
                    .catch(function(error){
                        
                        console.log("Error : ", error.response);
                    })
        
            };
        }
    },[isChooseButtonPlus])



    return (
        <div>
            {
                classes !== undefined ? (
                    <Dialog
                        open={isChooseButtonPlus}
                        onClose={closeModalNamaJabatan}
                        aria-labelledby="alert-dialog-title"
                        aria-describedby="alert-dialog-description"
                        // className={classes.dialogInputAnggota}
        
                    >
                        <DialogTitle id="alert-dialog-title" style={{textAlign: "left"}}>
                            <Typography variant='h6' className={classes.title}>
                                {/* Nama  Jabatan [{listLabelNamaJabatan}] */}
                                Tambah Nama Jabatan 
                            </Typography>
                            <hr/>
                        </DialogTitle>
                        <DialogContent style={{textAlign: "center"}}>
                            
                            <TextField
                                id="outlined-bare"
                                label="Nama Jabatan"
                                className={classes.textField}
                                onChange= {handleChange}
                                onKeyDown={handleEnterPress}
                                variant="outlined"
                                fullWidth
                            />
                            <br />
                            <br />
                            <TextField
                                id="outlined-bare"
                                label="Kode Jabatan"
                                className={classes.textField}
                                onChange= {handleChangeLabelCodeNamaJabatan}
                                // onKeyDown={handleEnterPress}
                                variant="outlined"
                                fullWidth
                                placeholder={listLabelNamaJabatan}
                            />
                            <DialogContentText id="alert-dialog-description">
                                <Typography variant='h6'>
                                    
                                </Typography>
                            </DialogContentText>
                        </DialogContent>
                        <DialogActions style={{alignItems: "right", justifyContent:'flex-end'}}>
                            <Button 
                                onClick={handleAdd}
                                variant='contained' 
                                className={classes.buttonModalInputAnggota}
                                // fullWidth
                            >  
                                Tambah
                            </Button>
                        </DialogActions>
                        <br />
                    </Dialog>
                ) : null
            }
        </div>
    )
};

// export default withStyles(styles)(ModalInputNamaJabatan);
export default ModalInputNamaJabatan;