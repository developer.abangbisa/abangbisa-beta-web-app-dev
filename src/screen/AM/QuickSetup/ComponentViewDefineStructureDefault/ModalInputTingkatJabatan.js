import React,{Component, useState, useEffect, useContext, useRef} from 'react';
import { Dialog, DialogTitle, Typography, DialogContent, DialogContentText, DialogActions, Button, TextField} from '@material-ui/core';
import axios from 'axios';
import { withStyles } from '@material-ui/core/styles';
// import MuiThemeProvider from "@material-ui/core/styles/MuiThemeProvider";
// import { Row,  Modal} from 'react-bootstrap';
import { URL_API } from '../../../../constants/config-api';
// import theme from './theme'; // NEXT INI MESTI GW HILANGKAN utk consistency !

// import ContextDefineStrucutureDefault from '../Context/ContextDefineStructureDefault';

// const styles = {

//     card: {

//       minWidth: 425,
//     },

//     textField: {
//         marginLeft: 7,
//         marginRight: 7,
//         minWidth: 425   

//     },
// };

const ModalInputTingkatJabatan = (props) => {

    // console.log("Props : ", props)
    
    const { 
    
        classes, 
        isChooseButtonPlus, 
        setChooseButtonPlus,
        listTingkatJabatan,
        setListTingkatJabatan

    } = props;

    // console.log("classes : ", classes)

    
    
    /* 
        ``````````````````````````````````````````````````````````````````````````````````````````
        Review Code : "Next fungsi 'setValueNamaJabatan' di ganti menjadi setValueTingkatJabatan"

        ``````````````````````````````````````````````````````````````````````````````````````````
    */
   
    const [valueNamaJabatan, setValueNamaJabatan] = useState('')

    const closeModalTingkatJabatan = () => setChooseButtonPlus(false);
    
    const handleChange = (e) => setValueNamaJabatan(e.target.value);
    const handleEnterPress = (e) => {

        if(e.keyCode == 13){
            setValueNamaJabatan(e.target.value);
            handleAdd();
        };
    };

    const handleAdd = () => {

        let data = {

            MasterStructurePositionLevel: {
            
                name: valueNamaJabatan,
                prev_id: props.label.prev_id,
                next_id:  props.label.next_id
                
            }
        };

        const userToken = localStorage.getItem('userToken');

        // console.log("Data Add Input Tingkat Jabatan : ", data)
        
        if(userToken !== undefined && data !== null && data !== undefined){
        
            const header =  {       
                'Accept': "application/json",
                'Content-Type' : "application/json",
                'Authorization' : "bearer " + userToken,
    
            };

            axios.defaults.headers.common = header;    

            axios
                .post(URL_API + '/human-resource/master-structure-position-level', data)
                .then(function(response){

                    console.log("Response Original : ", response)
                    closeModalTingkatJabatan();

                    if(response.status == 200 ){
                        if(response.data.data !== undefined){
                            // context.handleTabTingkatJabatan();
                            setListTingkatJabatan([...listTingkatJabatan, response.data.data ])

                        }
                    }
                    
                })
                .catch(function(error){
                    
                    console.log("Error : ", error.response)
               
                })

        } else { console.log("No Access Token available!")};
    };
    
    return (
        
        <Dialog
            open={isChooseButtonPlus}
            onClose={closeModalTingkatJabatan}
            aria-labelledby="alert-dialog-title"
            aria-describedby="alert-dialog-description"

        >
            <DialogTitle id="alert-dialog-title" style={{textAlign: "left"}}>
                <Typography variant='h6' className={classes.title}>
                    Tingkat  Jabatan
                </Typography>
                <hr/>
            </DialogTitle>
            <DialogContent style={{textAlign: "center"}}>
                
                <TextField
                    id="outlined-bare"
                    className={classes.textField}
                    onChange= {handleChange}
                    onKeyDown={handleEnterPress}
                    variant="outlined"
                    fullWidth
                />
                <DialogContentText id="alert-dialog-description">
                    <Typography variant='h6'>
                        
                    </Typography>
                </DialogContentText>
            </DialogContent>
            <DialogActions style={{alignItems: "right", justifyContent:'flex-end'}}>
                <Button 
                    onClick={handleAdd}
                    variant='contained' 
                    className={classes.buttonModalInputAnggota}
                    // fullWidth
                >  
                    Tambah
                </Button>
            </DialogActions>
            <br />
        </Dialog>
    )
};

// export default withStyles(styles)(ModalInputTingkatJabatan);
export default ModalInputTingkatJabatan;