import { createMuiTheme } from "@material-ui/core/styles";
import { cyan, lightBlue, lightGreen, grey, red} from "@material-ui/core/colors";
// import grey from '@material-ui/core/colors/grey';
export default createMuiTheme({
  palette: {
    primary: red, //linear-gradient(1deg, #c1272d, #c1272d 30%, #d1354a 67%, #f28181)
    secondary: grey
  },
  overrides: {
    MuiButton: {
      root: {
        // fontWeight: "bold",
        // backgroundColor: "red",
        // margin: "10px",
        // "&:hover": {
        //   backgroundColor: "green"
        // }
       
        // marginTop: theme.spacing.unit,
        // marginRight: theme.spacing.unit,
        marginTop: 30,
        marginRight: 40,
        width: '122.2px',
        height: '40px',
        borderRadius: 5,
        background: 'linear-gradient(1deg, #c1272d, #c1272d 30%, #d1354a 67%, #f28181)',
        border:0,
      },
      text: {
        color: 'white'
      },
      textSecondary: {
        color: 'white',
        fontFamily: 'Nunito'
      },
    },
    // MuiCard: {
    //     root: {
    //         backgroundColor: 'orange'
    //     }
    // },
    MuiStepper:{
      root:{
        backgroundColor: '#eaf3f5'
      }
    },
    MuiStepContent:{
        root:{
          backgroundColor: '#eaf3f5'
        }
    },
    MuiStepIcon: {
      root: {
          color: 'grey', // or 'rgba(0, 0, 0, 1)'
          '&$active': {
              color: '#c2322d',
          },
          '&$completed': {
              color: 'green',
          },
      },
  } 
  }
});