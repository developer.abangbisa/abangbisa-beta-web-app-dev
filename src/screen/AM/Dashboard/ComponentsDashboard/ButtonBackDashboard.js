import React,{ Component } from 'react';
import styled from 'styled-components';
// import IconArrowRight from '../assets/images/Group_1222.png';
// import IconArrowLeft from '../../../assets/images/Group_1186.png';
import IconArrowLeft from '../../../../assets/images/SVG/Group_1186.svg'


const Wrapper = styled.button`

    border-radius: 5px;
    background-color: #fefefe;
    margin: 1em 1em ;
    padding: 0.2em 1em 0.2em 1em;
    border: solid 1px #707070;
    cursor: pointer;
`;

const White = styled.span`
    color: grey;
`;

const Img = styled.img`

    margin-right: 7px;
`;

const ButtonBackDashboard = ({handleClick}) => (
    <Wrapper type="button" onClick={handleClick}>
        <White> 
            <Img src={IconArrowLeft} /> <b style={{paddingBottom: 14}}>Dashboard</b>
        </White>
    </Wrapper>    
)


export default ButtonBackDashboard;

