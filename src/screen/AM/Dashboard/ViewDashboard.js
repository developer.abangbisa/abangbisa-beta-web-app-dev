import React, {useEffect, useState} from 'react';
import { Redirect } from 'react-router';
import MuiThemeProvider from "@material-ui/core/styles/MuiThemeProvider";
import { withStyles } from '@material-ui/core/styles';
import { 
          Grid,
          Chip,
          Drawer,
          Button,
          Divider,
          List,
          ListItem,
          ListItemIcon,
          ListItemText,
          AppBar
        
        } from '@material-ui/core';
        
import InboxIcon from '@material-ui/icons/MoveToInbox';
import MailIcon from '@material-ui/icons/Mail';
import Toolbar from '@material-ui/core/Toolbar';
import IconButton from '@material-ui/core/IconButton';
import Typography from '@material-ui/core/Typography';
import InputBase from '@material-ui/core/InputBase';
import Badge from '@material-ui/core/Badge';
import MenuItem from '@material-ui/core/MenuItem';
import TextField from '@material-ui/core/TextField';
import Menu from '@material-ui/core/Menu';
import { fade } from '@material-ui/core/styles/colorManipulator';
import Slider from '@material-ui/core/Slide';
import MenuIcon from '@material-ui/icons/Menu';
import SearchIcon from '@material-ui/icons/Search';
import AccountCircle from '@material-ui/icons/AccountCircle';
import NotificationsIcon from '@material-ui/icons/Notifications';
import MoreIcon from '@material-ui/icons/MoreVert';
import LinearProgress from '@material-ui/core/LinearProgress';

import { createMuiTheme } from "@material-ui/core/styles";
import { makeStyles } from "@material-ui/styles";

import ListSubheader from '@material-ui/core/ListSubheader';
import Collapse from '@material-ui/core/Collapse';
import DraftsIcon from '@material-ui/icons/Drafts';
import SendIcon from '@material-ui/icons/Send';
import ExpandLess from '@material-ui/icons/ExpandLess';
import ExpandMore from '@material-ui/icons/ExpandMore';
import StarBorder from '@material-ui/icons/StarBorder';
import MahkotaLogoCompany from '../../../assets/images/Group-862.png';
import LogoCompany from '../../../assets/images/Group_661.png';
import ImageDashboardHome from '../../../assets/images/Subtraction_3.png';
import Image7WD from '../../../assets/images/Group_110.png';
import ImageHumanCapital from '../../../assets/images/Group_663.png';
import ImageMenuIconAbangBisa from '../../../assets/images/Group_1518.png';
import IconMenuOffCanvassGrey from '../../../assets/images/SVG/Group_709.svg';

import Card from '@material-ui/core/Card';

import PictDefaultDashboard from '../../../assets/images/Mask_Group_2.png';

import { cyan, lightBlue, lightGreen, grey, red,white, green } from "@material-ui/core/colors";
import theme  from './theme/theme-dashboard';

import Checkbox from '../../../components/Checkbox';
import IconMenuOffCanvass from '../../../assets/images/SVG/Group_1518.png';
import RedirectCustom from '../../../utilities/RedirectCustom';
import { ToMembershipStatus, ToCompanyProfile, ToUserManagement, ToEmptyStateGeneral } from '../../../constants/config-redirect-url';

const useStyles = makeStyles(theme => {

  return {

    list: {

      width: 250

    },
    fullList: {

      width: 'auto',
    },

    search: {
      position: 'relative',
      // borderRadius: theme.shape.borderRadius,
      borderRadius: 3,
      // backgroundColor: fade(white, 0.1),
      // '&:hover': {
      //   backgroundColor: fade(white, 0.25),
      // },
      backgroundColor: grey,
      // marginRight: theme.spacing.unit * 2,
      marginRight: 1 * 2,
      marginLeft: 0,
      width: '100%',
      // [theme.breakpoints.up('sm')]: {
      //   // marginLeft: theme.spacing.unit * 3,
      //   marginLeft: 1 * 3,
      //   width: 'auto',
      // },
    },
    // searchIcon: {
    //   width: 3 * 9,
    //   height: '100%',
    //   position: 'absolute',
    //   pointerEvents: 'none',
    //   display: 'flex',
    //   alignItems: 'center',
    //   justifyContent: 'center',
    // },
    searchIcon:{
      color:'grey',
      // marginTop: 10
    },

    sectionDesktop: {
      // display: 'none',
      display: 'flex',
    },

    //App Bar
    root: {
      flexGrow: 1,
    },
    grow: {
      flexGrow: 1,
    },
    menuButton: {
      marginLeft: -12,
      marginRight: 20,
    },
    accountCircle: {
      color: 'grey',

    },
    menuIcon: {
      color: 'grey'
    },
    mail: {
      color: 'grey'
    },

    notifIcon: {
      color: 'grey'
    },
    list: {
      backgroundColor: 'red'

    },
    listDrawer: {
      backgroundColor: 'red'
    },

    /* DRAWER */
    drawerRoot: {
      width: '100%',
      maxWidth: 360,
      // backgroundColor: theme.palette.background.paper,
      backgroundColor: 'transperent',
      color: 'white'
    },
    nested: {
      // paddingLeft: theme.spacing.unit * 4,
      paddingLeft: 7
    },
    iconMenuOffCanvass:{

      width: 30,
      height: 30,
      cursor: 'pointer',
      marginTop: 24
      
    },
    title: {

      fontFamily: 'Nunito'
    }

  };
});

const ViewDashboard = (props) => {
    
    const [isLogout, setLogout] = useState(false);
    const [isRedirect, setRedirectToDefineStructure] = useState(false);
    // const { classes } = props;

    /* ````````````START DRAWER`````````````````````*/

    const classesV2 = useStyles();

    const [state, setState] = useState({
      top: false,
      left: true,
      bottom: false,
      right: false

    });

    const handleCloseOffCanvass = () => {

      setState(state.left = false)
    };

    const toggleDrawer = (side, open) => () => {

        setState({ ...state, [side]: open });
    };

    const sideList = (

        <div className={classesV2.list}>
          <List className={classesV2.listDrawer}>

            {['Dashboard', '7WD'].map((text, index) => (
              
              <ListItem button key={text}>
                <ListItemIcon>{index % 2 === 0 ? <InboxIcon /> : <MailIcon />}</ListItemIcon>
                <ListItemText primary={text} />
              </ListItem>

            ))}
            
          </List>

          <Divider />
        </div>
      );

    /* ````````````END DRAWER ``````````````````````*/

    function handleLogOut (){
        
        localStorage.removeItem('userToken');
        // localStorage.clear();
        setLogout(true);
    };

    function handleRedirectToDefineStructure(){
        setRedirectToDefineStructure(true);
    };

 
    
    // ==> Expected an assignment or function call and instead saw an expression  no-unused-expressions

    /* 
        ````````````````````````````````
          START APP DRAWER

        ````````````````````````````````
      
    */

   const [anchorEl, setAnchorEl] = React.useState(null);
   const [mobileMoreAnchorEl, setMobileMoreAnchorEl] = useState(null);
 
   const isMenuOpen = Boolean(anchorEl);
   const isMobileMenuOpen = Boolean(mobileMoreAnchorEl);
 
   function handleProfileMenuOpen(event) {
     setAnchorEl(event.currentTarget);
   }
 
   function handleMobileMenuClose() {
     setMobileMoreAnchorEl(null);
   }
 
   function handleMenuClose() {
     setAnchorEl(null);
     handleMobileMenuClose();
   }
 
   function handleMobileMenuOpen(event) {
     setMobileMoreAnchorEl(event.currentTarget);
   }

  /*
      ````````````````````````````````
        SLIDER

      ````````````````````````````````
  */
  
  const [completed, setCompleted] = useState(10);

  /*
      ````````````````````````````````
        LIST IN DRAWER

      ````````````````````````````````
  */

  const [openNestedList, setOpenNestedList] = useState(false);

  function handleOpenNestedList() {

    setOpenNestedList(!openNestedList);
  }

  /*
      ````````````````````````````````
        ICON POJOK KANAN ATAS - MENU

      ````````````````````````````````
  */

 const ITEM_HEIGHT = 24;

  const options = [
    'Pengaturan',
    'Log-out'
  ];

  const [anchorElMenuPojokKananAtas, setAnchorElPoKaAtas] = useState(null);
  const isOpenMenuPojokKananAtas = Boolean(anchorElMenuPojokKananAtas);

  function handleMenuPojokKananAtas(event) {
    setAnchorElPoKaAtas(event.currentTarget);
  }

  function handleCloseMenuPojokKananAtas() {
    setAnchorElPoKaAtas(null);

  };

  /*
      ````````````````````````````````
        REDIRECT TO ROLE

      ````````````````````````````````
  */

  const [isRedirectToRole, setRedirectToRole ] = useState(false);

  function handleRedirectToRole(){
    
    setRedirectToRole(true);
  };

  const redirectToMembershipStatus = () => RedirectCustom(ToMembershipStatus);

  if(isRedirectToRole == true){

    return(
        <Redirect to='/role' />  
    )
  };

  if(isLogout == true){
    return(
        <Redirect to='/login' />  
    )
  }; 

  if(isRedirect == true){

      return (

          <Redirect to='/structure-organization' />  
      )
  };
  
  return (

      <MuiThemeProvider theme={theme}>
          <Grid  
              container
              direction="column"
              justify="center"
              alignItems="center"
              spacing={0}
          >
            <br />
            <br />
            <img src={PictDefaultDashboard} style={{height: 300}} alt='Default Dashboard Pict' />
            <br />
            <br />
            <Typography variant='h6' className={classesV2.title} style={{color: 'black', textAlign: 'center'}}>
              Belum ada data yang bisa ditampilkan di <i>Dashboard</i> ! 
            </Typography>
            {/* <Button 
                style={{fontFamily: 'Nunito', textTransform: 'capitalize', width: 300, }}
                onClick={handleRedirectToDefineStructure}                     
                
            >
          
              Menu ? 
            </Button> */}
            <br />
            <br />
            <br />
            <br />
            
        </Grid>
      </MuiThemeProvider>
    )
};

// export default withStyles(styles)(ViewDashboard);
export default ViewDashboard;