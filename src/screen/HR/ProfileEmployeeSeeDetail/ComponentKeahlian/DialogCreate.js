import React, { Component, useEffect, useState, useCallback } from "react";
import { makeStyles, createMuiTheme, withStyles, MuiThemeProvider } from '@material-ui/core/styles';
import { 
    Container, Paper, Typography, Grid, TextField, Checkbox, CircularProgress,
    FormControlLabel, Box, Button, MenuItem, Dialog, DialogTitle, DialogContent, 
    DialogContentText, DialogActions, Breadcrumbs, Link, IconButton, InputBase, Divider,
    Tooltip, List, ListItem, ListItemText, Avatar, Table, TableBody, TableRow, TableCell,
    Chip, TablePagination, TableHead, TableSortLabel, Radio, FormLabel, InputAdornment, ListItemSecondaryAction,
    GridList, GridListTile, GridListTileBar 

} from '@material-ui/core';

import Rating from '@material-ui/lab/Rating';

import {useDropzone} from 'react-dropzone';


import StickyFooter from 'react-sticky-footer';
import DoneIcon from '@material-ui/icons/Done';
import StarBorderIcon from '@material-ui/icons/StarBorder';
import StarBorderIconFulled from '@material-ui/icons/Star';

import { extractImageFileExtensionFromBase64 } from '../../../../utilities/ReusableUtils';
import Redirect from '../../../../utilities/Redirect';
import { ToHrEmployeeRiwayat } from '../../../../constants/config-redirect-url';
import PictPlusUploadDocument from '../../../../assets/images/Group_2268.png';




const DialogCreate = props => {

    const { classes, isOpenModalCreate, setOpenModalCreate } = props;

      /* 
       ````````````````````
       Feature Upload Foto 
       
       ````````````````````
    */
    // const [imageBinaryPreviewUrl, setImageBinaryPreviewUrl] = useState(dataIdentitasInfoDasarAfterParse.foto);
    const [imageBinaryPreviewUrl, setImageBinaryPreviewUrl] = useState('');
    const [imgSrcExt, setImgSrcExt] = useState();

    const[ nameFile, setNameFile ] = useState('');

    // const [imgSrc, setImgSrc ] = useState();

    const onDrop = useCallback(acceptedFiles => {
        
      // Do something with the files
      console.log("acceptedFiles : ", acceptedFiles);
      setNameFile(acceptedFiles[0].name)

      //*
      const reader = new FileReader()
  
      reader.onabort = () => console.log('file reading was aborted')
      reader.onerror = () => console.log('file reading has failed')
      reader.onload = () => {

          // Do whatever you want with the file contents
        //   console.log("Reader : ", reader)
          const binaryStr = reader.result
          console.log("Binary String : ",binaryStr);

          setImageBinaryPreviewUrl(binaryStr);
          setImgSrcExt(extractImageFileExtensionFromBase64(binaryStr))

      };
  
      // acceptedFiles.forEach(file => reader.readAsBinaryString(file))
      acceptedFiles.forEach(file => reader.readAsDataURL(file))

  }, []);

    const {getRootProps, getInputProps, isDragActive} = useDropzone({onDrop});



    /*
        `````````````````````
        HANDLE NAME KEAHLIAN 

        ````````````````````
    */
    const [ nameKeahlian, setNameKeahlian ] = useState('');  

    const handleChangeKeahlian = (e) => {

        setNameKeahlian(e.target.value);

    };

    /*
        LEVELING
    */
   const [value, setValue] = useState(2);

    return (

        <Dialog
            open={isOpenModalCreate}
            onClose={() => setOpenModalCreate(false)}
            aria-labelledby="alert-dialog-title"
            aria-describedby="alert-dialog-description"
            >
            <DialogTitle id="alert-dialog-title" style={{textAlign: "left"}}>
                <Typography variant='subtitle1' className={classes.title}>
                    <b>Tambah Keahlian</b>
                </Typography>
            </DialogTitle>
            <DialogContent>

                <List>
                    <ListItem >  
                        <ListItemText 
                            primary={
                                <Typography variant='subtitle1' className={classes.titleForm}>
                                    <b>Nama Keahlian</b>
                                </Typography>
                            } 
                        />
                        <TextField
                            id="outlined-bare"
                            onChange= {handleChangeKeahlian}
                            value={nameKeahlian}
                            className={classes.textField}
                            // placeholder={'NIP'}
                            variant="outlined"
                            // error={infoError == true && npwp == '' ? true : false}
                            // helperText={infoError == true && npwp == '' ? "Wajib di isi" : ' '}
                            // required={true} 
                            // name='nip'
                            // color='primary'
                            // onKeyDown={handleEnterPress}
                            // disabled= {isLockedStatusState == 1 ? true : false}
                            // fullWidth
                        />
                    </ListItem>

                    <ListItem>

                        <ListItemText 
                            primary={
                                <Typography variant='subtitle1' className={classes.titleForm}>
                                    <b>Level</b>
                                </Typography>
                            } 
                        />
                        <Box component="fieldset" borderColor="transparent">
                            {/* <Typography component="legend">Controlled</Typography> */}
                            <Rating
                                name="simple-controlled"
                                value={value}
                                onChange={(event, newValue) => {
                                    setValue(newValue);
                                }}
                                IconContainerComponent={IconContainer}
                            />
                        </Box>
                    </ListItem>

                    <ListItem>  
                        <ListItemText 
                            primary={
                                <Typography variant='subtitle1' className={classes.titleForm}>
                                    <b>Sertifikat</b>
                                </Typography>
                            } 
                        />

                        <div {...getRootProps()}>
                            <input {...getInputProps()} />

                            {
                                imageBinaryPreviewUrl ? (

                                    <Typography variant='caption' className={classes.title}>
                                        <i>
                                            {nameFile}
                                        </i>
                                    </Typography>
                                    
                                ) : (<img src={PictPlusUploadDocument} className={classes.pictUpload} />)
                            }
                        </div>
                    </ListItem> 

{/*                 
                    <ListItem >  
                        <ListItemText 
                            primary={
                                <Typography variant='subtitle1' className={classes.titleForm}>
                                    <b>Level</b>
                                </Typography>
                            } 
                        />

                        <ListItemSecondaryAction className={classes.iconStarList}>
                            <IconButton 
                                onClick={toggleIconButtonFirst}
                                className={isYellowIconFirst == true ? classes.iconStartButton : null}
                                edge="start" 
                                aria-label="Delete" 
                            >
                                {
                                    isYellowIconFirst == true ? (<StarBorderIconFulled />) : (<StarBorderIcon />)
                                }
                            </IconButton>
                            <IconButton 
                                onClick={toggleIconButtonSecond}
                                className={isYellowIconSecond == true ? classes.iconStartButton : null}
                                edge="start" 
                                aria-label="Delete"
                            >
                                {
                                    isYellowIconSecond == true ? (<StarBorderIconFulled />) : (<StarBorderIcon />)
                                }
                                
                            </IconButton>
                            <IconButton 
                                onClick={toggleIconButtonThree}
                                className={isYellowIconThree == true ? classes.iconStartButton : null}
                                edge="start" 
                                aria-label="Delete"
                            >
                                {
                                    isYellowIconThree == true ? (<StarBorderIconFulled />) : (<StarBorderIcon />)
                                }
                            </IconButton>
                            <IconButton edge="start" aria-label="Delete">
                                <StarBorderIcon />
                            </IconButton>
                            <IconButton edge="start" aria-label="Delete">
                                <StarBorderIcon />
                            </IconButton>   
                            <span style={{fontFamily: 'Nunito', fontSize: 12}}>
                                <b><i>
                                    {
                                        beginnerText 
                                    }
                                    {
                                        mediumText
                                    }
                                </i></b>
                            </span>
                        </ListItemSecondaryAction>
                    </ListItem>

                    <ListItem>  
                        <ListItemText 
                            primary={
                                <Typography variant='subtitle1' className={classes.titleForm}>
                                    <b>Sertifikat</b>
                                </Typography>
                            } 
                        />

                        <div {...getRootProps()}>
                            <input {...getInputProps()} />

                            {
                                imageBinaryPreviewUrl ? (

                                    <Typography variant='caption' className={classes.title}>
                                        <i>
                                            {nameFile}
                                        </i>
                                    </Typography>
                                    
                                ) : (<img src={PictPlusUploadDocument} className={classes.pictUpload} />)
                            }
                        </div>
                    </ListItem> */}
                </List>

                <DialogContentText id="alert-dialog-description">
                    <Typography variant='h6'>

                    </Typography>
                </DialogContentText>
            </DialogContent>
            <DialogActions style={{alignItems: "right", justifyContent:'flex-end'}}>
                <Button 
                    onClick={() => setOpenModalCreate(false)} 
                    variant='outlined' 
                    size='small'
                    className={classes.buttonModalCancel}    
                >
                    Batal
                </Button>
                
                <Button 
                    // onClick={() => Redirect(ToCompletionProfile)}
                    // onClick= {handleSimpanDataKeahlian}
                    variant='contained' 
                    color="primary" 
                    size='small'
                    className={classes.buttonModal}
                >  
                    Simpan
                </Button>
            </DialogActions>
            <br />
            <br />
            </Dialog>



    )
}

export default DialogCreate;
 


const labels = {
    // 0.5: 'Useless',
    1: 'Newbie',
    // 1.5: 'Poor',
    2: 'Beginner',
    // 2.5: 'Ok',
    3: 'Medium',
    // 3.5: 'Good',
    4: 'High Medium',
    // 4.5: 'Excellent',
    5: 'Professional',
  };
  
  function IconContainer(props) {
    const { value, ...other } = props;
    return (
      <Tooltip title={labels[value] || ''}>
        <div {...other} />
      </Tooltip>
    );
  }