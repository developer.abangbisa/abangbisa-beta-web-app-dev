import React, { Component, useEffect, useState } from "react";
import { makeStyles, createMuiTheme, withStyles, MuiThemeProvider } from '@material-ui/core/styles';
import { 
    Container, Paper, Typography, Grid, TextField, Checkbox, CircularProgress,
    FormControlLabel, Box, Button, MenuItem, Dialog, DialogTitle, DialogContent, 
    DialogContentText, DialogActions, Breadcrumbs, Link, IconButton, InputBase, Divider,
    Tooltip, List, ListItem, ListItemText, Avatar, Table, TableBody, TableRow, TableCell,
    Chip, TablePagination, TableHead, TableSortLabel, Radio, FormLabel

} from '@material-ui/core';

import axios from 'axios';

import DialogTambahPosisiForProfile from '../../Kepegawaian/Components/DialogTambahPosisiForProfile';
import DialogDeletePosisiForProfile from '../../Kepegawaian/Components/DialogDeletePosisiForProfile';
import DialogEditPosisiForProfile from '../../Kepegawaian/Components/DialogEditPosisiForProfile';


import { URL_API } from '../../../../constants/config-api';

const InfoPosisi = props => {

    const { classes } = props;

    /*
        ```````````````````
        COMPONENT DID MOUNT

        ```````````````````
    */
    
    // 
    const userToken = localStorage.getItem('userToken');
    const employeeId = localStorage.getItem('employee_id');
    
    const [ userTokenState, setUserTokenState ] = useState('');
    const [ employeeIdState, setEmployeeIdState ] = useState('');

    const [ listPositionNow, setlistPositionNow ] = useState('');

    useEffect(() => {
        
        setUserTokenState(userToken);
        setEmployeeIdState(employeeId);

        if(userToken !== undefined ){

            const header =  {    
 
                'Accept': "application/json",
                'Content-Type' : "application/json",
                'Authorization' : "bearer " + userToken,
            };
 
            axios.defaults.headers.common = header;    

           

            /* GET LIST POSITION NOW */
            axios
                .get(URL_API + `/human-resource/employee/batch/${employeeId}`)
                .then(function(response){
                    
                    console.log("Response Original : ", response);

                    if(response.status == 200){

                        if(response.data.data.position !== null){
                            if(response.data.data.position.length > 0){

                                setlistPositionNow(response.data.data.position);
                            }
                        }
                    };
                })
                .catch(function(error){

                    if(error.response.status == 422){
                        alert("Error : Unprocesity entity !");
                    };

                    console.log("Error : ", error.response)
                })
 
        } else { console.log("No Access Token available!")};
        
    }, [])

    /*
        ``````````````````````````
        HANDLE MODAL TAMBAH POSISI 

        ``````````````````````````
    */

    const [ modalTambahPosition, setModalTambahPosition ] = useState(false);
   
       /*
        ``````````````
        HANDLE CHEKBOX

        ``````````````
    */
    const [ isChecked, setChecked ] = useState (
        {
            id: '',
            value: '',
            label: ''
        }
    );

    // const [ idPosition, setIdPosition ] = useState([])
    const handleChangeCheckbox = (e, data) => {

        e.preventDefault();

        // console.log("Data checkbox : ", data);

        if(data !== undefined){

            setChecked(data);
            // setIdPosition(data.id)

        };
    };


    /*
        ```````````````````
        HANDLE MODAL DELETE

        ```````````````````
    */


    const [ modalDelete, setModalDelete ] = useState(false);

    const [ idPosition, setIdPosition ] = useState('');
    const [ positionTitleName, setPositionTitleName ] = useState('');


    const handleModalDelete = (e, data) => {

        e.preventDefault();

        console.log("Delete posisi now: ", data);
      
        
        setIdPosition(data.id);
        setPositionTitleName(data.structure_position_title_name);

        
        setModalDelete(true);

    };


    /*
        ``````````````````````````
        HANDLE MODAL EDIT POSISI 

        ``````````````````````````
    */

   const [ modalEditPosition, setModalEditPosition ] = useState(false);

    return (
        <Paper elevation={1} className={classes.paperInfoDasar} square={true} style={{marginRight: 16, marginBottom: 16, color: 'grey'}}> 
            <br />
            <Grid container>
                <Grid item xs={10} style={{textAlign : 'left'}}>
                    <br />
                    <Typography 
                        variant='subtitle2' 
                        className={classes.titleTambahAnggotaKeluarga}
                        onClick={() => setModalTambahPosition(true)}
                    >
                        <b>+ Tambah Posisi</b> 
                    </Typography>
                    <br />
                        {
                            listPositionNow.length > 0 ? listPositionNow.map((item, i) => (

                                <Grid container key={i}>
                                    <Grid item xs={3}>
                                        <Typography variant='subtitle2' className={classes.titleHeader}>
                                            <b>Posisi {i + 1}</b> 
                                        </Typography>
                                    </Grid>
                                    <Grid item xs={9}>
                                    
                                        <Typography variant='subtitle2' className={classes.titleHeader}>
                                            <b>{item.structure_position_title_name}</b> &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;  

                                            <span 
                                                // onClick= {(e) => setModalEditPosition(e, item )}
                                                onClick={() => setModalEditPosition(true)}
                                                style={{color: '#cc0707', cursor: 'pointer'}}
                                            >
                                                Edit 
                                            </span> &nbsp;&nbsp;- &nbsp;&nbsp; 
                                            <span
                                                
                                                onClick={(e) => handleModalDelete(e, item)}
                                                style={{color: '#cc0707', cursor: 'pointer'}}>
                                                    Hapus
                                            </span>
                                        </Typography>

                                        <Typography variant='subtitle2' className={classes.titleHeader}>
                                            Di Unit : &nbsp;
                                            {
                                                item.structure_unit_name
                                            }
                                        </Typography>
                                        <br />
                                    </Grid>
                                </Grid>
                            )) : (
                                <Grid container>
                                    <Grid item sm={12} style={{textAlign: 'center'}}> 
                                        {/* <CircularProgress size={32} style={{color: '#cc0707'}} /> */}
                                        <Typography variant='subtitle2' className={classes.titleHeader}>
                                            Anda belum menambahkan posisi !
                                        </Typography>
                                    </Grid>
                                </Grid>
                            )
                        }

                </Grid>
                <Grid item xs={2} style={{textAlign : 'center'}}>
                    
                    {/* 
                        <IconButton>
                            <i className='material-icons'>edit</i>
                        </IconButton>  
                    */}

                </Grid>
            </Grid>
            <br />
            <br />
            <br />
            <br />
            <br />
            <br />
            <br />
            <br />
            <br />
            <br />
            <br />
            <br />
            <br />
            <br />
            <br />
            <br />
            <br />
            <br />
            <br />
            <br />
            <br />

            <DialogTambahPosisiForProfile 
                classes={classes}
                modalTambahPosition = { modalTambahPosition}
                setModalTambahPosition = { setModalTambahPosition }

                isChecked = { isChecked }
                handleChangeCheckbox = { handleChangeCheckbox }
                // handleSimpanData = { handleSimpanData }

                employeeIdState = { employeeIdState }
                setlistPositionNow = { setlistPositionNow }
            />

            <DialogDeletePosisiForProfile 
                classes = { classes }
                modalDelete = { modalDelete }
                setModalDelete = { setModalDelete }

                idPosition = { idPosition }
                positionTitleName = { positionTitleName }
                setlistPositionNow = { setlistPositionNow }
                listPositionNow = { listPositionNow }
            />


            <DialogEditPosisiForProfile 
                classes={classes}
                modalEditPosition = { modalEditPosition}
                setModalEditPosition = { setModalEditPosition }

                isChecked = { isChecked }
                handleChangeCheckbox = { handleChangeCheckbox }
                // handleSimpanData = { handleSimpanData }

                employeeIdState = { employeeIdState }
                setlistPositionNow = { setlistPositionNow }
            />
        </Paper>

    )
};

export default InfoPosisi;
