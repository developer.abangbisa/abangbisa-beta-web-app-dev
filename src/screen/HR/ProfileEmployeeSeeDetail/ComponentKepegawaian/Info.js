import React, { Component, useEffect, useState, useCallback } from "react";
import { makeStyles, createMuiTheme, withStyles, MuiThemeProvider } from '@material-ui/core/styles';
import { 
    Container, Paper, Typography, Grid, TextField, Checkbox, CircularProgress,
    FormControlLabel, Box, Button, MenuItem, Dialog, DialogTitle, DialogContent, 
    DialogContentText, DialogActions, Breadcrumbs, Link, IconButton, InputBase, Divider,
    Tooltip, List, ListItem, ListItemText, Avatar, Table, TableBody, TableRow, TableCell,
    Chip, TablePagination, TableHead, TableSortLabel, Radio, FormLabel, Snackbar, Fab
} from '@material-ui/core';

import InfoPegawai from './InfoPegawai';
import InfoPosisi from "./InfoPosisi";


const Info = props => {

    const { classes } = props;

    /*
        ````````````````````````````
        HANDLE TAB INFO KEPEGAWAIAN

        ````````````````````````````
    */

    let informationKepegawaianLet = {

        value: 'informasi-pegawai',
        label: 'Informasi Pegawai'
    };

    const [ isChoosedInfoKepegawaian, setChoosedInfoKepegawaian ] = useState(informationKepegawaianLet);

    const [ isInfoPegawai, setInfoPegawai ] = useState(true);
    const [ isInfoPosition, setInfoPosition ] = useState(false);

    const handleChooseTab = (item) => {

        console.log("Item : ", item);
        setChoosedInfoKepegawaian(item);

        if(item.value == 'informasi-pegawai'){

            setInfoPegawai(true);
            setInfoPosition(false);
        };

        if(item.value == 'informasi-posisi'){

            setInfoPosition(true);
            setInfoPegawai(false);
        };

    };

    return (

        <Grid container>

            <Grid item xs={4} style={{textAlign : 'left'}}>

                <Paper elevation={1} className={classes.paperInfoIdentitas} square={true}> 
                    <br />
                    <Typography variant='subtitle2' className={classes.titleHeader}>
                        <b>Informasi Kepegawaian</b> 
                    </Typography>
                    <ul>
                        {
                            informationKepegawaian.length > 0 && informationKepegawaian.map((item, i) => (

                                <li 
                                    key={i}
                                    className={isChoosedInfoKepegawaian.value == item.value ? classes.titleActive : classes.titleNotActive }  
                                    style={{margin: 8, cursor: 'pointer'}} 
                                    onClick={() => handleChooseTab(item)}
                                >

                                    <Typography 
                                        variant='subtitle2' 
                                        className={isChoosedInfoKepegawaian.value == item.value ? classes.titleActive : classes.titleNotActive }
                                    >
                                        <b>{item.label}</b> 
                                    </Typography>
                                </li>
                            ))
                        }
                    </ul>   
                    
                    <br />
                    <Typography variant='subtitle2' className={classes.titleHeader}>
                        <b>Eksport ke PDF</b> 
                    </Typography>
                    <Button variant='outlined' size='large' className={classes.buttonEksportPDF}>
                        <b>Eksport</b>
                    </Button>                 
                    <br />
                </Paper>
            </Grid>

            <Grid item xs={8} style={{textAlign : 'left '}}>
                    
                {
                    isInfoPegawai == true && (

                        <InfoPegawai classes={classes} />
                    )   
                }

                {   
                    isInfoPosition == true && (

                        <InfoPosisi classes={classes} />
                    )
                }
            </Grid>
        </Grid>
    )

};

export default Info;

const informationKepegawaian = [
    {
        value: 'informasi-pegawai',
        label: 'Informasi Pegawai'
    },
    {
        value: 'informasi-posisi',
        label: 'Informasi Posisi'
    },

    
]
