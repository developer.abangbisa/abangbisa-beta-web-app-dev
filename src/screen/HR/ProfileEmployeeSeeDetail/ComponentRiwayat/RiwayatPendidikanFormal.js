import React, { Component, useEffect, useState, Fragment } from "react";
import { makeStyles, createMuiTheme, withStyles, MuiThemeProvider } from '@material-ui/core/styles';
import { 
    Container, Paper, Typography, Grid, TextField, Checkbox, CircularProgress,
    FormControlLabel, Box, Button, MenuItem, Dialog, DialogTitle, DialogContent, 
    DialogContentText, DialogActions, Breadcrumbs, Link, IconButton, InputBase, Divider,
    Tooltip, List, ListItem, ListItemText, Avatar, Table, TableBody, TableRow, TableCell,
    Chip, TablePagination, TableHead, TableSortLabel, Radio, FormLabel

} from '@material-ui/core';

const RiwayatPendidikanFormal = props => {

    const { classes } = props;

    /* 
        ````````````````````
        COMPONENT DID MOUNT
        
        ````````````````````
    */

   const [ listDataPendidikanFormal, setListDataPendidikanFormal ] = useState('');

   const employeeDataRiwayatFormal = localStorage.getItem('employee_data_riwayat_formal');
   let employeeDataRiwayatFormalAfterParse = employeeDataRiwayatFormal !== null ? JSON.parse(employeeDataRiwayatFormal) : [];

   useEffect(() => {

       console.log("employeeDataRiwayatFormalAfterParse : ", employeeDataRiwayatFormalAfterParse)

       setListDataPendidikanFormal(employeeDataRiwayatFormalAfterParse);

   },[])


    return (
        <Fragment>

            <br />

            {
                listDataPendidikanFormal.length == 0 && (
    
                    <Typography 
                        variant='subtitle2' 
                        className={classes.titleTambahAnggotaKeluarga}
                        // onClick={handleTambahAnggotaKeluarga}
                    >
                        <b>+ Tambah Pendidikan Formal</b> 
                    </Typography>
                )
            }
            {
                listDataPendidikanFormal.length > 0 && listDataPendidikanFormal.map((item, i) => (
                    <Paper elevation={1} className={classes.paperInfoDasar} square={true} style={{marginRight: 16}} key={i}> 
                        <br />
                        <Typography variant='subtitle1' className={classes.titleHeader}>
                            <b>Pendidikan Formal - {i + 1}</b> 
                        </Typography> 
                        <br />

                        <List className={classes.list} >
                            <ListItem alignItems='flex-start'>  
                                <ListItemText 
                                    primary={
                                        <Typography variant='subtitle1' className={classes.titleForm}>
                                            <b>Nama Instansi</b>
                                        </Typography>
                                    } 
                                />
                
                                <TextField  
                                    id="outlined-bare"
                                    value={item.instansi}
                                    className={classes.textField}
                                    inputProps={{className: classes.title}} 
                                    disabled
                                    // onChange= {handleChangeNIP}
                                    // placeholder={'NIP'}
                                    // variant="outlined"
                                />
                            </ListItem>

                            <ListItem alignItems='flex-start'>  
                                <ListItemText 
                                    primary={
                                        <Typography variant='subtitle1' className={classes.titleForm}>
                                            <b>Program Studi</b>
                                        </Typography>
                                    } 
                                />
                
                                <TextField  
                                    id="outlined-bare"
                                    value={item.program_studi}
                                    className={classes.textField}
                                    inputProps={{className: classes.title}} 
                                    disabled
                                />
                            </ListItem>

                            <ListItem alignItems='flex-start'>  
                                <ListItemText 
                                    primary={
                                        <Typography variant='subtitle1' className={classes.titleForm}>
                                            <b>Jenjang</b>
                                        </Typography>
                                    } 
                                />
                
                                <TextField  
                                    id="outlined-bare"
                                    value={item !== undefined ? item.jenjang.value : ''}
                                    className={classes.textField}
                                    inputProps={{className: classes.title}} 
                                    disabled
                                />
                            </ListItem>

                            <ListItem alignItems='flex-start'>  
                                <ListItemText 
                                    primary={
                                        <Typography variant='subtitle1' className={classes.titleForm}>
                                            <b>Kota</b>
                                        </Typography>
                                    } 
                                />
                
                                <TextField  
                                    id="outlined-bare"
                                    value={item.kota}
                                    className={classes.textField}
                                    inputProps={{className: classes.title}} 
                                    disabled
                                />
                            </ListItem>

                            <ListItem alignItems='flex-start'>  
                                <ListItemText 
                                    primary={
                                        <Typography variant='subtitle1' className={classes.titleForm}>
                                            <b>Tahun Masuk</b>
                                        </Typography>
                                    } 
                                />
                
                                <TextField  
                                    id="outlined-bare"
                                    value={item.tahun_masuk}
                                    className={classes.textField}
                                    inputProps={{className: classes.title}} 
                                    disabled
                                />
                            </ListItem>

                            <ListItem alignItems='flex-start'>  
                                <ListItemText 
                                    primary={
                                        <Typography variant='subtitle1' className={classes.titleForm}>
                                            <b>Tahun Selesai</b>
                                        </Typography>
                                    } 
                                />
                
                                <TextField  
                                    id="outlined-bare"
                                    value={item.tahun_selesai}
                                    className={classes.textField}
                                    inputProps={{className: classes.title}} 
                                    disabled
                                />
                            </ListItem>

                            <ListItem alignItems='flex-start'>  
                                <ListItemText 
                                    primary={
                                        <Typography variant='subtitle1' className={classes.titleForm}>
                                            <b>Status Pelajar</b>
                                        </Typography>
                                    } 
                                />
                
                                <TextField  
                                    id="outlined-bare"
                                    value={item.status_pelajar}
                                    className={classes.textField}
                                    inputProps={{className: classes.title}} 
                                    disabled
                                />
                            </ListItem>

                            <ListItem alignItems='flex-start'>  
                                <ListItemText 
                                    primary={
                                        <Typography variant='subtitle1' className={classes.titleForm}>
                                            <b>Sertifikat</b>
                                        </Typography>
                                    } 
                                />
                
                                <TextField  
                                    id="outlined-bare"
                                    value={item.name_file}
                                    className={classes.textField}
                                    inputProps={{className: classes.title}} 
                                    disabled
                                />
                            </ListItem>


                        </List>
                    </Paper>
                ))
            }

            <br />
            <br />
            <br />
            <br />
        </Fragment>

    )
};

export default RiwayatPendidikanFormal;
