import React, { Component, useEffect, useState } from "react";
import { makeStyles, createMuiTheme, withStyles, MuiThemeProvider } from '@material-ui/core/styles';
import { 
    Container, Paper, Typography, Grid, TextField, Checkbox, CircularProgress,
    FormControlLabel, Box, Button, MenuItem, Dialog, DialogTitle, DialogContent, 
    DialogContentText, DialogActions, Breadcrumbs, Link, IconButton, InputBase, Divider,
    Tooltip, List, ListItem, ListItemText, Avatar, Table, TableBody, TableRow, TableCell,
    Chip, TablePagination, TableHead, TableSortLabel, Radio, FormLabel

} from '@material-ui/core';

const RiwayatPendidikanINFORMAL = props => {

    const { classes } = props;

    /* 
        ````````````````````
        COMPONENT DID MOUNT
        
        ````````````````````
    */

   const [ listDataPendidikanINFORMAL, setListDataPendidikanINFORMAL ] = useState('');

   const employeeDataRiwayatINFORMAL = localStorage.getItem('employee_data_riwayat_informal');
   let employeeDataRiwayatINFORMALAfterParse = employeeDataRiwayatINFORMAL !== null ? JSON.parse(employeeDataRiwayatINFORMAL) : [];

   useEffect(() => {

       console.log("employeeDataRiwayatINFORMALAfterParse : ", employeeDataRiwayatINFORMALAfterParse)

       setListDataPendidikanINFORMAL(employeeDataRiwayatINFORMALAfterParse);

   },[])


    return (
        <div>
            {
                listDataPendidikanINFORMAL.length > 0 && listDataPendidikanINFORMAL.map((item, i) => (
                    <Paper elevation={1} className={classes.paperInfoDasar} square={true} style={{marginRight: 16}} key={i}> 
                        <br />
                        <Typography variant='subtitle1' className={classes.titleHeader}>
                            <b>Pendidikan Informal - {i + 1}</b> 
                        </Typography> 
                        <br />

                        <List className={classes.list} >
                            <ListItem alignItems='flex-start'>  
                                <ListItemText 
                                    primary={
                                        <Typography variant='subtitle1' className={classes.titleForm}>
                                            <b>Nama Instansi</b>
                                        </Typography>
                                    } 
                                />
                
                                <TextField  
                                    id="outlined-bare"
                                    value={item.instansi}
                                    className={classes.textField}
                                    inputProps={{className: classes.title}} 
                                    disabled
                                    // onChange= {handleChangeNIP}
                                    // placeholder={'NIP'}
                                    // variant="outlined"
                                />
                            </ListItem>

                            <ListItem alignItems='flex-start'>  
                                <ListItemText 
                                    primary={
                                        <Typography variant='subtitle1' className={classes.titleForm}>
                                            <b>Nama Pendidikan</b>
                                        </Typography>
                                    } 
                                />
                
                                <TextField  
                                    id="outlined-bare"
                                    value={item.program_studi}
                                    className={classes.textField}
                                    inputProps={{className: classes.title}} 
                                    disabled
                                />
                            </ListItem>

                            <ListItem alignItems='flex-start'>  
                                <ListItemText 
                                    primary={
                                        <Typography variant='subtitle1' className={classes.titleForm}>
                                            <b>Jenis Pendidikan</b>
                                        </Typography>
                                    } 
                                />
                
                                <TextField  
                                    id="outlined-bare"
                                    value={item !== undefined ? item.jenjang.value : ''}
                                    className={classes.textField}
                                    inputProps={{className: classes.title}} 
                                    disabled
                                />
                            </ListItem>

                            <ListItem alignItems='flex-start'>  
                                <ListItemText 
                                    primary={
                                        <Typography variant='subtitle1' className={classes.titleForm}>
                                            <b>Kota</b>
                                        </Typography>
                                    } 
                                />
                
                                <TextField  
                                    id="outlined-bare"
                                    value={item.kota}
                                    className={classes.textField}
                                    inputProps={{className: classes.title}} 
                                    disabled
                                />
                            </ListItem>

                            <ListItem alignItems='flex-start'>  
                                <ListItemText 
                                    primary={
                                        <Typography variant='subtitle1' className={classes.titleForm}>
                                            <b>Tahun Masuk</b>
                                        </Typography>
                                    } 
                                />
                
                                <TextField  
                                    id="outlined-bare"
                                    value={item.tahun_masuk}
                                    className={classes.textField}
                                    inputProps={{className: classes.title}} 
                                    disabled
                                />
                            </ListItem>

                            <ListItem alignItems='flex-start'>  
                                <ListItemText 
                                    primary={
                                        <Typography variant='subtitle1' className={classes.titleForm}>
                                            <b>Tahun Selesai</b>
                                        </Typography>
                                    } 
                                />
                
                                <TextField  
                                    id="outlined-bare"
                                    value={item.tahun_selesai}
                                    className={classes.textField}
                                    inputProps={{className: classes.title}} 
                                    disabled
                                />
                            </ListItem>

                            <ListItem alignItems='flex-start'>  
                                <ListItemText 
                                    primary={
                                        <Typography variant='subtitle1' className={classes.titleForm}>
                                            <b>Sertifikat</b>
                                        </Typography>
                                    } 
                                />
                
                                <TextField  
                                    id="outlined-bare"
                                    value={item.name_file}
                                    className={classes.textField}
                                    inputProps={{className: classes.title}} 
                                    disabled
                                />
                            </ListItem>

                        </List>
                    </Paper>
                ))
            }

            <br />
            <br />
            <br />
            <br />
        </div>

    )
};

export default RiwayatPendidikanINFORMAL;
