import React, { Component, useEffect, useState } from "react";
import { makeStyles, createMuiTheme, withStyles, MuiThemeProvider } from '@material-ui/core/styles';
import { 
    Container, Paper, Typography, Grid, TextField, Checkbox, CircularProgress,
    FormControlLabel, Box, Button, MenuItem, Dialog, DialogTitle, DialogContent, 
    DialogContentText, DialogActions, Breadcrumbs, Link, IconButton, InputBase, Divider,
    Tooltip, List, ListItem, ListItemText, Avatar, Table, TableBody, TableRow, TableCell,
    Chip, TablePagination, TableHead, TableSortLabel, Radio, FormLabel

} from '@material-ui/core';

import axios from 'axios';
import { URL_API } from '../../../../constants/config-api';
import Snackbeer from '../../../../components/Snackbeer';

const InfoPegawai = props => {

    const { classes } = props;



    const userToken = localStorage.getItem('userToken');
    const employeeId = localStorage.getItem('employee_id');

    const [ userTokenState, setUserTokenState ] = useState('');
    const [ employeeIdState, setEmployeeIdState ] = useState('');
    const [updatedAt, setUpdatedAt ] = useState('');

    

    useEffect(() => {

        setUserTokenState(userToken);
        setEmployeeIdState(employeeId);

        const header =  {     
            'Accept': "application/json",
            'Content-Type' : "application/json",
            'Authorization' : "bearer " + userToken,
        };
    
        axios.defaults.headers.common = header;    

        axios
            .get(URL_API + `/human-resource/employee/batch/${employeeId}`)
            .then(function(response){
                
                console.log("Response Original DETAIL EMPLOYEE : ", response)

                if(response.status == 200){ 

                    setSelectedValue(response.data.data.member.status_id); //* 1 == Aktif 
                    setGolongan({
                        name: 'Permanen'
                    });
                    setStatusKerja(response.data.data.member.status_id);
                    setBpjs(response.data.data.social_security_number);
                    setBpjsKetenagakerjaan(response.data.data.employment_security_number);
                    setNoJaminanHariTua(response.data.data.retirement_security_number)

                    // setNamaDepan(response.data.data.member.first_name);
                    // setNamaBelakang(response.data.data.member.last_name);
                    // setNamaPanggilan(response.data.data.member.nickname);
            
                    // setNip(response.data.data.employee_number);        
                    // setNoHp(response.data.data.member.mobile_number_personal);
                    // setTanggalLahir(response.data.data.member.date_of_birth);
                    // setSelectedValueJenisKelamin(response.data.data.member.sex_id);
                    // setAgama(response.data.data.member.religion.name);
                    // setTempatLahir(response.data.data.member.place_of_birth)
                };
        })
        .catch(function(error){
            
            console.log("Error : ", error.response)
            
        });
            
        // axios
        //     .get(URL_API + `/human-resource/employee/batch/${employeeId}/patch`)
        //     .then(function(response){
                
        //         console.log("Response Original : ", response);

        //         if(response.status == 200){

        //             setUpdatedAt(response.data.data.fields.Employee.updated_at);     
        //         };
                
        //     })
        //     .catch(function(error){
                
        //         console.log("Error : ", error.response)
                
        //     });

    }, []);

    /* 
        ````````````````````
        COMPONENT DID MOUNT
        
        ````````````````````

            bpjs_kesehatan: "3455345345"
            bpjs_ketenagakerjaan: "345345345"
            golongan: "Esseloen 3"
            no_jaminan_hari_tua: ""
            penghargaan: "Juara 3 Marathon"
            status_karyawan: "aktif"
            status_kerja: "masih_terikat_kontrak"

    */

   const dataInfoPegawai = localStorage.getItem('employee_data_info_pegawai');
   const dataInfoPegawaiAfterParse = JSON.parse(dataInfoPegawai);
   
   useEffect(() => {

       console.log("dataInfoPegawaiAfterParse : ", dataInfoPegawaiAfterParse);
       
   }, []);

    /*
        ````````````
        HANDLE EDIT

        ````````````
    */
    const [ isTextFieldDisabled, setTextFieldDisabled ] = useState(false);
            
    const handleEdit = () => {

        setTextFieldDisabled(true)

    };


       /*
        `````````````
        RADIO BUTTON

        `````````````

    */

   const [selectedValue, setSelectedValue] = useState('aktif'); // 1 = Aktif, 2 = Non Aktif

   function handleChangeRadioButton(event) {

      setSelectedValue(event.target.value);

      if(event.target.value == 'aktif'){

          console.log(event.target.value);
          
          // callListMasterRole(event.target.value)

      } else {

          console.log(event.target.value);
      }
   };

       /*
        ````````
        GOLONGAN

        ````````
    */
    const [ golongan, setGolongan ] = useState({

        name: ''
    });

    const handleChangeGolongan = name => event => {

        setGolongan({ ...golongan, [name]: event.target.value });

    };

    /*
        `````````````
        STATUS KERJA

        ````````````
    */

    const [ statusKerja, setStatusKerja ] = useState({

        name: ''
    });

    const handleChangeStatusKerja = name => event => {

        setStatusKerja({ ...statusKerja, [name]: event.target.value });

    };

    /*
        ``````````````````
        NOTES PENGHARGAAN

        ``````````````````
    */

    const [ penghargaan, setPenghargaan] = useState('');
    const handleChangePenghargaan = (e) => setPenghargaan(e.target.value);


    /*
        `````````````````` ````
        - BPJS KESEHATAN

        - BPJS KETENGAKERJAAN
        
        - NOMOR JAMINAN HARI TUA

        ``````````````````````
    */

    const [ bpjs, setBpjs ] = useState('');
    const handleChangeBpjsKesehatan = (e) => setBpjs(e.target.value);

    const [ bpjsKetenagakerjaan, setBpjsKetenagakerjaan ] = useState('');
    const handleChangeBpjsKetenagakerjaan  = (e) => setBpjsKetenagakerjaan(e.target.value);

    const [ noJaminanHariTua, setNoJaminanHariTua ] = useState('');
    const handleChangeNoJaminanTua = (e) => setNoJaminanHariTua(e.target.value);

    /*
        ```````````````````````
        HANDLE SIMPAN PERUBAHAN

        ````````````````````````
    */

    const handleSimpanPerubahan = () => {

        setTextFieldDisabled(false);

        const userToken = localStorage.getItem('userToken');
    
        let data = {
            
            Employee: {

                // golongan: golongan //*Belum ada di backend
                // panghargaan: penghargaan
                status_id: selectedValue == "aktif" ? 1 : 2,
                employment_status_id: statusKerja.name !== '' ? statusKerja.name.id : '',
                social_security_number: bpjs,
                employment_security_number: bpjsKetenagakerjaan,
                retirement_security_number: noJaminanHariTua
                
                // updated_at: updatedAt
                
            },
            _method: 'patch'
        };

        console.log("Data : ", data);


        if(userTokenState !== undefined){
            
            const header =  {       
                'Accept': "application/json",
                'Content-Type' : "application/json",
                'Authorization' : "bearer " + userTokenState,
            };

            axios.defaults.headers.common = header;    

            axios
                .post(URL_API + `/human-resource/employee/${employeeIdState}`, data)
                .then(function(response){

                    console.log("Response Original : ", response)

                    if(response.status == 200 ){

                        setModalResponse200(true);

                        if(response.data.data !== undefined){
                        
                        };
                    };
                })
                .catch(function(error){
                    
                    alert('Whoops something went wrong !');
                    console.log("Error : ", error.response)
                    
                })

        } else { console.log("No Access Token available!")};
    };

    /*
        ```````````````````
        HANDLE RESPONSE 200
        
        ```````````````````
    */

    const [ isModalResponse200, setModalResponse200 ] = useState(false);

   return (

    <Paper elevation={1} className={classes.paperInfoDasar} square={true} style={{marginRight: 16, marginBottom: 16}}> 
        <br />

        <Grid container>
            <Grid item sm={8}>
                <Typography variant='subtitle1' className={classes.titleHeader}>
                    <b>Informasi Pegawai</b> 
                </Typography>
            </Grid>
            <Grid item sm={4} style={{textAlign : 'right'}}>
                <IconButton 
                    onClick={handleEdit}
                    className={classes.iconEdit}
                >
                    <i className='material-icons'>edit</i>
                </IconButton>
            </Grid>
        </Grid>

        <br />
        <List className={classes.list}>
            <ListItem alignItems='flex-start'>  
                <ListItemText 
                    primary={
                        <Typography variant='subtitle1' className={classes.titleForm}>
                            <b>Status Karyawan</b>
                        </Typography>
                    } 
                />

                {
                    isTextFieldDisabled !== true ? (
              
                        <TextField  
                            id="outlined-bare"
                            value={dataInfoPegawaiAfterParse !== null ? dataInfoPegawaiAfterParse.status_karyawan : ''}
                            className={classes.textField}
                            inputProps={{className: classes.title}} 
                            disabled={isTextFieldDisabled !== true ? true : false}                        
                        
                        />
                        
                    ) : (

                        <span style={{marginRight: 82}}>
                            <Radio 
                                checked={selectedValue === 'aktif'}
                                onChange={handleChangeRadioButton}
                                value="aktif"
                                name='Aktif'
                                // disabled= {isLockedStatusState == 1 ? true : false}
                            />
                            <span style={{fontFamily: 'Nunito'}}>Aktif</span>
    
                            <Radio 
                                checked={selectedValue === 'notAktif'}
                                onChange={handleChangeRadioButton}
                                value="notAktif"
                                name='Tidak Aktif'
                            />
                            <span style={{fontFamily: 'Nunito'}}>Tidak Aktif</span>
                        </span>
                    )
                }
            </ListItem>

            <ListItem alignItems='flex-start'>  
                <ListItemText 
                    primary={
                        <Typography variant='subtitle1' className={classes.titleForm}>
                            <b>Golongan</b>
                        </Typography>
                    } 
                />

                {
                    isTextFieldDisabled !== true ? (

                        <TextField  
                            id="outlined-bare"
                            value={dataInfoPegawaiAfterParse !== null ? dataInfoPegawaiAfterParse.golongan : ''}
                            className={classes.textField}
                            inputProps={{className: classes.title}} 
                            disabled
                        />

                    ) : (

                        <TextField
                            id="outlined-select-provinsi"
                            select
                            // label="Pilih Negara : "
                            className={classes.textField}
                            value={golongan.name}
                            onChange={handleChangeGolongan('name')}
                            SelectProps={{
                                MenuProps: {
                                    className: classes.menu,
                                },
                            }}
                            margin="normal"
                            variant="outlined"
                            // error={infoError == true && provinsi.name == '' ? true : false}
                            // helperText={infoError == true && provinsi.name == '' ? "Wajib di isi" : ' '}
                        >
                            {
                                golongans.map (

                                    option => (
                                        <MenuItem key={option.value} value={option.value}>
                                            {option.label}
                                        </MenuItem>
                                    )
                                )
                            }

                        </TextField>
                    )
                }

            </ListItem>

            <ListItem alignItems='flex-start'>  
                <ListItemText 
                    primary={
                        <Typography variant='subtitle1' className={classes.titleForm}>
                            <b>Status Kerja</b>
                        </Typography>
                    } 
                />

                {
                    isTextFieldDisabled !== true ? (

                        <TextField  
                            id="outlined-bare"
                            value={dataInfoPegawaiAfterParse !== null ? dataInfoPegawaiAfterParse.status_kerja.label : ''}
                            className={classes.textField}
                            inputProps={{className: classes.title}} 
                            disabled
                        />

                    ) : (

                        <TextField
                            id="outlined-select-provinsi"
                            select
                            // label="Pilih Negara : "
                            className={classes.textField}
                            value={statusKerja.name}
                            onChange={handleChangeStatusKerja('name')}
                            SelectProps={{
                                MenuProps: {
                                    className: classes.menu,
                                },
                            }}
                            margin="normal"
                            variant="outlined"
                        >
                            {
                                statusKerjas.map (

                                    option => (
                                        <MenuItem key={option.value} value={option}>
                                            {option.label}
                                        </MenuItem>
                                    )
                                )
                            }

                        </TextField>
                    )
                }
            </ListItem>

            <ListItem alignItems='flex-start'>  
                <ListItemText 
                    primary={
                        <Typography variant='subtitle1' className={classes.titleForm}>
                            <b>Penghargaan</b>
                        </Typography>
                    } 
                />

                <TextField  
                    id="outlined-bare"
                    onChange={handleChangePenghargaan}
                    value={penghargaan}
                    placeholder={penghargaan !== '' ? penghargaan : "-"}
                    // value={dataInfoPegawaiAfterParse.penghargaan}
                    className={classes.textField}
                    inputProps={{className: classes.title}} 
                    disabled={ isTextFieldDisabled !== true ? true : false }                        
                />
            </ListItem>

            <ListItem alignItems='flex-start'>  
                <ListItemText 
                    primary={
                        <Typography variant='subtitle1' className={classes.titleForm}>
                            <b>Nomor BPJS Kesehatan</b>
                        </Typography>
                    } 
                />

                <TextField  
                    id="outlined-bare"
                    onChange={handleChangeBpjsKesehatan}
                    value={bpjs}
                    // value={dataInfoPegawaiAfterParse.bpjs_kesehatan}
                    className={classes.textField}
                    inputProps={{className: classes.title}} 
                    disabled={ isTextFieldDisabled !== true ? true : false }                                              
                />
            </ListItem>

            <ListItem alignItems='flex-start'>  
                <ListItemText 
                    primary={
                        <Typography variant='subtitle1' className={classes.titleForm}>
                            <b>Nomor BPJS Ketenagakerjaan</b>
                        </Typography>
                    } 
                />

                <TextField  
                    id="outlined-bare"
                    onChange={handleChangeBpjsKetenagakerjaan}
                    value={bpjsKetenagakerjaan}
                    // value={dataInfoPegawaiAfterParse.bpjs_ketenagakerjaan}
                    className={classes.textField}
                    inputProps={{className: classes.title}} 
                    disabled={ isTextFieldDisabled !== true ? true : false }                                            
                />
            </ListItem>

            <ListItem alignItems='flex-start'>  
                <ListItemText 
                    primary={
                        <Typography variant='subtitle1' className={classes.titleForm}>
                            <b>Nomor Jaminan Hari Tua</b>
                        </Typography>
                    } 
                />

                <TextField  
                    id="outlined-bare"
                    onChange={handleChangeNoJaminanTua}
                    value={noJaminanHariTua}
                    // value={dataInfoPegawaiAfterParse.no_jaminan_hari_tua}
                    className={classes.textField}
                    inputProps={{className: classes.title}} 
                    disabled={ isTextFieldDisabled !== true ? true : false }                                                                
                />
            </ListItem>
        </List>

        <Grid container>
            <Grid item sm={12} style={{textAlign: 'right'}}>

                {
                    isTextFieldDisabled == true ? (
                        <Button 
                            variant='contained'
                            className={classes.button}
                            style={{marginRight: 32, marginBottom: 24, marginTop: 24}}
                            onClick={handleSimpanPerubahan}
                            
                        >
                            Simpan Perubahan
                        </Button>

                    ) : null
                }

            </Grid>
        </Grid>

        <Snackbeer
            classes={classes}
            isModalResponse200= {isModalResponse200}
            setModalResponse200 = {setModalResponse200}
            messages = 'Perubahan data berhasil di simpan !'
        />
    </Paper>
   )
};

export default InfoPegawai;

const golongans = [
    {
        value: 'Esseloen 1',
        label: 'Esseloen 1'
    },
    {
        value: 'Esseloen 2',
        label: 'Esseloen 2'
    },
    {
        value: 'Esseloen 3',
        label: 'Esseloen 3'
    }
];


const statusKerjas = [

    {
        id: 1,
        value: 'permanen',
        label: 'Permanen'
    },
    {
        id: 2,
        value: 'kontrak',
        label: 'Masih Terikat Kontrak'
    },
    {
        id: 3,
        value: 'freelance',
        label: 'Freelance'
    },
    {
        id: 4,
        value: 'magang',
        label: 'Magang'
    }
];