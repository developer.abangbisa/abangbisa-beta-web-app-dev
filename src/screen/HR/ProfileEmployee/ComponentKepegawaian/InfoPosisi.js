import React, { Component, useEffect, useState } from "react";
import { makeStyles, createMuiTheme, withStyles, MuiThemeProvider } from '@material-ui/core/styles';
import { 
    Container, Paper, Typography, Grid, TextField, Checkbox, CircularProgress,
    FormControlLabel, Box, Button, MenuItem, Dialog, DialogTitle, DialogContent, 
    DialogContentText, DialogActions, Breadcrumbs, Link, IconButton, InputBase, Divider,
    Tooltip, List, ListItem, ListItemText, Avatar, Table, TableBody, TableRow, TableCell,
    Chip, TablePagination, TableHead, TableSortLabel, Radio, FormLabel

} from '@material-ui/core';

import DialogTambahPosisiForProfile from '../../Kepegawaian/Components/DialogTambahPosisiForProfile';

const InfoPosisi = props => {

    const { classes } = props;

    /*
        ```````````````````
        COMPONENT DID MOUNT

        ```````````````````
    */

    const employeeId = localStorage.getItem('employee_id');
    const [ employeeIdState, setEmployeeIdState ] = useState('');


    const [ listPosisitionForLocal, setListPositionForLocal ] = useState('');
        
    let dataList = [];
    const employeeDataPosition = localStorage.getItem('employee_data_info_position');
    let employeeDataPositionAfterParse = employeeDataPosition !== null ? JSON.parse(employeeDataPosition) : [];

    useEffect(() => {

        console.log("employeeDataKeluargaAfterParse : ", employeeDataPositionAfterParse);
        setListPositionForLocal(employeeDataPositionAfterParse);

    },[]);

    /*
        ````````````````````
        HANDLE TAMBAH POSISI 

        ````````````````````
    */
    const [ modalTambahPosition, setModalTambahPosition ] = useState(false);
   
    /*
        ``````````````
        HANDLE CHEKBOX

        ``````````````
    */
     const [ isChecked, setChecked ] = useState (
        {
            id: '',
            value: '',
            label: ''
        }
    );

    // const [ idPosition, setIdPosition ] = useState([])
    const handleChangeCheckbox = (e, data) => {

        e.preventDefault();

        console.log("Data checkbox : ", data);

        if(data !== undefined){

            setChecked(data);

            // setIdPosition(data.id)

        };
    };


    const handleSimpanData = () => {

        // let data = {
        
        //     position_id: isChecked
        
        // };

        console.log("Data checked : ", isChecked);
        
        if(listPosisitionForLocal.length > 0){
            
            console.log("Run v1")
            
            const newList = [...listPosisitionForLocal, isChecked];

            console.log("newList : ", newList);

            localStorage.setItem('employee_data_info_position', JSON.stringify(newList));
            setListPositionForLocal([...listPosisitionForLocal, isChecked])

        } else {

            console.log("Run v2");
            
            dataList.push(isChecked);
            localStorage.setItem('employee_data_info_position', JSON.stringify(dataList));
            // localStorage.setItem('employee_data_info_position', JSON.stringify(data));


            setListPositionForLocal(isChecked);
            window.location.reload();
        };
        

        setModalTambahPosition(false);

    };

    return (
        <Paper elevation={1} className={classes.paperInfoDasar} square={true} style={{marginRight: 16, marginBottom: 16, color: 'grey'}}> 
            <br />
            <Grid container>
                <Grid item xs={10} style={{textAlign : 'left'}}>
                    <br />
                    <Typography 
                        variant='subtitle2' 
                        className={classes.titleTambahAnggotaKeluarga}
                        onClick={() => setModalTambahPosition(true)}
                        // onClick={handleTambahAnggotaKeluarga}
                    >
                        <b>+ Tambah Posisi</b> 
                    </Typography>
                    <br />
                        {
                            listPosisitionForLocal.length > 0 && listPosisitionForLocal.map((item, i) => (

                                <Grid container key={i}>
                                    <Grid item xs={3}>
                                        <Typography variant='subtitle2' className={classes.titleHeader}>
                                            <b>Posisi {i + 1}</b> 
                                        </Typography>
                                    </Grid>
                                    <Grid item xs={9}>
                                    
                                        <Typography variant='subtitle2' className={classes.titleHeader}>
                                            <b>{item.structure_position_title_name}</b> &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;  

                                            <span 
                                                // onClick= {(e) => handleEditDataDetail(e, item )}
                                                style={{color: '#cc0707', cursor: 'pointer'}}
                                            >
                                                Edit 
                                            </span> &nbsp;&nbsp;- &nbsp;&nbsp; 
                                            <span
                                                
                                                onClick={() => localStorage.removeItem('employee_data_info_position')}
                                                style={{color: '#cc0707', cursor: 'pointer'}}>
                                                    Hapus
                                            </span>
                                        </Typography>

                                        <Typography variant='subtitle2' className={classes.titleHeader}>
                                            Di Unit : &nbsp;
                                            {
                                                item.structure_unit_name
                                            }
                                        </Typography>
                                        <br />
                                    </Grid>
                                </Grid>
                            )) 
                        }

                </Grid>
                <Grid item xs={2} style={{textAlign : 'center'}}>
                    
                        {/* <IconButton>
                            <i className='material-icons'>edit</i>
                        </IconButton>  */}
                   
                </Grid>
            </Grid>
            <br />
            <br />
            <br />
            <br />
            <br />
            <br />
            <br />
            <br />
            <br />
            <br />
            <br />
            <br />
            <br />
            <br />
            <br />
            <br />
            <br />
            <br />
            <br />
            <br />
            <br />

            <DialogTambahPosisiForProfile 
                classes={classes}
                modalTambahPosition = { modalTambahPosition}
                setModalTambahPosition = { setModalTambahPosition }

                isChecked = { isChecked }
                handleChangeCheckbox = { handleChangeCheckbox }
                handleSimpanData = { handleSimpanData }
                
                employeeIdState = { employeeIdState }
            />
        </Paper>

    )
};

export default InfoPosisi;
