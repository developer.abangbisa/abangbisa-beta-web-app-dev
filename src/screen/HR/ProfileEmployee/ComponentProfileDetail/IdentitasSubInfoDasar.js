import React, { Component, useEffect, useState, useCallback } from "react";
import { makeStyles, createMuiTheme, withStyles, MuiThemeProvider } from '@material-ui/core/styles';
import { 
    Container, Paper, Typography, Grid, TextField, Checkbox, CircularProgress,
    FormControlLabel, Box, Button, MenuItem, Dialog, DialogTitle, DialogContent, 
    DialogContentText, DialogActions, Breadcrumbs, Link, IconButton, InputBase, Divider,
    Tooltip, List, ListItem, ListItemText, Avatar, Table, TableBody, TableRow, TableCell,
    Chip, TablePagination, TableHead, TableSortLabel, Radio, FormLabel, Snackbar, Fab

} from '@material-ui/core';

import axios from 'axios';
import {useDropzone} from 'react-dropzone';



import Snackbeer from '../../../../components/Snackbeer';
import Redirect from '../../../../utilities/Redirect';
import { ToHrEmployeeIdentitasAlamat } from '../../../../constants/config-redirect-url';
import { URL_API } from '../../../../constants/config-api';
import { extractImageFileExtensionFromBase64 } from '../../../../utilities/ReusableUtils';

import AvatarDummy from '../../../../assets/images/Avatar_dummy.png';
import { useGetHttp } from '../../../../utilities-hook/useGetHttp';

const IdentitasSubInfoDasar = props => {

    const { classes } = props;

    const employeeId = localStorage.getItem('employee_id');

    const [loading, fetchedData, setFetchedData ] = useGetHttp(URL_API + `/human-resource/employee/batch/${employeeId}/patch`, []);

    let updatedAt = null;
    if(fetchedData !== null){

        console.log("fetchedData : ", fetchedData);

        if(fetchedData.fields !== null){
            
            updatedAt = fetchedData.fields.Employee.updated_at

        };
    }

    /* 
        ````````````````````
        COMPONENT DID MOUNT
        
        ````````````````````
    */

    const dataIdentitasInfoDasar = localStorage.getItem('data_identitas');
    const dataIdentitasInfoDasarAfterParse = JSON.parse(dataIdentitasInfoDasar);
   
    useEffect(() => {

        console.log("dataIdentitasInfoDasarAfterParse : ", dataIdentitasInfoDasarAfterParse);
        
    }, []);

    /* 
        ````````````````````
        Feature Upload Foto 
        
        ````````````````````
    */
    const [imageBinaryPreviewUrl, setImageBinaryPreviewUrl] = useState(dataIdentitasInfoDasarAfterParse !== null ? dataIdentitasInfoDasarAfterParse.foto : '');
    const [imgSrcExt, setImgSrcExt] = useState();

    // const [imgSrc, setImgSrc ] = useState();

    const onDrop = useCallback(acceptedFiles => {
      
      // Do something with the files

      //*
      const reader = new FileReader()
  
      reader.onabort = () => console.log('file reading was aborted')
      reader.onerror = () => console.log('file reading has failed')
      reader.onload = () => {

          // Do whatever you want with the file contents
          const binaryStr = reader.result;
          console.log("Binary String : ",binaryStr);

          setImageBinaryPreviewUrl(binaryStr);
          setImgSrcExt(extractImageFileExtensionFromBase64(binaryStr))

      };
  
      // acceptedFiles.forEach(file => reader.readAsBinaryString(file))
      acceptedFiles.forEach(file => reader.readAsDataURL(file))

  }, []);

    const {getRootProps, getInputProps, isDragActive} = useDropzone({onDrop});
    
    /* 
        ```````````
        HANDLE EDIT
        
        ```````````
    */

    
    const [ isTextFieldDisabled, setTextFieldDisabled ] = useState(false);
    
    const handleEdit = () => {

        setTextFieldDisabled(true)

    };
   
    /*
        `````````
        FORM DATA

        `````````
    */

    const [nip, setNip] = useState('');
    const [namaDepan, setNamaDepan] = useState('');
    const [namaBelakang, setNamaBelakang] = useState('');
    const [namaPanggilan, setNamaPanggilan] = useState('');
    const [noHp, setNoHp] = useState('');
    const [email, setEmail] = useState('');
    const [tempatLahir, setTempatLahir] = useState('');
    const [tanggalLahir, setTanggalLahir ] = useState('');


    const handleChangeNIP = (e) => setNip(e.target.value);
    const handleChangeNamaDepan = (e) => setNamaDepan(e.target.value);
    const handleChangeNamaBelakang = (e) => setNamaBelakang(e.target.value);
    const handleChangeNamaPanggilan = (e) => setNamaPanggilan(e.target.value);
    const handleChangeNoHp = (e) => setNoHp(e.target.value);
    const handleChangeEmail = (e) => setEmail(e.target.value);
    const handleChangeTempatLahir = (e) => setTempatLahir(e.target.value);
    const handleChangeTanggalLahir = (e) => setTanggalLahir(e.target.value);

    const [selectedValue, setSelectedValue] = useState('lakiLaki');

    function handleChangeRadioButton(event) {

        setSelectedValue(event.target.value);

        if(event.target.value == 'lakiLaki'){

            console.log(event.target.value);
            
            // callListMasterRole(event.target.value)

        } else {

            console.log(event.target.value);
        };
    };

    /*
        ```````````````````````
        HANDLE SIMPAN PERUBAHAN

        ````````````````````````
    */
    const handleSimpanPerubahan = () => {

        setTextFieldDisabled(false);

        const userToken = localStorage.getItem('userToken');
        
        let data = {
            
            Employee: {
                
                employee_number: nip !== '' ? nip : dataIdentitasInfoDasarAfterParse.nip,
                first_name : namaDepan !== '' ? namaDepan : dataIdentitasInfoDasarAfterParse.nama_depan,
                last_name: namaBelakang !== '' ? namaBelakang : dataIdentitasInfoDasarAfterParse.nama_belakang,
                nickname: namaPanggilan !== '' ? namaPanggilan : dataIdentitasInfoDasarAfterParse.nama_panggilan,
                mobile_number_personal: noHp !== '' ? noHp : dataIdentitasInfoDasarAfterParse.no_hp,
                date_of_birth: dataIdentitasInfoDasarAfterParse.tanggal_lahir,
                religion_id: dataIdentitasInfoDasarAfterParse.agama.id,
                sex_id: dataIdentitasInfoDasarAfterParse.jenis_kelamin == 'lakiLaki' ? 1 : 2,
                updated_at: updatedAt
            },
            _method: 'patch'
        };

        console.log("Simpan Perubahan ... : ", data);
        // alert('API update is still revision...');
        
        if(userToken !== undefined){
            
            const header =  {       
                'Accept': "application/json",
                'Content-Type' : "application/json",
                // 'Content-Type' : "",
                'Authorization' : "bearer " + userToken,
            };

            axios.defaults.headers.common = header;    

            axios
                .post(URL_API + `/human-resource/employee/${employeeId}`, data)
                .then(function(response){

                    console.log("Response Original : ", response)

                    if(response.status == 200 ){

                        setModalResponse200(true);

                        if(response.data.data !== undefined){
                          
                        };
                    };
                })
                .catch(function(error){
                    
                    alert('Whoops something went wrong !');
                    console.log("Error : ", error.response)
                    
                })

        } else { console.log("No Access Token available!")};

    };

    /*
        ```````````````
        HANDLE SNACKBAR

        ```````````````
    */

    const [ isModalResponse200, setModalResponse200 ] = useState(false);

    return (

        <Paper elevation={1} className={classes.paperInfoDasar} square={true} style={{marginRight: 16}}> 
            <br />

            <Grid container>
                <Grid item sm={10}>
                    <Typography variant='subtitle1' className={classes.titleHeader}>
                        <b>Informasi Dasar</b> 
                    </Typography>             
                </Grid>
                <Grid item sm={2}>
                    <IconButton
                        onClick={handleEdit }
                    >
                        <i className='material-icons'>
                            edit
                        </i>
                    </IconButton>
                </Grid>
            </Grid>


            <br />
            <List className={classes.list}>

                <ListItem style={{textAlign: 'left'}}>  
                    <ListItemText primary = {
                            <Typography variant='subtitle1' className={classes.titleForm}>
                                <b>Foto
                                    {/* <span style={{color: '#cc0707'}}>*</span> */}
                                </b>
                            </Typography>
                        } 
                    />

                    <div {...getRootProps()}>
                        <input {...getInputProps()} />
                        {
                            imageBinaryPreviewUrl ? (
                                <Avatar alt="You" src={ imageBinaryPreviewUrl} className={classes.realAvatar}  />
                                
                            ) : (
                                    <Avatar alt="You" 
                                        src={AvatarDummy} className={classes.bigAvatar} />
                            )
                        }
                    </div>

                </ListItem>
                <br />

                <ListItem alignItems='flex-start'>  
                    <ListItemText 
                        primary={
                            <Typography variant='subtitle1' className={classes.titleForm}>
                                <b>NIP</b>
                            </Typography>
                        } 
                    />
    
                    <TextField  
                        id="outlined-bare"
                        onChange={handleChangeNIP}
                        value={nip}
                        placeholder={dataIdentitasInfoDasarAfterParse !== null ? dataIdentitasInfoDasarAfterParse.nip : ''}
                        className={classes.textField}
                        inputProps={{className: classes.titleTextField}} 
                        disabled={isTextFieldDisabled !== true ? true : false}
                        // onChange= {handleChangeNIP}
                        // placeholder={'NIP'}
                        // variant="outlined"
                    />
                </ListItem>

                <ListItem alignItems='flex-start'>  
                    <ListItemText 
                        primary={
                            <Typography variant='subtitle1' className={classes.titleForm}>
                                <b>Nama Depan</b>
                            </Typography>
                        } 
                    />
                    <TextField  
                        id="outlined-bare"
                        onChange={handleChangeNamaDepan}
                        value={namaDepan}
                        placeholder={dataIdentitasInfoDasarAfterParse !== null ? dataIdentitasInfoDasarAfterParse.nama_depan : ''}
                        className={classes.textField}
                        inputProps={{className: classes.titleTextField}} 
                        disabled={isTextFieldDisabled !== true ? true : false}                        
                    />

                </ListItem>

                <ListItem  alignItems='center'>  
                    <ListItemText 
                        primary={
                            <Typography variant='subtitle1' className={classes.titleForm} >
                                <b>Nama Belakang</b>
                            </Typography>
                        } 
                    />
                    <TextField  
                        id="outlined-bare"
                        onChange={handleChangeNamaBelakang}
                        value={namaBelakang}
                        placeholder={dataIdentitasInfoDasarAfterParse !== null ? dataIdentitasInfoDasarAfterParse.nama_belakang : ''}
                        className={classes.textField}
                        inputProps={{className: classes.titleTextField}} 
                        disabled={isTextFieldDisabled !== true ? true : false}                        
                    />
                </ListItem>

                <ListItem  style={{textAlign: 'left'}}>  
                    <ListItemText 
                        primary={
                            <Typography variant='subtitle1' className={classes.titleForm}>
                                <b>Nama Panggilan</b>
                            </Typography>
                        } 
                    />
                    <TextField  
                        id="outlined-bare"
                        onChange={handleChangeNamaPanggilan}
                        value={namaPanggilan}
                        placeholder={dataIdentitasInfoDasarAfterParse !== null ? dataIdentitasInfoDasarAfterParse.nama_panggilan : ''}
                        className={classes.textField}
                        inputProps={{className: classes.titleTextField}} 
                        disabled={isTextFieldDisabled !== true ? true : false}
                        
                    />
                </ListItem>

                <ListItem  style={{textAlign: 'left'}}>  
                    <ListItemText 
                        primary = {
                            <Typography variant='subtitle1' className={classes.titleForm} >
                                <b>No Handphone</b>
                            </Typography>
                        } 
                    />
                    <TextField 
                        id="outlined-bare"
                        onChange={handleChangeNoHp}
                        value={noHp}
                        placeholder={dataIdentitasInfoDasarAfterParse !== null ? dataIdentitasInfoDasarAfterParse.no_hp : ''}
                        className={classes.textField}
                        inputProps={{className: classes.titleTextField}} 
                        disabled={isTextFieldDisabled !== true ? true : false}
                        
                    />
                </ListItem>

                <ListItem  style={{textAlign: 'left'}}>  
                    <ListItemText 
                        primary={
                            <Typography variant='subtitle1' className={classes.titleForm} >
                                <b>Tanggal Lahir</b>
                            </Typography>
                        } 
                    />
                    <TextField 
                        id="outlined-bare"
                        value={tanggalLahir}
                        placeholder={dataIdentitasInfoDasarAfterParse !== null ? dataIdentitasInfoDasarAfterParse.tanggal_lahir : ''}
                        className={classes.textField}
                        inputProps={{className: classes.titleTextField}} 
                        disabled
                        
                    />
                </ListItem>

                <ListItem  style={{textAlign: 'left'}}>  
                    <ListItemText 
                        primary={
                            <Typography variant='subtitle1' className={classes.titleForm} >
                                <b>Agama</b>
                            </Typography>
                        } 
                    />
                    <TextField 
                        id="outlined-bare"
                        // value={selectedValue}
                        placeholder={dataIdentitasInfoDasarAfterParse !== null ? dataIdentitasInfoDasarAfterParse.agama.value : ''}
                        className={classes.textField}
                        inputProps={{className: classes.titleTextField}} 
                        disabled
                    />
                </ListItem>

                <ListItem  style={{textAlign: 'left'}}>  
                    <ListItemText 
                        primary = {
                            <Typography variant='subtitle1' className={classes.titleForm} >
                                <b>Jenis Kelamin</b>
                            </Typography>
                        } 
                    />
                    <TextField 
                        id="outlined-bare"
                        // value={}
                        placeholder={dataIdentitasInfoDasarAfterParse !== null ? dataIdentitasInfoDasarAfterParse.jenis_kelamin : ''}
                        className={classes.textField}
                        inputProps={{className: classes.titleTextField}} 
                        disabled
                    />                
                </ListItem>
            </List>

            <Grid container>
                <Grid item sm={12} style={{textAlign: 'right'}}>

                    {
                        isTextFieldDisabled == true ? (
                            <Button 
                                variant='contained'
                                className={classes.button}
                                style={{marginRight: 32, marginBottom: 24, marginTop: 24}}
                                onClick={handleSimpanPerubahan}
                                
                            >
                                Simpan Perubahan
                            </Button>

                        ) : null
                    }

                </Grid>
            </Grid>
            
            <Snackbeer
                classes={classes}
                isModalResponse200= {isModalResponse200}
                setModalResponse200 = {setModalResponse200}
                messages = 'Perubahan data berhasil di simpan !'
            />
        </Paper>
    )
};

export default IdentitasSubInfoDasar;