import React, { Component, useEffect, useState } from "react";
import { makeStyles, createMuiTheme, withStyles, MuiThemeProvider } from '@material-ui/core/styles';
import { 
    Container, Paper, Typography, Grid, TextField, Checkbox, CircularProgress,
    FormControlLabel, Box, Button, MenuItem, Dialog, DialogTitle, DialogContent, 
    DialogContentText, DialogActions, Breadcrumbs, Link, IconButton, InputBase, Divider,
    Tooltip, List, ListItem, ListItemText, Avatar, Table, TableBody, TableRow, TableCell,
    Chip, TablePagination, TableHead, TableSortLabel

} from '@material-ui/core';

import axios from 'axios';

import SearchIcon from '@material-ui/icons/Search';
import DoneIcon from '@material-ui/icons/Done';    
import LensIcon from '@material-ui/icons/LensOutlined'; 

import ButtonColorGrey from '../../../components/ButtonColorGrey';
import EnhancedTableToolbar from './Components/EnhancedTableToolbar';
import LightTooltip from './Components/LightTooltip';

import PictDefaultDashboard from '../../../assets/images/Mask_Group_2.png';
import IconPreviewGrid from '../../../assets/images/SVG/Group_1002.svg';
import IconPreviewList from '../../../assets/images/SVG/Group_1001_red.svg';
import IconFilter from '../../../assets/images/SVG/Group_1250.svg';
import IconFilterNew from '../../../assets/images/SVG/Group_1117.svg';

import Redirect from '../../../utilities/Redirect';
import { URL_API } from '../../../constants/config-api';
import { 
        ToMembershipStatus, 
        ToHrEmployeeIdentitas, 
        ToHrEmployeeProfileDetail, 
        ToHrEmployeeProfileDetailSeeDetail

     } from '../../../constants/config-redirect-url';

import { styles } from './Style/StyleEmpty';

const theme = createMuiTheme({
    
    palette: {

        primary: {
            main: '#cc0707', //#cc0707, #c62828
            light: '#ff5f52',
            dark: '#8e0000',
            contrastText: '#ffffff'
        }
    }       
});
    
const ViewEmpty = props => {

    const { classes } = props;

    const userToken =localStorage.getItem('userToken');

    const [ userTokenState, setUserTokenState ] = useState('');
    const [ data, setData ] = useState([]);

    const [ loader, setLoader ] = useState(false);

    useEffect(() => {

        setData([]);
        setLoader(true);
        
        setUserTokenState(userToken);

        if(userToken !== undefined){

            const headers =  {

                'Accept': "application/json",
                'Content-Type' : "application/json",
                'Access-Control-Allow-Origin': '*',
                'crossorigin':true,
                'crossDomain': true,
                'Authorization': 'bearer ' + userToken
            };
        
            axios.defaults.headers.common = headers;    
            axios
                .get(URL_API + `/human-resource/employee`)
                .then((response) => {
                    
                    setLoader(false);
                    console.log("Original response : ",response);
                    
                    if(response.status == 200){
                        if(response.data.data !== null && response.data.data !== undefined){

                            setData(response.data.data); // GET EMPLOYEE COLLECTION;
                        };
                    };
                    
                })
                .catch((error) => {

                    setLoader(false);
                    console.log("Error response : ",error.response);
                    
                });

        } else { console.log("Ga dapet User Token !")}

    },[])

    /* 
   
     ```````````````````````````
     Get List, Table functional

     ```````````````````````````
     
   */

    const [order, setOrder] = useState('asc');
    const [orderBy, setOrderBy] = useState('calories');
    const [selected, setSelected] = useState([]);

    const [page, setPage] = useState(0);
    const [rowsPerPage, setRowsPerPage] = useState(10);
    
    function handleRequestSort(event, property) {

        const isDesc = orderBy === property && order === 'desc';
        setOrder(isDesc ? 'asc' : 'desc');
        setOrderBy(property);
    
    };
 
    function handleSelectAllClick(event) {

        if (event.target.checked) {
        const newSelecteds = data.map(n => n.id);
        setSelected(newSelecteds);
        return;
        };

        setSelected([]);
    };

    
    /*
        ````````````````
        Get ID EMPLOYEE
        
        ````````````````
    */
    const [ idEmployee, setIdEmployee ] = useState([]);
 
    function handleClick(event, id) {

        let ids = [];

        console.log("Id :", id);
        const newIds = [...ids, {id: id}]
  
        setIdEmployee(newIds);

        /*
            ````````````````
            
            *********

            ````````````````
        */

        const selectedIndex = selected.indexOf(id);

        let newSelected = [];
    
        if (selectedIndex === -1) {

            newSelected = newSelected.concat(selected, id);

        } else if (selectedIndex === 0) {

            newSelected = newSelected.concat(selected.slice(1));

        } else if (selectedIndex === selected.length - 1) {

            newSelected = newSelected.concat(selected.slice(0, -1));

        } else if (selectedIndex > 0) {

            newSelected = newSelected.concat(
                selected.slice(0, selectedIndex),
                selected.slice(selectedIndex + 1),
            );
        }
    
        setSelected(newSelected);
    };
 
    function handleChangePage(event, newPage) {
        setPage(newPage);
    };
    
    function handleChangeRowsPerPage(event) {
        setRowsPerPage(event.target.value);
    }
 
    const isSelected = id => selected.indexOf(id) !== -1;
    
    const emptyRows = rowsPerPage - Math.min(rowsPerPage, (data !== null && data !== undefined  ? data.length : 0 ) - page * rowsPerPage);

    /*

        ````````````
        MODAL DETAIL

        ````````````
    */
    const [isChooseDetail, setChooseDetail ] = useState(false);
    const handleCloseModalEdit = () => setChooseDetail(false);


    /*
      ```````````````````````````
      HANDLE TAMBAH KARYAWAN NEW

      ```````````````````````````
    */

    const handleTambahKaryawanNEW = () => {
        
        Redirect(ToHrEmployeeIdentitas);

    };

    /*
        ```````````````````
        HANDLE CLICK DETAIL

        ```````````````````
    */
    const handleClickDetail = (e, idEmployee) => {

        e.preventDefault();

        localStorage.setItem('employee_id', idEmployee);

        if(userTokenState !== undefined){

            const headers =  {

                'Accept': "application/json",
                'Content-Type' : "application/json",
                'Access-Control-Allow-Origin': '*',
                'crossorigin':true,
                'crossDomain': true,
                'Authorization': 'bearer ' + userTokenState
            };
        
            axios.defaults.headers.common = headers;    
            axios
                .get(URL_API + `/human-resource/employee/batch/${idEmployee}`)
                .then((response) => {
                    
                    console.log("Original response : ",response);
                    
                    if(response.status == 200){
                        
                        localStorage.setItem('response_employee_detail', JSON.stringify(response.data.data))
                        Redirect(ToHrEmployeeProfileDetailSeeDetail);
                    };
                    
                })
                .catch((error) => {

                    console.log("Error response : ",error.response);
                    
                });

        } else { console.log("Ga dapet User Token !")}

    };

    return (

        <MuiThemeProvider theme={theme}>

            {/* 
                ```````````
                EMPTY STATE
                
                ```````````
            */}

            {
                loader == true && (

                    <Grid  
                        container
                        spacing={0}
                        direction="row"
                        justify="center"
                        alignItems="center"
                    >
                        <CircularProgress size={32} style={{marginTop: 72, color: 'red'}} />

                    </Grid>
                )
            }
            
            {
                data.length == 0 && data !== undefined &&  loader == false && (

                    <Grid  
                        container
                        direction="column"
                        justify="center"
                        alignItems="center"
                        spacing={0}
                    >
                    <br />
                    <br />
                        <img src={PictDefaultDashboard} style={{height: 300}} alt='Default Empty State Pict' />
                        <br />
                        <br />
                        <Typography variant='h6' className={classes.title} style={{color: 'black', textAlign: 'center'}}>
                            Belum ada data karyawan yang bisa ditampilkan !
                        </Typography>
                        <br />
                        <Button 
                            variant= 'contained'
                            className={classes.button}
                            // onClick={() => Redirect(ToEm)}  
                            onClick={() => Redirect(ToHrEmployeeIdentitas)}
                        >
                            Tambah karyawan
                        </Button>
                        <br />
                        <br />
                        <br />
                        <br />
                    </Grid>
                )
            }

            <br />

            {/* 
                ````````````` 
                LIST EMPLOYEE 
                
                ````````````` 
            */}

            {
                data.length > 0 && data !== undefined && ( 

                    <Paper className={classes.rootEmployee}  elevation={0}>
                        <Grid container>   
                            <Grid item sm={5}></Grid>
                            <Grid item sm={5} style={{paddingTop: 0}}>

                                <Paper className={classes.paper} elevation={1}>
                                    <IconButton className={classes.iconButton} aria-label="Search">
                                        <SearchIcon />
                                    </IconButton>
                                    <InputBase className={classes.input} placeholder="Cari ..." />
                                    <Divider className={classes.divider} />
                                </Paper>
                            </Grid>

                            <Grid item sm={1} style={{textAlign: 'right'}}>
                                <img src={IconPreviewList} className={classes.iconPreviewList} />
                            </Grid>
                            <Grid item sm={1} style={{textAlign: 'left'}}>
                                <img src={IconPreviewGrid} className={classes.iconPreviewGrid} />
                            </Grid>
                        </Grid>
                        <br />
                        <br />

                        {/* 
                        
                            ``````````````````````````
                                Search, Button Invite

                            ``````````````````````````
                        */}

                        <Grid  
                            container
                            spacing={8}
                            direction="row"
                            justify="center"
                            alignItems="center"
                        >   
                            <Grid item sm={9} style={{textAlign: 'left'}}>
                                <Typography variant='h6' className={classes.title} style={{marginLeft: 42}}>
                                    Data Karyawan                        
                                </Typography>
                            </Grid>

                            <Grid item sm={1} style={{textAlign: 'right'}}>
                                <Tooltip title="Filter" placement="right-start">
                                    <img src={IconFilterNew} className={classes.iconFilter} />
                                </Tooltip>
                            </Grid>

                            <Grid item sm={2} style={{textAlign: 'left'}}>
                                <Button 
                                    onClick= {handleTambahKaryawanNEW}
                                    variant="contained" 
                                    color="secondary" 
                                    className={classes.buttonTambah} 
                                    style={{marginRight: 24}}
                                >
                                    Tambah 
                                </Button>
                            </Grid>
                        </Grid>
                        {/* 
                        
                            ```
                            END

                            ```
                        */}
                    </Paper>
                )
            }    

            {/* 
                `````
                TABLE

                `````
            */}

            <Table className={classes.table} aria-labelledby="tableTitle">

                {
                    data !== null && data !== undefined && data.length > 0 ? (

                        <EnhancedTableHead
                            numSelected={selected.length}
                            order={order}
                            orderBy={orderBy}
                            onSelectAllClick={handleSelectAllClick}
                            onRequestSort={handleRequestSort}
                            rowCount={data !== null ? data.length : 0}
                        /> 
                    ) : null
                }

                <TableBody>

                    {
                        stableSort(data, getSorting(order, orderBy))
                            .slice(page * rowsPerPage, page * rowsPerPage + rowsPerPage)
                            .map(n => {

                                const isItemSelected = isSelected(n.id);

                                return (
                                    <TableRow
                                        key={n.id}
                                        onClick={event => handleClick(event, n.id)}
                                        hover
                                        role="checkbox"
                                        aria-checked={ isItemSelected }
                                        tabIndex={-1}
                                        selected={ isItemSelected }
                                    >
                                        <TableCell padding="checkbox">
                                            <Checkbox checked={isItemSelected} />
                                        </TableCell>

                                        <TableCell component="th" scope="row" padding="none">
                                            <LensIcon className={classes.lensIcon} />
                                        </TableCell>

                                        <TableCell>

                                            {
                                                n.status == 3 ? (

                                                    <Chip  
                                                        icon={ <DoneIcon style={{color: 'white'}} /> }
                                                        label="Aktif" 
                                                        className={classes.chipVerifified} 
                                                        style={{backgroundColor: '#16e616', color: 'white'}}
                                                    />

                                                ): null 
                                            }

                                        </TableCell>
                                        <TableCell className={classes.title}>{n.employee_number !== null ? n.employee_number : '-'}</TableCell>

                                        <TableCell className={classes.title}>

                                            {
                                                n.member.role.length > 0 ? (

                                                    <LightTooltip open={true} title={n.member.role[0].name == 'superadmin' ? 'Superadmin' : '' } placement="right-start">
                                                        <span> 
                                                            {n.member.first_name !== null ? n.member.first_name : n.member.email }
                                                        </span>
                                                    </LightTooltip>

                                                ) : (

                                                    <span>
                                                        {n.member.first_name !== null ? n.member.first_name : n.member.email }
                                                    </span>
                                                )
                                            }


                                        </TableCell>

                                        <TableCell align='left'>
                                            ***
                                        </TableCell>

                                        <TableCell align='left'>
                                            ***
                                        </TableCell>

                                        <TableCell align='left'>
                                            
                                            <Button 
                                                // onClick={() => Redirect(ToHrEmployeeProfileDetail)}
                                                onClick= { (e) => handleClickDetail(e, n.id)}
                                                variant="outlined" 
                                                color="secondary" 
                                                className={classes.buttonDetail} 
                                                // style={{marginRight: 24}}
                                            >
                                                Lihat Detail
                                            </Button>
                                        </TableCell>
                                    </TableRow>

                                );
                            })
                    }
                    {/* {
                        emptyRows > 0 && (
                            <TableRow style={{ height: 49 * emptyRows }}>
                            <TableCell colSpan={6} />
                            </TableRow>
                        )
                    } */}

                </TableBody>
            </Table>

            {/* ROW OF DELETE  */}
            <EnhancedTableToolbar numSelected={selected.length} idEmployee={idEmployee} />

            <TablePagination
                rowsPerPageOptions={[5, 10, 25]}
                component="div"
                count={data !== null && data !== undefined ? data.length : 0}
                rowsPerPage={rowsPerPage}
                page={page}
                backIconButtonProps={{
                'aria-label': 'Previous Page',
                }}
                nextIconButtonProps={{
                'aria-label': 'Next Page',
                }}
                onChangePage={handleChangePage}
                onChangeRowsPerPage={handleChangeRowsPerPage}
            />


        </MuiThemeProvider>
    )
};

export default withStyles(styles)(ViewEmpty);


/*

    ```````````````````````````````````
    functions utilities TABLE COMPONENT

    ```````````````````````````````````
*/

let counter = 0;  

function desc(a, b, orderBy) {
    if (b[orderBy] < a[orderBy]) {
      return -1;
    }
    if (b[orderBy] > a[orderBy]) {
      return 1;
    }
    return 0;

};  

function stableSort(array, cmp) {

    // console.log("stableSort : ", array)

    if(array !== null && array !== undefined){

      const stabilizedThis = array.map((el, index) => [el, index]);

      stabilizedThis.sort((a, b) => {
      
        const order = cmp(a[0], b[0]);
      
        if (order !== 0) return order;
      
        return a[1] - b[1];
      });
  
      return stabilizedThis.map(el => el[0]);
    };

    return [];

};
  
function getSorting(order, orderBy) {
    return order === 'desc' ? (a, b) => desc(a, b, orderBy) : (a, b) => -desc(a, b, orderBy);
};

const rows = [
    { id: 'name', align: 'center', disablePadding: true, label: '' },
    { id: 'statusAcitve', align: 'right', disablePadding: false, label: 'Status' },
    { id: 'nip', align: 'center', disablePadding: false, label: 'NIP' },
    { id: 'dataKaryawan', align: 'center', disablePadding: false, label: 'Karyawan' },
    { id: 'tingkatJabatan', align: 'center', disablePadding: false, label: 'Tingkat Jabatan' },
    { id: 'jabatan', align: 'center', disablePadding: false, label: 'Jabatan' },
  ];

function EnhancedTableHead(props) {

    const { onSelectAllClick, order, orderBy, numSelected, rowCount, onRequestSort } = props;

    const createSortHandler = property => event => {

      onRequestSort(event, property);
    };
  
    return (

      <TableHead>
        <TableRow>
          <TableCell padding="checkbox">
            <Checkbox
              indeterminate={numSelected > 0 && numSelected < rowCount}
              checked={numSelected === rowCount}
              onChange={onSelectAllClick}
            />
          </TableCell>
          {rows.map(

            row => (
              <TableCell
                key={row.id}
                numeric={row.numeric}
                padding={row.disablePadding ? 'none' : 'default'}
                sortDirection={orderBy === row.id ? order : false}
              >
                <Tooltip
                  title="Sort"
                  placement={row.numeric ? 'bottom-end' : 'bottom-start'}
                  enterDelay={300}
                >
                  <TableSortLabel
                    active={orderBy === row.id}
                    direction={order}
                    onClick={createSortHandler(row.id)}
                  >
                    {row.label}
                  </TableSortLabel>
                </Tooltip>
              </TableCell>
            ),
            this,
            )}
            </TableRow>
      </TableHead>
    );
  };
