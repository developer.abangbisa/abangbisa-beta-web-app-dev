import React, { Component, useEffect, useState } from "react";
import { makeStyles, createMuiTheme, withStyles, MuiThemeProvider } from '@material-ui/core/styles';
import { 
    Container, Paper, Typography, Grid, TextField, Checkbox, CircularProgress,
    FormControlLabel, Box, Button, MenuItem, Dialog, DialogTitle, DialogContent, 
    DialogContentText, DialogActions, Breadcrumbs, Link, IconButton, InputBase, Divider,
    Tooltip, List, ListItem, ListItemText, Avatar, Table, TableBody, TableRow, TableCell,
    Chip, TablePagination, TableHead, TableSortLabel, Radio, FormLabel, Badge

} from '@material-ui/core';

import axios from 'axios';
import { URL_API } from '../../../../constants/config-api';

const DialogEditPosisiForProfile = props => {

    // modalEditPosition = { modalEditPosition}
    // setModalEditPosition = { setModalEditPosition }

    const { 
            classes,
            modalEditPosition,
            setModalEditPosition,
            isChecked,
            handleChangeCheckbox,
            
            employeeIdState,
            setlistPositionNow

    } = props;


    /*
        ``````````````````````````
        GET LIST AVAILABLE FROM DB
        
        ``````````````````````````
        */

       const userToken = localStorage.getItem('userToken');
       
       const [ listPosition, setListPosition ] = useState([]);
       const [ listPositionNowOfEmployee, setListPositionNowOfEmployee ] = useState([]);
       const [ updatedAt, setUpdatedAt ] = useState('');

       const [ userTokenState, setUserTokenState ] = useState('');
       
    useEffect(() => {
           
        setUserTokenState(userToken)

        if(userToken !== undefined ){
        
            const header =  {    

                'Accept': "application/json",
                'Content-Type' : "application/json",
                'Authorization' : "bearer " + userToken,
            };

            axios.defaults.headers.common = header;    

            if(modalEditPosition == true){

                    /*
                        ```````````````````````````
                        GET ALL COLLECTION POSITION

                        ```````````````````````````
                    */
                    axios
                        .get(URL_API + `/human-resource/structure-position`)
                        .then(function(response){
                            
                            console.log("Response Original - ALL COLLECTION POSITION: ", response)
                            setListPosition(response.data.data);
        
                        })
                        .catch(function(error){
                            
                            if(error.response !== undefined){

                                if(error.response.status == 422){
                                    alert("Error : Unprocesity entity !");
                                };

                            } else {
                                alert('Periksa koneksi internet Anda !')
                            };
        
                            console.log("Error : ", error.response)
                        })

                    /*
                        ``````````````````````````````````````````````````````````
                        GET DETAIL EMPLOYEE, UPDATED AT & POSITION NOW OF EMPLOYEE

                        ``````````````````````````````````````````````````````````
                    */
                    axios
                        .get(URL_API + `/human-resource/employee/${employeeIdState}/patch`)
                        .then(function(response){
                            
                            console.log("Response Original - DETAIL EMPLOYEE : ", response)
                            
                            if(response.status == 200){

                                setUpdatedAt(response.data.data.updated_at);

                                if(response.data.data !== null){
                                    if(response.data.data.fields.position_id !== null){
                                        if(response.data.data.fields.position_id.value !== null){
                                            if(response.data.data.fields.position_id.value.length > 0){
                                                
                                                setListPositionNowOfEmployee(response.data.data.fields.position_id.value);

                                            }
                                        }
                                    }
                                }
                            };

                        })
                        .catch(function(error){
                            
                            if(error.response !== undefined){

                                if(error.response.status == 422){
                                    alert("Error : Unprocesity entity !");
                                };

                            } else {
                                alert('Periksa koneksi internet Anda !')
                            };

                            console.log("Error : ", error.response)
                        })
                        

            };

        } else { console.log("No Access Token available!")};
       

   }, [modalEditPosition])

   /*
        ``````````````````
        HANDLE SIMPAN DATA

        ``````````````````
   */

    const handleSimpanData = () => {

        // console.log("Data checked : ", isChecked);
        // console.log('listPositionNowOfEmployee : ', listPositionNowOfEmployee);

        let templateListPositionId = [];
        
        if(listPositionNowOfEmployee.length > 0){

            listPositionNowOfEmployee.map((item) => {

                templateListPositionId.push(item);
            })
        };

        templateListPositionId.push(isChecked.id);


        let data = {

            Employee : {

                updated_at: updatedAt,
                position_id : templateListPositionId

            },
            _method: 'patch'
        };

        console.log("Data : ", data);

        if(userTokenState !== undefined && userTokenState !== null ){

            const header =  {       

                'Accept': "application/json",
                'Content-Type' : "multipart/form-data",
                'Authorization' : "bearer " + userTokenState,
            };
          
            axios.defaults.headers.common = header;    
      
            axios
                .post(URL_API + `/human-resource/employee/${employeeIdState}`, data)
                .then(function(response){
      
                    console.log("Response Original : ", response)
      
                    if(response.status == 200 ){
      
                        setlistPositionNow(response.data.data.position)
                        setModalEditPosition(false);
                    };
                })
                .catch(function(error){
                    
                    console.log("Error : ", error.response)
                    if(error.response == 500){
      
                      alert(error.response.data.message !== undefined ? error.response.data.message : "500");
      
                    } else {
      
                      alert("Ooops, something went wrong !")
                    }
                    
                })
      
          } else { console.log("No Access Token available!")};

    };

    return (

        <Dialog
            open={modalEditPosition}
            onClose={() => setModalEditPosition(false)}
            aria-labelledby="alert-dialog-title"
            aria-describedby="alert-dialog-description"
            // fullWidth
        >
            <DialogTitle id="alert-dialog-title" style={{textAlign: "center"}}>
                <Typography variant='subtitle1' className={classes.title}>
                    <b>Edit Posisi</b>
                </Typography>
            </DialogTitle>
            <DialogContent>

                <List style={{width: 300}}>
                {/* <List> */}

                    {
                        // positions.length > 0 ? positions.map((item, i) => (
                        listPosition.length > 0 ? listPosition.map((item, i) => (

                            <ListItem key={i}>  

                                <Grid container>
                                    <Grid item sm={10} style={{textAlign: 'left'}}>

                                        <FormControlLabel
                                            className={classes.checkboxStyle}
                                            control={
                                                <Checkbox 
                                                    // className={classes.checkboxStyle}
                                                    checked={isChecked.id == item.id ? true : false} 
                                                    // onChange={handleChangeCheckbox('name')} 
                                                    onChange={(e) => handleChangeCheckbox(e, item)}
                                                    value={item} 
                                                />
                                            }
                                            label={

                                                <Tooltip title="Available Position" placement="right-start">
                                                    <Badge 
                                                        color="primary"  variant="dot"
                                                        // classes={{ 'colorPrimary' : 'green' }} //*NOT WORK
                                                        >
                                                        <Typography variant='subtitle1' className={classes.title}>
                                                            { item.structure_position_title_name }

                                                        </Typography>
                                                    </Badge>
                                                </Tooltip>

                                            }
                                        />
                                    </Grid>

                                    <Grid item sm={2} >
                                        <Typography variant='subtitle2' className={classes.title} style={{color: '#2194e1', marginTop: 8, cursor: 'pointer'}}> 
                                            Detail
                                        </Typography>
                                    </Grid>
                                </Grid>
                            </ListItem>

                        )) : (

                            <Grid container>
                                <Grid item sm={12} style={{textAlign: 'center'}}> 
                                    <CircularProgress size={32} style={{color: '#cc0707'}} />
                                </Grid>
                            </Grid>
                        )
                    }

                </List>

                <DialogContentText id="alert-dialog-description">
                
                </DialogContentText>
            </DialogContent>
            <DialogActions style={{alignItems: "right", justifyContent:'flex-end'}}>
                <Button 
                    onClick={() => setModalEditPosition(false)} 
                    variant='outlined' 
                    size='small'
                    className={classes.buttonModalCancel}    
                >
                    Batal
                </Button>
                
                <Button 
                    // onClick={() => Redirect(ToCompletionProfile)}
                    onClick= {handleSimpanData}
                    variant='contained' 
                    color="primary" 
                    size='small'
                    className={classes.buttonModal}
                >  
                    Simpan perubahan
                </Button>
            </DialogActions>
            <br />
            <br />
        </Dialog>
    )
};

export default DialogEditPosisiForProfile;