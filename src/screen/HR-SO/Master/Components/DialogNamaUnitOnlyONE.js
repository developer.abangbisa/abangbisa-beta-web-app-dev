import React, { Component, useEffect, useState } from "react";
import { makeStyles, createMuiTheme, withStyles, MuiThemeProvider } from '@material-ui/core/styles';
import { 
    Container, Paper, Typography, Grid, TextField, Checkbox, CircularProgress,
    FormControlLabel, Box, Button, MenuItem, Dialog, DialogTitle, DialogContent, 
    DialogContentText, DialogActions, Breadcrumbs, Link, IconButton, InputBase, Divider,
    Tooltip, List, ListItem, ListItemText, Avatar, Table, TableBody, TableRow, TableCell,
    Chip, TablePagination, TableHead, TableSortLabel, Radio, FormLabel, Icon

} from '@material-ui/core';

import axios from 'axios';
import { URL_API } from '../../../../constants/config-api';

const DialogNamaUnitOnlyONE = props => {

    const { 
        
            classes,
            setModalTambahNamaUnit,
            isModalTambahNamaUnit,
            dataDetail
        
        } = props;


    /*
        `````````````````````
        HANDLE NAMA UNIT ONLY

        `````````````````````
    */
        
    const [ namaUnit, setNamaUnit ] = useState('');
    const handleChangeNamaUnit = (e) => setNamaUnit(e.target.value)

    /*
        ````````````````````````````````````````````````
        HANDLE SIMPAN NAMA UNIT ONLY

        ````````````````````````````````````````````````
    */

    const handleSimpan = () => {


            //*context
            let data = {
        
                MasterStructureUnit : {
                    
                    structure_unit_type_id: dataDetail.id,
                    name : namaUnit,
                }
            };
  
            console.log("Data : ", data);
      
            const userToken = localStorage.getItem('userToken');
                
              if(userToken !== undefined ){
            
                const header =  {       
                    'Accept': "application/json",
                    'Content-Type' : "application/json",
                    'Authorization' : "bearer " + userToken,
    
                };

                axios.defaults.headers.common = header;    

                axios
                    .post(URL_API + '/human-resource/master-structure-unit', data)
                    .then(function(response){

                        // closeModalAnggota();
                        console.log("Response Original : ", response);
                        setModalTambahNamaUnit(false)
                    
                        if(response.status == 200 ){

                            window.location.reload();

                            if(response.data.data !== undefined){
                        
                            };
                        };
                    })
                    .catch(function(error){

                        alert('Whoops, something went wrong !')
                        console.log("Error : ", error.response)
                    
                    })
    
              } else { console.log("No Access Token available!")};
    };

    return (

        <Dialog
            open={isModalTambahNamaUnit}
            onClose={() => setModalTambahNamaUnit(false)}
            aria-labelledby="alert-dialog-title"
            aria-describedby="alert-dialog-description"
        >
        <DialogTitle id="alert-dialog-title" style={{textAlign: "center"}}>
            <Typography variant='subtitle1' className={classes.title}>
                <b>Nama Unit baru</b>
            </Typography>
        </DialogTitle>
        <DialogContent>

            <Typography variant='subtitle1' className={classes.titleForm}>
                <b>Nama Unit</b>
            </Typography>

            <TextField
                id="outlined-bare"
                onChange= {handleChangeNamaUnit}
                // value={nameAnggotaKeluarga}
                className={classes.textField}
                placeholder='Contoh : Sales'
                variant="outlined"
            />

            <DialogContentText id="alert-dialog-description"></DialogContentText>

        </DialogContent>
        <DialogActions style={{alignItems: "right", justifyContent:'flex-end'}}>
            <Button 
                onClick={() => setModalTambahNamaUnit(false)} 
                variant='outlined' 
                size='small'
                className={classes.buttonModalCancel}    
            >
                Batal
            </Button>
            
            <Button 
                // onClick={() => Redirect(ToHrEmployeeKepegawaianInfoPegawai)}
                onClick= {handleSimpan}
                variant='contained' 
                color="primary" 
                size='small'
                className={classes.buttonModal}
            >  
                Tambah
            </Button>
        </DialogActions>
        <br />
        <br />
        </Dialog>
    )   
};

export default DialogNamaUnitOnlyONE;
