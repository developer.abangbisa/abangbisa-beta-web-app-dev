import React, { Component, useEffect, useState } from "react";
import { makeStyles, createMuiTheme, withStyles, MuiThemeProvider } from '@material-ui/core/styles';
import { 
    Container, Paper, Typography, Grid, TextField, Checkbox, CircularProgress,
    FormControlLabel, Box, Button, MenuItem, Dialog, DialogTitle, DialogContent, 
    DialogContentText, DialogActions, Breadcrumbs, Link, IconButton, InputBase, Divider,
    Tooltip, List, ListItem, ListItemText, Avatar, Table, TableBody, TableRow, TableCell,
    Chip, TablePagination, TableHead, TableSortLabel, Radio, FormLabel, Icon

} from '@material-ui/core';

import axios from 'axios';
import { URL_API } from '../../../../constants/config-api';

const DialogNamaUnit = props => {

    const { 
        
            classes,
            setModalTambahNamaUnit,
            isModalTambahNamaUnit,
            dataDetail
        
        } = props;

    /*
        ```````````````````````````````
        REMOVE & ADDING NAMA UNIT ONLY

        ``````````````````````````````
    */

    const [isAnyArrayComponent, setArrayComponent] = useState([]);

    
    const [ todoValue, setTodoValue ] = useState('');
    const [ todos, setTodo ] = useState([]);

    const handleChange = (e) => setTodoValue(e.target.value);

    const handleEnterPress = (e) => {

        if(e.keyCode == 13){
            setTodoValue(e.target.value);
            handleAddCircleIcon();
        };
    };

    const handleDone = e => {
  
        const { id } = e.target.parentElement;
        todos[id].done = !todos[id].done
        setTodo([...todos])
    };

    const handleDelete = e => {

        const { id } = e.target.parentElement;
    
        todos.splice(id, 1)
    
        setTodo([...todos]);
  
        isAnyArrayComponent.splice(id, 1);
        setArrayComponent([...isAnyArrayComponent])
    }



    const handleAddCircleIcon = () => {

        // e.preventDefault();  
        
        // setHideTextHelpComponentUnit(true);
  
        const todo = {
          value: todoValue,
          done: false    
  
        };
        
        if(!todoValue) return;
        
            setTodo([...todos, todo]);
  
            setArrayComponent([...isAnyArrayComponent, todoValue]); //Add element array inside Component 'ViewUnitStructureFORM'
            document.getElementById('todoValue').value = '';
  
    };

    /*
        ````````````````````````````````````````````````
        HANDLE SIMPAN PENAMBAHAN JENIS UNIT & NAMA UNIT

        ````````````````````````````````````````````````
    */

    const handleSimpan = () => {


            //*context
            let data = {
        
                MasterStructureUnit : {
                    
                    structure_unit_type_id: dataDetail.id,
                    name : isAnyArrayComponent,
                }
            };
  
            console.log("Data : ", data);
      
            const userToken = localStorage.getItem('userToken');
                
            //   if(userToken !== undefined ){
            
            //     const header =  {       
            //         'Accept': "application/json",
            //         'Content-Type' : "application/json",
            //         'Authorization' : "bearer " + userToken,
    
            //     };

            //     axios.defaults.headers.common = header;    

            //     axios
            //         .post(URL_API + '/human-resource/master-structure-unit-type/batch', data)
            //         .then(function(response){

            //             // closeModalAnggota();
            //             console.log("Response Original : ", response)
                    
            //             if(response.status == 200 ){

            //                 window.location.reload();

            //                 if(response.data.data !== undefined){
                        
            //                 };
            //             };
            //         })
            //         .catch(function(error){

            //             alert('Whoops, something went wrong !')
            //             console.log("Error : ", error.response)
                    
            //         })
    
            //   } else { console.log("No Access Token available!")};
    };

    return (

        <Dialog
            open={isModalTambahNamaUnit}
            onClose={() => setModalTambahNamaUnit(false)}
            aria-labelledby="alert-dialog-title"
            aria-describedby="alert-dialog-description"
        >
        <DialogTitle id="alert-dialog-title" style={{textAlign: "center"}}>
            <Typography variant='subtitle1' className={classes.title}>
                <b>Nama Unit baru</b>
            </Typography>
        </DialogTitle>
        <DialogContent>

            <Typography variant='subtitle1' className={classes.titleForm}>
                <b>Nama Unit</b>
            </Typography>

            {
                todos && todos.map((todo, i) => (

                    <div key={todo.value} id={i}>
                        <Paper className={classes.rootPaper} elevation={1} >
                        
                            <InputBase 
                                disabled
                                className={classes.input} 
                                defaultValue={todo.value}
                                onChange={handleDone}
                                
                            />

                            <Divider className={classes.divider} />

                            <IconButton 
                                onClick={handleDelete}
                                color="secondary" 
                                className={classes.iconButton} 
                                aria-label="Remove"
                            >
                                <Icon className={classes.iconHover} color="secondary" style={{ fontSize: 27 }}>
                                    remove_circle
                                </Icon>
                            </IconButton>

                            {/* <button className={todo.done ? 'done' : 'not-done'} onClick={handleDone}>{todo.value}</button> */}
                            {/* <button className="delete-todo" onClick={handleDelete} >x</button>               */}
                        </Paper>
                        <br/>
                    </div>
                ))
            }

            <Paper className={classes.rootPaper} elevation={1}>
                <InputBase 
                    className={classes.input} 
                    placeholder="Contoh : CEO" 
                    id="todoValue" 
                    onChange={handleChange}
                    onKeyDown={handleEnterPress}
                />

                <Divider className={classes.divider} />

                <IconButton 
                    onClick={handleAddCircleIcon}
                    color="primary" 
                    className={classes.iconButton} 
                    aria-label="Adding"   
                    disabled = { todoValue !== '' ? false : true }
                >
                    <Icon className={todoValue !== '' ? classes.iconHoverAdd : classes.iconHoverAddDisabled } color="secondary" style={{ fontSize: 27 }}>
                        add_circle
                    </Icon>
                </IconButton>
            </Paper>

            <DialogContentText id="alert-dialog-description"></DialogContentText>

        </DialogContent>
        <DialogActions style={{alignItems: "right", justifyContent:'flex-end'}}>
            <Button 
                onClick={() => setModalTambahNamaUnit(false)} 
                variant='outlined' 
                size='small'
                className={classes.buttonModalCancel}    
            >
                Batal
            </Button>
            
            <Button 
                // onClick={() => Redirect(ToHrEmployeeKepegawaianInfoPegawai)}
                onClick= {handleSimpan}
                variant='contained' 
                color="primary" 
                size='small'
                className={classes.buttonModal}
            >  
                Tambah Unit
            </Button>
        </DialogActions>
        <br />
        <br />
        </Dialog>
    )   
};

export default DialogNamaUnit;
