import React, { Component, useEffect, useState, createRef } from "react";
import { makeStyles, createMuiTheme, withStyles, MuiThemeProvider } from '@material-ui/core/styles';
import { 
    Container, Paper, Typography, Grid, TextField, Checkbox, CircularProgress,
    FormControlLabel, Box, Button, MenuItem, Dialog, DialogTitle, DialogContent, 
    DialogContentText, DialogActions, Breadcrumbs, Link, IconButton, InputBase, Divider,
    Tooltip, List, ListItem, ListItemText, Avatar, Table, TableBody, TableRow, TableCell,
    Chip, TablePagination, TableHead, TableSortLabel, Fab, Icon

} from '@material-ui/core';

import axios from 'axios';

import UnitItem from './UnitItem';
import DialogNamaUnitOnlyONE from '../Components/DialogNamaUnitOnlyONE';
import DialogEditJenisUnit from '../ComponentsEdit/DialogEditJenisUnit';
// import DialogNamaUnit from '../Components/DialogNamaUnit';



import { URL_API } from '../../../../constants/config-api';


const Unit = props => {

    const { classes } = props;

    // const [ hoverRef, isHovering ] = useHover(100/*enterDelay*/, 100/*leaveDelay*/);
    // const [ hoverRefIconPlus, isHoveringIconPlus ] = useHover(100/*enterDelay*/, 100/*leaveDelay*/);

    const userToken = localStorage.getItem('userToken');
    const [ userTokenState, setUserTokenState ] = useState('');

    /*
        `````````````````````````````
        GET JENIS UNIT & NAMA UNIT

        `````````````````````````````
    */

    const [ listJenisUnitAndNamaUnit, setListJenisUnitAndNamaUnit ] = useState([]);

    useEffect(() => {

        setUserTokenState(userToken);

        if(userToken !== undefined){
    
            const header =  {       
                'Accept': "application/json",
                'Content-Type' : "application/json",
                'Authorization' : "bearer " + userToken,
            };
        
            axios.defaults.headers.common = header;    


            /*
                ```````````
                JENIS UNIT

                ```````````
            */
            axios
                .get(URL_API + `/human-resource/master-structure-unit-type/batch?`)
                .then(function(response){

                    console.log("Response Original JENIS UNIT & NAMA UNIT   : ", response);
                    setListJenisUnitAndNamaUnit(response.data.data);

                })
                .catch(function(error){
                    
                    console.log("Error : ", error.response)
                })
        
        };

    },[]);


    /*
        ``````
        HOVER

        ``````
    */

    // const element = (hovered) => {
        
    //     return (

    //         <div>
    //             Hover me! {hovered && 'Thanks!'}
    //         </div>
    //     )
    // };

    // const [hoverable, hovered] = useHover(element);

    /*
        ``````````````````````````````````
        HANDLE MODAL TAMBAH NAMA UNIT ONLY

        ```````````````````````````````````
    */

    const [ isModalTambahNamaUnit, setModalTambahNamaUnit ] = useState(false);

    const [ dataDetail, setDataDetail ] = useState(null);

    const handleTambah = (e, item) => {

        e.preventDefault();

        setModalTambahNamaUnit(true);

        console.log("Items : ", item);
        setDataDetail(item)
    };

    
    /*
        ```````````````````````````````````
        HANDLE MODAL UPDATE JENIS UNIT ONLY

        ```````````````````````````````````
    */

    const [ isModalEdit, setModalEdit ] = useState(false);
    const [ dataDetailEdit, setDataDetailEdit ] = useState(null);

    const handleEdit = (e, item) => {

        e.preventDefault();

        setModalEdit(true);
        setDataDetailEdit(item)

        console.log("Edit : ", item);

    };

    return (

        <div>
            {
                listJenisUnitAndNamaUnit.length > 0 ? listJenisUnitAndNamaUnit.map((item, i) => (

                    <div  key={item.id}>
                        <Typography variant='body1' className={classes.titleSub2}>
                            <b>{item.name}</b>
                            <i 
                                onClick = { (e) => handleEdit(e, item)}
                                className='material-icons' 
                                style={{fontSize: 17, marginLeft: 8, cursor: 'pointer'}}
                            >
                                edit
                            </i>
                        </Typography>
                        <br />
                        
                        {
                            item.masterStructureUnit.length > 0 ? item.masterStructureUnit.map((data, j) => {
                                
                                
                                return (
    
                                    <span  key={data.id}>
                                     
                                        <UnitItem 
                                            classes={classes}
                                            data={data}
                                        />
                                    </span>
                                    
                                )
                            }) : null
                            
                            
                        }

                        {console.log("Item : ", item)}

                        {
                            item.name !== 'Pimpinan' ? (

                                <Tooltip title="Tambah" placement="right">
                                    <span
                                        // ref={hoverRefIconPlus}
                                    >
                                        <IconButton
                                            // className={ isHoveringIconPlus ? classes.hoverIconPlus : classes.hoverIconPlusLeave}
                                            className={classes.hoverIconPlusLeave}
                                            onClick={(e) => handleTambah(e,item)}
                                            style={{marginTop: 8}}

                                        >
                                            <i className='material-icons' style={{fontSize: 12}}>
                                                add
                                            </i>
                                        </IconButton>
                                    
                                    </span>
                                </Tooltip>
                                
                            ) : null
                        }

                        <br />
                        <br />
                        <br />
                    </div>

                )) : (

                       <Grid container>
                            <Grid item xs={12} style={{textAlign: 'center'}}>

                                <CircularProgress 
                                    style={{color:'#cc0707'}}
                                />
                            </Grid>
                       </Grid>
                    )
            }
            
            <br />

            <DialogNamaUnitOnlyONE
                classes = { classes }
                isModalTambahNamaUnit = { isModalTambahNamaUnit }
                setModalTambahNamaUnit = { setModalTambahNamaUnit}
                dataDetail = { dataDetail }

            />

            <DialogEditJenisUnit 
                classes = { classes }
                isModalEdit = { isModalEdit }
                setModalEdit = { setModalEdit }
                dataDetailEdit = { dataDetailEdit }
            />
        </div>
    )
};


export default Unit;