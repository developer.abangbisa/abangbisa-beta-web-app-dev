import React, { Component, useEffect, useState, createRef } from "react";
import { makeStyles, createMuiTheme, withStyles, MuiThemeProvider } from '@material-ui/core/styles';
import { 
    Container, Paper, Typography, Grid, TextField, Checkbox, CircularProgress,
    FormControlLabel, Box, Button, MenuItem, Dialog, DialogTitle, DialogContent, 
    DialogContentText, DialogActions, Breadcrumbs, Link, IconButton, InputBase, Divider,
    Tooltip, List, ListItem, ListItemText, Avatar, Table, TableBody, TableRow, TableCell,
    Chip, TablePagination, TableHead, TableSortLabel, Fab, Icon

} from '@material-ui/core';

import axios from 'axios';

import EditIcon from '@material-ui/icons/Edit';
import { URL_API } from '../../../../constants/config-api';

import DialogTingkatJabatan from '../Components/DialogTingkatJabatan';
import DialogDeleteTingkatJabatan from '../ComponentsDelete/DialogDeleteTingkatJabatan';
import DialogEditTingkatJabatan from '../ComponentsEdit/DialogEditTingkatJabatan';

const TingkatJabatan = props => {

    const { classes } = props;

    const userToken = localStorage.getItem('userToken');
    const [ userTokenState, setUserTokenState ] = useState('');

    /*
        ````````````````````````````````````
        GET TINGKAT JABATAN / POSITION LEVEL

        ````````````````````````````````````
    */

    const [ listTingkatJabatan, setListTingkatJabatan ] = useState([]);

    useEffect(() => {

        setUserTokenState(userToken);

        if(userToken !== undefined){
    
            const header =  {       
                'Accept': "application/json",
                'Content-Type' : "application/json",
                'Authorization' : "bearer " + userToken,
            };
        
            axios.defaults.headers.common = header;    

            axios
                .get(URL_API + `/human-resource/master-structure-position-level`)
                .then(function(response){

                    console.log("Response Original TINGKAT JABATAN   : ", response);
                    setListTingkatJabatan(response.data.data);

                })
                .catch(function(error){
                    
                    console.log("Error : ", error.response)
                })
        
        };

    },[]);

    /*
        ````````````````````````````````````````
        HANDLE MODAL TAMBAH TINGKAT JABATAN ONLY

        ````````````````````````````````````````
    */

    const [ isModalTingkatJabatan, setModalTingkatJabatan ] = useState(false);

    // const handleTambah = (e, item) => {
        const handleTambah = () => {
            
            setModalTingkatJabatan(true);
        };
        
        /*
        ``````````````````````````````````
        HANDLE DELETE TINGKAT JABATAN ONLY
        
        ``````````````````````````````````
        */
       
    const [ isOpenModalDelete, setOpenModalDelete ] = useState(false);   
    const [ dataDetail, setDataDetail ] = useState(null);

    const handleDelete = (e, item) => {

        console.log("Item : ", item)

        e.preventDefault();
        setOpenModalDelete(true);
        
        setDataDetail(item);


    };

        /*
        ```````````````````````````````````
        HANDLE MODAL UPDATE JENIS UNIT ONLY

        ```````````````````````````````````
    */

   const [ isModalEdit, setModalEdit ] = useState(false);
   const [ dataDetailEdit, setDataDetailEdit ] = useState(null);

   const handleEdit = (e, item) => {

       e.preventDefault();

       setModalEdit(true);
       setDataDetailEdit(item)

       console.log("Edit : ", item);

   };


    return (

        <div>
            {
                listTingkatJabatan.length > 0 ? listTingkatJabatan.map((item, i) => (

                    <span key={i}>

                        <Chip
                            style={{fontFamily: 'Nunito', marginLeft: 40, marginTop: 8, backgroundColor: '#ffbc39', color: 'white', fontWeight: 'bold'}}                        
                            label={item.name}
                            onDelete = { (e) => handleDelete(e, item) }
                            icon = {
                                <Tooltip title="Edit" placement="bottom">
                                    <IconButton
                                        onClick={ (e) => handleEdit(e, item)}
                                    >
                                        <EditIcon style={{fontSize: 17, color: 'white', cursor: 'pointer'}} />
                                    </IconButton>
                                </Tooltip>
                            }
                        />

                    </span>
                )) : null
            }

            <Tooltip title="Tambah" placement="right">
                <span
                    // ref={hoverRefIconPlus}
                >
                    <IconButton
                        // className={ isHoveringIconPlus ? classes.hoverIconPlus : classes.hoverIconPlusLeave}
                        // onClick={(e) => handleTambah(e,item)}
                        className={classes.hoverIconPlusLeave}
                        onClick={handleTambah}
                        style={{marginTop: 8}}
                    >
                        <i className='material-icons' style={{fontSize: 12}}>
                            add
                        </i>
                    </IconButton>
                
                </span>
            </Tooltip>

            <DialogTingkatJabatan 
                classes = { classes }
                isModalTingkatJabatan = { isModalTingkatJabatan }
                setModalTingkatJabatan = { setModalTingkatJabatan }
                dataDetail = { dataDetail }

            />

            <DialogDeleteTingkatJabatan 
                classes = { classes }
                setOpenModalDelete = { setOpenModalDelete }
                isOpenModalDelete = { isOpenModalDelete }
                dataDetail = { dataDetail }

            />


            <DialogEditTingkatJabatan 
                classes = { classes }
                isModalEdit = { isModalEdit }
                setModalEdit = { setModalEdit }
                dataDetailEdit = { dataDetailEdit }

            />

        </div>
    )
};


export default TingkatJabatan;
