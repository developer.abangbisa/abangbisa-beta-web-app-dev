import React, { useCallback, useEffect, useState, Fragment } from "react";
import {
	fade, makeStyles, createMuiTheme, withStyles, MuiThemeProvider,
} from '@material-ui/core/styles';
import Divider from '@material-ui/core/Divider';

import Hidden from '@material-ui/core/Hidden';
import axios from 'axios';

import { fromJS } from 'immutable';
import { URL_API } from '../../../constants/config-api';
import { useGetHttp } from '../../../utilities-hook/useGetHttp';
//import { GET_LABEL_REGISTER_GROUP, POST_REGISTER_GROUP } from '../../.././../constants/config-';

import Input from '@material-ui/core/Input';
import OutlinedInput from '@material-ui/core/OutlinedInput';
import FilledInput from '@material-ui/core/FilledInput';
import InputLabel from '@material-ui/core/InputLabel';
import FormHelperText from '@material-ui/core/FormHelperText';
import FormControl from '@material-ui/core/FormControl';
import Select from '@material-ui/core/Select';
import NativeSelect from '@material-ui/core/NativeSelect';

/**
 * Icons
 */
import ArrowDropDownIcon from '@material-ui/icons/ArrowDropDown';
import FolderOutlinedIcon from '@material-ui/icons/FolderOutlined';


import ClickAwayListener from '@material-ui/core/ClickAwayListener';
import Grow from '@material-ui/core/Grow';
//import Paper from '@material-ui/core/Paper';
import Popper from '@material-ui/core/Popper';
//import MenuItem from '@material-ui/core/MenuItem';
import MenuList from '@material-ui/core/MenuList';
//import ButtonGroup from '@material-ui/core/ButtonGroup';

import Menu from '@material-ui/core/Menu';

/**
 * Import Utilities
 */
import moment from 'moment';

/**
 * Import List component
 */
import List from '@material-ui/core/List';
import ListItem from '@material-ui/core/ListItem';
import ListItemIcon from '@material-ui/core/ListItemIcon';
import ListItemText from '@material-ui/core/ListItemText';
import ListSubheader from '@material-ui/core/ListSubheader';

/**
 * Import Card component
 */
import Card from '@material-ui/core/Card';
import CardActions from '@material-ui/core/CardActions';
import CardContent from '@material-ui/core/CardContent';

import InboxIcon from '@material-ui/icons/MoveToInbox';
import DraftsIcon from '@material-ui/icons/Drafts';

import clsx from 'clsx';

import SendIcon from '@material-ui/icons/Send';
import IconButton from '@material-ui/core/IconButton';
import MenuIcon from '@material-ui/icons/Menu';
import SearchIcon from '@material-ui/icons/Search';
import DirectionsIcon from '@material-ui/icons/Directions';
import ArrowLeftIcon from '@material-ui/icons/ArrowLeft';

import { 
    Container, Paper, Typography, Grid, TextField, Checkbox, CircularProgress,
    FormControlLabel, Box, Button, MenuItem, Dialog, DialogTitle, DialogContent, 
    DialogContentText, DialogActions, Breadcrumbs, Link

} from '@material-ui/core';

import { green, white } from '@material-ui/core/colors';

import InputBase from '@material-ui/core/InputBase';

//import DatePicker from "react-datepicker";
 
// import "react-datepicker/dist/react-datepicker.css";

import { DatePicker } from '@y0c/react-datepicker';

/**
 * Import CRUD Component
 * 
 */
import { CreatePeriod } from './ComponentViewPeriode/CreatePeriod';
import { DetailPeriod } from './ComponentViewPeriode/DetailPeriod';
import { UpdatePeriod } from './ComponentViewPeriode/UpdatePeriod';
import { DeletePeriod } from './ComponentViewPeriode/DeletePeriod';

const styles = theme => ({
    root: {
        width: '100%',
        marginTop: 1 * 3
    },
	rootDateRange: {
		padding: '2px 4px',
		display: 'flex',
		alignItems: 'center',
		marginTop:6,
		marginBottom:10,
		/*width: 400, */
	},
	container: {
		backgroundColor: 'white',
		paddingLeft: 0,
		paddingRight: 0,
		minHeight: '800px',
	},
    button: {
        borderRadius: 5,
        background: 'linear-gradient(1deg, #c1272d, #c1272d 30%, #d1354a 67%, #f28181)',
        border:0,
        fontFamily:'Nunito',
        marginLeft:theme.spacing(1),
        textTransform: 'capitalize',
        color: 'white'
    },
    title: {
        fontFamily: 'Nunito'
	},
	subTitle: {
		color: 'gray',
	},
	divSearch : {
		border:0,
		borderRadius: 20,
		background:'white',
		padding:10,
		width:'90%',
		marginTop:10
	},
	textSearch : {
		width:'100%', 
		fontSize:16, 
		border:0, 
		backgroundColor:'white', 
		outline:'none',
	},
	periodListleftTopHeader : {
		background: 'linear-gradient(1deg, #0083ff, #0083ff 30%, #00b8ff 67%, #00b8ff)',
		color: 'white',
		marginTop: theme.spacing(7.5),
		marginLeft: theme.spacing(7.5),
		padding: theme.spacing(2),
	},
	periodListLeftMenu : {
		marginLeft: theme.spacing(7.5),
		paddingLeft: theme.spacing(7.5),
	},
	periodListLeftItem : {
		marginTop: 0,
		marginBottom: 0,
	},
	periodRightDetail: {
		marginTop: theme.spacing(7.5),
		paddingLeft: theme.spacing(3),
		paddingTop: theme.spacing(3),
	},
	labelSearch : {
		fontWeight:'bold',
		fontSize:17,
	},
	multiSelect : {
		padding : 0,
		margin: 0,
		
	},
	textFieldCustom : { /*
		margin: theme.spacing(1),
		padding : theme.spacing(1), */
		padding:8,
		border:'1px solid rgba(242, 38, 19, 1)',
		borderRadius:4,
		'&:focus': {
		  borderRadius: 4,
		  outline: 'none',
		  //boxShadow: '0 0 0 0.1rem rgba(242, 38, 19, 1)',
		  boxShadow: '0 0 3px rgba(242, 38, 19, 1)',
		  border: '1px solid rgba(242, 38, 19, 1)',
		},
	},
  	iconButton: {
    	padding: 6,
  	},
  	divider: {
		width: 1,
		height: 28,
		margin: 4,
  	},
	buttonBase: {
		margin: theme.spacing(1),
		margin:0,
		flex:1
	},
	buttonMargin: {
		margin: theme.spacing(1),
	},
	periodListleftTopHeaderSearch: {
		backgroundColor: 'white',
		borderRadius: '30px',
		height: '40px', 
	},
});

//menu item style
const StyledMenu = withStyles({
  	paper: {
    	border: '1px solid #d3d4d5',
  	},
})(props => (
  <Menu
    elevation={0}
    getContentAnchorEl={null}
    anchorOrigin={{
      vertical: 'bottom',
      horizontal: 'center',
    }}
    transformOrigin={{
      vertical: 'top',
      horizontal: 'center',
    }}
    {...props}
  />
));

const StyledMenuItem = withStyles(theme => ({
	root: {
		'&:focus': {
			backgroundColor: theme.palette.primary.main,
			'& .MuiListItemIcon-root, & .MuiListItemText-primary': {
				color: theme.palette.common.white,
			},
		},
  	},
}))(MenuItem);


const ViewPeriode = props => {

    const { classes } = props;
	
	/*
		````````````````````````````````````
		DEFINE GENERAL STATE

		````````````````````````````````````
	*/
	const [	state, setState] = React.useState({ checkedA: true, checkedB: true, checkedF: true, checkedG: true, });
    const userToken = localStorage.getItem('userToken');
	const [ userTokenState, setUserTokenState ] = useState('');
	const [ periodListLeftItemLoader, setPeriodListLeftItemLoader] = useState(true);

    /*
        ````````````````````````````````````
        GET PERIODE / MASTER PERIOD LIST

        ````````````````````````````````````
    */
   	const [ listPeriode, setListPeriode ] = useState([]);
	useEffect(() => {
		
		setUserTokenState(userToken);
		
        if (userToken !== undefined) {
			const header = {
                'Accept': "application/json",
                'Content-Type' : "application/json",
                'Authorization' : "bearer " + userToken,
			};
			
			axios.defaults.headers.common = header;    
			
			// GET LIST PERIODE
            axios
                .get(URL_API + `/swd/master-period`)
                .then(function(response){

                    console.log("Response Original PERIODE   : ", response);
                    setListPeriode(response.data.data);
					setPeriodListLeftItemLoader(false);
                })
                .catch(function(error){
					setPeriodListLeftItemLoader(false);
                    console.log("Error : ", error.response)
				})
				
			// GET FILTER OPTIONS
            axios
                .get(URL_API + `/swd/master-period/options`)
                .then(function(response){
                    console.log("Response Original PERIODE OPTIONS  : ", response);
                    setIntervalList(response.data.data.masterIntervalCollections);
                })
                .catch(function(error){
                    console.log("Error : ", error.response)
                })
		}
		
	},[]);
	
	
	/*
		````````````````````````````````````
		HANDLE PERIOD DETAIL

		````````````````````````````````````
	*/
	const [ periodDetail, setPeriodDetail ] = useState(null);
	const handleViewPeriod = (srcId) => {
		setPeriodDetail(null);
		if ( listPeriode !== undefined && listPeriode.length > 0 ) {
			let len = listPeriode.length;
			for (let i = 0; i < len; i++) {
				if (listPeriode[i]['id'] == srcId) {
					setPeriodDetail(listPeriode[i]);
					console.log("View Period Detail : ", listPeriode[i]);
					break;
				}
			}
		}
	};

	/*
		````````````````````````````````````
		HANDLE PERIOD FILTER BY INTERVAL

		````````````````````````````````````
	*/
	const [ intervalList, setIntervalList ] = useState([]);
	const [ intervalFilterOpen, setIntervalFilterOpen ] = useState(false);

	function handleClickIntervalFilter( evt ) {
		setIntervalFilterOpen( evt.currentTarget );
	}
	function handleCloseIntervalFilter( evt ) {
		setIntervalFilterOpen( false );
	}

	//ADD PERIOD ----------------------------------------------------------------------------------------------------
	//Period Group Dialog
	const [selectedStartDate, setSelectedStartDate] = React.useState(new Date('2014-08-18T21:11:54'));
	const [selectedEndDate, setSelectedEndDate] = React.useState(new Date('2014-08-18T21:11:54'));

	function handleStartDateChange(event) {
		//selectedStartDate(event);
		
		var startDate=new Intl.DateTimeFormat('en-GB', { 
          year: 'numeric', 
          month: 'short', 
          day: '2-digit' 
        }).format(event.toDate());
		
		var startDate2 = moment(event.toDate()).format("DD, MMMM YY");
		
		console.log('Start Date : '+startDate2);
	}
	
	function handleEndDateChange(event) {
		//selectedEndDate(event);
		console.log('End Date : '+event.toDate());
	}
  
	//const [startDate, setStartDate] = React.useState(false);
	const [openDialogAddPeriodSingle, setOpenDialogAddperiodSingle] = React.useState(false);
	// function handleAddPeriodSingleOpen() {
	// 	//show dialog input name period 
	// 	setOpenDialogAddperiodSingle(true);
		
	// 	//hide context menu add period
	// 	setAnchorEl(null); 
	// }
	
	function handleAddPeriodSingle() {
		setOpenDialogAddperiodSingle(false);
	}
	
	function handleAddPeriodSingleClose() {
		//close dialog input name period single
		setOpenDialogAddperiodSingle(false);
	}
	
	function handleChange() {
		
	}
	
	// Period Group Dialog
	// const [openDialogAddPeriodGroupName, setOpenDialogAddperiodGroupName] = React.useState(false);
	// const [openDialogAddPeriodGroupDateRange, setOpenDialogAddperiodGroupDateRange] = React.useState(false);
	// const [openDialogAddPeriodGroupInterval, setOpenDialogAddPeriodGroupInterval] = React.useState(false);
	// const [openDialogAddPeriodGroupAutoGenerate, setOpenDialogAddPeriodGroupAutoGenerate] = React.useState(false);
	
	// function handleAddPeriodGroup() {
	// 	//show dialog input name period group
	// 	setOpenDialogAddperiodGroupName(true);
		
	// 	//hide context menu add period
	// 	setAnchorEl(null); 
	// }
	
	// function handleAddPeriodGroupName() {
	// 	//close dialog input name
	// 	setOpenDialogAddperiodGroupName(false); 
		
	// 	//show next dialog to input date range
	// 	setOpenDialogAddperiodGroupDateRange(true);
	// }
	
	// function handleAddPeriodGroupDateRange() {
	// 	//close dialog to input date range
	// 	setOpenDialogAddperiodGroupDateRange(false);
		
	// 	//show next dialog to choose interval
	// 	setOpenDialogAddPeriodGroupInterval(true);
		
	// }
	
	// function handleAddPeriodGroupInterval() {
	// 	//close dialog to choose interval
	// 	setOpenDialogAddPeriodGroupInterval(false);
		
	// 	//show next dialog to generate period by interval and date range
	// 	setOpenDialogAddPeriodGroupAutoGenerate(true);
	// }
	
	// function handleAddPeriodGroupAutoGenerate() {
	// 	//close dialog to generate period by interval and date range
	// 	setOpenDialogAddPeriodGroupAutoGenerate(false);
		
	// 	//set value params
	// }
	
	
	//Send request
	const handleSubmit = (e) => {

        e.preventDefault();

        localStorage.clear();

        //setIsLoading(true);

        let data = {

            MasterPeriod : {
                name: '',
                interval_id: '',
                start_date: '',
                end_date: '',
            }
        };

        console.log(data);

		const header =  {
			'Accept': "application/json",
			'Content-Type' : "application/json",
		};

		axios.defaults.headers.common = header;

		axios
			.post(URL_API + '/swd/master-period', data)
			.then((response) => {

				console.log("Response Original : ", response)
				/*
				const immutableDataRegister = fromJS(response);

				setIsLoading(false);
			  
				if(immutableDataRegister.getIn(['status']) == 200){

					console.log("200");

					setInfoOTPUrl(immutableDataRegister.getIn(['data', 'data', 'otp_url']));

					if(immutableDataRegister.getIn(['data', 'data', 'verification_token']) !== null ){
						setInfoVerifyToken(immutableDataRegister.getIn(['data', 'data', 'verification_token']));
						localStorage.setItem("verifyToken", immutableDataRegister.getIn(['data', 'data', 'verification_token']) );
					};

					Redirect(ToOTP);

				} */
			})
        .catch((error) => {

            console.log("Error : ", error.response);
/*
            setIsLoading(false);
            const immutableDataRegister = fromJS(error.response);

            if(immutableDataRegister.getIn(['status']) == 400){ 

                setResponse400(true);
				
                setInfoErrorEmail(immutableDataRegister.getIn(['data', 'info', 'errors', 'email']));
                setInfoErrorPassword(immutableDataRegister.getIn(['data', 'info','errors', 'password']));
                setInfoErrorMobileNumber(immutableDataRegister.getIn(['data', 'info','errors', 'mobile_number']));
                setInfoErrorRequiredAccountType(immutableDataRegister.getIn(['data', 'info','errors', 'group_category_id']));
				
            };

            if(immutableDataRegister.getIn(['status']) == 500){
                setResponse500(true);
            };
*/
        })
    };
	
	
/*	
	function handleCloseDialogAddPeriodGroup() {
		//close period group name
		setOpenDialogAddperiodGroup(false); 
		//show period gorup range
		setOpenDialogAddperiodGroupDateRange(true);
	}
	
	function handleCloseDialogAddPeriodGroupDateRange() {
		setOpenDialogAddperiodGroupDateRange(false); 
	}
	
	function handleCloseDialogPeriod() {
		setOpenDialogAddperiodGroup(false); 
		setOpenDialogAddperiodGroupDateRange(false); 
		setOpenDialogAddPeriodGroupInterval(false); 
	}
*/
    return (
		<Container className={ classes.container }>
        	<Grid container > 
			{
				// <!-- Period Menu List -->
			}
            <Grid item md={3} xs={12}>
				<Grid container spacing={2}>
					<Grid item xs={12}>
						<Card className={classes.periodListleftTopHeader}>
							<CardContent>
								<Typography variant="h6" component="h6">
									Periode
								</Typography>
									<TextField
										className={classes.periodListleftTopHeaderSearch}
										placeholder="Cari Periode"
									/>
									
							</CardContent>
						</Card>
					</Grid>
				</Grid>
				<Grid container spacing={2}>
					<Grid item xs={12} className={classes.periodListLeftMenu}>
					{
						periodListLeftItemLoader == true && (
							<Grid container style={{ marginTop: '100px', marginLeft: '50px' }}>
								<Grid item xs={3}></Grid>
								<Grid item xs={6} md={3}>
									<CircularProgress />
								</Grid>
								<Grid item xs={3}></Grid>
							</Grid>
						)
					}
					{
						listPeriode.length > 0 ? listPeriode.map((item, i) => (
							<List dense="3">
								<ListItem className={classes.periodListLeftItem} button onClick={ () => handleViewPeriod(item['id']) }>
									<ListItemIcon>
									<FolderOutlinedIcon />
									</ListItemIcon>
									<ListItemText primary={item.name} />
								</ListItem>
							</List>
						)) : null
					}
					</Grid>
				</Grid>
            </Grid>
			{
				// <!-- /Period Menu List --> 
			}

			{ 
				// <!-- Period Detail Title --> 
			}
			
			<Hidden mdUp>
				<Grid item xs={1}></Grid>
			</Hidden>
			
			<Grid item md={8} xs={11} className={classes.periodRightDetail}>			
				<Grid container>
					<Grid item md={6} xs={12}>
						{ periodDetail !== undefined && periodDetail !== null && (
							<Fragment>
								<Typography variant="h6" className={classes.title}>
									{ periodDetail.name }
								</Typography>
								<Typography component="body1" className={classes.subTitle}>
									{ moment(periodDetail.start_date).format('DD MMMM YYYY') + ' - ' + moment(periodDetail.end_date).format('DD MMMM YYYY') }
								</Typography>
							</Fragment>
						) }
					</Grid>
					
					<Grid item md={6} style={{textAlign: 'right'}}>						
						{ intervalList.length > 0 && intervalList.map((item, i) => (
							<Fragment>
								<Button
									aria-controls="customized-menu"
									aria-haspopup="true"
									variant="contained"
									color="primary"
									onClick={handleClickIntervalFilter}
								>
									Open Menu
								</Button>
								<StyledMenu
									id="interval-filter"
									anchorEl={intervalFilterOpen}
									keepMounted
									open={Boolean(intervalFilterOpen)}
									onClose={handleCloseIntervalFilter}
								>
									<StyledMenuItem>
									<ListItemIcon>
										<SendIcon />
									</ListItemIcon>
									<ListItemText primary="Sent mail" />
									</StyledMenuItem>
									<StyledMenuItem>
									<ListItemIcon>
										<DraftsIcon />
									</ListItemIcon>
									<ListItemText primary="Drafts" />
									</StyledMenuItem>
									<StyledMenuItem>
									<ListItemIcon>
										<InboxIcon />
									</ListItemIcon>
									<ListItemText primary="Inbox" />
									</StyledMenuItem>
								</StyledMenu>
							</Fragment>
						))}
					</Grid> 
				</Grid>
				{ 
					// <!-- /Period detail Title --> 
				}
				<Divider light />

				{
					// <-- View Period Detail -->
				}
				<Grid container>
					<Grid item xs={12}>
						Test
					</Grid>
				</Grid>
				{
					// <-- /View Period Detail
				}

{/*Dialog Add Periode Single*/}				
<Dialog open={openDialogAddPeriodSingle} onClose={handleClose} aria-labelledby="form-dialog-title" fullWidth>
    {/*}<DialogTitle id="form-dialog-title">  {*/}
		<Grid className={classes.rootDateRange}>
			<IconButton className={classes.iconButton} aria-label="menu" style={{visibility:'hidden'}}>
				<i className='material-icons'>arrow_back</i>
				{/*<ArrowLeftIcon />*/}
			</IconButton>
			<span style={{flex:1,alignItems: 'center', textAlign:'center'}}><strong>Tambah Periode</strong></span>
			<IconButton className={classes.iconButton} aria-label="menu" style={{float:'right'}} onClick={handleAddPeriodSingleClose}><i className='material-icons'>clear</i></IconButton>
		</Grid>
		{/*}</DialogTitle> {*/}
        <DialogContent
			style={{
				root: classes.test
			}}
		>
			{ /*
			  <DialogContentText>
			  context
			  </DialogContentText> 
			*/}
			Title <br/>
			<input type='text' className={classes.textFieldCustom} style={{width:400}}/>
			
			<Grid container xs={12} style={{marginTop:20}}>
				<Grid xs={6}>
					Mulai
				</Grid>
				<Grid xs={5} style={{paddingLeft:10}}>
					Berakhir
				</Grid>
			</Grid>
			<Paper className={classes.rootDateRange}>
				<Button className={classes.buttonBase}>09-08-2019</Button>-
				<Button className={classes.buttonBase}>09-08-2019</Button>
			</Paper>
			
			<DatePicker 
				style={{zIndex: 100000}}
				onChange={handleStartDateChange}/>
				
			<DatePicker 
				style={{zIndex: 100000}}
				onChange={handleEndDateChange}/>
			
        </DialogContent>
        <DialogActions>
			<Button onClick={handleSubmit} style={{marginLeft:15,marginBottom:15,marginRight:15}} variant="contained" color="secondary" fullWidth>
				Buat Periode
			</Button>  
        </DialogActions>
</Dialog>


{/*Dialog Add Periode Group Name*/}				
<Dialog open={openDialogAddPeriodGroupName} onClose={handleClose} aria-labelledby="form-dialog-title">
    {/*}<DialogTitle id="form-dialog-title">  {*/}
		<Grid className={classes.rootDateRange}>
			<IconButton className={classes.iconButton} aria-label="menu" style={{visibility:'hidden'}}>
				<i className='material-icons'>arrow_back</i>
				{/*<ArrowLeftIcon />*/}
			</IconButton>
			<span style={{flex:1,alignItems: 'center', textAlign:'center'}}><strong>Tambah Grup Periode</strong></span>
			<IconButton className={classes.iconButton} aria-label="menu" style={{float:'right'}}><i className='material-icons'>clear</i></IconButton>
		</Grid>
		{/*}</DialogTitle> {*/}
        <DialogContent>
			{ /*
			  <DialogContentText>
			  context
			  </DialogContentText> 
			*/}
			Apa nama grup periode Anda ? <br/>
			<TextField
			error
			id="outlined-bare"
			className={classes.textField}
			defaultValue=""
			margin="normal"
			variant="outlined"
			style={{ width:400,marginBottom:0,marginTop:2 }}
			inputProps={{ 'aria-label': 'bare' }}
		/>
        </DialogContent>
        <DialogActions>
			<Button onClick={handleAddPeriodGroupName} style={{marginLeft:15,marginBottom:15,marginRight:15}} variant="contained" color="secondary" fullWidth>
				Lanjut
			</Button>  
        </DialogActions>
</Dialog>

{/*Dialog Add Periode Group Date Range*/}	
<Dialog open={openDialogAddPeriodGroupDateRange} onClose={handleClose} aria-labelledby="form-dialog-title">
   
		<Grid className={classes.rootDateRange}>
			<IconButton className={classes.iconButton} aria-label="menu">
				<i className='material-icons'>arrow_back</i>
				{/*<ArrowLeftIcon />*/}
			</IconButton>
			<span style={{flex:1,alignItems: 'center', textAlign:'center',paddingLeft:20,paddingRight:20}}><strong>Pilih Waktu Dimulai dan Berakhir</strong></span>
			<IconButton className={classes.iconButton} aria-label="menu" style={{float:'right'}}><i className='material-icons'>clear</i></IconButton>
		</Grid>
	
	<DialogContent>
		{ /*
		  <DialogContentText>
		  context
		  </DialogContentText> 
		*/}
		<br/>
		<Paper className={classes.rootDateRange}>
			<Button className={classes.buttonBase}>09-08-2019</Button>-
			<Button className={classes.buttonBase}>09-08-2019</Button>
		</Paper>
	</DialogContent>
	<DialogActions>
		<Button onClick={handleAddPeriodGroupDateRange} style={{marginLeft:15,marginBottom:15,marginRight:15}} variant="contained" color="secondary" fullWidth>
			Lanjut
		</Button>  
	</DialogActions>
</Dialog>

{/*Dialog Add Periode Group Interval*/}				
<Dialog open={openDialogAddPeriodGroupInterval} onClose={handleClose} aria-labelledby="form-dialog-title">
    
		<Grid className={classes.rootDateRange}>
			<IconButton className={classes.iconButton} aria-label="menu">
				<i className='material-icons'>arrow_back</i>
			</IconButton>
			<span style={{flex:1,alignItems: 'center', textAlign:'center'}}><strong>Pilih Jenis Interval</strong></span>
			<IconButton className={classes.iconButton} aria-label="menu" style={{float:'right'}}><i className='material-icons'>clear</i></IconButton>
		</Grid>
	
	<DialogContent>
		<Button size="small" variant="outlined" color="secondary" className={classes.buttonMargin}>Yearly</Button>
		<Button size="small" variant="outlined" color="secondary" className={classes.buttonMargin}>Quarterly</Button>
		<Button size="small" variant="outlined" color="secondary" className={classes.buttonMargin}>Monthly</Button>
		<Button size="small" variant="outlined" color="secondary" className={classes.buttonMargin}>Weekly</Button>
		<Button size="small" variant="outlined" color="secondary" className={classes.buttonMargin}>Custom</Button>
	</DialogContent>
	<DialogActions>
		<Button onClick={handleAddPeriodGroupInterval} style={{marginLeft:15,marginBottom:15,marginRight:15}} variant="contained" color="secondary" fullWidth>
			Lanjut
		</Button>  
	</DialogActions>
</Dialog>

{/*Dialog Generate Periode Group */}				
<Dialog open={openDialogAddPeriodGroupAutoGenerate} onClose={handleClose} aria-labelledby="form-dialog-title">
    <Grid className={classes.rootDateRange}>
		<IconButton className={classes.iconButton} aria-label="menu">
			<i className='material-icons'>arrow_back</i>
		</IconButton>
		<span style={{flex:1,alignItems: 'center', textAlign:'center'}}><strong>Tambah Grup Periode</strong></span>
		<IconButton className={classes.iconButton} aria-label="menu" style={{float:'right'}}><i className='material-icons'>clear</i></IconButton>
	</Grid>
	<DialogContent>
		{ /*
		  <DialogContentText>
		  context
		  </DialogContentText> 
		*/}
		Preview <br/>
		Samurai <br/>
		<Grid container xs={12} style={{marginTop:10}}>
			<Grid item xs={9}>
				Interval Title <br/>
				<TextField
					error
					id="outlined-bare"
					fullWidth
					defaultValue=""
					margin="normal"
					variant="outlined"
					style={{marginBottom:0,marginTop:2, padding : '0px' }}
					inputProps={{ 'aria-label': 'bare' }}
				/>
			</Grid>
			<Grid item xs={1}>
			</Grid>
			<Grid item xs={2}>
				Start <br/>
				<TextField
					error
					id="outlined-bare"
					fullWidth
					defaultValue=""
					margin="normal"
					variant="outlined"
					style={{marginBottom:0,marginTop:2, padding : '0px' }}
					inputProps={{ 'aria-label': 'bare' }}
				/>
			</Grid>
		</Grid>
	</DialogContent>
	<DialogActions>
		<Button onClick={handleAddPeriodGroupAutoGenerate} style={{marginLeft:15,marginBottom:15,marginRight:15}} variant="contained" color="secondary" fullWidth>
			Tambah Periode
		</Button>  
	</DialogActions>
</Dialog>

            </Grid> 
        </Grid>
		</Container>
	)
};

export default withStyles(styles)(ViewPeriode);

const filterInterval = [
    {
        id: 1,
        value: "Semua",
        label: "Semua",
    },
    {
        id: 2,
        value: "Tahun",
        label: "Tahun"
    },
    {
        id: 3,
        value: "Bulan",
        label: "Bulan"
    },
	{
        id: 3,
        value: "Minggu",
        label: "Minggu"
    },
	{
        id: 3,
        value: "Quarter",
        label: "Quarter"
    },

	
];

const hubunganKeluargas = [
    {
        id: 1,
        value: "Hari ini",
        label: "Hari ini",
    },
    {
        id: 2,
        value: "Besok",
        label: "Besok"
    },
    {
        id: 3,
        value: "Minggu Lalu",
        label: "Minggu Lalu"
    },
    {
        id: 4,
        value: "Bulan Lalu",
        label: "Bulan Lalu"
    },
    {
        id: 5,
        value: "Tahun Lalu",
        label: "Tahun Lalu"
    },
	{
        id: 6,
        value: "Minggu Depan",
        label: "Minggu Depan"
    },
    {
        id: 7,
        value: "Bulan Depan",
        label: "Bulan Depan"
    },
    {
        id: 8,
        value: "Tahun Depan",
        label: "Tahun Depan"
    },
	
];