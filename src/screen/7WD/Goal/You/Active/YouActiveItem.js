import React, { useCallback, useEffect, useState, useContext, Fragment} from "react";
import { makeStyles, createMuiTheme, withStyles, MuiThemeProvider } from '@material-ui/core/styles';
import { 

    Container, Paper, Typography, Grid, TextField, Checkbox, CircularProgress,
    FormControlLabel, Box, Button, MenuItem, Dialog, DialogTitle, DialogContent, 
    DialogContentText, DialogActions, Breadcrumbs, Link, List, ListItem, ListItemText, 
    ListItemAvatar, Avatar, IconButton, InputBase, Divider, Tooltip, Table, Icon,
    TableBody, TableRow, TableCell, TablePagination, TableSortLabel, TableHead, Menu,
    ListItemIcon, Chip
    
} from '@material-ui/core';

import moment from 'moment';
import clsx from 'clsx';
import { CircularProgressbarWithChildren, buildStyles, CircularProgressbar } from 'react-circular-progressbar';

import DoneIcon from '@material-ui/icons/Done';

import DialogCreate from '../../Components/DialogCreate';


import LightTooltip from '../../../../../components/LightTooltip';

import TruncateText from '../../../../../utilities/TruncateText';
import TruncateTextShort from '../../../../../utilities/TruncateTextShort';

import { URL_API } from '../../../../../constants/config-api';
import Redirect from '../../../../../utilities/Redirect';
import { ToGoalDetail } from '../../../../../constants/config-redirect-url';


import AvatarDummy from '../../../../../assets/images/Avatar_dummy.png';
import OrganigramGoal from '../../../../../assets/images/Group_2491.png';

const numeral = require('numeral');

const YouActiveItem = props => {

    const { 

        classes, 
        isModalCreate, 
        setModalCreate, 
        fotoQuery, 
        userLoginName, 
        memberPositionList, 
        collectionGoalList, 
        setCollectionGoalList,
        loader,


        item,
        index


    } = props;

    console.log("Item : ", item);

    return (

        <Fragment>
            

             
                {
                    loader == true && (

                        <Grid  
                            container
                            spacing={0}
                            direction="row"
                            justify="center"
                            alignItems="center"
                        >
                            <CircularProgress size={32} style={{marginTop: 72, color: 'red'}} />

                        </Grid>
                    )
                }

                {/* {
                       item.status_id == 2 && 
                } */}

                <Paper 
                    elevation={1} 
                    className={
                        
                        item.calculatedValue.gap_value_percent > item.calculatedValue.result_value_percent ? 

                            classes.paperListKebakaran : classes.paperList
                    
                    } 
                    key={index} 
                    style={{marginBottom: 16}}>
                       
                       <Grid container alignItems="flex-end" >
                            
                            <div className={classes.circularProgressBar}>


                                {/* 
                                    ``````````````````````````````````````````````````````````````````````````````````````````````````````````````````
                                    - INI SCENARIO KALAU ANTARA "GAP" LEBIH BESAR DARI "GOAL RESULT" ==> MERAH untuk "trail GOAL RESULT/Pencapaiannya" 
                                    
                                    - INI SCENARIO KALAU "POSISI AMAN" LEBIH BESAR DARI "GOAL RESULT" ==> YELLOW untuk "trail GAP-nya"
                                    
                                    `````````````````````````````````````````````````````````````````````````````````````````````````````````````````
                                    
                                */}

                                {
                                    item.calculatedValue.expected_value_percent > item.calculatedValue.result_value_percent && (

                                        <CircularProgressbarWithChildren
                                            value={numeral(item.calculatedValue.gap_value_percent).format('0,0')}
                                            // value={5}
                                            styles= {

                                                    buildStyles({
                                                        pathColor: 'yellow',  
                                                        trailColor: '#eee',
                                                        strokeLinecap: 'butt',//butt
                                                    })
                                            }
                                        >
                                            <CircularProgressbar
                                                value={numeral(item.calculatedValue.result_value_percent).format('0,0')} 
                                                // value={20} 
                                                styles = {

                                                    item.calculatedValue.gap_value_percent > item.calculatedValue.result_value_percent ?

                                                        buildStyles({

                                                            pathColor:'red',                                                             
                                                            trailColor: 'transparent',
                                                            strokeLinecap: 'butt',
                                                            textColor: 'black'
                                                        }) : 

                                                            buildStyles({

                                                                pathColor:'rgba(61, 255, 41, 0.87)', //*Green                                                          
                                                                trailColor: 'transparent',
                                                                strokeLinecap: 'butt',
                                                                textColor: 'black',
                                                            })
                                                }

                                                text={`${numeral(item.calculatedValue.result_value_percent).format('0,0')}%`}
                                            />
                                        </CircularProgressbarWithChildren>
                                    )
                                }

                                                                    
                                    {

                                    item.calculatedValue.result_value_percent > item.calculatedValue.expected_value_percent ||
                                    item.calculatedValue.result_value_percent == item.calculatedValue.expected_value_percent  ? (

                                        <CircularProgressbarWithChildren
                                            value = { numeral(item.calculatedValue.result_value_percent).format('0,0') }
                                            styles= {

                                                    buildStyles({
                                                        pathColor: 'rgba(61, 255, 41, 0.87)',  
                                                        trailColor: '#eee',
                                                        strokeLinecap: 'butt',//butt
                                                    })
                                            }
                                        >

                                            <CircularProgressbar
                                                value={numeral(item.calculatedValue.gap_value_percent).format('0,0')} 
                                                styles = {

                                                    buildStyles({

                                                        pathColor:'yellow',                                                             
                                                        trailColor: 'transparent',
                                                        strokeLinecap: 'butt',
                                                        textColor: 'black'
                                                    })

                                                    // item.calculatedValue.gap_value_percent > item.calculatedValue.result_value_percent ?

                                                    //     buildStyles({

                                                    //         pathColor:'yellow',                                                             
                                                    //         trailColor: 'transparent',
                                                    //         strokeLinecap: 'butt',
                                                    //         textColor: 'black'
                                                    //     }) : 

                                                    //         buildStyles({

                                                    //             pathColor:'rgba(61, 255, 41, 0.87)', //*Green                                                          
                                                    //             trailColor: 'transparent',
                                                    //             strokeLinecap: 'butt',
                                                    //             textColor: 'black',
                                                    //         })
                                                }

                                                text={`${numeral(item.calculatedValue.result_value_percent).format('0,0')}%`}
                                            />

                                        </CircularProgressbarWithChildren>

                                    ) : null
                                    }
                            </div>

                            <List className={classes.layoutMarginLeftList} style={{width: 200}}>

                                    {// *PERIOD
                                        item.period !== null && (
                                            
                                            <ListItem style={{paddingLeft: 0, marginRight: 24}}>
                                                <ListItemText 
                                                    className={classes.listItemtext}
                                                    primary = {
                                                        <Typography variant='subtitle2' className={classes.title}>
                                                            <b>{item.name }</b>
                                                        </Typography>
                                                    } 
                                                    secondary={
                                                        
                                                        <Typography variant='subtitle2' className={classes.title}>
                                                                    
                                                           {item.period.name}
                                                        </Typography>
                                                    } 
                                                />
                                            
                                            </ListItem>
                                        )
                                    }

                                    {
                                        // *DATE RANGE
                                        item.start_date !== null && item.end_date !== null && item.period == null && (
                                                                                            
                                            <ListItem style={{paddingLeft: 0, marginRight: 24}}>
                                                <ListItemText 
                                                    className={classes.listItemtext}
                                                    primary = {
                                                        <Typography variant='subtitle2' className={classes.title}>
                                                            <b>{item.name }</b>
                                                        </Typography>
                                                    } 
                                                    secondary={
                                                        
                                                        <Typography variant='subtitle2' className={classes.title}>
                                                                    
                                                            <LightTooltip title={moment(item.start_date).format('DD MMMM YYYY') + " - " + moment(item.end_date).format('DD MMMM YYYY')} placement="right-start">
                                                                <span>
                                                                    {TruncateTextShort(moment(item.start_date).format('DD MMMM YYYY') + " - " + moment(item.end_date).format('DD MMMM YYYY'))}
                                                                </span>
                                                            </LightTooltip>
                                                        </Typography>
                                                    } 
                                                />

                                            </ListItem>
                                        )
                                    }

                                    
                                    {// *DUE DATE

                                        item.start_date == null && item.end_date !== null && item.period == null && (
                                                            
                                            <ListItem style={{paddingLeft: 0, marginRight: 24}}>
                                            <ListItemText 
                                                className={classes.listItemtext}
                                                primary = {
                                                    <Typography variant='subtitle2' className={classes.title}>
                                                        <b>{item.name }</b>
                                                    </Typography>
                                                } 
                                                secondary={
                                                    
                                                    <Typography variant='subtitle2' className={classes.title}>
                                                                
                                                        { moment(item.end_date).format('DD MMMM YYYY')}
                                                    
                                                    </Typography>
                                                } 
                                            />

                                        </ListItem>
                                            
                                        )
                                    }                            
                            </List>
                        </Grid>
                </Paper>
        </Fragment>

    )

};


export default YouActiveItem;