
import React,{Component, useState, useEffect, useContext, useRef, Fragment} from 'react';
import { 

    Container, Paper, Typography, Grid, TextField, Checkbox, CircularProgress,
    FormControlLabel, Box, Button, MenuItem, Dialog, DialogTitle, DialogContent, 
    DialogContentText, DialogActions, Breadcrumbs, Link, List, ListItem, ListItemText, 
    ListItemAvatar, Avatar, IconButton, InputBase, Divider, Tooltip, Table, Icon,
    TableBody, TableRow, TableCell, TablePagination, TableSortLabel, TableHead, Menu,
    ListItemIcon, Radio, Chip
    
} from '@material-ui/core';

import { createMuiTheme } from "@material-ui/core";
import { ThemeProvider } from "@material-ui/styles";
import { lightBlue } from "@material-ui/core/colors";
import axios from 'axios';

import idLocale from "date-fns/locale/id";
import DateFnsUtils from "@date-io/date-fns";
import MomentUtils from "@date-io/moment";
import moment from 'moment';
import clsx from 'clsx';

import { DatePicker, MuiPickersUtilsProvider, } from "@material-ui/pickers";
import { DateRangePicker, DateRange } from "@matharumanpreet00/react-daterange-picker";

import { StyledMenu, StyledMenuItem } from '../../../../components/StyledMenuDropdown';

import { URL_API } from '../../../../constants/config-api';
import Capitalize from '../../../../utilities/Capitalize';

import AvatarDummy from '../../../../assets/images/Avatar_dummy.png';
import 'moment/locale/id';

const numeral = require('numeral');

class LocalizedUtils extends MomentUtils {

    getDatePickerHeaderText(date) {
        return this.format(date, "DD MMMM ", { locale: this.locale });
    }
};
  
const DialogEdit = (props) => {
    
    const { 
        
            classes, 
            isModalEdit, 
            setModalEdit,
            collectionGoalResultList, 
            setCollectionGoalResultList, 
            setFireGoalDetail,
            goalDetailState,
            goalResultDataDetail

    } = props;

    /*
        ```````````````````
        COMPONENT DID MOUNT 

        ```````````````````
    */
    const [locale, setLocale] = useState("id");
    const userToken = localStorage.getItem('userToken');
    const goalDetail = localStorage.getItem('goal_detail');
    const goalDetailAfterParse = JSON.parse(goalDetail);
    
    const [ userTokenState, setUserTokenState ] = useState('');

    // const [ goalDetailState, setGoalDetailState ] = useState({
    //     id: null
    // });
    const [ updatedAt, setUpdatedAt ] = useState('');

    useEffect(() => {

        if(isModalEdit == true){

            setDisabledButton(false)
            setGoalResult('');
            setKomentar('');


            /*

                `````````
                GET LABEL

                `````````
            */
            if(userToken !== ''){

                const header =  {   
    
                    'Accept': "application/json",
                    'Content-Type' : "application/json",
                    'Authorization' : "bearer " + userToken
                };
    
                axios.defaults.headers.common = header;    
    
                axios
                    .get(URL_API + `/swd/goal/${goalDetailState.id}/result/${goalResultDataDetail.id}/update`)
                    .then(function(response){
    
                        // setLoader(false);
                        console.log("Response Original LABEL : ", response);
    
                        if(response.status == 200){
                            
                            setUpdatedAt(response.data.data.updated_at);

                            if(response.data.data.fields.result_date.value){

                                handleChangeDueDate(response.data.data.fields.result_date.value);
                            };

                            if(response.data.data.fields.description.value !== null){

                                setKomentar(response.data.data.fields.description.value);
                            };

                        };
                    })
                    .catch(function(error){
                        
                        setLoader(false);
                        // setDisabledButton(false)
    
                        if(error.response !== undefined ){
                            if(error.response.status == 400){
                                if(error.response.data.info.errors !== null ){
                                    if(error.response.data.info.errors.result_value !== null ){
                                        alert("Error 400 : " + error.response.data.info.errors.result_value);
                                    }
                                }
                            }
    
                        } else {
    
                            alert("Whoops, something went wrong !");
                        };
    
                        console.log("Error : ", error.response);
                    })
    
            } else { console.log("No Access Token available!")};

        };

    },[isModalEdit]);

    useEffect(() => {

        setUserTokenState(userToken);

        if(isModalEdit == true){

            console.log("goalDetailAfterParse : ", goalDetailAfterParse)
            setFireGoalDetail(false)
            
        };
        
    }, [isModalEdit]);

    /*
        `````````````````````````
        HANDLE CHANGE GOAL RESULT

        `````````````````````````
    */
    const [ goalResult, setGoalResult ] = useState('');
    const handleChangGoalResult = (e) => {

        // console.log("GOAL RESULT : ", e.target.value);

        setGoalResult(e.target.value);
    };

    /*
        ``````````````````````````````````
        HANDLE CHANGE DESCRIPTION/KOMENTAR

        ``````````````````````````````````
    */
    const [ komentar, setKomentar ] = useState('');
    const handleChangeKomentar = (e) => {

        e.preventDefault();
        setKomentar(e.target.value);

    };  

    /*
        ``````````````````````````
        HANDLE CHANGE NILAI TARGET

        ``````````````````````````
    */
    // const [ nilaiTarget, setNilaiTarget ] = useState('');
    // const handleChangeNilaiTarget = (e) => {

    //     e.preventDefault();
    //     setNilaiTarget(e.target.value);

    // };
    
    /*
        ````````
        DUE DATE

        ````````
    */
    const [selectedDueDate, handleChangeDueDate] = useState(null);

    /*
        `````````````````````````
        HANDLE BUTTON SUBMIT GOAL

        `````````````````````````
    */
    const [ loader, setLoader ] = useState(false);
    const [ disabledButton, setDisabledButton ] = useState(false);

    const handleSUBMIT = () => {
        
        setLoader(true);
        setDisabledButton(true)

        let data = { 
            
            GoalResult : {
                result_value: goalResult,
                description: komentar,
                result_date: moment(selectedDueDate).format('YYYY-MM-DD'),
                updated_at: updatedAt
            }
        };

        console.log("Data SUBMIT GOAL RESULT  : ", data);

        if(userTokenState !== ''){
        
            const header =  {   

                'Accept': "application/json",
                'Content-Type' : "application/json",
                'Authorization' : "bearer " + userTokenState,
    
            };

            axios.defaults.headers.common = header;    

            axios
                .put(URL_API + `/swd/goal/${goalDetailState.id}/result/${goalResultDataDetail.id}`, data)
                .then(function(response){

                    setLoader(false);
                    setDisabledButton(false)

                    console.log("Response Original : ", response);

                    if(response.status == 200){
                        
                        setModalEdit(false);
                        setCollectionGoalResultList(collectionGoalResultList.map(item => (item.id === response.data.data.id ? response.data.data : item)));

                        setFireGoalDetail(true);
                    };
                })
                .catch(function(error){
                    
                    setLoader(false);
                    setDisabledButton(false)

                    if(error.response !== undefined ){
                        if(error.response.status == 400){
                            if(error.response.data.info.errors !== null ){
                                if(error.response.data.info.errors.result_value !== null ){
                                    alert("Error 400 : " + error.response.data.info.errors.result_value);
                                }
                            }
                        }

                    } else {

                        alert("Whoops, something went wrong !");
                    };

                    console.log("Error : ", error.response);
                })

        } else { console.log("No Access Token available!")};
    };

    return (

        <Dialog
            open={isModalEdit}
            onClose={() => setModalEdit(false)}
            aria-labelledby="alert-dialog-title"
            aria-describedby="alert-dialog-description"
            // fullWidth
        >
            <DialogTitle id="alert-dialog-title" style={{textAlign: "center"}}>
                <Typography variant='h6' className={classes.title}>
                    <b>Edit <i>Goal Result</i></b>
                </Typography>
            </DialogTitle>

            <DialogContent style={{textAlign: "left"}}>
                <DialogContentText id="alert-dialog-description">
                    <Typography variant='subtitle1' className={classes.title} >
                        <b>Masukan data dengan nilai Angka</b>
                    </Typography>
                    <TextField
                        id="outlined-bare"
                        className={classes.textField}
                        onChange= {handleChangGoalResult}
                        // onKeyDown={handleEnterPress}
                        // value= {goalResult !== '' ? goalResult : numeral(goalResultDataDetail.result_value).format('0,0')}
                        // placeholder= {goalResult !== '' ? goalResult : numeral(goalResultDataDetail.result_value).format('0,0')}
                        value= {goalResult !== '' ? goalResult : numeral(goalResultDataDetail.result_value).format('0,0')}
                        variant="outlined"
                        fullWidth
                    />
                </DialogContentText>

                <Typography variant='subtitle1' className={classes.title} style={{color: 'grey'}}>
                    <b>Tambahkan komentar/ keterangan status update Goal Result Anda</b>
                </Typography>
                <TextField
                    id="outlined-bare"
                    className={classes.textField}
                    onChange= {handleChangeKomentar}
                    // onKeyDown={handleEnterPress}
                    // value= {komentar !== '' ? komentar : goalResultDataDetail.description}
                    // placeholder= {komentar !== '' ? komentar : goalResultDataDetail.description}
                    value= {komentar !== '' ? komentar : goalResultDataDetail.description}
                    variant="outlined"
                    fullWidth
                />
                <br />
                <MuiPickersUtilsProvider utils={LocalizedUtils} locale={locale}>
                    <ThemeProvider theme={theme}>
                        <Fragment>
                            <DatePicker
                                // value={selectedDueDate !== null ? selectedDueDate : goalResultDataDetail.date}
                                value={selectedDueDate}
                                onChange={handleChangeDueDate}
                                animateYearScrolling
                                variant="dialog"// dialog, static, inline
                                disableToolbar={false}
                                label="Pilih tanggal"
                                format='DD MMMM YYYY'
                                // open={isShowDueDateComponentDirectly}
                                // onOpen={() => setShowDueDateComponentDirectly(true)}
                                // onClose={() => setShowDueDateComponentDirectly(false)}
                                // orientation="landscape"
                                // format
                                // TextFieldComponent =
                                // ToolbarComponent 
                            />
                        </Fragment>
                    </ThemeProvider>
                </MuiPickersUtilsProvider>

            </DialogContent>
            <DialogActions style={{alignItems: "center", justifyContent:'center'}}>

                {

                    // komentar == '' ||
                    goalResult == '' ? (

                        <Button 
                            // onClick={handleSUBMIT}
                            variant='contained' 
                            className={classes.buttonDisabled}
                            fullWidth
                            disableTouchRipple 
                        >  
                            Simpan 
                        </Button>
                    ) : (

                        <Button 
                            onClick={handleSUBMIT}
                            variant='contained' 
                            className={classes.button}
                            fullWidth
                            disabled={disabledButton == true ? true : false}
                        >  
                            {       
                                loader == true ? (

                                    <CircularProgress size={20} style={{color: 'white'}} />

                                ) : '  Simpan !'
                            }
                        
                        </Button>
                    )
                }
            </DialogActions>
            <br />
        </Dialog>
    )
};

export default DialogEdit;

const theme = createMuiTheme({

    palette: {

        primary: {
            
            main: '#cc0707', //#cc0707, #c62828
            light: '#ff5f52',
            dark: '#8e0000',
            contrastText: '#ffffff'
        }
    },

    typography: {
        fontFamily: 'Nunito',
        textTransform: 'capitalize'
    },
    textfield: {
        width: 200
    }
  })