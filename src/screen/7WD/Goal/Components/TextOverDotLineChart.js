import React, { useCallback, useEffect, useState, useContext, Fragment} from "react";
import { makeStyles, createMuiTheme, withStyles, MuiThemeProvider, fade} from '@material-ui/core/styles';
import { 

    Container, Paper, Typography, Grid, TextField, Checkbox, CircularProgress,
    FormControlLabel, Box, Button, MenuItem, Dialog, DialogTitle, DialogContent, 
    DialogContentText, DialogActions, Breadcrumbs, Link, List, ListItem, ListItemText, 
    ListItemAvatar, ListItemSecondaryAction, Avatar, IconButton, InputBase, Divider, Tooltip, Table, Icon,
    TableBody, TableRow, TableCell, TablePagination, TableSortLabel, TableHead, Menu,
    ListItemIcon, AppBar, Toolbar, Chip
    
} from '@material-ui/core';

import PictResultValue from '../../../../assets/images/Group_2620.png';

const numeral = require('numeral');




const TextOverDotLineChart = props => {


    const { classes, goalDetailState } = props;

    return (

        <List>

            <ListItem>
                <ListItemIcon 
                    style={{
                        marginLeft: 8
                        
                    }}
                >
                    <IconButton
                        style={{backgroundColor: '#aed9ff', padding: '6px'}}
                    >
                        <i className="material-icons" style={{color: '#4aa9fb'}}>
                            radio_button_checked
                        </i>
                    </IconButton>
                </ListItemIcon>
                                
                <ListItemText 
                    className={classes.listItemtext}
                    primary = {
                        <Typography 
                            variant='subtitle2' 
                            className={classes.title}
                            // style={{marginRight: 32}}
                        >
                            
                            <b>{numeral(goalDetailState.target_value).format('0,0')}</b>

                        </Typography>
                    } 

                    secondary = {        
                        <Typography variant='subtitle2' className={classes.title} style={{color: 'grey'}}>
                            Target value
                        </Typography>
                    }
                
                />
            </ListItem>
        </List>
    )
};

export default TextOverDotLineChart;
