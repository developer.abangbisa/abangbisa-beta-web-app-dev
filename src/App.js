import React from 'react';
import logo from './logo.svg';
import './App.css';


import { BrowserRouter as Router} from "react-router-dom";
import Root from './navigation/Root'

import configureStore from './store/configureStore';
const store = configureStore();

function App() {

  return (

    <Router>
      <Root store={store} />
    </Router>,
    document.getElementById('root')
  );
}

export default App;
