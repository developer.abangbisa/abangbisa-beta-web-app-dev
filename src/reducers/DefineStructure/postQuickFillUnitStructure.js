import { RESULT_POST_QUICK_FILL_UNIT_STRUCTURE } from '../../constants/action-types';

const initialState = {
    loading : false,
    list : [],
    data: {}
};

const postQuickFillUnitStructureReducer = (state = initialState, action) => {

    switch (action.type) {

        case RESULT_POST_QUICK_FILL_UNIT_STRUCTURE.PENDING:
            return{
                ...state,
                loading: true
            }

        case RESULT_POST_QUICK_FILL_UNIT_STRUCTURE.SUCCESS:

            return{
                ...state,
                loading: false,
                data: action.payload
            };

        case RESULT_POST_QUICK_FILL_UNIT_STRUCTURE.ERROR:

            // console.log("Action : ", action );
            return{
                ...state,
                loading: false,
                data: action.payload
            }
    
        default:
            break;
    };

    return state;
}

export default postQuickFillUnitStructureReducer;