
import { GET_LABEL_UNIT } from '../../constants/action-types';

const initialState = {
    loading : false,
    list : [],
    data: {}
};

const getLabelUnitReducer = (state = initialState, action) => {

    switch (action.type) {

        case GET_LABEL_UNIT.PENDING:
            return{
                ...state,
                loading: true
            }

        case GET_LABEL_UNIT.SUCCESS:
            return{
                ...state,
                loading: false,
                data: action.payload
            }

        case GET_LABEL_UNIT.ERROR:
            return {
                ...state,
                loading: false,
                data: action.payload
            }
        
        default:
            break;
    }

    return state;
}

export default getLabelUnitReducer;
