import { RESULT_POST_VERIFY_OTP } from '../../constants/action-types';

const initialState = {
    loading : false,
    list : [],
    data: {}
};

const postVerifyOtpReducer = (state = initialState, action) => {

    switch (action.type) {

        case RESULT_POST_VERIFY_OTP.PENDING:

            return{
                ...state,
                loading: true
            }

        case RESULT_POST_VERIFY_OTP.SUCCESS:

            return{
                ...state,
                loading: false,
                data: action.payload
            };

        case RESULT_POST_VERIFY_OTP.ERROR:

            console.log("Action : ", action );
            return{
                ...state,
                loading: false,
                data: action.payload
            }
    
        default:
            break;
    };

    return state;
}

export default postVerifyOtpReducer;