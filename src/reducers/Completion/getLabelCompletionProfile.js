
import { GET_LABEL_COMPLETION_PROFILE } from '../../constants/action-types';

const initialState = {
    loading : false,
    list : [],
    data: {}
};

const getLabelCompletionProfileReducer = (state = initialState, action) => {

    switch (action.type) {

        case GET_LABEL_COMPLETION_PROFILE.PENDING:
            return{
                ...state,
                loading: true
            }

        case GET_LABEL_COMPLETION_PROFILE.SUCCESS:
            return{
                ...state,
                loading: false,
                data: action.payload
            }

        case GET_LABEL_COMPLETION_PROFILE.ERROR:
            return {
                ...state,
                loading: false,
                data: action.payload
            }
        
        default:
            break;
    }

    return state;
}

export default getLabelCompletionProfileReducer;
