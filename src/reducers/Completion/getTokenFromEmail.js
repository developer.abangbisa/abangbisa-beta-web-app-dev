
// import { GET_TOKEN_FROM_EMAIL } from '../../constants/action-types';
import { GET_TOKEN_FROM_EMAIL } from '../../constants/action-types';

const initialState = {
    loading : false,
    list : [],
    data: {}
};

const getTokenFromEmailReducer = (state = initialState, action) => {

    switch (action.type) {

        case GET_TOKEN_FROM_EMAIL.PENDING:
            return{
                ...state,
                loading: true
            }

        case GET_TOKEN_FROM_EMAIL.SUCCESS:
            return{
                ...state,
                loading: false,
                data: action.payload
            }

        case GET_TOKEN_FROM_EMAIL.ERROR:
            return {
                ...state,
                loading: false,
                data: action.payload
            }
        
        default:
            break;
    }

    return state;
}

export default getTokenFromEmailReducer;
