import { RESULT_POST_SIGNIN } from '../../constants/action-types';

const initialState = {
    loading : false,
    list : [],
    data: {}
};

const postSigninReducer = (state = initialState, action) => {

    switch (action.type) {

        case RESULT_POST_SIGNIN.PENDING:
            return{
                ...state,
                loading: true
            }

        case RESULT_POST_SIGNIN.SUCCESS:

            return{
                ...state,
                loading: false,
                data: action.payload
            };

        case RESULT_POST_SIGNIN.ERROR:

            // console.log("Action : ", action );
            return{
                ...state,
                loading: false,
                data: action.payload
            }
    
        default:
            break;
    };

    return state;
}

export default postSigninReducer;