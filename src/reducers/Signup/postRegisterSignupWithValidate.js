import { RESULT_POST_REGISTER_SIGNUP_WITH_VALIDATE } from '../../constants/action-types';

const initialState = {
    loading : false,
    list : [],
    data: {}
};

const postRegisterSignupWithValidateReducer = (state = initialState, action) => {

    switch (action.type) {

        case RESULT_POST_REGISTER_SIGNUP_WITH_VALIDATE.PENDING:
            return{
                ...state,
                loading: true
            }

        case RESULT_POST_REGISTER_SIGNUP_WITH_VALIDATE.SUCCESS:

            return{
                ...state,
                loading: false,
                data: action.payload
            };

        case RESULT_POST_REGISTER_SIGNUP_WITH_VALIDATE.ERROR:

            // console.log("Action : ", action );
            return{
                ...state,
                loading: false,
                data: action.payload
            }
    
        default:
            break;
    };

    return state;
}

export default postRegisterSignupWithValidateReducer;