import { RESULT_POST_REGISTER_SIGNUP } from '../../constants/action-types';

const initialState = {
    loading : false,
    list : [],
    data: {}
};

const postRegisterSignupReducer = (state = initialState, action) => {

    switch (action.type) {

        case RESULT_POST_REGISTER_SIGNUP.PENDING:
            return{
                ...state,
                loading: true
            }

        case RESULT_POST_REGISTER_SIGNUP.SUCCESS:

            return{
                ...state,
                loading: false,
                data: action.payload
            };

        case RESULT_POST_REGISTER_SIGNUP.ERROR:

            // console.log("Action : ", action );
            return{
                ...state,
                loading: false,
                data: action.payload
            }
    
        default:
            break;
    };

    return state;
}

export default postRegisterSignupReducer;